// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDDEN
// GENERATED ON 2023-03-10 15:05

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

using UnityEngine;

namespace Harmony
{
    public static partial class InputActionMaps
    {
        public const string BoxPull = "BoxPull"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Settings\PlayerControls.inputactions".
        public const string Player = "Player"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Settings\PlayerControls.inputactions".
        public const string PlayerChess = "PlayerChess"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Settings\PlayerControls.inputactions".
        public const string UI = "UI"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Settings\PlayerControls.inputactions".
        
    }
}