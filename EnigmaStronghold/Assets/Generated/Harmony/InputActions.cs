// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDDEN
// GENERATED ON 2023-03-10 15:05

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

using UnityEngine;

namespace Harmony
{
    public static partial class InputActions
    {
        public const string Back = "Back"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Settings\PlayerControls.inputactions".
        public const string Cancel = "Cancel"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Settings\PlayerControls.inputactions".
        public const string Click = "Click"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Settings\PlayerControls.inputactions".
        public const string Cursor = "Cursor"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Settings\PlayerControls.inputactions".
        public const string Fire = "Fire"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Settings\PlayerControls.inputactions".
        public const string Fire2 = "Fire2"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Settings\PlayerControls.inputactions".
        public const string GhostMove = "GhostMove"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Settings\PlayerControls.inputactions".
        public const string Hint = "Hint"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Settings\PlayerControls.inputactions".
        public const string InteractPendant = "InteractPendant"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Settings\PlayerControls.inputactions".
        public const string Jump = "Jump"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Settings\PlayerControls.inputactions".
        public const string Look = "Look"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Settings\PlayerControls.inputactions".
        public const string MiddleClick = "MiddleClick"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Settings\PlayerControls.inputactions".
        public const string Move = "Move"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Settings\PlayerControls.inputactions".
        public const string Navigate = "Navigate"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Settings\PlayerControls.inputactions".
        public const string Pause = "Pause"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Settings\PlayerControls.inputactions".
        public const string Point = "Point"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Settings\PlayerControls.inputactions".
        public const string Potion = "Potion"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Settings\PlayerControls.inputactions".
        public const string RightClick = "RightClick"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Settings\PlayerControls.inputactions".
        public const string ScrollWheel = "ScrollWheel"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Settings\PlayerControls.inputactions".
        public const string Select = "Select"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Settings\PlayerControls.inputactions".
        public const string Submit = "Submit"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Settings\PlayerControls.inputactions".
        public const string TrackedDeviceOrientation = "TrackedDeviceOrientation"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Settings\PlayerControls.inputactions".
        public const string TrackedDevicePosition = "TrackedDevicePosition"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Settings\PlayerControls.inputactions".
        public const string Use = "Use"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Settings\PlayerControls.inputactions".
        
    }
}