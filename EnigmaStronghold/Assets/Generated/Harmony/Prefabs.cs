// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDEN
// GENERATED ON 2023-02-09 15:46:22


// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".


using UnityEngine;

namespace Harmony
{
    public partial class Prefabs : MonoBehaviour
    {
        private static Prefabs instance;
    
        [SerializeField] private GameObject ScrollbarPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Visuals\UI\Clean Settings UI\Prefabs\Scrollbar.prefab".
        [SerializeField] private GameObject Watermelon_512Prefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Visuals\Models\Objects\Watermelon\Prefabs\Watermelon_512.prefab".
        // "Point Light" is invalid. See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Light\Point Light.prefab".
        [SerializeField] private GameObject ButtonPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\UI\Button.prefab".
        [SerializeField] private GameObject RHEF_SpruceTerrainCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\RHEF_SpruceTerrainC.prefab".
        [SerializeField] private GameObject SM_AFS_Log06_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log06_LowEndPC.prefab".
        [SerializeField] private GameObject SM_AFS_Log18_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log18_LowEndPC.prefab".
        [SerializeField] private GameObject SM_AFS_Log23_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log23_LowEndPC.prefab".
        [SerializeField] private GameObject SM_AFS_Log27_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log27_LowEndPC.prefab".
        [SerializeField] private GameObject SM_AFS_Log28_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log28_LowEndPC.prefab".
        [SerializeField] private GameObject TogglePrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Visuals\UI\Clean Settings UI\Prefabs\Toggle.prefab".
        [SerializeField] private GameObject SliderPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Visuals\UI\Clean Settings UI\Prefabs\Slider.prefab".
        [SerializeField] private GameObject PressurePlatePrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Objects\PressurePlateRoom\PressurePlate.prefab".
        [SerializeField] private GameObject RHEF_GenericCliffAPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\RHEF_GenericCliffA.prefab".
        [SerializeField] private GameObject RHEF_RockDetailsBPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\RHEF_RockDetailsB.prefab".
        // "Header - Dark" is invalid. See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Visuals\UI\Clean Settings UI\Prefabs\Header - Dark.prefab".
        [SerializeField] private GameObject SM_AFS_Log17_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log17_LowEndPC.prefab".
        // "Button 1" is invalid. See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\UI\Button 1.prefab".
        [SerializeField] private GameObject CampfirePrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Objects\Campfire.prefab".
        [SerializeField] private GameObject Fish_Hallways_DecoratedPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        [SerializeField] private GameObject SM_AFS_Log05_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log05_LowEndPC.prefab".
        [SerializeField] private GameObject SM_AFS_Log09_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log09_LowEndPC.prefab".
        [SerializeField] private GameObject SM_AFS_Log24_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log24_LowEndPC.prefab".
        [SerializeField] private GameObject SM_AFS_Log25_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log25_LowEndPC.prefab".
        [SerializeField] private GameObject SM_AFS_Log03_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log03_LowEndPC.prefab".
        [SerializeField] private GameObject GargoyleDragonPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Decorations\GargoyleDragon.prefab".
        [SerializeField] private GameObject Key_IcePrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Objects\Gems\Key_Ice.prefab".
        [SerializeField] private GameObject RHEF_SpruceBPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\RHEF_SpruceB.prefab".
        [SerializeField] private GameObject SM_AFS_Log22_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log22_LowEndPC.prefab".
        [SerializeField] private GameObject BarPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Visuals\UI\Clean Settings UI\Prefabs\Bar.prefab".
        [SerializeField] private GameObject FirePrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Objects\PressurePlateRoom\Fire.prefab".
        [SerializeField] private GameObject RHEF_LowRockMedium_MobilePrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Visuals\MenuTerrain\Prefabs\RHEF_LowRockMedium_Mobile.prefab".
        [SerializeField] private GameObject Hub_World_LightPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Rooms\Hub_World_Light.prefab".
        [SerializeField] private GameObject SM_AFS_Log29_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log29_LowEndPC.prefab".
        [SerializeField] private GameObject Botez_GambitPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        [SerializeField] private GameObject Medieval_WindmillPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Polygrade\Prefabs\Medieval_Windmill.prefab".
        // "Header - Light" is invalid. See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Visuals\UI\Clean Settings UI\Prefabs\Header - Light.prefab".
        // "Sub header - Light" is invalid. See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Visuals\UI\Clean Settings UI\Prefabs\Sub header - Light.prefab".
        // "DeathBoxChild 1" is invalid. See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Objects\PressurePlateRoom\DeathBoxChild 1.prefab".
        [SerializeField] private GameObject SwordPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Visuals\Models\Objects\SwordSheath\Prefabs\Sword.prefab".
        [SerializeField] private GameObject StairsPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Objects\Stairs.prefab".
        [SerializeField] private GameObject RHEF_SpruceAPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\RHEF_SpruceA.prefab".
        [SerializeField] private GameObject TreePrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Polygrade\Prefabs\Tree.prefab".
        [SerializeField] private GameObject RHEF_GenericCliffA_MobilePrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Visuals\MenuTerrain\Prefabs\RHEF_GenericCliffA_Mobile.prefab".
        [SerializeField] private GameObject SM_AFS_Log01_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log01_LowEndPC.prefab".
        [SerializeField] private GameObject SM_AFS_Log02_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log02_LowEndPC.prefab".
        [SerializeField] private GameObject SM_AFS_Log16_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log16_LowEndPC.prefab".
        [SerializeField] private GameObject RHEF_GenericCliffBPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\RHEF_GenericCliffB.prefab".
        [SerializeField] private GameObject SM_AFS_Log11_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log11_LowEndPC.prefab".
        [SerializeField] private GameObject RHEF_RockMediumB_MobilePrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Visuals\MenuTerrain\Prefabs\RHEF_RockMediumB_Mobile.prefab".
        [SerializeField] private GameObject RHEF_RockMediumA_MobilePrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Visuals\MenuTerrain\Prefabs\RHEF_RockMediumA_Mobile.prefab".
        [SerializeField] private GameObject SM_AFS_Log15_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log15_LowEndPC.prefab".
        [SerializeField] private GameObject RHEF_SpruceCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\RHEF_SpruceC.prefab".
        [SerializeField] private GameObject MagicalBarrierPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Objects\MagicalBarrier.prefab".
        [SerializeField] private GameObject Key_PoisonPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Objects\Gems\Key_Poison.prefab".
        [SerializeField] private GameObject SM_AFS_Log04_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log04_LowEndPC.prefab".
        // "Sub header - Dark" is invalid. See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Visuals\UI\Clean Settings UI\Prefabs\Sub header - Dark.prefab".
        [SerializeField] private GameObject PressurePlateRoomPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Rooms\PressurePlateRoom.prefab".
        [SerializeField] private GameObject SM_AFS_Log20_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log20_LowEndPC.prefab".
        [SerializeField] private GameObject RHEF_RockMediumAPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\RHEF_RockMediumA.prefab".
        [SerializeField] private GameObject SkeletonPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Enemies\Skeleton.prefab".
        [SerializeField] private GameObject SquareButtonPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Visuals\UI\Clean Settings UI\Prefabs\SquareButton.prefab".
        [SerializeField] private GameObject RoundButtonPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Visuals\UI\Clean Settings UI\Prefabs\RoundButton.prefab".
        [SerializeField] private GameObject SM_AFS_Log12_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log12_LowEndPC.prefab".
        [SerializeField] private GameObject RHEF_RockMediumCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\RHEF_RockMediumC.prefab".
        [SerializeField] private GameObject SM_AFS_Log13_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log13_LowEndPC.prefab".
        [SerializeField] private GameObject PlayerUIPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Player\PlayerUI.prefab".
        [SerializeField] private GameObject LavaFlowingPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Decorations\LavaFlowing.prefab".
        [SerializeField] private GameObject RHEF_RockMediumBPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\RHEF_RockMediumB.prefab".
        [SerializeField] private GameObject SM_AFS_Log21_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log21_LowEndPC.prefab".
        [SerializeField] private GameObject RHEF_RockDetailsAPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\RHEF_RockDetailsA.prefab".
        [SerializeField] private GameObject InputFieldPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Visuals\UI\Clean Settings UI\Prefabs\InputField.prefab".
        // "Description text - Light" is invalid. See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Visuals\UI\Clean Settings UI\Prefabs\Description text - Light.prefab".
        [SerializeField] private GameObject SM_AFS_Log07_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log07_LowEndPC.prefab".
        [SerializeField] private GameObject SM_AFS_Log08_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log08_LowEndPC.prefab".
        [SerializeField] private GameObject RHEF_SpruceTerrainAPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\RHEF_SpruceTerrainA.prefab".
        [SerializeField] private GameObject BoxSidePrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Objects\PressurePlateRoom\BoxSide.prefab".
        [SerializeField] private GameObject Training_TargetPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Objects\Training_Target.prefab".
        [SerializeField] private GameObject SM_AFS_Log19_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log19_LowEndPC.prefab".
        // "Description text - Dark" is invalid. See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Visuals\UI\Clean Settings UI\Prefabs\Description text - Dark.prefab".
        [SerializeField] private GameObject Key_AirPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Objects\Gems\Key_Air.prefab".
        // "RHEF_GenericCliffB_,obile" is invalid. See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Visuals\MenuTerrain\Prefabs\RHEF_GenericCliffB_,obile.prefab".
        [SerializeField] private GameObject StepPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Objects\Step.prefab".
        [SerializeField] private GameObject SM_AFS_Log10_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log10_LowEndPC.prefab".
        [SerializeField] private GameObject PlayerModelPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Player\PlayerModel.prefab".
        [SerializeField] private GameObject SM_AFS_Log14_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log14_LowEndPC.prefab".
        [SerializeField] private GameObject Chess_RoomPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Rooms\Chess_Room.prefab".
        [SerializeField] private GameObject SM_AFS_Log26_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log26_LowEndPC.prefab".
        [SerializeField] private GameObject PlayerPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Player\Player.prefab".
        [SerializeField] private GameObject Key_FirePrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Objects\Gems\Key_Fire.prefab".
        [SerializeField] private GameObject RHEF_LowRockMediumPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\RHEF_LowRockMedium.prefab".
        [SerializeField] private GameObject RHEF_CavePrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\RHEF_Cave.prefab".
        [SerializeField] private GameObject RHEF_SpruceTerrainBPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\RHEF_SpruceTerrainB.prefab".
    
        public static GameObject Scrollbar => instance.ScrollbarPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Visuals\UI\Clean Settings UI\Prefabs\Scrollbar.prefab".
        public static GameObject Watermelon_512 => instance.Watermelon_512Prefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Visuals\Models\Objects\Watermelon\Prefabs\Watermelon_512.prefab".
        public static GameObject Button => instance.ButtonPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\UI\Button.prefab".
        public static GameObject RHEF_SpruceTerrainC => instance.RHEF_SpruceTerrainCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\RHEF_SpruceTerrainC.prefab".
        public static GameObject SM_AFS_Log06_LowEndPC => instance.SM_AFS_Log06_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log06_LowEndPC.prefab".
        public static GameObject SM_AFS_Log18_LowEndPC => instance.SM_AFS_Log18_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log18_LowEndPC.prefab".
        public static GameObject SM_AFS_Log23_LowEndPC => instance.SM_AFS_Log23_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log23_LowEndPC.prefab".
        public static GameObject SM_AFS_Log27_LowEndPC => instance.SM_AFS_Log27_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log27_LowEndPC.prefab".
        public static GameObject SM_AFS_Log28_LowEndPC => instance.SM_AFS_Log28_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log28_LowEndPC.prefab".
        public static GameObject Toggle => instance.TogglePrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Visuals\UI\Clean Settings UI\Prefabs\Toggle.prefab".
        public static GameObject Slider => instance.SliderPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Visuals\UI\Clean Settings UI\Prefabs\Slider.prefab".
        public static GameObject PressurePlate => instance.PressurePlatePrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Objects\PressurePlateRoom\PressurePlate.prefab".
        public static GameObject RHEF_GenericCliffA => instance.RHEF_GenericCliffAPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\RHEF_GenericCliffA.prefab".
        public static GameObject RHEF_RockDetailsB => instance.RHEF_RockDetailsBPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\RHEF_RockDetailsB.prefab".
        public static GameObject SM_AFS_Log17_LowEndPC => instance.SM_AFS_Log17_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log17_LowEndPC.prefab".
        public static GameObject Campfire => instance.CampfirePrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Objects\Campfire.prefab".
        public static GameObject Fish_Hallways_Decorated => instance.Fish_Hallways_DecoratedPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        public static GameObject SM_AFS_Log05_LowEndPC => instance.SM_AFS_Log05_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log05_LowEndPC.prefab".
        public static GameObject SM_AFS_Log09_LowEndPC => instance.SM_AFS_Log09_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log09_LowEndPC.prefab".
        public static GameObject SM_AFS_Log24_LowEndPC => instance.SM_AFS_Log24_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log24_LowEndPC.prefab".
        public static GameObject SM_AFS_Log25_LowEndPC => instance.SM_AFS_Log25_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log25_LowEndPC.prefab".
        public static GameObject SM_AFS_Log03_LowEndPC => instance.SM_AFS_Log03_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log03_LowEndPC.prefab".
        public static GameObject GargoyleDragon => instance.GargoyleDragonPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Decorations\GargoyleDragon.prefab".
        public static GameObject Key_Ice => instance.Key_IcePrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Objects\Gems\Key_Ice.prefab".
        public static GameObject RHEF_SpruceB => instance.RHEF_SpruceBPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\RHEF_SpruceB.prefab".
        public static GameObject SM_AFS_Log22_LowEndPC => instance.SM_AFS_Log22_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log22_LowEndPC.prefab".
        public static GameObject Bar => instance.BarPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Visuals\UI\Clean Settings UI\Prefabs\Bar.prefab".
        public static GameObject Fire => instance.FirePrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Objects\PressurePlateRoom\Fire.prefab".
        public static GameObject RHEF_LowRockMedium_Mobile => instance.RHEF_LowRockMedium_MobilePrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Visuals\MenuTerrain\Prefabs\RHEF_LowRockMedium_Mobile.prefab".
        public static GameObject Hub_World_Light => instance.Hub_World_LightPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Rooms\Hub_World_Light.prefab".
        public static GameObject SM_AFS_Log29_LowEndPC => instance.SM_AFS_Log29_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log29_LowEndPC.prefab".
        public static GameObject Botez_Gambit => instance.Botez_GambitPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public static GameObject Medieval_Windmill => instance.Medieval_WindmillPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Polygrade\Prefabs\Medieval_Windmill.prefab".
        public static GameObject Sword => instance.SwordPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Visuals\Models\Objects\SwordSheath\Prefabs\Sword.prefab".
        public static GameObject Stairs => instance.StairsPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Objects\Stairs.prefab".
        public static GameObject RHEF_SpruceA => instance.RHEF_SpruceAPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\RHEF_SpruceA.prefab".
        public static GameObject Tree => instance.TreePrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Polygrade\Prefabs\Tree.prefab".
        public static GameObject RHEF_GenericCliffA_Mobile => instance.RHEF_GenericCliffA_MobilePrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Visuals\MenuTerrain\Prefabs\RHEF_GenericCliffA_Mobile.prefab".
        public static GameObject SM_AFS_Log01_LowEndPC => instance.SM_AFS_Log01_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log01_LowEndPC.prefab".
        public static GameObject SM_AFS_Log02_LowEndPC => instance.SM_AFS_Log02_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log02_LowEndPC.prefab".
        public static GameObject SM_AFS_Log16_LowEndPC => instance.SM_AFS_Log16_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log16_LowEndPC.prefab".
        public static GameObject RHEF_GenericCliffB => instance.RHEF_GenericCliffBPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\RHEF_GenericCliffB.prefab".
        public static GameObject SM_AFS_Log11_LowEndPC => instance.SM_AFS_Log11_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log11_LowEndPC.prefab".
        public static GameObject RHEF_RockMediumB_Mobile => instance.RHEF_RockMediumB_MobilePrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Visuals\MenuTerrain\Prefabs\RHEF_RockMediumB_Mobile.prefab".
        public static GameObject RHEF_RockMediumA_Mobile => instance.RHEF_RockMediumA_MobilePrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Visuals\MenuTerrain\Prefabs\RHEF_RockMediumA_Mobile.prefab".
        public static GameObject SM_AFS_Log15_LowEndPC => instance.SM_AFS_Log15_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log15_LowEndPC.prefab".
        public static GameObject RHEF_SpruceC => instance.RHEF_SpruceCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\RHEF_SpruceC.prefab".
        public static GameObject MagicalBarrier => instance.MagicalBarrierPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Objects\MagicalBarrier.prefab".
        public static GameObject Key_Poison => instance.Key_PoisonPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Objects\Gems\Key_Poison.prefab".
        public static GameObject SM_AFS_Log04_LowEndPC => instance.SM_AFS_Log04_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log04_LowEndPC.prefab".
        public static GameObject PressurePlateRoom => instance.PressurePlateRoomPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Rooms\PressurePlateRoom.prefab".
        public static GameObject SM_AFS_Log20_LowEndPC => instance.SM_AFS_Log20_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log20_LowEndPC.prefab".
        public static GameObject RHEF_RockMediumA => instance.RHEF_RockMediumAPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\RHEF_RockMediumA.prefab".
        public static GameObject Skeleton => instance.SkeletonPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Enemies\Skeleton.prefab".
        public static GameObject SquareButton => instance.SquareButtonPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Visuals\UI\Clean Settings UI\Prefabs\SquareButton.prefab".
        public static GameObject RoundButton => instance.RoundButtonPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Visuals\UI\Clean Settings UI\Prefabs\RoundButton.prefab".
        public static GameObject SM_AFS_Log12_LowEndPC => instance.SM_AFS_Log12_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log12_LowEndPC.prefab".
        public static GameObject RHEF_RockMediumC => instance.RHEF_RockMediumCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\RHEF_RockMediumC.prefab".
        public static GameObject SM_AFS_Log13_LowEndPC => instance.SM_AFS_Log13_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log13_LowEndPC.prefab".
        public static GameObject PlayerUI => instance.PlayerUIPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Player\PlayerUI.prefab".
        public static GameObject LavaFlowing => instance.LavaFlowingPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Decorations\LavaFlowing.prefab".
        public static GameObject RHEF_RockMediumB => instance.RHEF_RockMediumBPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\RHEF_RockMediumB.prefab".
        public static GameObject SM_AFS_Log21_LowEndPC => instance.SM_AFS_Log21_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log21_LowEndPC.prefab".
        public static GameObject RHEF_RockDetailsA => instance.RHEF_RockDetailsAPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\RHEF_RockDetailsA.prefab".
        public static GameObject InputField => instance.InputFieldPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Visuals\UI\Clean Settings UI\Prefabs\InputField.prefab".
        public static GameObject SM_AFS_Log07_LowEndPC => instance.SM_AFS_Log07_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log07_LowEndPC.prefab".
        public static GameObject SM_AFS_Log08_LowEndPC => instance.SM_AFS_Log08_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log08_LowEndPC.prefab".
        public static GameObject RHEF_SpruceTerrainA => instance.RHEF_SpruceTerrainAPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\RHEF_SpruceTerrainA.prefab".
        public static GameObject BoxSide => instance.BoxSidePrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Objects\PressurePlateRoom\BoxSide.prefab".
        public static GameObject Training_Target => instance.Training_TargetPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Objects\Training_Target.prefab".
        public static GameObject SM_AFS_Log19_LowEndPC => instance.SM_AFS_Log19_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log19_LowEndPC.prefab".
        public static GameObject Key_Air => instance.Key_AirPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Objects\Gems\Key_Air.prefab".
        public static GameObject Step => instance.StepPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Objects\Step.prefab".
        public static GameObject SM_AFS_Log10_LowEndPC => instance.SM_AFS_Log10_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log10_LowEndPC.prefab".
        public static GameObject PlayerModel => instance.PlayerModelPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Player\PlayerModel.prefab".
        public static GameObject SM_AFS_Log14_LowEndPC => instance.SM_AFS_Log14_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log14_LowEndPC.prefab".
        public static GameObject Chess_Room => instance.Chess_RoomPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public static GameObject SM_AFS_Log26_LowEndPC => instance.SM_AFS_Log26_LowEndPCPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\SM_AFS_Log26_LowEndPC.prefab".
        public static GameObject Player => instance.PlayerPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Player\Player.prefab".
        public static GameObject Key_Fire => instance.Key_FirePrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Objects\Gems\Key_Fire.prefab".
        public static GameObject RHEF_LowRockMedium => instance.RHEF_LowRockMediumPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\RHEF_LowRockMedium.prefab".
        public static GameObject RHEF_Cave => instance.RHEF_CavePrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\RHEF_Cave.prefab".
        public static GameObject RHEF_SpruceTerrainB => instance.RHEF_SpruceTerrainBPrefab; // See "C:\Dev\Ecole\Projet_Synthese\projet-synthese\Assets\Prefabs\Forest\RHEF_SpruceTerrainB.prefab".

        public Prefabs()
        {
            instance = this;
        }
    }
}