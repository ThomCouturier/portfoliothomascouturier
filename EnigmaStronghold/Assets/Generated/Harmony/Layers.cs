// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDDEN
// GENERATED ON 2023-03-10 15:05

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

using UnityEngine;

namespace Harmony
{
    public static partial class Layers
    {
        public static readonly Layer DeathBox = new Layer(LayerMask.NameToLayer("DeathBox")); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public static readonly Layer Default = new Layer(LayerMask.NameToLayer("Default")); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public static readonly Layer Enemy = new Layer(LayerMask.NameToLayer("Enemy")); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public static readonly Layer GhostAI = new Layer(LayerMask.NameToLayer("GhostAI")); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public static readonly Layer GhostDoor = new Layer(LayerMask.NameToLayer("GhostDoor")); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public static readonly Layer Grabbed = new Layer(LayerMask.NameToLayer("Grabbed")); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public static readonly Layer Icon = new Layer(LayerMask.NameToLayer("Icon")); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        // "Ignore Raycast" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public static readonly Layer Sensor = new Layer(LayerMask.NameToLayer("Sensor")); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public static readonly Layer Sliding = new Layer(LayerMask.NameToLayer("Sliding")); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public static readonly Layer TransparentFX = new Layer(LayerMask.NameToLayer("TransparentFX")); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public static readonly Layer UI = new Layer(LayerMask.NameToLayer("UI")); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public static readonly Layer Water = new Layer(LayerMask.NameToLayer("Water")); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        
        public struct Layer
        {
            public int Index;
            public int Mask;

            public Layer(int index)
            {
                this.Index = index;
                this.Mask = 1 << index;
            }
        }

    }
}