// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDDEN
// GENERATED ON 2023-03-10 15:05

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

using UnityEngine;

namespace Harmony
{
    public static partial class AnimatorStates
    {
        // "Basic Attack" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Animators\NightmareCTRL.controller".
        public const string BlastL = "BlastL"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Elements\Animations\Controller\Abe.controller".
        public const string BlastR = "BlastR"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Elements\Animations\Controller\Abe.controller".
        public const string CreditAnimation = "CreditAnimation"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\Text.controller".
        public const string Damaged_Small = "Damaged_Small"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\Dummy.controller".
        // "Damaged_Small 0" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\Dummy.controller".
        public const string Diamond = "Diamond"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\Diamond.controller".
        public const string Die = "Die"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Animators\NightmareCTRL.controller".
        public const string Disabled = "Disabled"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\UI\Clean Settings UI\Animations\InputField.controller".
        public const string DragonDeath = "DragonDeath"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\Dragon\Dragon.controller".
        public const string DragonFly = "DragonFly"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\Dragon\Dragon.controller".
        public const string DragonHurt = "DragonHurt"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\Dragon\Dragon.controller".
        public const string DragonIdle = "DragonIdle"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\Dragon\Dragon.controller".
        public const string DragonMouthClosed = "DragonMouthClosed"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\Dragon\Dragon.controller".
        public const string DragonShoot = "DragonShoot"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\Dragon\Dragon.controller".
        public const string DragonVerticalFly = "DragonVerticalFly"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\Dragon\Dragon.controller".
        // "Fireball Shoot" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Animators\SouleaterCTRL.controller".
        // "Flame Attack" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Animators\TerrorbringerCTRL.controller".
        // "Fly Flame Attack" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Animators\UsurperCTRL.controller".
        // "Fly Float" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Animators\UsurperCTRL.controller".
        // "Get Hit" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Animators\NightmareCTRL.controller".
        public const string GhostIdle = "GhostIdle"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\Toon Ghost Bones.controller".
        public const string HandBlock = "HandBlock"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\Player\Hand.controller".
        public const string HandIdle = "HandIdle"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\Player\Hand.controller".
        public const string HandSwing = "HandSwing"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\Player\Hand.controller".
        public const string HandSwing2 = "HandSwing2"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\Player\Hand.controller".
        public const string HandWalk = "HandWalk"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\Player\Hand.controller".
        public const string Highlighted = "Highlighted"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\UI\Clean Settings UI\Animations\InputField.controller".
        public const string Idle = "Idle"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Animators\NightmareCTRL.controller".
        public const string Idle01 = "Idle01"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Animators\UsurperCTRL.controller".
        public const string Jump = "Jump"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Animators\NightmareCTRL.controller".
        public const string Land = "Land"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Animators\UsurperCTRL.controller".
        public const string Lava = "Lava"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\Nature\Lava\Lava.controller".
        public const string Normal = "Normal"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\UI\Clean Settings UI\Animations\InputField.controller".
        public const string Pendant = "Pendant"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\PickablePendant.controller".
        public const string PlayerIdle = "PlayerIdle"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\Player\Player.controller".
        public const string PlayerSit = "PlayerSit"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\Player\Player.controller".
        public const string Potato = "Potato"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\Potato.controller".
        public const string Pressed = "Pressed"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\UI\Clean Settings UI\Animations\InputField.controller".
        public const string Run = "Run"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Animators\NightmareCTRL.controller".
        public const string Scream = "Scream"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Animators\TerrorbringerCTRL.controller".
        public const string SkeletonAttack = "SkeletonAttack"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\Skeleton\Skeleton.controller".
        public const string SkeletonDeath = "SkeletonDeath"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\Skeleton\Skeleton.controller".
        public const string SkeletonIdle = "SkeletonIdle"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\Skeleton\Skeleton.controller".
        public const string SkeletonWalk = "SkeletonWalk"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\Skeleton\Skeleton.controller".
        public const string Sleep = "Sleep"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Animators\SouleaterCTRL.controller".
        public const string StoneKnightAttack = "StoneKnightAttack"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\StoneKnight\StoneKnight.controller".
        public const string StoneKnightIdle = "StoneKnightIdle"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\StoneKnight\StoneKnight.controller".
        public const string StoneKnightKneel = "StoneKnightKneel"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\StoneKnight\StoneKnight.controller".
        public const string StoneKnightPrepare = "StoneKnightPrepare"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\StoneKnight\StoneKnight.controller".
        public const string StoneKnightSpinAttack = "StoneKnightSpinAttack"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\StoneKnight\StoneKnight.controller".
        public const string StoneKnightWalk = "StoneKnightWalk"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\StoneKnight\StoneKnight.controller".
        public const string StoneKnightWorldWind = "StoneKnightWorldWind"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\StoneKnight\StoneKnight.controller".
        // "Take Off" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Animators\UsurperCTRL.controller".
        public const string WindMill = "WindMill"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\Windmill\WindMillController.controller".
        
    }
}