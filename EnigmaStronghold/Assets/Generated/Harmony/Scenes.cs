// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDDEN
// GENERATED ON 2023-03-10 15:05

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

using UnityEngine;

namespace Harmony
{
    public static partial class Scenes
    {
        public const string AirDragonRoom = "AirDragonRoom"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MiniBosses\AirDragonRoom.unity".
        public const string BossRoom = "BossRoom"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string ChessScene = "ChessScene"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\ChessScene.unity".
        public const string FireDragonRoom = "FireDragonRoom"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MiniBosses\FireDragonRoom.unity".
        public const string GhostRoom = "GhostRoom"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\GhostRoom.unity".
        public const string Hub = "Hub"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string IceDragonRoom = "IceDragonRoom"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MiniBosses\IceDragonRoom.unity".
        public const string Key_tests = "Key_tests"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Key_tests.unity".
        public const string LeftHallway = "LeftHallway"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\LeftHallway.unity".
        public const string Lineup = "Lineup"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Polygrade\Lineup.unity".
        public const string Main = "Main"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string MainMenu = "MainMenu"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string PoisonDragonRoom = "PoisonDragonRoom"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MiniBosses\PoisonDragonRoom.unity".
        public const string PressurePlateRoom = "PressurePlateRoom"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\PressurePlateRoom.unity".
        public const string RightHallway = "RightHallway"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\RightHallway.unity".
        public const string SkullScene = "SkullScene"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\SkullScene.unity".
        public const string Tutoriel = "Tutoriel"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        
    }
}