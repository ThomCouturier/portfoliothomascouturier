// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDDEN
// GENERATED ON 2023-03-10 15:05

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

using UnityEngine;

namespace Harmony
{
    public static partial class Finder
    {
        private static Events findableEvents = null; //See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scripts\Events\Events.cs".
        public static Events Events
        {
            get
            {
                if (!findableEvents)
                {
                    findableEvents = FindWithTag<Events>(Tags.Events);
                }
                return findableEvents;
            }
        }

        private static Ghost findableGhost = null; //See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scripts\Rooms\GhostRoom\Ghost.cs".
        public static Ghost Ghost
        {
            get
            {
                if (!findableGhost)
                {
                    findableGhost = FindWithTag<Ghost>(Tags.Ghost);
                }
                return findableGhost;
            }
        }

        private static Player findablePlayer = null; //See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scripts\Player\Player.cs".
        public static Player Player
        {
            get
            {
                if (!findablePlayer)
                {
                    findablePlayer = FindWithTag<Player>(Tags.Player);
                }
                return findablePlayer;
            }
        }

        private static PlayerUI findablePlayerUI = null; //See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scripts\UI\PlayerUI.cs".
        public static PlayerUI PlayerUI
        {
            get
            {
                if (!findablePlayerUI)
                {
                    findablePlayerUI = FindWithTag<PlayerUI>(Tags.PlayerUI);
                }
                return findablePlayerUI;
            }
        }

        private static SaveManager findableSaveManager = null; //See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scripts\Save\SaveManager.cs".
        public static SaveManager SaveManager
        {
            get
            {
                if (!findableSaveManager)
                {
                    findableSaveManager = FindWithTag<SaveManager>(Tags.SaveManager);
                }
                return findableSaveManager;
            }
        }

        private static SkullPuzzleManager findableSkullPuzzleManager = null; //See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scripts\Rooms\SkullRoom\SkullPuzzleManager.cs".
        public static SkullPuzzleManager SkullPuzzleManager
        {
            get
            {
                if (!findableSkullPuzzleManager)
                {
                    findableSkullPuzzleManager = FindWithTag<SkullPuzzleManager>(Tags.SkullPuzzleManager);
                }
                return findableSkullPuzzleManager;
            }
        }

        private static Game.Main findableMain = null; //See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scripts\Play\Mode\Main\Main.cs".
        public static Game.Main Main
        {
            get
            {
                if (!findableMain)
                {
                    findableMain = FindWithTag<Game.Main>(Tags.MainController);
                }
                return findableMain;
            }
        }

        private static Game.VirtualCamera findableVirtualCamera = null; //See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scripts\Play\Camera\VirtualCamera.cs".
        public static Game.VirtualCamera VirtualCamera
        {
            get
            {
                if (!findableVirtualCamera)
                {
                    findableVirtualCamera = FindWithTag<Game.VirtualCamera>(Tags.GameCamera);
                }
                return findableVirtualCamera;
            }
        }

        public static T FindWithTag<T>(string tag) where T : class
        {
            return GameObject.FindWithTag(tag)?.GetComponent<T>();
        }

    }
}