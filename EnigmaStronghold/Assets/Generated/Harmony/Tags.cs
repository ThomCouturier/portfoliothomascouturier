// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDDEN
// GENERATED ON 2023-03-10 15:05

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

using UnityEngine;

namespace Harmony
{
    public static partial class Tags
    {
        public const string B_Piece = "B_Piece"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public const string Ballista = "Ballista"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public const string BlueBox = "BlueBox"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public const string Box = "Box"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public const string BoxLedge = "BoxLedge"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public const string ChessPuzzleManager = "ChessPuzzleManager"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public const string DragonBall = "DragonBall"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public const string EditorOnly = "EditorOnly"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public const string Events = "Events"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public const string Finish = "Finish"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public const string FlyingPoint = "FlyingPoint"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public const string GameCamera = "GameCamera"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public const string GameController = "GameController"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public const string Ghost = "Ghost"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public const string GhostForceField = "GhostForceField"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public const string GreenBox = "GreenBox"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public const string Ground = "Ground"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public const string MainCamera = "MainCamera"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public const string MainController = "MainController"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public const string Pickable = "Pickable"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public const string Pillar = "Pillar"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public const string Player = "Player"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public const string PlayerUI = "PlayerUI"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public const string Possible_Position = "Possible_Position"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public const string Potato = "Potato"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public const string RedBox = "RedBox"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public const string Respawn = "Respawn"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public const string SaveManager = "SaveManager"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public const string Skull = "Skull"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public const string SkullPuzzleManager = "SkullPuzzleManager"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public const string Sword = "Sword"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        public const string Untagged = "Untagged"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\ProjectSettings\TagManager.asset".
        
    }
}