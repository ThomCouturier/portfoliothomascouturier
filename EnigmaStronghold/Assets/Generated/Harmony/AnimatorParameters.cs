// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDDEN
// GENERATED ON 2023-03-10 15:05

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

using UnityEngine;

namespace Harmony
{
    public static partial class AnimatorParameters
    {
        public static readonly int Activate = Animator.StringToHash("Activate"); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\StoneKnight\StoneKnight.controller".
        public static readonly int Attack = Animator.StringToHash("Attack"); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\StoneKnight\StoneKnight.controller".
        public static readonly int AttackState = Animator.StringToHash("AttackState"); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\Player\Hand.controller".
        public static readonly int Block = Animator.StringToHash("Block"); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\Player\Hand.controller".
        public static readonly int Charge = Animator.StringToHash("Charge"); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Animators\NightmareCTRL.controller".
        public static readonly int Dead = Animator.StringToHash("Dead"); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Animators\NightmareCTRL.controller".
        public static readonly int Death = Animator.StringToHash("Death"); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\StoneKnight\StoneKnight.controller".
        public static readonly int Disabled = Animator.StringToHash("Disabled"); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\UI\Clean Settings UI\Animations\InputField.controller".
        public static readonly int Entrance = Animator.StringToHash("Entrance"); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Animators\NightmareCTRL.controller".
        public static readonly int Fly = Animator.StringToHash("Fly"); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\Dragon\Dragon.controller".
        public static readonly int Highlighted = Animator.StringToHash("Highlighted"); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\UI\Clean Settings UI\Animations\InputField.controller".
        public static readonly int Hit = Animator.StringToHash("Hit"); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\Dummy.controller".
        public static readonly int Hover = Animator.StringToHash("Hover"); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\Dragon\Dragon.controller".
        public static readonly int Hurt = Animator.StringToHash("Hurt"); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Animators\NightmareCTRL.controller".
        public static readonly int Idle = Animator.StringToHash("Idle"); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Animators\NightmareCTRL.controller".
        public static readonly int MouthClose = Animator.StringToHash("MouthClose"); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\Dragon\Dragon.controller".
        public static readonly int Normal = Animator.StringToHash("Normal"); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\UI\Clean Settings UI\Animations\InputField.controller".
        public static readonly int Prepare = Animator.StringToHash("Prepare"); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\StoneKnight\StoneKnight.controller".
        public static readonly int Pressed = Animator.StringToHash("Pressed"); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\UI\Clean Settings UI\Animations\InputField.controller".
        public static readonly int Shoot = Animator.StringToHash("Shoot"); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Animators\NightmareCTRL.controller".
        public static readonly int Sitting = Animator.StringToHash("Sitting"); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\Player\Player.controller".
        public static readonly int Speed = Animator.StringToHash("Speed"); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\StoneKnight\StoneKnight.controller".
        public static readonly int Walk = Animator.StringToHash("Walk"); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\Player\Hand.controller".
        public static readonly int WhirlWind = Animator.StringToHash("WhirlWind"); // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Animations\StoneKnight\StoneKnight.controller".
        
    }
}