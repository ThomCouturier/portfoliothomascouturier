// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDDEN
// GENERATED ON 2023-03-10 15:05

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

using UnityEngine;

namespace Harmony
{
    public static partial class GameObjects
    {
        // " Jewelry Box" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        // "6" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Training_Target.prefab".
        public const string A3 = "A3"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string A4 = "A4"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string A5 = "A5"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string Abdomen = "Abdomen"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string Achievement = "Achievement"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string AchievementButton = "AchievementButton"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string AchievementIcon = "AchievementIcon"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string AchievementMenu = "AchievementMenu"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string AchievementName = "AchievementName"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string AchievementPlaceholder = "AchievementPlaceholder"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "AchievementPlaceholder (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "AchievementPlaceholder (10)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "AchievementPlaceholder (11)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "AchievementPlaceholder (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "AchievementPlaceholder (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "AchievementPlaceholder (4)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "AchievementPlaceholder (5)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "AchievementPlaceholder (6)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "AchievementPlaceholder (7)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "AchievementPlaceholder (8)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "AchievementPlaceholder (9)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string AchievementText = "AchievementText"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string AchievementsList = "AchievementsList"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string AcidDrops = "AcidDrops"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string AcidPuddle = "AcidPuddle"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Attacks\AcideDrop.prefab".
        // "AcidPuddle (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Attacks\AcideDrop.prefab".
        public const string AcidWaste = "AcidWaste"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Attacks\AcideDrop.prefab".
        public const string AcideDrop = "AcideDrop"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Attacks\AcideDrop.prefab".
        public const string ActivableStopperSecond = "ActivableStopperSecond"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string ActivableStopperUp = "ActivableStopperUp"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string AirBallPool = "AirBallPool"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Managers\DragonBallSpawner.prefab".
        public const string AirDragonBall = "AirDragonBall"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\DragonBalls\AirDragonBall.prefab".
        public const string AirLight = "AirLight"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Keys\Keyhole_Air.prefab".
        public const string Albino = "Albino"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string Alert = "Alert"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Attacks\IceStalagmite.prefab".
        public const string AmbientSound = "AmbientSound"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "Area Light" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Area Light (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string Arm_High_L = "Arm_High_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Arm_High_R = "Arm_High_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Arm_Low_L = "Arm_Low_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Arm_Low_R = "Arm_Low_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Armature = "Armature"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string AttackStep = "AttackStep"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string B3 = "B3"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string B4 = "B4"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "BW_Torch (4)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Hub_World_Light.prefab".
        // "BW_Torch (5)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Hub_World_Light.prefab".
        public const string B_Bishop = "B_Bishop"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        public const string B_King = "B_King"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        public const string B_Knight = "B_Knight"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        public const string B_Pawn = "B_Pawn"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        public const string B_Queen = "B_Queen"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "B_Queen (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\LeftHallway.unity".
        public const string B_Rook = "B_Rook"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        public const string Back = "Back"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string BackFace = "BackFace"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        public const string BackGroundPanel = "BackGroundPanel"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\UI\AchievementPlaceholder.prefab".
        public const string BackTopWall = "BackTopWall"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string Back_Wall = "Back_Wall"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "Back_Wall (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string Back_Wall_1 = "Back_Wall_1"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Background = "Background"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\UI\Clean Settings UI\Prefabs\Bar.prefab".
        public const string BallStimuli = "BallStimuli"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\DragonBall.prefab".
        public const string Ballista = "Ballista"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string Bar = "Bar"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\UI\Clean Settings UI\Prefabs\Bar.prefab".
        public const string BarricadedDoor = "BarricadedDoor"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        public const string Behind_Wall = "Behind_Wall"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\LionCorridor.prefab".
        public const string BigHousePrefab = "BigHousePrefab"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\BigHouse\BigHousePrefab.prefab".
        public const string Black_Pieces = "Black_Pieces"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        public const string BlockableSensor = "BlockableSensor"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        public const string Blue = "Blue"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string BlueFire = "BlueFire"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\PressurePlateRoom\ColoredPressurePlates\ColoredFire\BlueFire.prefab".
        public const string BlueMiniBoss = "BlueMiniBoss"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\KhrallidBossRoom.prefab".
        public const string BluePlate = "BluePlate"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string BluePressurePlate = "BluePressurePlate"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\PressurePlateRoom\ColoredPressurePlates\BluePressurePlate.prefab".
        public const string BoardTorches = "BoardTorches"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string Board_Layer = "Board_Layer"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string Body_High = "Body_High"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Body_Low = "Body_Low"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        // "Bone.014" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Bone.016" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Bone.016_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Bone.017" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Bone.017_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Bone.018" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Bone.018_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Bone.023" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Bone.025" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Bone.025_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Bone.026" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Bone.026_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Bone.027" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Bone.027_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Bone.028" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Bone.028_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Bone.031" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Bone.032" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Bone.032_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Bone.036" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Bone.037" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Bone.037_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string Book = "Book"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\SkullScene.unity".
        public const string Book1 = "Book1"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\SkullScene.unity".
        public const string Book2 = "Book2"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\SkullScene.unity".
        public const string Book3 = "Book3"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\SkullScene.unity".
        public const string Book4 = "Book4"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\SkullScene.unity".
        public const string Book5 = "Book5"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\SkullScene.unity".
        public const string Bookcase_1 = "Bookcase_1"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\SkullScene.unity".
        public const string Bookcase_Books_Seperate = "Bookcase_Books_Seperate"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SkullRoom.prefab".
        // "Bookcase_GRP (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\SkullScene.unity".
        public const string Borders = "Borders"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string BossAccessTrigger = "BossAccessTrigger"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Hub_World_Light.prefab".
        public const string BossHealthBar = "BossHealthBar"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string BossRoom = "BossRoom"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string BossRoomManager = "BossRoomManager"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string BossWalls = "BossWalls"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string BotezGambit = "BotezGambit"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string Botez_Gambit = "Botez_Gambit"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Bottom = "Bottom"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\LionCorridor.prefab".
        public const string Box = "Box"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\LionCorridor.prefab".
        public const string Box004 = "Box004"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Box005 = "Box005"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Box010 = "Box010"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Box1 = "Box1"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Props\WoodBox.prefab".
        public const string Box2 = "Box2"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Props\WoodBox.prefab".
        public const string Box3 = "Box3"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Props\WoodBox.prefab".
        public const string Box4 = "Box4"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Props\WoodBox.prefab".
        public const string Box5 = "Box5"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Props\WoodBox.prefab".
        public const string Box6 = "Box6"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Props\WoodBox.prefab".
        public const string Box7 = "Box7"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Props\WoodBox.prefab".
        public const string Box8 = "Box8"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Props\WoodBox.prefab".
        public const string BoxSide = "BoxSide"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        // "BoxSide (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        // "BoxSide (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        // "BoxSide (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        public const string BoxSideTop = "BoxSideTop"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        public const string BoxStimuli = "BoxStimuli"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Props\WoodBox.prefab".
        public const string Boxes = "Boxes"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        public const string BrokenBarrel = "BrokenBarrel"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string Buildings = "Buildings"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string Button = "Button"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\UI\Button 1.prefab".
        public const string C3 = "C3"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string C4 = "C4"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string Camera = "Camera"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\OldHouse\OldHousePrefab.prefab".
        public const string CameraUI = "CameraUI"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        public const string Campfire = "Campfire"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string CampfireBase = "CampfireBase"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string Candles = "Candles"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\LeftHallway.unity".
        public const string CannotEnterMessage = "CannotEnterMessage"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string Capsule = "Capsule"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        // "Capsule (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        // "Capsule (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        // "Capsule (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        public const string Castle = "Castle"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string Categories = "Categories"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string Ceiling = "Ceiling"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SkullRoom.prefab".
        public const string Ceilling = "Ceilling"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\LionCorridor.prefab".
        public const string Chain = "Chain"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Pendant.prefab".
        public const string ChainMail_low = "ChainMail_low"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerModel.prefab".
        public const string Chandalier = "Chandalier"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\KhromBossRoom.prefab".
        public const string Chandelier = "Chandelier"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "Chandelier asset" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        public const string Checkmark = "Checkmark"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\UI\Clean Settings UI\Prefabs\Toggle.prefab".
        public const string ChessCamera = "ChessCamera"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\ChessScene.unity".
        public const string ChessCubby = "ChessCubby"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string ChessPositions = "ChessPositions"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string ChessPuzzleManager = "ChessPuzzleManager"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\ChessScene.unity".
        public const string ChessRoomManager = "ChessRoomManager"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\ChessScene.unity".
        public const string ChessRoomRespawn = "ChessRoomRespawn"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string Chess_PreRoom = "Chess_PreRoom"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        public const string Chess_Room = "Chess_Room"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\ChessScene.unity".
        public const string Chest = "Chest"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        // "Chest (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Hub_World_Light.prefab".
        public const string Chibi_Wings = "Chibi_Wings"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Potato.prefab".
        public const string Circle = "Circle"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Circle.001" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        public const string Clip = "Clip"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string Colliders = "Colliders"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Objects\SwordSheath\Prefabs\Sword.prefab".
        public const string ConstructionSite = "ConstructionSite"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string Cork = "Cork"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Potion.prefab".
        public const string Corners = "Corners"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "Corners (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "Corners (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "Corners (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string Corridor = "Corridor"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\LionCorridor.prefab".
        public const string CountCircle = "CountCircle"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string Credits = "Credits"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string Crosshair = "Crosshair"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string Cube = "Cube"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cube (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "Cube (10)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "Cube (11)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        // "Cube (12)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        // "Cube (13)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        // "Cube (14)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        // "Cube (15)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        // "Cube (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "Cube (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "Cube (4)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "Cube (5)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "Cube (6)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "Cube (7)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "Cube (8)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "Cube (9)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "Cube.000" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\OldHouse\OldHousePrefab.prefab".
        // "Cube.001" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cube.002" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\OldHouse\OldHousePrefab.prefab".
        // "Cube.003" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\BigHouse\BigHousePrefab.prefab".
        // "Cube.004" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\BigHouse\BigHousePrefab.prefab".
        // "Cube.005" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\BigHouse\BigHousePrefab.prefab".
        // "Cube.006" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\BigHouse\BigHousePrefab.prefab".
        // "Cube.016" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\BigHouse\BigHousePrefab.prefab".
        // "Cube.017" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\BigHouse\BigHousePrefab.prefab".
        // "Cube.018" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\BigHouse\BigHousePrefab.prefab".
        // "Cube.019" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\BigHouse\BigHousePrefab.prefab".
        // "Cube.020" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\BigHouse\BigHousePrefab.prefab".
        // "Cube.021" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\BigHouse\BigHousePrefab.prefab".
        // "Cube.022" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\OldHouse\OldHousePrefab.prefab".
        // "Cube.023" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\OldHouse\OldHousePrefab.prefab".
        // "Cube.024" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\OldHouse\OldHousePrefab.prefab".
        // "Cube.025" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\OldHouse\OldHousePrefab.prefab".
        // "Cube.026" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\OldHouse\OldHousePrefab.prefab".
        // "Cube.027" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\OldHouse\OldHousePrefab.prefab".
        // "Cube.029" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\OldHouse\OldHousePrefab.prefab".
        // "Cube.038" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\Village Buildings\Prefabs\Village_WindMill.prefab".
        public const string Cylinder = "Cylinder"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.000" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.001" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.002" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.003" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.004" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.005" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.006" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.007" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.008" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.009" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.010" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.011" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.012" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.013" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.014" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.015" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.016" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.017" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.018" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.019" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.020" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.021" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.022" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.023" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.024" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.025" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.026" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.027" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.028" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.029" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.030" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.031" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.032" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.033" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.034" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.035" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.036" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.037" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.038" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.039" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.040" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.041" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.042" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.043" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.044" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.045" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.046" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.047" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.048" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.049" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.050" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.051" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.052" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.053" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.054" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.055" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.056" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.057" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.058" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.059" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.060" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.061" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.062" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.063" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.133" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        // "Cylinder.134" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        public const string Cylinder003 = "Cylinder003"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Cylinder004 = "Cylinder004"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Cylinder005 = "Cylinder005"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Cylinder006 = "Cylinder006"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Cylinder010 = "Cylinder010"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Cylinder011 = "Cylinder011"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Cylinder012 = "Cylinder012"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Cylinder013 = "Cylinder013"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Cylinder014 = "Cylinder014"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Cylinder015 = "Cylinder015"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Cylinder017 = "Cylinder017"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string D2 = "D2"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string D3 = "D3"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string D5 = "D5"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string DarkBlue = "DarkBlue"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\DarkBlue.prefab".
        public const string DeadPlayerIcon = "DeadPlayerIcon"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string DeathBoxBase = "DeathBoxBase"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\PressurePlateRoom\DeathBoxBase.prefab".
        public const string DeathBoxBlack = "DeathBoxBlack"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string DeathBoxBlue = "DeathBoxBlue"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        // "DeathBoxChild 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        public const string DeathBoxChild2 = "DeathBoxChild2"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        public const string DeathBoxChild3 = "DeathBoxChild3"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        public const string DeathBoxChild4 = "DeathBoxChild4"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        public const string DeathBoxGreen = "DeathBoxGreen"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string DeathBoxGreenElevator = "DeathBoxGreenElevator"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string DeathBoxRed = "DeathBoxRed"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string Decoration = "Decoration"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string Decorations = "Decorations"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SkullRoom.prefab".
        public const string Defeat = "Defeat"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string DefeatTMP = "DefeatTMP"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string DeleteButton = "DeleteButton"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "Description text - Dark" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\UI\Clean Settings UI\Prefabs\Description text - Dark.prefab".
        // "Description text - Light" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\UI\Clean Settings UI\Prefabs\Description text - Light.prefab".
        public const string Desk = "Desk"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        public const string Details = "Details"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string Diamond = "Diamond"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "Directional Light" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Directional light (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "Directional light (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string Dog = "Dog"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string Door = "Door"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string DoorEntrance = "DoorEntrance"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string DoorFrame = "DoorFrame"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        // "DoorFrame (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        public const string DoorMedieval = "DoorMedieval"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string DoorTrigger = "DoorTrigger"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Triggers\DoorTrigger.prefab".
        public const string Doorway = "Doorway"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\KhalledBossRoom.prefab".
        // "Doorway1-2" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        // "Doorway2-3" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string DoorwayWithMetalDoor = "DoorwayWithMetalDoor"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Doorways\DoorwayWithMetalDoor.prefab".
        public const string Dragon = "Dragon"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string DragonBall = "DragonBall"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\DragonBall.prefab".
        public const string DragonBallSpawner = "DragonBallSpawner"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string DragonBody = "DragonBody"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string DragonMesh = "DragonMesh"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string DragonMouth = "DragonMouth"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string DragonSoulEater = "DragonSoulEater"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string DragonStimuli = "DragonStimuli"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string Droplets = "Droplets"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\SkullScene.unity".
        public const string Dummy = "Dummy"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string DummyStimuli = "DummyStimuli"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Dummy.prefab".
        public const string E3 = "E3"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string E4 = "E4"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string Elements = "Elements"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string Elevator = "Elevator"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string Ennemies = "Ennemies"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SkullRoom.prefab".
        public const string EntitySensor = "EntitySensor"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\DragonBall.prefab".
        public const string Entrance = "Entrance"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\LionCorridor.prefab".
        public const string Entry = "Entry"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        public const string EntryPointGhost = "EntryPointGhost"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        public const string Entry_Wall = "Entry_Wall"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "Entry_Wall (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "Entry_Wall (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string EventSystem = "EventSystem"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string Events = "Events"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string Exit = "Exit"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string ExitForceField = "ExitForceField"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        public const string ExitTutorialTrigger = "ExitTutorialTrigger"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string Ext = "Ext"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Polygrade\Prefabs\Medieval_Windmill.prefab".
        public const string Eye_L = "Eye_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Eye_L_end = "Eye_L_end"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        public const string Eye_Left = "Eye_Left"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string Eye_R = "Eye_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Eye_R_end = "Eye_R_end"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        public const string Eye_Right = "Eye_Right"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string Eye_Under_L = "Eye_Under_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Eye_Under_R = "Eye_Under_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        // "Eyebrow._R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        public const string Eyebrow_L = "Eyebrow_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Eyebrow_L.001" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Eyebrow_L.002" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Eyebrow_L.002_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Eyebrow_R.001" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Eyebrow_R.002" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Eyebrow_R.002_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        public const string Eyes_low = "Eyes_low"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerModel.prefab".
        public const string F3 = "F3"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string F4 = "F4"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string Face = "Face"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        public const string Face_low = "Face_low"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerModel.prefab".
        public const string Fan = "Fan"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Polygrade\Prefabs\Medieval_Windmill.prefab".
        public const string Feet_L = "Feet_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        // "Feet_L 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string Feet_Left = "Feet_Left"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string Feet_R = "Feet_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Feet_Right = "Feet_Right"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string FencePrefab = "FencePrefab"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Fence\FencePrefab.prefab".
        // "FencePrefab (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "FencePrefab (10)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "FencePrefab (11)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "FencePrefab (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "FencePrefab (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "FencePrefab (4)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "FencePrefab (5)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "FencePrefab (6)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "FencePrefab (7)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "FencePrefab (8)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "FencePrefab (9)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string Fences = "Fences"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "Fight_Rework_:Belly_01" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Belly_02" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Clavicle_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Clavicle_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Collar" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:EBLOW_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:EBLOW_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:ELBOW_HELP_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:ELBOW_HELP_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Elbow_Helper_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Elbow_Helper_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Elbow_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Elbow_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:FOOT_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:FOOT_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Foot_IK_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Foot_IK_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Foot_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Foot_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Geometry" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:HAND_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:HAND_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:HEAD" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:HEEL_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:HEEL_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Handle_Disconnect_Point_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Handle_End_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Handle_End_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Handle_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Handle_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Head" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Hip_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Hip_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:IK_Hand_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:IK_Hand_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Index01_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Index01_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Index02_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Index02_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Index03_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Index03_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:JAW" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Jaw" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Joints" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:KNEE_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:KNEE_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Knee_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Knee_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:LEG_IK_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Leg_IK_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:MASTER" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Middle01_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Middle01_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Middle02_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Middle02_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Middle03_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Middle03_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:NECK" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Neck" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Nose" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Pinky01_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Pinky01_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Pinky02_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Pinky02_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Pinky03_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Pinky03_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Ring01_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Ring01_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Ring02_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Ring02_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Ring03_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Ring03_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:SHIELD" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:SPINE_01" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:SPINE_02" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:SPINE_03" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:SPINE_ROOT" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:SWORD" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Shield" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Skeleton.prefab".
        // "Fight_Rework_:Shoulder_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Shoulder_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Spine_01" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Spine_02" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Spine_03" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Spine_root" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:TOE_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:TOE_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Teeth" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Thumb01_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Thumb01_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Thumb02_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Thumb02_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Thumb03_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Thumb03_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Toe_IK_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Toe_IK_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Toe_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Toe_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Toe_tip_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Toe_tip_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:W_sword_01" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Skeleton.prefab".
        // "Fight_Rework_:Wrist_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:Wrist_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:handle_Disconnect_Point_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:jar_01" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Fight_Rework_:skeleton_mesh" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string Fight_Rework_RNannotation = "Fight_Rework_RNannotation"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string Fill = "Fill"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\UI\Clean Settings UI\Prefabs\Bar.prefab".
        // "Fill Area" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\UI\Clean Settings UI\Prefabs\Bar.prefab".
        public const string FinalFloor = "FinalFloor"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string Finger1_L = "Finger1_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        // "Finger1_L 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string Finger2_L = "Finger2_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        // "Finger2_L 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string Finger_1_1_L = "Finger_1_1_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Finger_1_1_R = "Finger_1_1_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Finger_1_2_L = "Finger_1_2_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Finger_1_2_R = "Finger_1_2_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Finger_2_1_L = "Finger_2_1_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Finger_2_1_R = "Finger_2_1_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Finger_2_2_L = "Finger_2_2_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Finger_2_2_R = "Finger_2_2_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Finger_2_3_L = "Finger_2_3_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Finger_2_3_R = "Finger_2_3_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Finger_3_1_L = "Finger_3_1_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Finger_3_1_R = "Finger_3_1_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Finger_3_2_L = "Finger_3_2_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Finger_3_2_R = "Finger_3_2_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Finger_3_3_L = "Finger_3_3_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Finger_3_3_R = "Finger_3_3_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Finger_4_1_L = "Finger_4_1_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Finger_4_1_R = "Finger_4_1_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Finger_4_2_L = "Finger_4_2_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Finger_4_2_R = "Finger_4_2_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Finger_4_3_L = "Finger_4_3_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Finger_4_3_R = "Finger_4_3_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Finger_5_1_L = "Finger_5_1_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Finger_5_1_R = "Finger_5_1_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Finger_5_2_L = "Finger_5_2_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Finger_5_2_R = "Finger_5_2_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Finger_5_3_L = "Finger_5_3_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Finger_5_3_R = "Finger_5_3_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Finger_L = "Finger_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Finger_L.001" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Finger_L.002" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Finger_L.002_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        public const string Finger_R = "Finger_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Finger_R.001" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Finger_R.002" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Finger_R.002_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        public const string Fire = "Fire"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        // "Fire (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        public const string FireBallPool = "FireBallPool"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Managers\DragonBallSpawner.prefab".
        public const string FireDragonBall = "FireDragonBall"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\DragonBalls\FireDragonBall.prefab".
        public const string FireLeft = "FireLeft"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\PressurePlateRoom\ColoredPressurePlates\BluePressurePlate.prefab".
        public const string FireLight = "FireLight"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\DragonBall.prefab".
        public const string FireRight = "FireRight"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\PressurePlateRoom\ColoredPressurePlates\BluePressurePlate.prefab".
        public const string Fish_Hallways_Decorated = "Fish_Hallways_Decorated"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\LeftHallway.unity".
        public const string FlameThrower = "FlameThrower"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string Flames = "Flames"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\SkullScene.unity".
        public const string Floor = "Floor"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Floor (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "Floor (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "Floor (4)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "Floor (5)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "Floor (6)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "Floor (7)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "Floor (8)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "Floor (9)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "Floor.001" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string FloorExtra = "FloorExtra"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        public const string FlyingPoint = "FlyingPoint"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "FlyingPoint (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "FlyingPoint (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "FlyingPoint (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "FlyingPoint (4)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "FlyingPoint (5)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "FlyingPoint (6)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "FlyingPoint (7)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string FlyingPoints = "FlyingPoints"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string Foot_L = "Foot_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Foot_R = "Foot_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string ForceField = "ForceField"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        // "ForceField (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        // "ForceField (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        public const string ForeArm_L = "ForeArm_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        public const string ForeArm_R = "ForeArm_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        public const string Forehead = "Forehead"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        public const string Forest = "Forest"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string Frame = "Frame"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Pendant.prefab".
        public const string FrontFace = "FrontFace"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        public const string Front_Wall = "Front_Wall"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "Front_Wall (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string G3 = "G3"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string G4 = "G4"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string G5 = "G5"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string GW_Torch = "GW_Torch"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "GW_Torch (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "GW_Torch (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "GW_Torch (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "GW_Torch (4)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "GW_Torch (5)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "GW_Torch (6)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "GW_Torch (7)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string GameManager = "GameManager"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Key_tests.unity".
        public const string Garbage = "Garbage"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string GargoyleDragon = "GargoyleDragon"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Decorations\GargoyleDragon.prefab".
        public const string Gargoyle_Dragon_Air = "Gargoyle_Dragon_Air"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Gargoyle_Dragon_Fire = "Gargoyle_Dragon_Fire"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Gargoyle_Dragon_Ice = "Gargoyle_Dragon_Ice"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Gargoyle_Dragon_Poison = "Gargoyle_Dragon_Poison"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Gargoyles = "Gargoyles"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Hub_World_Light.prefab".
        public const string Gate_Door = "Gate_Door"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Hub_World_Light.prefab".
        // "Gate_Door (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Hub_World_Light.prefab".
        public const string Gate_Door_COL = "Gate_Door_COL"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Hub_World_Light.prefab".
        public const string Gate_Door_LOD0 = "Gate_Door_LOD0"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Hub_World_Light.prefab".
        public const string Gate_Door_LOD1 = "Gate_Door_LOD1"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Hub_World_Light.prefab".
        public const string Gem = "Gem"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Pendant.prefab".
        public const string GemLight = "GemLight"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        public const string Ghost = "Ghost"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        public const string GhostMazeRoom = "GhostMazeRoom"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        public const string GhostRespawnHint = "GhostRespawnHint"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string GhostRoomManager = "GhostRoomManager"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\GhostRoom.unity".
        public const string GhostRoomRespawn = "GhostRoomRespawn"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string GhostTooFarHint = "GhostTooFarHint"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string Glass_Floor = "Glass_Floor"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "Glass_Floor (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "Glass_Floor (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string Gold = "Gold"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string GrabableSensor = "GrabableSensor"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        public const string Green = "Green"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Green.prefab".
        public const string GreenFire = "GreenFire"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\PressurePlateRoom\ColoredPressurePlates\ColoredFire\GreenFire.prefab".
        public const string GreenMiniBoss = "GreenMiniBoss"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\KhalledBossRoom.prefab".
        public const string GreenPlate = "GreenPlate"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string GreenPressurePlate = "GreenPressurePlate"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\PressurePlateRoom\ColoredPressurePlates\GreenPressurePlate.prefab".
        public const string Grey = "Grey"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Grey.prefab".
        public const string Ground = "Ground"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string Ground_Bottom = "Ground_Bottom"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\LionCorridor.prefab".
        public const string Ground_Top = "Ground_Top"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\LionCorridor.prefab".
        public const string Group001 = "Group001"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Group002 = "Group002"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "Guard Mesh.002" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        public const string H3 = "H3"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string H4 = "H4"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string Hand = "Hand"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        public const string HandTip_Left = "HandTip_Left"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string HandTip_Right = "HandTip_Right"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string Hand_L = "Hand_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        // "Hand_L 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string Hand_Left = "Hand_Left"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string Hand_R = "Hand_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Hand_Right = "Hand_Right"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string Handle = "Handle"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\UI\Clean Settings UI\Prefabs\Scrollbar.prefab".
        // "Handle Slide Area" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\UI\Clean Settings UI\Prefabs\Slider.prefab".
        public const string Handle_Neck = "Handle_Neck"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Pendant.prefab".
        public const string Hazards = "Hazards"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        public const string Head = "Head"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        // "Head.001" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Head.002" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Head.003" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Head.004" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Head.004_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Header - Dark" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\UI\Clean Settings UI\Prefabs\Header - Dark.prefab".
        // "Header - Light" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\UI\Clean Settings UI\Prefabs\Header - Light.prefab".
        public const string HealTutorial = "HealTutorial"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string HealthBar = "HealthBar"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string Helmet_low = "Helmet_low"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerModel.prefab".
        public const string HighDoorway = "HighDoorway"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Doorways\HighDoorway.prefab".
        public const string HighTorch = "HighTorch"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "HighTorch (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "HighTorch (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "HighTorch (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "HighTorch (4)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SkullRoom.prefab".
        // "HighTorch (5)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SkullRoom.prefab".
        // "HighTorch (6)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SkullRoom.prefab".
        // "HighTorch (7)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SkullRoom.prefab".
        // "HighTorch (8)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SkullRoom.prefab".
        // "HighTorch (9)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SkullRoom.prefab".
        public const string HintLight = "HintLight"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\ChessScene.unity".
        public const string HintTrigger = "HintTrigger"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string Hnad_R = "Hnad_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Hole = "Hole"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string Horse_Rider = "Horse_Rider"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string HubRespawn = "HubRespawn"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string Hub_World_Light = "Hub_World_Light"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "IK Chain001" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        // "IK Chain002" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string IceBallPool = "IceBallPool"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Managers\DragonBallSpawner.prefab".
        public const string IceDragonBall = "IceDragonBall"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\DragonBalls\IceDragonBall.prefab".
        public const string IceLight = "IceLight"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Keys\Keyhole_Ice.prefab".
        public const string IceStalagmite = "IceStalagmite"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Attacks\IceStalagmite.prefab".
        public const string Icon = "Icon"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\UI\Clean Settings UI\Prefabs\RoundButton.prefab".
        public const string Image = "Image"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\UI\Clean Settings UI\Prefabs\Scrollbar.prefab".
        // "Image 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\UI\Clean Settings UI\Prefabs\Scrollbar.prefab".
        public const string Index01_L = "Index01_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Index01_R = "Index01_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Index02_L = "Index02_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Index02_R = "Index02_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Index03_L = "Index03_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Index03_R = "Index03_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string IndexToe01_L = "IndexToe01_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string IndexToe01_R = "IndexToe01_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string IndexToe02_L = "IndexToe02_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string IndexToe02_R = "IndexToe02_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string IndexToe03_L = "IndexToe03_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string IndexToe03_R = "IndexToe03_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Inner = "Inner"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string Input = "Input"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\UI\Clean Settings UI\Prefabs\InputField.prefab".
        public const string InputField = "InputField"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\UI\Clean Settings UI\Prefabs\InputField.prefab".
        public const string InputManager = "InputManager"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string Intersection_Wall = "Intersection_Wall"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\LionCorridor.prefab".
        // "Intersection_Wall (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\RightHallway.unity".
        // "Item Label" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string Jaw = "Jaw"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string Jaw01 = "Jaw01"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Jaw02 = "Jaw02"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Jaw03 = "Jaw03"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string Jaw1 = "Jaw1"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string Jaw2 = "Jaw2"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string Jaw3 = "Jaw3"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string JawTip = "JawTip"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string JumpStep = "JumpStep"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string JumpTutorial = "JumpTutorial"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string KeyFinalPosition = "KeyFinalPosition"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\GhostRoom.unity".
        public const string Key_Air = "Key_Air"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Key_Fire = "Key_Fire"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Key_Ice = "Key_Ice"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Key_Poison = "Key_Poison"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Keybinds = "Keybinds"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string KeybindsMenu = "KeybindsMenu"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string KeyboardBinds1 = "KeyboardBinds1"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string KeyboardBinds2 = "KeyboardBinds2"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string Keyhole_Air = "Keyhole_Air"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Hub_World_Light.prefab".
        public const string Keyhole_Fire = "Keyhole_Fire"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Hub_World_Light.prefab".
        public const string Keyhole_Ice = "Keyhole_Ice"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Hub_World_Light.prefab".
        public const string Keyhole_Poison = "Keyhole_Poison"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Hub_World_Light.prefab".
        public const string KhalledBossRoom = "KhalledBossRoom"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\GhostRoom.unity".
        public const string KhalledSFX = "KhalledSFX"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\KhalledBossRoom.prefab".
        public const string KhrallidBossRoom = "KhrallidBossRoom"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\ChessScene.unity".
        public const string KhrallidSFX = "KhrallidSFX"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\KhrallidBossRoom.prefab".
        public const string KhrenBossRoom = "KhrenBossRoom"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\SkullScene.unity".
        public const string KhrenSFX = "KhrenSFX"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\KhrenBossRoom.prefab".
        public const string KhromBossRoom = "KhromBossRoom"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\PressurePlateRoom.unity".
        public const string KhromSFX = "KhromSFX"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\KhromBossRoom.prefab".
        public const string King = "King"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string KingLight = "KingLight"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\ChessScene.unity".
        public const string Knighty92 = "Knighty92"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string KnightyHands = "KnightyHands"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        public const string LEyeLight = "LEyeLight"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string LW_Torch = "LW_Torch"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "LW_Torch (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string L_Branch = "L_Branch"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        public const string L_LowerLeg02 = "L_LowerLeg02"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string L_Lowerleg01 = "L_Lowerleg01"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string L_Shoulder01 = "L_Shoulder01"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string L_Shoulder02 = "L_Shoulder02"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string L_Toe_Index01 = "L_Toe_Index01"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string L_Toe_Index02 = "L_Toe_Index02"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string L_Toe_Middle01 = "L_Toe_Middle01"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string L_Toe_Middle02 = "L_Toe_Middle02"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string L_Toe_Pinky01 = "L_Toe_Pinky01"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string L_Toe_Pinky02 = "L_Toe_Pinky02"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string L_UpperLeg = "L_UpperLeg"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string L_Wall = "L_Wall"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\LionCorridor.prefab".
        // "L_Wall " is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\LionCorridor.prefab".
        public const string L_WallTurn = "L_WallTurn"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\LionCorridor.prefab".
        public const string L_feet = "L_feet"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string Label = "Label"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\UI\Clean Settings UI\Prefabs\Bar.prefab".
        public const string Labyrinth_PreRoom = "Labyrinth_PreRoom"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        public const string Lamp = "Lamp"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\OldHouse\OldHousePrefab.prefab".
        public const string LargeDoorway = "LargeDoorway"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        public const string Lava = "Lava"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        // "Lava (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        // "Lava (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        // "Lava (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        public const string LavaFlowing = "LavaFlowing"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Decorations\LavaFlowing.prefab".
        public const string LavaFountain = "LavaFountain"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        public const string Leather_low = "Leather_low"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerModel.prefab".
        public const string Ledge = "Ledge"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string Ledge1 = "Ledge1"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\PressurePlateRoom\LedgeBlock.prefab".
        public const string Ledge2 = "Ledge2"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\PressurePlateRoom\LedgeBlock.prefab".
        public const string Ledge3 = "Ledge3"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\PressurePlateRoom\LedgeBlock.prefab".
        public const string Ledge4 = "Ledge4"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\PressurePlateRoom\LedgeBlock.prefab".
        public const string LedgeBlock = "LedgeBlock"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        // "LedgeBlock (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        // "LedgeBlock (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        // "LedgeBlock (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string LedgeWall = "LedgeWall"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        // "LedgeWall (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        // "LedgeWall (10)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        // "LedgeWall (11)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        // "LedgeWall (12)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        // "LedgeWall (13)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        // "LedgeWall (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        // "LedgeWall (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        // "LedgeWall (4)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        // "LedgeWall (5)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        // "LedgeWall (6)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        // "LedgeWall (7)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        // "LedgeWall (8)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        // "LedgeWall (9)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string LedgeWallBack = "LedgeWallBack"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string LedgeWallFront = "LedgeWallFront"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string LedgeWallLeft = "LedgeWallLeft"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string LedgeWallRight = "LedgeWallRight"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string Left = "Left"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "Left Eye Light" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Decorations\GargoyleDragon.prefab".
        public const string Left1 = "Left1"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        public const string Left2 = "Left2"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        public const string LeftBack = "LeftBack"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        public const string LeftBackWall = "LeftBackWall"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        public const string LeftCorridor = "LeftCorridor"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string LeftFront = "LeftFront"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        public const string LeftLeg = "LeftLeg"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string LeftLeg2 = "LeftLeg2"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string LeftRoom = "LeftRoom"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        public const string LeftShoulder_low = "LeftShoulder_low"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerModel.prefab".
        public const string LeftStairs = "LeftStairs"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string LeftWall = "LeftWall"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        // "LeftWall (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string LeftWing = "LeftWing"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string LeftWing2 = "LeftWing2"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string Left_Decoration = "Left_Decoration"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        public const string Left_Shelves = "Left_Shelves"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        public const string Left_Torches = "Left_Torches"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        public const string Left_Wall = "Left_Wall"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "Left_Wall (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "Left_Wall (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "Left_Wall (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "Left_Wall (4)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "Left_Wall (5)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "Left_Wall (6)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "Left_Wall (7)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        public const string Left_Wall_Door = "Left_Wall_Door"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Leg_High_L = "Leg_High_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Leg_High_R = "Leg_High_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Leg_Low_L = "Leg_Low_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Leg_Low_R = "Leg_Low_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Lever = "Lever"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string LeverHandle = "LeverHandle"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string LeverLight = "LeverLight"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\ChessScene.unity".
        public const string LeverStimuli = "LeverStimuli"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string Lid = "Lid"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string LightLeft = "LightLeft"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\PressurePlateRoom\ColoredPressurePlates\BluePressurePlate.prefab".
        public const string LightPieces = "LightPieces"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\ChessScene.unity".
        public const string LightPositions = "LightPositions"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\ChessScene.unity".
        public const string LightRight = "LightRight"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\PressurePlateRoom\ColoredPressurePlates\BluePressurePlate.prefab".
        public const string Lights = "Lights"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string Line = "Line"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\UI\Clean Settings UI\Prefabs\InputField.prefab".
        public const string LionCorridor = "LionCorridor"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\LionCorridor.prefab".
        public const string Liquid = "Liquid"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Potion.prefab".
        public const string Loading = "Loading"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string Loewenkopf = "Loewenkopf"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Hub_World_Light.prefab".
        // "Loewenkopf (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Hub_World_Light.prefab".
        public const string Logo = "Logo"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string LotCandles = "LotCandles"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\SkullScene.unity".
        public const string LowerArm_L = "LowerArm_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        // "LowerArm_L 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string LowerArm_Left = "LowerArm_Left"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string LowerArm_Right = "LowerArm_Right"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string LowerLeg2_L = "LowerLeg2_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        // "LowerLeg2_L 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string LowerLeg_L = "LowerLeg_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        // "LowerLeg_L 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string LowerLeg_Left = "LowerLeg_Left"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string LowerLeg_Right = "LowerLeg_Right"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string MagicalBarrier = "MagicalBarrier"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        public const string Main = "Main"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "Main Camera" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        public const string MainCamera = "MainCamera"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string MainLight = "MainLight"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string MainMenu = "MainMenu"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string Map = "Map"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string Maze = "Maze"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        public const string Medieval_Windmill = "Medieval_Windmill"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Polygrade\Prefabs\Medieval_Windmill.prefab".
        public const string MediumDoorway = "MediumDoorway"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Doorways\MediumDoorway.prefab".
        public const string Menu = "Menu"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string MetalDoor = "MetalDoor"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\RightHallway.unity".
        // "MetalDoor (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\RightHallway.unity".
        // "MetalDoor (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\LeftHallway.unity".
        // "MetalDoor (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\LeftHallway.unity".
        // "MetalDoor1-2" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        // "MetalDoor2-3" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string Middle01_L = "Middle01_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Middle01_R = "Middle01_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Middle02_L = "Middle02_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Middle02_R = "Middle02_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Middle3_L = "Middle3_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Middle3_R = "Middle3_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string MiddleToe01_L = "MiddleToe01_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string MiddleToe01_R = "MiddleToe01_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string MiddleToe02_L = "MiddleToe02_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string MiddleToe02_R = "MiddleToe02_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string MiddleToe03_L = "MiddleToe03_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string MiddleToe03_R = "MiddleToe03_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string MiniBossMesh = "MiniBossMesh"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\KhalledBossRoom.prefab".
        public const string MiniBossRoom = "MiniBossRoom"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\MiniBossRoom.prefab".
        public const string Mouth = "Mouth"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\KhalledBossRoom.prefab".
        // "Mouth Lower" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Mouth Lower_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Mouth Lower_L.001" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Mouth Lower_L.002" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Mouth Lower_L.002_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Mouth Lower_R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Mouth Lower_R.001" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Mouth Lower_R.002" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Mouth Lower_R.002_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Mouth Upper" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        public const string Mouth_Upper_L = "Mouth_Upper_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Mouth_Upper_R.001" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Mouth_Upper_R.002" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Mouth_Upper_R.002_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        public const string Mouth__Upper_R = "Mouth__Upper_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Mouth_upper_L.001" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Mouth_upper_L.002" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Mouth_upper_L.002_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        public const string MovementStep = "MovementStep"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string MovementTutorial = "MovementTutorial"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string MovingGhost = "MovingGhost"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        public const string Names = "Names"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string Neck = "Neck"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string Neck01 = "Neck01"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Neck02 = "Neck02"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Neck03 = "Neck03"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Neck1 = "Neck1"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string Neck2 = "Neck2"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string Neck3 = "Neck3"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string Neck4 = "Neck4"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string NeckFat_Left = "NeckFat_Left"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string NeckFat_Right = "NeckFat_Right"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string NightMareDragon = "NightMareDragon"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string Object002 = "Object002"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Object004 = "Object004"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Object005 = "Object005"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Object006 = "Object006"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Object030 = "Object030"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Object031 = "Object031"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Object032 = "Object032"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Object033 = "Object033"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Object034 = "Object034"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Object035 = "Object035"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Object036 = "Object036"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Object037 = "Object037"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Object038 = "Object038"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Object039 = "Object039"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Object040 = "Object040"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Object041 = "Object041"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Object042 = "Object042"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Object043 = "Object043"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Object044 = "Object044"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Object045 = "Object045"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Object046 = "Object046"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Object047 = "Object047"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Object048 = "Object048"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Object049 = "Object049"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Object050 = "Object050"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Object051 = "Object051"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Object052 = "Object052"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Object053 = "Object053"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Object054 = "Object054"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Object055 = "Object055"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Object056 = "Object056"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string ObsidianBallPool = "ObsidianBallPool"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Managers\DragonBallSpawner.prefab".
        public const string ObsidianDragonBall = "ObsidianDragonBall"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\DragonBalls\ObsidianDragonBall.prefab".
        public const string OldHouse = "OldHouse"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string OldHousePrefab = "OldHousePrefab"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\OldHouse\OldHousePrefab.prefab".
        public const string Options = "Options"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string Ornament = "Ornament"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Polygrade\Prefabs\Medieval_Windmill.prefab".
        public const string Outer = "Outer"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string Overall = "Overall"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string Panel = "Panel"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string Pause = "Pause"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string PauseTxt = "PauseTxt"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string Pawn_02 = "Pawn_02"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string Pawn_Mate_Sketchfab = "Pawn_Mate_Sketchfab"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "Pawn_Mate_Sketchfab (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string Pedestals = "Pedestals"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SkullRoom.prefab".
        public const string Pelvis = "Pelvis"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        public const string Pendant = "Pendant"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        public const string PerchingPoints = "PerchingPoints"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Persian Malayer Rug" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "Persian Malayer Rug (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "Persian Malayer Rug (10)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "Persian Malayer Rug (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "Persian Malayer Rug (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "Persian Malayer Rug (4)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "Persian Malayer Rug (5)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "Persian Malayer Rug (6)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "Persian Malayer Rug (7)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "Persian Malayer Rug (8)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "Persian Malayer Rug (9)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string PickablePendant = "PickablePendant"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        public const string PickupCrosshair = "PickupCrosshair"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string PickupStep = "PickupStep"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string Pillar = "Pillar"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Pillar\Pillar.prefab".
        public const string PillarStimuli = "PillarStimuli"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Pillar\Pillar.prefab".
        public const string Pinky01_L = "Pinky01_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Pinky01_R = "Pinky01_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Pinky02_L = "Pinky02_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Pinky02_R = "Pinky02_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Pinky03_L = "Pinky03_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Pinky03_R = "Pinky03_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string PinkyToe01_L = "PinkyToe01_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string PinkyToe01_R = "PinkyToe01_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string PinkyToe02_L = "PinkyToe02_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string PinkyToe02_R = "PinkyToe02_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string PinkyToe03_L = "PinkyToe03_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string PinkyToe03_R = "PinkyToe03_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Plane = "Plane"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Plane (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "Plane (10)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        // "Plane (11)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        // "Plane (12)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        // "Plane (13)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        // "Plane (14)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\LeftHallway.unity".
        // "Plane (15)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        // "Plane (16)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        // "Plane (17)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        // "Plane (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "Plane (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        // "Plane (4)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        // "Plane (5)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        // "Plane (6)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        // "Plane (7)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        // "Plane (8)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        // "Plane (9)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        // "Plane.000" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\OldHouse\OldHousePrefab.prefab".
        public const string Plane001 = "Plane001"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ChessPreset\Botez_Gambit.prefab".
        public const string Plane006 = "Plane006"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Plane007 = "Plane007"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Plane008 = "Plane008"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Plane011 = "Plane011"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Plane012 = "Plane012"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Plank_Hor = "Plank_Hor"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Training_Target.prefab".
        public const string Plank_Vert = "Plank_Vert"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Training_Target.prefab".
        public const string Planks = "Planks"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string PlateTourso_low = "PlateTourso_low"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerModel.prefab".
        public const string Plate_low = "Plate_low"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerModel.prefab".
        public const string Platform_Skull_01_LOD0 = "Platform_Skull_01_LOD0"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        public const string Platform_Skull_01_LOD1 = "Platform_Skull_01_LOD1"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        public const string Play = "Play"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string Player = "Player"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Key_tests.unity".
        public const string PlayerIcon = "PlayerIcon"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string PlayerIconBack = "PlayerIconBack"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string PlayerIconCircle = "PlayerIconCircle"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string PlayerModel = "PlayerModel"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerModel.prefab".
        public const string PlayerSensor = "PlayerSensor"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\KhalledBossRoom.prefab".
        public const string PlayerUI = "PlayerUI"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        // "Point Light" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "Point Light (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "Point Light (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "Point Light (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "Point Light (4)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "Point Light (5)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        public const string Point001 = "Point001"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Point002 = "Point002"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Point3 = "Point3"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string Point4 = "Point4"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string PoisonBallPool = "PoisonBallPool"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Managers\DragonBallSpawner.prefab".
        public const string PoisonDragonBall = "PoisonDragonBall"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\DragonBalls\PoisonDragonBall.prefab".
        public const string PoisonLight = "PoisonLight"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Keys\Keyhole_Poison.prefab".
        public const string PoolManager = "PoolManager"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SkullRoom.prefab".
        public const string PoolPotions = "PoolPotions"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        // "Post Processing" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string Potato = "Potato"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Potato.prefab".
        public const string PotatoShooter = "PotatoShooter"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string Potion = "Potion"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Potion.prefab".
        public const string PotionBack = "PotionBack"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string PotionCount = "PotionCount"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string PotionSpawner = "PotionSpawner"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string PotionText = "PotionText"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string Potions = "Potions"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string PotionsCircle = "PotionsCircle"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string PotionsIcon = "PotionsIcon"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string PreRoom = "PreRoom"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\LionCorridor.prefab".
        public const string PressurePlate = "PressurePlate"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        // "PressurePlate (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        // "PressurePlate (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        // "PressurePlate (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        public const string PressurePlateDoor = "PressurePlateDoor"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Doors\PressurePlateDoor.prefab".
        public const string PressurePlateRoom = "PressurePlateRoom"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\PressurePlateRoom.unity".
        // "PressurePlateRoom 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        public const string PressurePlateSensor = "PressurePlateSensor"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        public const string PressurePlates = "PressurePlates"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        public const string PressureRoomRespawn = "PressureRoomRespawn"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string PrimaryBar = "PrimaryBar"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string Props = "Props"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string Purple = "Purple"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Purple.prefab".
        public const string PuzzleItems = "PuzzleItems"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string QueenLight = "QueenLight"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\ChessScene.unity".
        public const string Queen_Enlarged = "Queen_Enlarged"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "Queen_Enlarged (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string REyeLight = "REyeLight"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string RHEF_Cave = "RHEF_Cave"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "RHEF_Cave (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "RHEF_Cave (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string RHEF_GenericCliffA = "RHEF_GenericCliffA"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\RHEF_GenericCliffA.prefab".
        // "RHEF_GenericCliffA (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_GenericCliffA (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string RHEF_GenericCliffA_Mobile = "RHEF_GenericCliffA_Mobile"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\MenuTerrain\Prefabs\RHEF_GenericCliffA_Mobile.prefab".
        public const string RHEF_GenericCliffB = "RHEF_GenericCliffB"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "RHEF_GenericCliffB (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "RHEF_GenericCliffB (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_GenericCliffB (4)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_GenericCliffB (6)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_GenericCliffB (7)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_GenericCliffB_,obile" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\MenuTerrain\Prefabs\RHEF_GenericCliffB_,obile.prefab".
        public const string RHEF_LowRockMedium = "RHEF_LowRockMedium"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "RHEF_LowRockMedium (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "RHEF_LowRockMedium (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string RHEF_LowRockMedium_Mobile = "RHEF_LowRockMedium_Mobile"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\MenuTerrain\Prefabs\RHEF_LowRockMedium_Mobile.prefab".
        public const string RHEF_RockDetailsA = "RHEF_RockDetailsA"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\RHEF_RockDetailsA.prefab".
        // "RHEF_RockDetailsA (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsA (10)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsA (11)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsA (12)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsA (13)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsA (14)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsA (15)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsA (16)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsA (17)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsA (18)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsA (19)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsA (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsA (20)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsA (21)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsA (22)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsA (23)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsA (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsA (4)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsA (5)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsA (6)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsA (7)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsA (8)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsA (9)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string RHEF_RockDetailsB = "RHEF_RockDetailsB"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\RHEF_RockDetailsB.prefab".
        // "RHEF_RockDetailsB (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsB (10)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsB (11)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsB (12)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsB (13)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsB (14)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsB (15)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsB (16)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsB (17)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsB (18)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsB (19)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsB (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsB (20)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsB (21)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsB (22)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsB (23)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsB (24)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsB (25)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsB (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsB (4)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsB (5)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsB (6)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsB (7)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsB (8)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockDetailsB (9)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string RHEF_RockMediumA = "RHEF_RockMediumA"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "RHEF_RockMediumA (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockMediumA (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockMediumA (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockMediumA (4)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string RHEF_RockMediumA_Mobile = "RHEF_RockMediumA_Mobile"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\MenuTerrain\Prefabs\RHEF_RockMediumA_Mobile.prefab".
        public const string RHEF_RockMediumB = "RHEF_RockMediumB"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "RHEF_RockMediumB (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockMediumB (10)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockMediumB (13)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockMediumB (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockMediumB (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockMediumB (4)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockMediumB (5)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "RHEF_RockMediumB (6)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockMediumB (7)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockMediumB (8)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockMediumB (9)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string RHEF_RockMediumB_Mobile = "RHEF_RockMediumB_Mobile"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\MenuTerrain\Prefabs\RHEF_RockMediumB_Mobile.prefab".
        public const string RHEF_RockMediumC = "RHEF_RockMediumC"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "RHEF_RockMediumC (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockMediumC (11)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockMediumC (12)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockMediumC (13)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockMediumC (14)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockMediumC (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockMediumC (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockMediumC (4)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockMediumC (6)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_RockMediumC (7)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string RHEF_SpruceA = "RHEF_SpruceA"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\RHEF_SpruceA.prefab".
        // "RHEF_SpruceA (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_SpruceA (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_SpruceA (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_SpruceA (4)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_SpruceA (5)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_SpruceA (6)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_SpruceA (7)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_SpruceA (8)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string RHEF_SpruceB = "RHEF_SpruceB"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\RHEF_SpruceB.prefab".
        // "RHEF_SpruceB (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_SpruceB (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_SpruceB (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string RHEF_SpruceC = "RHEF_SpruceC"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\RHEF_SpruceC.prefab".
        // "RHEF_SpruceC (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_SpruceC (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "RHEF_SpruceC (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string RHEF_SpruceTerrainA = "RHEF_SpruceTerrainA"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\RHEF_SpruceTerrainA.prefab".
        public const string RHEF_SpruceTerrainB = "RHEF_SpruceTerrainB"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\RHEF_SpruceTerrainB.prefab".
        public const string RHEF_SpruceTerrainC = "RHEF_SpruceTerrainC"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\RHEF_SpruceTerrainC.prefab".
        public const string RHE_Cave = "RHE_Cave"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\RHEF_Cave.prefab".
        public const string RHE_CaveTop = "RHE_CaveTop"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\RHEF_Cave.prefab".
        public const string RHE_SpruceA_Leaves = "RHE_SpruceA_Leaves"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\RHEF_SpruceA.prefab".
        public const string RHE_SpruceA_Trunk = "RHE_SpruceA_Trunk"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\RHEF_SpruceA.prefab".
        public const string RHE_SpruceB_Leaves = "RHE_SpruceB_Leaves"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\RHEF_SpruceB.prefab".
        public const string RHE_SpruceB_Trunk = "RHE_SpruceB_Trunk"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\RHEF_SpruceB.prefab".
        public const string RHE_SpruceC_Leaves = "RHE_SpruceC_Leaves"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\RHEF_SpruceC.prefab".
        public const string RHE_SpruceC_Trunk = "RHE_SpruceC_Trunk"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\RHEF_SpruceC.prefab".
        // "RW_Torch (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "RW_Torch (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "RW_Torch (4)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Hub_World_Light.prefab".
        // "RW_Torch (5)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Hub_World_Light.prefab".
        // "RW_Torch (6)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string R_Branch = "R_Branch"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        public const string R_Hand = "R_Hand"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        // "R_Hand 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string R_Index01 = "R_Index01"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        // "R_Index01 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string R_Index02 = "R_Index02"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        // "R_Index02 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string R_Index03 = "R_Index03"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        // "R_Index03 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string R_Middle01 = "R_Middle01"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        // "R_Middle01 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string R_Middle02 = "R_Middle02"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        // "R_Middle02 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string R_Middle03 = "R_Middle03"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        // "R_Middle03 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string R_Pinky01 = "R_Pinky01"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        // "R_Pinky01 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string R_Pinky02 = "R_Pinky02"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        // "R_Pinky02 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string R_Pinky03 = "R_Pinky03"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        // "R_Pinky03 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string R_RowerArm = "R_RowerArm"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        // "R_RowerArm 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string R_RowerReg02 = "R_RowerReg02"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string R_Rowerleg01 = "R_Rowerleg01"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string R_Shoulder01 = "R_Shoulder01"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string R_Shoulder02 = "R_Shoulder02"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string R_Toe_Index01 = "R_Toe_Index01"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string R_Toe_Index02 = "R_Toe_Index02"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string R_Toe_Middle01 = "R_Toe_Middle01"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string R_Toe_Middle02 = "R_Toe_Middle02"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string R_Toe_Pinky01 = "R_Toe_Pinky01"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string R_Toe_Pinky02 = "R_Toe_Pinky02"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string R_UpperArm = "R_UpperArm"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string R_UpperArm1 = "R_UpperArm1"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string R_UpperReg = "R_UpperReg"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string R_Wall = "R_Wall"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        // "R_Wall (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        // "R_Wall (10)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "R_Wall (11)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "R_Wall (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        // "R_Wall (7)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "R_Wall (8)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "R_Wall (9)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string R_WallTurn = "R_WallTurn"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\LionCorridor.prefab".
        public const string R_Wrist = "R_Wrist"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        // "R_Wrist 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string R_armHorn = "R_armHorn"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        // "R_armHorn 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string R_feet = "R_feet"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string Red = "Red"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Red.prefab".
        public const string RedFire = "RedFire"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\PressurePlateRoom\ColoredPressurePlates\ColoredFire\RedFire.prefab".
        public const string RedMiniBoss = "RedMiniBoss"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\KhromBossRoom.prefab".
        public const string RedPlate = "RedPlate"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string RedPressurePlate = "RedPressurePlate"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\PressurePlateRoom\ColoredPressurePlates\RedPressurePlate.prefab".
        public const string ReflectStep = "ReflectStep"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        // "Reflection Probe" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string Reload = "Reload"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string Replay = "Replay"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string RespawnManager = "RespawnManager"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string Return = "Return"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string ReverseLeg_Left = "ReverseLeg_Left"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string ReverseLeg_Right = "ReverseLeg_Right"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string Rib_L = "Rib_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_L.001" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_L.002" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_L.003" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_L.004" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_L.005" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_L.006" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_L.006_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_L.007" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_L.007_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_L.008" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_L.008_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_L.009" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_L.010" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_L.011" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_L.012" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_L.012_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_L.013" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_L.013_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_L.014" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_L.014_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        public const string Rib_R = "Rib_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_R.001" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_R.002" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_R.003" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_R.004" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_R.005" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_R.006" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_R.006_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_R.007" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_R.007_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_R.008" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_R.008_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_R.009" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_R.010" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_R.011" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_R.012" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_R.012_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_R.013" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_R.013_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_R.014" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Rib_R.014_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        public const string Right = "Right"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "Right Eye Light" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Decorations\GargoyleDragon.prefab".
        public const string Right1 = "Right1"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        public const string Right2 = "Right2"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        public const string RightBackWall = "RightBackWall"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        public const string RightCorridor = "RightCorridor"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\RightHallway.unity".
        public const string RightFront = "RightFront"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        public const string RightLeg = "RightLeg"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string RightLeg2 = "RightLeg2"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string RightShoulder_low = "RightShoulder_low"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerModel.prefab".
        public const string RightStairs = "RightStairs"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string RightWing = "RightWing"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string RightWing2 = "RightWing2"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string Right_Decoration = "Right_Decoration"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        public const string Right_Shelves = "Right_Shelves"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        public const string Right_Torches = "Right_Torches"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        public const string Right_Wall = "Right_Wall"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "Right_Wall (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "Right_Wall (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "Right_Wall (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        // "Right_Wall (4)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string Right_Wall_Door = "Right_Wall_Door"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string RockEntrance = "RockEntrance"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        // "RockEntrance (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string Rocks = "Rocks"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string Roof = "Roof"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "RoofExtra (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        public const string Room1 = "Room1"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string Room2 = "Room2"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string Room3 = "Room3"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string RoomManager = "RoomManager"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string Root = "Root"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Root_Pelvis = "Root_Pelvis"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string RoundButton = "RoundButton"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\UI\Clean Settings UI\Prefabs\RoundButton.prefab".
        public const string RowerArm_R = "RowerArm_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string RowerReg_R = "RowerReg_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Rug = "Rug"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string SM_AFS_Log01_LowEndPC = "SM_AFS_Log01_LowEndPC"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\SM_AFS_Log01_LowEndPC.prefab".
        public const string SM_AFS_Log02_LowEndPC = "SM_AFS_Log02_LowEndPC"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\SM_AFS_Log02_LowEndPC.prefab".
        public const string SM_AFS_Log03_LowEndPC = "SM_AFS_Log03_LowEndPC"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\SM_AFS_Log03_LowEndPC.prefab".
        public const string SM_AFS_Log04_LowEndPC = "SM_AFS_Log04_LowEndPC"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\SM_AFS_Log04_LowEndPC.prefab".
        public const string SM_AFS_Log05_LowEndPC = "SM_AFS_Log05_LowEndPC"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Props\Campfire.prefab".
        // "SM_AFS_Log05_LowEndPC (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Props\Campfire.prefab".
        public const string SM_AFS_Log06_LowEndPC = "SM_AFS_Log06_LowEndPC"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\SM_AFS_Log06_LowEndPC.prefab".
        public const string SM_AFS_Log07_LowEndPC = "SM_AFS_Log07_LowEndPC"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\SM_AFS_Log07_LowEndPC.prefab".
        public const string SM_AFS_Log08_LowEndPC = "SM_AFS_Log08_LowEndPC"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\SM_AFS_Log08_LowEndPC.prefab".
        public const string SM_AFS_Log09_LowEndPC = "SM_AFS_Log09_LowEndPC"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Props\Campfire.prefab".
        public const string SM_AFS_Log10_LowEndPC = "SM_AFS_Log10_LowEndPC"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\SM_AFS_Log10_LowEndPC.prefab".
        public const string SM_AFS_Log11_LowEndPC = "SM_AFS_Log11_LowEndPC"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\SM_AFS_Log11_LowEndPC.prefab".
        public const string SM_AFS_Log12_LowEndPC = "SM_AFS_Log12_LowEndPC"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\SM_AFS_Log12_LowEndPC.prefab".
        public const string SM_AFS_Log13_LowEndPC = "SM_AFS_Log13_LowEndPC"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\SM_AFS_Log13_LowEndPC.prefab".
        public const string SM_AFS_Log14_LowEndPC = "SM_AFS_Log14_LowEndPC"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\SM_AFS_Log14_LowEndPC.prefab".
        public const string SM_AFS_Log15_LowEndPC = "SM_AFS_Log15_LowEndPC"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\SM_AFS_Log15_LowEndPC.prefab".
        public const string SM_AFS_Log16_LowEndPC = "SM_AFS_Log16_LowEndPC"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\SM_AFS_Log16_LowEndPC.prefab".
        public const string SM_AFS_Log17_LowEndPC = "SM_AFS_Log17_LowEndPC"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\SM_AFS_Log17_LowEndPC.prefab".
        public const string SM_AFS_Log18_LowEndPC = "SM_AFS_Log18_LowEndPC"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\SM_AFS_Log18_LowEndPC.prefab".
        public const string SM_AFS_Log19_LowEndPC = "SM_AFS_Log19_LowEndPC"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\SM_AFS_Log19_LowEndPC.prefab".
        public const string SM_AFS_Log20_LowEndPC = "SM_AFS_Log20_LowEndPC"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\SM_AFS_Log20_LowEndPC.prefab".
        public const string SM_AFS_Log21_LowEndPC = "SM_AFS_Log21_LowEndPC"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\SM_AFS_Log21_LowEndPC.prefab".
        public const string SM_AFS_Log22_LowEndPC = "SM_AFS_Log22_LowEndPC"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\SM_AFS_Log22_LowEndPC.prefab".
        public const string SM_AFS_Log23_LowEndPC = "SM_AFS_Log23_LowEndPC"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\SM_AFS_Log23_LowEndPC.prefab".
        public const string SM_AFS_Log24_LowEndPC = "SM_AFS_Log24_LowEndPC"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\SM_AFS_Log24_LowEndPC.prefab".
        public const string SM_AFS_Log25_LowEndPC = "SM_AFS_Log25_LowEndPC"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Props\Campfire.prefab".
        public const string SM_AFS_Log26_LowEndPC = "SM_AFS_Log26_LowEndPC"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\SM_AFS_Log26_LowEndPC.prefab".
        public const string SM_AFS_Log27_LowEndPC = "SM_AFS_Log27_LowEndPC"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\SM_AFS_Log27_LowEndPC.prefab".
        public const string SM_AFS_Log28_LowEndPC = "SM_AFS_Log28_LowEndPC"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Props\Campfire.prefab".
        public const string SM_AFS_Log29_LowEndPC = "SM_AFS_Log29_LowEndPC"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Forest\SM_AFS_Log29_LowEndPC.prefab".
        public const string SM_Chest = "SM_Chest"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Props\Chest.prefab".
        public const string SM_ST_TorchHolder_LP = "SM_ST_TorchHolder_LP"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Torch.prefab".
        // "SM_ST_TorchHolder_LP (11)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        public const string Save1 = "Save1"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string Save2 = "Save2"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string Save3 = "Save3"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string SaveManager = "SaveManager"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string SceneManager = "SceneManager"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string Scrollbar = "Scrollbar"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\UI\Clean Settings UI\Prefabs\Scrollbar.prefab".
        public const string Seat = "Seat"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string SecondLevel = "SecondLevel"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        public const string Second_Floor = "Second_Floor"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string SecondaryBar = "SecondaryBar"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string SecondaryLight = "SecondaryLight"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string Shelf_Low = "Shelf_Low"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        // "Shelf_Low (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        // "Shelf_Low (10)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        // "Shelf_Low (11)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        // "Shelf_Low (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        // "Shelf_Low (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        // "Shelf_Low (4)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        // "Shelf_Low (5)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        // "Shelf_Low (6)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        // "Shelf_Low (7)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        // "Shelf_Low (8)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        // "Shelf_Low (9)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        public const string Shield = "Shield"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Side = "Side"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        public const string Skeleton = "Skeleton"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Skeleton (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\LeftHallway.unity".
        // "Skeleton (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\LeftHallway.unity".
        // "Skeleton (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\SkullScene.unity".
        // "Skeleton (4)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\SkullScene.unity".
        // "Skeleton (5)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\SkullScene.unity".
        public const string Skeleton1 = "Skeleton1"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string SkeletonBoss = "SkeletonBoss"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\SkeletonBoss.prefab".
        public const string SkeletonBossAir = "SkeletonBossAir"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SkullRoom.prefab".
        public const string SkeletonBossFire = "SkeletonBossFire"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SkullRoom.prefab".
        public const string SkeletonBossIce = "SkeletonBossIce"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SkullRoom.prefab".
        public const string SkeletonBossPoison = "SkeletonBossPoison"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SkullRoom.prefab".
        public const string SkeletonMeshRenderer = "SkeletonMeshRenderer"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Skeleton.prefab".
        public const string SkeletonSongTrigger = "SkeletonSongTrigger"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SkullRoom.prefab".
        public const string SkeletonStimuli = "SkeletonStimuli"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Skeleton.prefab".
        public const string Skruv_low = "Skruv_low"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerModel.prefab".
        public const string Skull = "Skull"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "Skull (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string SkullPedestal = "SkullPedestal"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Pillar\SkullPedestal.prefab".
        public const string SkullPedestalAir = "SkullPedestalAir"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SkullRoom.prefab".
        public const string SkullPedestalFire = "SkullPedestalFire"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SkullRoom.prefab".
        public const string SkullPedestalIce = "SkullPedestalIce"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SkullRoom.prefab".
        public const string SkullPedestalPoison = "SkullPedestalPoison"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SkullRoom.prefab".
        public const string SkullPlacement = "SkullPlacement"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Pillar\Pillar.prefab".
        public const string SkullPuzzleManager = "SkullPuzzleManager"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SkullRoom.prefab".
        public const string SkullRoom = "SkullRoom"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\SkullScene.unity".
        public const string SkullRoomRespawn = "SkullRoomRespawn"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string SkullSensor = "SkullSensor"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        public const string SkullSpawner = "SkullSpawner"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SkullRoom.prefab".
        public const string SlashableSensor = "SlashableSensor"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Objects\SwordSheath\Prefabs\Sword.prefab".
        public const string SlideFloor = "SlideFloor"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string SlidePressurePlateRoom = "SlidePressurePlateRoom"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string Slider = "Slider"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\UI\Clean Settings UI\Prefabs\Slider.prefab".
        // "Sliding Area" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\UI\Clean Settings UI\Prefabs\Scrollbar.prefab".
        public const string SmallChain = "SmallChain"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string SmallDoorway = "SmallDoorway"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Doorways\SmallDoorway.prefab".
        public const string SmallGhostMaze = "SmallGhostMaze"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        public const string SmallRocks = "SmallRocks"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string SmallRocks2 = "SmallRocks2"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string Small_Gargoyle_Left = "Small_Gargoyle_Left"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Small_Gargoyle_Left2 = "Small_Gargoyle_Left2"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Small_Gargoyle_Right = "Small_Gargoyle_Right"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Small_Gargoyle_Right2 = "Small_Gargoyle_Right2"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Smoke = "Smoke"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Props\Campfire.prefab".
        public const string Snowflakes = "Snowflakes"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\SkullScene.unity".
        public const string Soldier_01 = "Soldier_01"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string Soldier_02 = "Soldier_02"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Chess_Room.prefab".
        public const string SongTrigger = "SongTrigger"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string SoundManager = "SoundManager"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string SpawnForceField = "SpawnForceField"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        public const string SpecialAttacksSpawner = "SpecialAttacksSpawner"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string Sphere = "Sphere"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\ForceField\ExitForceField.prefab".
        public const string Spine = "Spine"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string Spine01 = "Spine01"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Spine02 = "Spine02"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Spine1 = "Spine1"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string Spine2 = "Spine2"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string SpineDown = "SpineDown"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string SpineUp = "SpineUp"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string Splash = "Splash"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\DragonBall.prefab".
        // "Spot Light" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        // "Spot Light Down" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "Spot Light Mid" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        // "Spot Light Up" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Square = "Square"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Props\Campfire.prefab".
        // "Square (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Props\Campfire.prefab".
        public const string SquareButton = "SquareButton"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\UI\Clean Settings UI\Prefabs\SquareButton.prefab".
        public const string Stairs = "Stairs"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        // "Stairs (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "Stairs (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SkullRoom.prefab".
        // "Stairs (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        // "Stairs (4)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string StairsAndStep = "StairsAndStep"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Stalagmites = "Stalagmites"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string StaminaBar = "StaminaBar"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string StaminaManager = "StaminaManager"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        public const string StandardDoorway = "StandardDoorway"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Doorways\StandardDoorway.prefab".
        public const string Statues = "Statues"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Step = "Step"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        // "Step (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        // "Step (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        public const string Stimuli = "Stimuli"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\KhalledBossRoom.prefab".
        public const string StoneKnight = "StoneKnight"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\ChessScene.unity".
        public const string StoneKnightMeshRenderer = "StoneKnightMeshRenderer"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string StoneKnightStimuli = "StoneKnightStimuli"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string StoneSword = "StoneSword"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        // "Sub header - Dark" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\UI\Clean Settings UI\Prefabs\Sub header - Dark.prefab".
        // "Sub header - Light" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\UI\Clean Settings UI\Prefabs\Sub header - Light.prefab".
        public const string Sun = "Sun"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        public const string Sword = "Sword"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Objects\SwordSheath\Prefabs\Sword.prefab".
        public const string SwordHandler = "SwordHandler"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        public const string Sword_rack = "Sword_rack"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string TadaLight = "TadaLight"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string Tail = "Tail"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Tail.001" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Tail.002" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Tail.003" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Tail.004" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Tail.005" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Tail.006" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Tail.007" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Tail.008" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Tail.009" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Tail.010" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Tail.011" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Tail.012" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Tail.013" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Tail.014" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Tail.015" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Tail.016" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Tail.016_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        public const string Tail01 = "Tail01"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string Tail02 = "Tail02"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string Tail03 = "Tail03"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string Tail04 = "Tail04"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string Tail1 = "Tail1"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string Tail2 = "Tail2"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string Tail3 = "Tail3"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string Tail4 = "Tail4"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string Tail5 = "Tail5"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string Tail5_end = "Tail5_end"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string TailEnd = "TailEnd"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string TaskHint = "TaskHint"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string TaskIcon = "TaskIcon"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string TaskKey = "TaskKey"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string TaskLabelText = "TaskLabelText"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string TaskMenu = "TaskMenu"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string TaskText = "TaskText"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string Terrain = "Terrain"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string Text = "Text"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\UI\Clean Settings UI\Prefabs\SquareButton.prefab".
        // "Text (TMP)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string Thumb01_L = "Thumb01_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Thumb01_R = "Thumb01_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Thumb02_L = "Thumb02_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Thumb02_R = "Thumb02_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string ThumbToe01_L = "ThumbToe01_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string ThumbToe01_R = "ThumbToe01_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string ThumbToe02_L = "ThumbToe02_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string ThumbToe02_R = "ThumbToe02_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Thumb_L = "Thumb_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Thumb_L.001" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Thumb_L.002" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Thumb_L.002_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        public const string Thumb_R = "Thumb_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Thumb_R.001" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Thumb_R.002" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Thumb_R.002_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        public const string Title = "Title"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string TitleBack = "TitleBack"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string Toe1_L = "Toe1_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        // "Toe1_L 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string Toe2_L = "Toe2_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        // "Toe2_L 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string Toe_Left = "Toe_Left"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string Toe_Right = "Toe_Right"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string Toggle = "Toggle"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\UI\Clean Settings UI\Prefabs\Toggle.prefab".
        public const string Tongue = "Tongue"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string Tongue01 = "Tongue01"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Tongue02 = "Tongue02"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Tongue03 = "Tongue03"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Tongue04 = "Tongue04"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Tongue_end = "Tongue_end"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Toon Ghost" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Toon Ghost Bones" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        public const string Top = "Top"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\LionCorridor.prefab".
        public const string TopBackWall = "TopBackWall"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        public const string Torch = "Torch"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        // "Torch (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        // "Torch (10)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\LeftHallway.unity".
        // "Torch (11)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\LeftHallway.unity".
        // "Torch (12)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        // "Torch (13)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\LeftHallway.unity".
        // "Torch (14)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\LeftHallway.unity".
        // "Torch (15)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\LeftHallway.unity".
        // "Torch (16)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\LeftHallway.unity".
        // "Torch (17)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\LeftHallway.unity".
        // "Torch (18)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\LeftHallway.unity".
        // "Torch (19)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\LeftHallway.unity".
        // "Torch (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        // "Torch (20)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\LeftHallway.unity".
        // "Torch (21)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\LionCorridor.prefab".
        // "Torch (22)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\LionCorridor.prefab".
        // "Torch (23)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\RightHallway.unity".
        // "Torch (24)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\RightHallway.unity".
        // "Torch (25)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\RightHallway.unity".
        // "Torch (26)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\RightHallway.unity".
        // "Torch (27)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\RightHallway.unity".
        // "Torch (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        // "Torch (4)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        // "Torch (5)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        // "Torch (6)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\LeftHallway.unity".
        // "Torch (7)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        // "Torch (8)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\LeftHallway.unity".
        // "Torch (9)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        public const string TorchFlame = "TorchFlame"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "TorchFlame (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "TorchFlame (10)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "TorchFlame (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\KhrallidBossRoom.prefab".
        // "TorchFlame (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\KhrallidBossRoom.prefab".
        // "TorchFlame (7)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "TorchFlame (8)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "TorchFlame (9)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string TorchLeft = "TorchLeft"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        public const string TorchLeftBack = "TorchLeftBack"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string TorchLeftFront = "TorchLeftFront"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string TorchLeftStairs = "TorchLeftStairs"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string TorchLeftUp1 = "TorchLeftUp1"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string TorchLeftUp2 = "TorchLeftUp2"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string TorchRight = "TorchRight"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        public const string TorchRightBack = "TorchRightBack"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string TorchRightFront = "TorchRightFront"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string TorchRightStairs = "TorchRightStairs"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        // "TorchRightStairs (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\PressurePlateRoom.unity".
        // "TorchRightStairs (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\PressurePlateRoom.unity".
        public const string TorchRightUp1 = "TorchRightUp1"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string TorchRightUp2 = "TorchRightUp2"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SlidePressurePlateRoom.prefab".
        public const string Torches = "Torches"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Torso = "Torso"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        public const string Torso_L = "Torso_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        public const string Torso_R = "Torso_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        public const string Tower = "Tower"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Tower.001" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Tower.002" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "Tower.003" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string TowerRoof = "TowerRoof"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string Training_Target = "Training_Target"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Training_Target.prefab".
        // "Transition Mini-B" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\PressurePlateRoom 1.prefab".
        public const string TreasureChest = "TreasureChest"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string Tree = "Tree"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Polygrade\Prefabs\Tree.prefab".
        // "Tree (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Polygrade\Lineup.unity".
        // "Tree (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Polygrade\Lineup.unity".
        public const string Trees = "Trees"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string Trigger = "Trigger"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string TriggerLoadChess = "TriggerLoadChess"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string TriggerLoadGhost = "TriggerLoadGhost"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string TriggerLoadHubBox = "TriggerLoadHubBox"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string TriggerLoadHubChess = "TriggerLoadHubChess"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string TriggerLoadHubGhost = "TriggerLoadHubGhost"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string TriggerLoadHubSkull = "TriggerLoadHubSkull"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string TriggerLoadLeftHallway = "TriggerLoadLeftHallway"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string TriggerLoadPlate = "TriggerLoadPlate"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string TriggerLoadRightHallway = "TriggerLoadRightHallway"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string TriggerLoadSkull = "TriggerLoadSkull"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string TriggerLoading = "TriggerLoading"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Triggers\TriggerLoading.prefab".
        public const string TriggerUnloadChess = "TriggerUnloadChess"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string TriggerUnloadGhost = "TriggerUnloadGhost"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string TriggerUnloadHubBox = "TriggerUnloadHubBox"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string TriggerUnloadHubChess = "TriggerUnloadHubChess"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string TriggerUnloadHubGhost = "TriggerUnloadHubGhost"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string TriggerUnloadHubSkull = "TriggerUnloadHubSkull"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string TriggerUnloadLeftHallway = "TriggerUnloadLeftHallway"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string TriggerUnloadPlate = "TriggerUnloadPlate"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string TriggerUnloadRightHallway = "TriggerUnloadRightHallway"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string TriggerUnloadScenesLeft = "TriggerUnloadScenesLeft"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string TriggerUnloadScenesRight = "TriggerUnloadScenesRight"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string TriggerUnloadSkulls = "TriggerUnloadSkulls"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string TriggerValidationAir = "TriggerValidationAir"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string TriggerValidationFire = "TriggerValidationFire"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string TriggerValidationIce = "TriggerValidationIce"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string TriggerValidationPoison = "TriggerValidationPoison"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string Triggers = "Triggers"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Main.unity".
        public const string TutorialBorders = "TutorialBorders"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string TutorialSFX = "TutorialSFX"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string UI = "UI"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string UIManager = "UIManager"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        // "Upper Arm_L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        // "Upper Arm_R." is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\Ghost.prefab".
        public const string UpperArm_L = "UpperArm_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string UpperArm_L1 = "UpperArm_L1"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string UpperArm_Left = "UpperArm_Left"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string UpperArm_R = "UpperArm_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string UpperArm_Right = "UpperArm_Right"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string UpperHead01 = "UpperHead01"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string UpperHead02 = "UpperHead02"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonNightmare\Albino.prefab".
        public const string UpperHead1 = "UpperHead1"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string UpperHead2 = "UpperHead2"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string UpperLeg_L = "UpperLeg_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string UpperLeg_L1 = "UpperLeg_L1"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string UpperLeg_Left = "UpperLeg_Left"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string UpperLeg_Right = "UpperLeg_Right"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string UpperMouth = "UpperMouth"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string UpperMouth01 = "UpperMouth01"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string UpperMouth02 = "UpperMouth02"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string UpperReg_R = "UpperReg_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Usage1 = "Usage1"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string Usage2 = "Usage2"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string UserSettings = "UserSettings"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string ValleyFat = "ValleyFat"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string Victory = "Victory"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string VictoryTMP = "VictoryTMP"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\PlayerUI.prefab".
        public const string Village_WindMill = "Village_WindMill"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\Village Buildings\Prefabs\Village_WindMill.prefab".
        public const string W_Bishop = "W_Bishop"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        public const string W_King = "W_King"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        public const string W_Knight = "W_Knight"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        public const string W_Pawn = "W_Pawn"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        public const string W_Queen = "W_Queen"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        public const string W_Rook = "W_Rook"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        public const string WallBehindStairs = "WallBehindStairs"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\SkullRoom.prefab".
        public const string WallLamp = "WallLamp"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string Wall_Gate = "Wall_Gate"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Walls = "Walls"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Watermelon_512 = "Watermelon_512"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Objects\Watermelon\Prefabs\Watermelon_512.prefab".
        public const string Weapon = "Weapon"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Enemies\StoneKnight.prefab".
        public const string Weapon_Misc = "Weapon_Misc"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string Well = "Well"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Vendor\VillagePack\Well\Well.prefab".
        public const string White_Pieces = "White_Pieces"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        public const string Wind = "Wind"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Rooms\SkullScene.unity".
        public const string WindMill = "WindMill"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string Wing = "Wing"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string Wing01_L = "Wing01_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Wing01_Left = "Wing01_Left"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string Wing01_R = "Wing01_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Wing01_Right = "Wing01_Right"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string Wing02_L = "Wing02_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Wing02_Left = "Wing02_Left"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string Wing02_R = "Wing02_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Wing02_Right = "Wing02_Right"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string Wing03_L = "Wing03_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Wing03_Left = "Wing03_Left"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string Wing03_R = "Wing03_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Wing03_Right = "Wing03_Right"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string Wing1_L = "Wing1_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string Wing1_L1 = "Wing1_L1"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string Wing2_L = "Wing2_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        // "Wing2_L 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string Wing3_L = "Wing3_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        // "Wing3_L 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string Wing4_L = "Wing4_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        // "Wing4_L 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string WingClaw1_L = "WingClaw1_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        // "WingClaw1_L 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string WingClaw2_L = "WingClaw2_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        // "WingClaw2_L 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string WingClaw3_L = "WingClaw3_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        // "WingClaw3_L 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string WingClaw4_L = "WingClaw4_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        // "WingClaw4_L 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string WingDetail10_L = "WingDetail10_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        // "WingDetail10_L 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string WingDetail11_L = "WingDetail11_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        // "WingDetail11_L 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string WingDetail12_L = "WingDetail12_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        // "WingDetail12_L 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string WingDetail13_L = "WingDetail13_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        // "WingDetail13_L 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string WingDetail1_L = "WingDetail1_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        // "WingDetail1_L 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string WingDetail2_L = "WingDetail2_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        // "WingDetail2_L 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string WingDetail3_L = "WingDetail3_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        // "WingDetail3_L 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string WingDetail4_L = "WingDetail4_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        // "WingDetail4_L 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string WingDetail5_L = "WingDetail5_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        // "WingDetail5_L 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string WingDetail6_L = "WingDetail6_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        // "WingDetail6_L 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string WingDetail7_L = "WingDetail7_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        // "WingDetail7_L 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string WingDetail8_L = "WingDetail8_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        // "WingDetail8_L 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string WingDetail9_L = "WingDetail9_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        // "WingDetail9_L 1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonTerrorBringer\Blue.prefab".
        public const string WingHand_Left = "WingHand_Left"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string WingHand_Right = "WingHand_Right"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string WingIndex01_L = "WingIndex01_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string WingIndex01_Left = "WingIndex01_Left"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string WingIndex01_R = "WingIndex01_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string WingIndex01_Right = "WingIndex01_Right"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string WingIndex02_L = "WingIndex02_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string WingIndex02_Left = "WingIndex02_Left"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string WingIndex02_R = "WingIndex02_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string WingIndex02_Right = "WingIndex02_Right"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string WingIndex03_L = "WingIndex03_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string WingIndex03_Left = "WingIndex03_Left"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string WingIndex03_R = "WingIndex03_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string WingIndex03_Right = "WingIndex03_Right"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string WingMiddle01_L = "WingMiddle01_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string WingMiddle01_Left = "WingMiddle01_Left"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string WingMiddle01_R = "WingMiddle01_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string WingMiddle01_Right = "WingMiddle01_Right"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string WingMiddle02_L = "WingMiddle02_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string WingMiddle02_Left = "WingMiddle02_Left"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string WingMiddle02_R = "WingMiddle02_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string WingMiddle02_Right = "WingMiddle02_Right"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string WingMiddle03_Left = "WingMiddle03_Left"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string WingMiddle03_Right = "WingMiddle03_Right"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string WingMiddle04_L = "WingMiddle04_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string WingMiddle04_R = "WingMiddle04_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string WingMiddle05_L = "WingMiddle05_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string WingMiddle05_R = "WingMiddle05_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string WingPinky01_L = "WingPinky01_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string WingPinky01_Left = "WingPinky01_Left"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string WingPinky01_R = "WingPinky01_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string WingPinky01_Right = "WingPinky01_Right"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string WingPinky02_L = "WingPinky02_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string WingPinky02_Left = "WingPinky02_Left"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string WingPinky02_R = "WingPinky02_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string WingPinky02_Right = "WingPinky02_Right"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string WingPinky03_Left = "WingPinky03_Left"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string WingPinky03_Right = "WingPinky03_Right"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonSoulEater\Blue.prefab".
        public const string WingPinky04_L = "WingPinky04_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string WingPinky04_R = "WingPinky04_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string WingPinky05_L = "WingPinky05_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string WingPinky05_R = "WingPinky05_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string WingRing01_L = "WingRing01_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string WingRing01_R = "WingRing01_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string WingRing02_L = "WingRing02_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string WingRing02_R = "WingRing02_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string WingRing04_L = "WingRing04_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string WingRing04_R = "WingRing04_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string WingRing05_L = "WingRing05_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string WingRing05_R = "WingRing05_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string WingThumb01_L = "WingThumb01_L"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string WingThumb01_R = "WingThumb01_R"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string Wing_end = "Wing_end"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string WoodBalcony = "WoodBalcony"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string WoodBarrel = "WoodBarrel"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string WoodBox = "WoodBox"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "WoodBox (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "WoodBox (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "WoodBox (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string WoodTable = "WoodTable"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\LeftHallway.unity".
        public const string Work = "Work"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\MainMenu.unity".
        public const string YellowMiniBoss = "YellowMiniBoss"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\KhrenBossRoom.prefab".
        public const string ZLPC_Chain_A_1 = "ZLPC_Chain_A_1"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string ZLPC_Chain_A_2 = "ZLPC_Chain_A_2"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Decorations\Chains\ZLPC_Chain_A_2.prefab".
        public const string ZLPC_Chain_A_3 = "ZLPC_Chain_A_3"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Decorations\Chains\ZLPC_Chain_A_3.prefab".
        public const string ZLPC_Chain_A_4 = "ZLPC_Chain_A_4"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Decorations\Chains\ZLPC_Chain_A_4.prefab".
        public const string ZLPC_Chain_A_5 = "ZLPC_Chain_A_5"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string ZLPC_Chain_A_6 = "ZLPC_Chain_A_6"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Decorations\Chains\ZLPC_Chain_A_6.prefab".
        public const string ZLPC_Chain_A_7 = "ZLPC_Chain_A_7"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Decorations\Chains\ZLPC_Chain_A_7.prefab".
        public const string ZLPC_Dragon_A_1 = "ZLPC_Dragon_A_1"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string ZLPC_Prop_A_06 = "ZLPC_Prop_A_06"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string ZLPC_Prop_B_03 = "ZLPC_Prop_B_03"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string ZLPC_Prop_B_11 = "ZLPC_Prop_B_11"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string ZLPC_Rock_E_2 = "ZLPC_Rock_E_2"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string ZLPC_Rock_E_3 = "ZLPC_Rock_E_3"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string ZLPC_Rock_E_7 = "ZLPC_Rock_E_7"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string ZLPC_Stairs_A_1 = "ZLPC_Stairs_A_1"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        // "ZLPC_Stairs_A_1 (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        public const string ZLPC_Torch_01 = "ZLPC_Torch_01"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Decorations\Torches\ZLPC_Torch_01.prefab".
        public const string ZLPC_Torch_01_1 = "ZLPC_Torch_01_1"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Decorations\Torches\ZLPC_Torch_01_1.prefab".
        public const string ZLPC_Torch_02 = "ZLPC_Torch_02"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Decorations\Torches\ZLPC_Torch_02.prefab".
        public const string ZLPC_Torch_03 = "ZLPC_Torch_03"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Decorations\Torches\ZLPC_Torch_03.prefab".
        public const string ZLPC_Torch_04 = "ZLPC_Torch_04"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Decorations\Torches\ZLPC_Torch_04.prefab".
        public const string ZLPC_Torch_05 = "ZLPC_Torch_05"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Decorations\Torches\ZLPC_Torch_05.prefab".
        public const string ZLPC_Torch_06 = "ZLPC_Torch_06"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Decorations\Torches\ZLPC_Torch_06.prefab".
        public const string ZLPC_Torch_07 = "ZLPC_Torch_07"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Decorations\Torches\ZLPC_Torch_07.prefab".
        public const string ZLPC_Torch_08 = "ZLPC_Torch_08"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Decorations\Torches\ZLPC_Torch_08.prefab".
        public const string ZLPC_Torch_09 = "ZLPC_Torch_09"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string ZLPC_Torch_10 = "ZLPC_Torch_10"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\LeftHallway.unity".
        public const string ZLP_Halo_A_3 = "ZLP_Halo_A_3"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Decorations\Torches\ZLPC_Torch_01_1.prefab".
        public const string ballista_01_low = "ballista_01_low"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string ballista_02_low = "ballista_02_low"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string ballista_03_low = "ballista_03_low"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string ballista_05_low = "ballista_05_low"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string ballista_06_low = "ballista_06_low"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string ballista_07_low = "ballista_07_low"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string ballista_08_low = "ballista_08_low"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string ballista_09_low = "ballista_09_low"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string ballista_10_low = "ballista_10_low"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string ballista_11_low = "ballista_11_low"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string ballista_12_low = "ballista_12_low"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string ballista_13_low = "ballista_13_low"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string ballista_14_low = "ballista_14_low"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string ballista_15_low = "ballista_15_low"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string ballista_16_low = "ballista_16_low"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string ballista_17_low = "ballista_17_low"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string ballista_18_low = "ballista_18_low"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string ballista_19_low = "ballista_19_low"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string ballista_20_low = "ballista_20_low"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string curve1 = "curve1"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Pendant.prefab".
        // "default (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\GhostMazeRoom.prefab".
        public const string dengxin = "dengxin"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string dengzhao = "dengzhao"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string diaozhui = "diaozhui"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Hub_World_Light.prefab".
        public const string door = "door"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "f_index.01.L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "f_index.01.R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "f_index.02.L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "f_index.02.L_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "f_index.02.R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "f_index.02.R_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "f_middle.01.L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "f_middle.01.R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "f_middle.02.L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "f_middle.02.L_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "f_middle.02.R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "f_middle.02.R_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "f_pinky.01.L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "f_pinky.01.R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "f_pinky.02.L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "f_pinky.02.L_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "f_pinky.02.R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "f_pinky.02.R_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "f_ring.01.L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "f_ring.01.R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "f_ring.02.L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "f_ring.02.L_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "f_ring.02.R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "f_ring.02.R_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "forearm.L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "forearm.R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "g Group1" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Decorations\small_gargoyle.prefab".
        // "g Group13006" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Decorations\small_gargoyle.prefab".
        // "g Group15653" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Decorations\small_gargoyle.prefab".
        // "g Group17" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Decorations\small_gargoyle.prefab".
        // "g Group24955" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Decorations\small_gargoyle.prefab".
        // "g Group34005" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Decorations\small_gargoyle.prefab".
        // "g Group3426" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Decorations\small_gargoyle.prefab".
        // "g Group38890" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Decorations\small_gargoyle.prefab".
        // "hand.L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "hand.R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        public const string hei = "hei"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string jinshu = "jinshu"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string joint19 = "joint19"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string joint20 = "joint20"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string joint21 = "joint21"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string joint22 = "joint22"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string joint23 = "joint23"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Visuals\Models\Creatures\FourEvilDragonsHP\Prefab\DragonUsurper\Blue.prefab".
        public const string lochhinge = "lochhinge"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        // "lock" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\BossRoom.unity".
        public const string metarig = "metarig"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        public const string mmGroup0 = "mmGroup0"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Potato.prefab".
        public const string pCylinder1 = "pCylinder1"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Pendant.prefab".
        public const string pCylinder2 = "pCylinder2"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Pendant.prefab".
        public const string pCylinder3 = "pCylinder3"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Pendant.prefab".
        public const string pPipe1 = "pPipe1"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Pendant.prefab".
        public const string pPipe2 = "pPipe2"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Pendant.prefab".
        public const string pPipe3 = "pPipe3"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Pendant.prefab".
        public const string pTorus2 = "pTorus2"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Pendant.prefab".
        public const string pTorus3 = "pTorus3"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Pendant.prefab".
        public const string pTorus4 = "pTorus4"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Pendant.prefab".
        // "palm.01.L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "palm.01.R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "palm.02.L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "palm.02.R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "palm.03.L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "palm.03.R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "palm.04.L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "palm.04.R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        public const string potato = "potato"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Tutoriel.unity".
        public const string root = "root"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Props\Chest.prefab".
        // "shoulder.L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "shoulder.R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "skyrm drgn skll low" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        public const string small_gargoyle = "small_gargoyle"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Hub_World_Light.prefab".
        // "small_gargoyle (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Hub_World_Light.prefab".
        // "small_gargoyle (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Hub_World_Light.prefab".
        // "spine.003" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "spine.004" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "spine.005" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "spine.005_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "thumb.01.L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "thumb.01.R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "thumb.02.L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "thumb.02.L_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "thumb.02.R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "thumb.02.R_end" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        public const string top = "top"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Props\Chest.prefab".
        public const string transform1 = "transform1"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Pendant.prefab".
        public const string transform2 = "transform2"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Pendant.prefab".
        public const string transform3 = "transform3"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Pendant.prefab".
        public const string transform4 = "transform4"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Pendant.prefab".
        public const string transform5 = "transform5"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Pendant.prefab".
        public const string transform6 = "transform6"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Pendant.prefab".
        public const string transform7 = "transform7"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Pendant.prefab".
        public const string transform8 = "transform8"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Pendant.prefab".
        public const string under = "under"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Objects\Props\Campfire.prefab".
        // "upper_arm.L" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        // "upper_arm.R" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Player\Player.prefab".
        public const string wall_door_square = "wall_door_square"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "wall_door_square (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "wall_door_square (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\Fish_Hallways_Decorated.prefab".
        // "wall_door_square (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Prefabs\Rooms\LionCorridor.prefab".
        // "weapon rack with spears" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "weapon rack with spears (1)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "weapon rack with spears (2)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        // "weapon rack with spears (3)" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Scenes\Hub.unity".
        
    }
}