// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDDEN
// GENERATED ON 2023-03-10 15:05

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

using UnityEngine;

namespace Harmony
{
    public static partial class InputControlSchemes
    {
        public const string Gamepad = "Gamepad"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Settings\PlayerControls.inputactions".
        public const string Joystick = "Joystick"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Settings\PlayerControls.inputactions".
        // "Keyboard&Mouse" is invalid. See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Settings\PlayerControls.inputactions".
        public const string Touch = "Touch"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Settings\PlayerControls.inputactions".
        public const string XR = "XR"; // See "C:\Users\zacka\Desktop\projet-synthese-post-alpha\Assets\Settings\PlayerControls.inputactions".
        
    }
}