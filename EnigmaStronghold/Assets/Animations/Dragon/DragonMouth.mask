%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: DragonMouth
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Armature
    m_Weight: 0
  - m_Path: Armature/Chest
    m_Weight: 0
  - m_Path: Armature/Chest/LeftWing
    m_Weight: 0
  - m_Path: Armature/Chest/LeftWing/LeftWing2
    m_Weight: 0
  - m_Path: Armature/Chest/LeftWing/LeftWing2/Bone.014
    m_Weight: 0
  - m_Path: Armature/Chest/LeftWing/LeftWing2/Bone.014/Bone.016
    m_Weight: 0
  - m_Path: Armature/Chest/LeftWing/LeftWing2/Bone.014/Bone.016/Bone.016_end
    m_Weight: 0
  - m_Path: Armature/Chest/LeftWing/LeftWing2/Bone.017
    m_Weight: 0
  - m_Path: Armature/Chest/LeftWing/LeftWing2/Bone.017/Bone.017_end
    m_Weight: 0
  - m_Path: Armature/Chest/LeftWing/LeftWing2/Bone.018
    m_Weight: 0
  - m_Path: Armature/Chest/LeftWing/LeftWing2/Bone.018/Bone.018_end
    m_Weight: 0
  - m_Path: Armature/Chest/LeftWing/Wing
    m_Weight: 0
  - m_Path: Armature/Chest/LeftWing/Wing/Wing_end
    m_Weight: 0
  - m_Path: Armature/Chest/RightWing
    m_Weight: 0
  - m_Path: Armature/Chest/RightWing/Bone.028
    m_Weight: 0
  - m_Path: Armature/Chest/RightWing/Bone.028/Bone.028_end
    m_Weight: 0
  - m_Path: Armature/Chest/RightWing/RightWing2
    m_Weight: 0
  - m_Path: Armature/Chest/RightWing/RightWing2/Bone.023
    m_Weight: 0
  - m_Path: Armature/Chest/RightWing/RightWing2/Bone.023/Bone.025
    m_Weight: 0
  - m_Path: Armature/Chest/RightWing/RightWing2/Bone.023/Bone.025/Bone.025_end
    m_Weight: 0
  - m_Path: Armature/Chest/RightWing/RightWing2/Bone.026
    m_Weight: 0
  - m_Path: Armature/Chest/RightWing/RightWing2/Bone.026/Bone.026_end
    m_Weight: 0
  - m_Path: Armature/Chest/RightWing/RightWing2/Bone.027
    m_Weight: 0
  - m_Path: Armature/Chest/RightWing/RightWing2/Bone.027/Bone.027_end
    m_Weight: 0
  - m_Path: Armature/Chest/SpineDown
    m_Weight: 0
  - m_Path: Armature/Chest/SpineDown/Abdomen
    m_Weight: 0
  - m_Path: Armature/Chest/SpineDown/Abdomen/LeftLeg
    m_Weight: 0
  - m_Path: Armature/Chest/SpineDown/Abdomen/LeftLeg/LeftLeg2
    m_Weight: 0
  - m_Path: Armature/Chest/SpineDown/Abdomen/LeftLeg/LeftLeg2/Bone.036
    m_Weight: 0
  - m_Path: Armature/Chest/SpineDown/Abdomen/LeftLeg/LeftLeg2/Bone.036/Bone.037
    m_Weight: 0
  - m_Path: Armature/Chest/SpineDown/Abdomen/LeftLeg/LeftLeg2/Bone.036/Bone.037/Bone.037_end
    m_Weight: 0
  - m_Path: Armature/Chest/SpineDown/Abdomen/RightLeg
    m_Weight: 0
  - m_Path: Armature/Chest/SpineDown/Abdomen/RightLeg/RightLeg2
    m_Weight: 0
  - m_Path: Armature/Chest/SpineDown/Abdomen/RightLeg/RightLeg2/Bone.031
    m_Weight: 0
  - m_Path: Armature/Chest/SpineDown/Abdomen/RightLeg/RightLeg2/Bone.031/Bone.032
    m_Weight: 0
  - m_Path: Armature/Chest/SpineDown/Abdomen/RightLeg/RightLeg2/Bone.031/Bone.032/Bone.032_end
    m_Weight: 0
  - m_Path: Armature/Chest/SpineDown/Abdomen/Tail
    m_Weight: 0
  - m_Path: Armature/Chest/SpineDown/Abdomen/Tail/Tail2
    m_Weight: 0
  - m_Path: Armature/Chest/SpineDown/Abdomen/Tail/Tail2/Tail3
    m_Weight: 0
  - m_Path: Armature/Chest/SpineDown/Abdomen/Tail/Tail2/Tail3/Tail4
    m_Weight: 0
  - m_Path: Armature/Chest/SpineDown/Abdomen/Tail/Tail2/Tail3/Tail4/Tail5
    m_Weight: 0
  - m_Path: Armature/Chest/SpineDown/Abdomen/Tail/Tail2/Tail3/Tail4/Tail5/Tail5_end
    m_Weight: 0
  - m_Path: Armature/Chest/SpineUp
    m_Weight: 0
  - m_Path: Armature/Chest/SpineUp/Neck1
    m_Weight: 0
  - m_Path: Armature/Chest/SpineUp/Neck1/Neck2
    m_Weight: 0
  - m_Path: Armature/Chest/SpineUp/Neck1/Neck2/Neck3
    m_Weight: 0
  - m_Path: Armature/Chest/SpineUp/Neck1/Neck2/Neck3/Head
    m_Weight: 1
  - m_Path: Armature/Chest/SpineUp/Neck1/Neck2/Neck3/Head/Jaw
    m_Weight: 1
  - m_Path: Armature/Chest/SpineUp/Neck1/Neck2/Neck3/Head/Jaw/Tongue
    m_Weight: 1
  - m_Path: Armature/Chest/SpineUp/Neck1/Neck2/Neck3/Head/Jaw/Tongue/Tongue_end
    m_Weight: 1
  - m_Path: DragonBody
    m_Weight: 0
