using Harmony;
using UnityEngine;

public class RespawnManager : MonoBehaviour
{
    public static RespawnManager Instance;
    
    private readonly Vector3 TUTORIAL_SPAWN_POINT = new Vector3(108, 14, 32);  
    private readonly Vector3 BOSS_SPAWN_POINT = new Vector3(-15.72f, 2.76f, -23.51f);
    private readonly Vector3 HUB_SPAWN_POINT = new Vector3(8.883f, 1.5f, -7.65f);

    private const int TUTORIAL_CAMERA_ROTATION = -120;
    private const int HUB_CAMERA_ROTATION = -90;
    private const int BOSS_CAMERA_ROTATION = 0;

    private GameObject player;
    private PlayerRespawn playerRespawn;
    private GameObject defaultSpawn;
    private bool initialize;

    private GameObject selectedSpawnPoint;

    private void Awake()
    {
        initialize = true;
        Events.KeyPickedUpEvent += SetToDefaultSpawnPoint;
        player = Finder.Player.gameObject;
        playerRespawn = player.GetComponent<PlayerRespawn>();

        if (Instance == null)
            Instance = this;

        else if (Instance != this)
            Destroy(gameObject);

        gameObject.DontDestroyOnLoad();
    }

    private void Start()
    {
        if (Finder.SaveManager.HasReachedBoss())
        {
            MovePlayerToBoss();
        }
        else if (!Finder.SaveManager.IsTutorialDone())
        {
            MovePLayerTutorial();
        }
        else
            MovePlayerToHub();
    }

    private void MovePLayerTutorial()
    {
        playerRespawn.Respawn(TUTORIAL_SPAWN_POINT, TUTORIAL_CAMERA_ROTATION);
    }

    public void MovePlayerToHub()
    {
        Finder.Player.GetComponent<Rigidbody>().velocity = Vector3.zero;
        playerRespawn.Respawn(HUB_SPAWN_POINT, HUB_CAMERA_ROTATION);
    }

    public void OnRespawnClick()
    {
        if(!Finder.SaveManager.IsTutorialDone())
        {
            MovePLayerTutorial();
            SceneLoadingManager.Instance.ReloadScene(Scenes.Tutoriel);
        }
        else if(Finder.SaveManager.HasReachedBoss())
        {
            MovePlayerToBoss();
            SceneLoadingManager.Instance.ReloadScene(Scenes.BossRoom);
        }
        else
        {
            playerRespawn.Respawn(selectedSpawnPoint.transform.position, HUB_CAMERA_ROTATION);
            SceneLoadingManager.Instance.ReloadScene(selectedSpawnPoint.GetComponent<RespawnTriggerController>().GetAssignedScene());
        }
        InputManager.Instance.ActivatePlayerInputs();
    }

    public void SelectSpawnPoint(GameObject spawn)
    {
        selectedSpawnPoint = spawn;
        if (!initialize) return;
        defaultSpawn = spawn;
        initialize = false;
    }

    public void MovePlayerToBoss()
    {
        playerRespawn.Respawn(BOSS_SPAWN_POINT, BOSS_CAMERA_ROTATION);
    }

    private void SetToDefaultSpawnPoint(ElementType elementType)
    {
        selectedSpawnPoint = defaultSpawn;
    }

    private void OnDestroy()
    {
        Events.KeyPickedUpEvent -= SetToDefaultSpawnPoint;
    }
}
