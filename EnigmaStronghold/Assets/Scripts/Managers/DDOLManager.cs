using System.Collections.Generic;
using UnityEngine;

public static class DDOLManager
{
    static List<GameObject> dontDestroyOnLoadObjects = new List<GameObject>();

    public static void DontDestroyOnLoad(this GameObject obj)
    {
        Object.DontDestroyOnLoad(obj);
        dontDestroyOnLoadObjects.Add(obj);
    }

    public static void DestroyAll()
    {
        foreach (var obj in dontDestroyOnLoadObjects)
        {
            if (obj != null)
                Object.Destroy(obj);
        }

        dontDestroyOnLoadObjects.Clear();
    }
}
