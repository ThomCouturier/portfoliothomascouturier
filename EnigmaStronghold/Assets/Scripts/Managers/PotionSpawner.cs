using UnityEngine;

public class PotionSpawner : MonoBehaviour
{

    [SerializeField] private PoolManager potions;

    public static PotionSpawner Instance;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;

        else if (Instance != this)
            Destroy(gameObject);

        gameObject.DontDestroyOnLoad();
    }

    public void Spawn(Vector3 position)
    {
        GameObject potion = potions.FetchFirstInactiveActor();
        potion.SetActive(true);
        potion.transform.position = position;
    }
}
