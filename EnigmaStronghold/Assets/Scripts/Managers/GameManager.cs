using Harmony;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    
    private PlayerUI playerUI;
    
    private GameObject player;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;

        else if (Instance != this)
            Destroy(gameObject);

        gameObject.DontDestroyOnLoad();
        Random.InitState(System.DateTime.Now.Millisecond); // Random seed
    }

    private void Start()
    {
        FetchPlayer();
    }

    public void Defeat()
    {
        playerUI.SetDefeatMenuState(true);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
        Time.timeScale = 0;
    }

    [UsedImplicitly]
    public void ReturnMenu()
    {
        DDOLManager.DestroyAll();
        Time.timeScale = 1;
        SceneManager.LoadScene(Scenes.MainMenu);
    }

    [UsedImplicitly]
    public void Replay()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        player.GetComponent<Player>().Reset();
        player.transform.Find(GameObjects.StaminaManager).GetComponent<StaminaManager>().Reset();
        playerUI.SetDefeatMenuState(false);
        Time.timeScale = 1;
    }
    
    public void FetchPlayer()
    {
        playerUI = GameObject.Find(GameObjects.PlayerUI).GetComponent<PlayerUI>();
        player = Finder.Player.gameObject;
    }

    public GameObject GetPlayer()
    {
        return player;
    }
}
