using UnityEngine;

public class InputManager : MonoBehaviour
{
    public static InputManager Instance;
    public InputActions Inputs;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;

        else if (Instance != this)
            Destroy(gameObject);

        gameObject.DontDestroyOnLoad();
        Inputs = new InputActions();
    }

    private void Start()
    {
        ActivatePlayerInputs();
    }

    private void DeactivateInputs()
    {
        Inputs.Player.Disable();
        Inputs.BoxPull.Disable();
        Inputs.UI.Disable();
        Inputs.PlayerChess.Disable();
    }

    public void ActivatePlayerInputs()
    {
        DeactivateInputs();
        Inputs.Player.Enable();
    }

    public void ActivateBoxPullInputs()
    {
        DeactivateInputs();
        Inputs.BoxPull.Enable();
    }

    public void ActivateUIInputs()
    {
        DeactivateInputs();
        Inputs.UI.Enable();
    }

    public void ActivatePlayerChessInputs()
    {
        DeactivateInputs();
        Inputs.PlayerChess.Enable();
    }
}
