using System.Collections;
using UnityEngine;

public class DragonBallSpawner : MonoBehaviour
{
    [SerializeField] private PoolManager obsidianPool;
    [SerializeField] private PoolManager firePool;
    [SerializeField] private PoolManager icePool;
    [SerializeField] private PoolManager poisonPool;
    [SerializeField] private PoolManager airPool;
    public static DragonBallSpawner instance;

    private void Awake()
    {
        if (instance == null)
            instance = this;

        else if (instance != this)
            Destroy(gameObject);

        gameObject.DontDestroyOnLoad();
    }

    private void Start()
    {
        Events.BossFightEndEvent += UnloadDragonBalls;
    }

    public void Spawn(GameObject sender, GameObject target, ElementType type, float angle, float summonTime = 0, float delay = 0)
    {

        GameObject ball;
        switch(type)
        {
            case ElementType.Fire:
                ball = firePool.FetchFirstInactiveActor();
                break;
            case ElementType.Ice:
                ball = icePool.FetchFirstInactiveActor();
                break;
            case ElementType.Poison:
                ball = poisonPool.FetchFirstInactiveActor();
                break;
            case ElementType.Air:
                ball = airPool.FetchFirstInactiveActor();
                break;
            default:
                ball = obsidianPool.FetchFirstInactiveActor();
                break;
        }
        ball.transform.position = sender.transform.position + sender.transform.forward * (DragonAttack.DRAGON_MOUTH_OFFSET);
        StartCoroutine(DelaySpawn(ball.GetComponent<DragonBall>(), sender, target, angle, summonTime, delay));
        
    }

    private IEnumerator DelaySpawn(DragonBall dragonBall, GameObject sender, GameObject target, float angle, float summonTime, float delay)
    {
        yield return new WaitForSeconds(delay);
        dragonBall.gameObject.SetActive(true);
        dragonBall.Summon(sender, target, angle, summonTime);
    }

    private void UnloadDragonBalls()
    {
        firePool.DestroyAll();
        obsidianPool.DestroyAll();
        icePool.DestroyAll();
        poisonPool.DestroyAll();
        airPool.DestroyAll();
    }

    private void OnDestroy()
    {
        Events.BossFightEndEvent -= UnloadDragonBalls;
    }
}
