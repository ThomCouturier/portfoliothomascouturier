using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour
{
    [SerializeField] int nbActors = 5;
    [SerializeField] private List<GameObject> actors = new List<GameObject>();
    [SerializeField] private GameObject prefab;

    void Start()
    {
        LoadActors();
    }


    public void DestroyAll()
    {
        foreach (GameObject actor in actors)
        {
            if(actor.activeInHierarchy)
                actor.SetActive(false);
        }
    }


    void LoadActors()
    {
        for (int i = 0; i < nbActors; i++)
        {
            GameObject newActor = Instantiate(prefab, gameObject.transform, true);
            newActor.SetActive(false);
            actors.Add(newActor);
        }
    }

    public GameObject FetchFirstInactiveActor()
    {
        foreach (GameObject actor in actors)
        {
            if (!actor.activeInHierarchy)
            {
                return actor;
            }
        }
        Debug.LogError("PoolManager is dried out!");
        actors[0].SetActive(false);
        return actors[0];
    }
}