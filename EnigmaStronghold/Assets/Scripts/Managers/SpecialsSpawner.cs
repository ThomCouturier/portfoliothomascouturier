using UnityEngine;

public class SpecialsSpawner : MonoBehaviour
{
    private const int STALAGMITES_PER_ATTACK = 7;
    private const float STALAGMITES_OFFSET = 1.7f;
    private const float ACID_DROP_HEIGHT = 20f;

    [SerializeField] private PoolManager stalagmites;
    [SerializeField] private PoolManager acidDrops;

    public static SpecialsSpawner Instance;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;

        else if (Instance != this)
            Destroy(gameObject);

        gameObject.DontDestroyOnLoad();
    }

    public void Spawn(GameObject sender, GameObject target, ElementType type)
    {
        switch (type)
        {
            case ElementType.Ice:
                SpawnStalagmites(target);
                break;
            case ElementType.Poison:
                SpawnAcidPuddle(target);
                break;
            default:
                SpawnAcidPuddle(target);
                break;
        }
    }

    private void SpawnAcidPuddle(GameObject target)
    {
        GameObject drop = acidDrops.FetchFirstInactiveActor();
        drop.SetActive(true);
        drop.transform.position = target.transform.position + (Vector3.up * ACID_DROP_HEIGHT);
    }


    private void SpawnStalagmites(GameObject target)
    {
        int center = Mathf.CeilToInt((float)STALAGMITES_PER_ATTACK / 2);
        Vector3 direction = new Vector3(Random.value, 0, Random.value).normalized;
        for (int i = -center; i <= center + Mathf.FloorToInt((float)STALAGMITES_PER_ATTACK / 2); i++)
        {
            Stalagmite stalagmite = stalagmites.FetchFirstInactiveActor().GetComponent<Stalagmite>();
            stalagmite.gameObject.SetActive(true);
            stalagmite.Summon(target.transform.position + (direction * STALAGMITES_OFFSET * i));
        }

    }
}
