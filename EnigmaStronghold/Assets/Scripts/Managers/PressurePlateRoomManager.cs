using Harmony;
using UnityEngine;

public class PressurePlateRoomManager : MonoBehaviour
{
    [SerializeField] public AudioClip khromSong;

    private const int PLATE_FOR_FIRST_DOOR = 2;
    private const int PLATE_FOR_ELEVATOR = 3;
    private const int PLATE_FOR_SECOND_DOOR = 4;
    private const int PLATE_FOR_MINIBOSS_DOOR = 5;

    private const float ELEVATOR_HEIGHT_DIFFERENCE = 6.325f;

    public static PressurePlateRoomManager instance;
    private GameObject player;
    private GameObject elevator;
    private GameObject elevatorStopper;
    private GameObject elevatorStopperUp;
    private SlidableBox elevatorBox;
    private PlateRoomSFX plateSFX;
    private int nbPressurePlatesActivated;
    private bool elevatorActive = false;
    private Vector3 initialPosition;

    private void Awake()
    {
        if (instance == null)
            instance = this;

        else if (instance != this)
            Destroy(gameObject);

        gameObject.DontDestroyOnLoad();

        plateSFX = GetComponent<PlateRoomSFX>();
    }

    private void Start()
    {
        elevator = GameObject.Find(GameObjects.Elevator);
        elevatorStopper = GameObject.Find(GameObjects.ActivableStopperSecond);
        elevatorStopperUp = GameObject.Find(GameObjects.ActivableStopperUp);
        player = GameObject.Find(Tags.Player);
        elevatorBox = GameObject.Find(GameObjects.DeathBoxGreenElevator).GetComponent<SlidableBox>();

        player.AddComponent<BoxCollider>();
        initialPosition = elevator.transform.position;
        elevatorStopper.SetActive(true);
        elevatorBox.SetSlashable(false);
        Finder.SaveManager.CheckRoomCompletionState(RoomType.PressureFinal);
    }

    private void Update()
    {
        if (elevatorActive)
        {
            RaiseElevator();
            if (elevator.transform.position.y >= initialPosition.y + ELEVATOR_HEIGHT_DIFFERENCE)
            {
                elevatorActive = false;
                plateSFX.StopElevatorRise();
                elevatorStopper.SetActive(false);
                elevatorStopperUp.SetActive(false);
                elevatorBox.SetSlashable(true);
            }
        }
    }

    public void OnPressurePlatePressed()
    {
        nbPressurePlatesActivated++;
        if (nbPressurePlatesActivated >= PLATE_FOR_MINIBOSS_DOOR)
        {
            Finder.Events.RoomCompleted(RoomType.PressureFinal);
        }
        else if (nbPressurePlatesActivated >= PLATE_FOR_SECOND_DOOR)
        {
            Finder.Events.RoomCompleted(RoomType.PressureRoomSecondDoor);
        }
        else if (nbPressurePlatesActivated >= PLATE_FOR_ELEVATOR)
        {
            plateSFX.PlayElevatorRise();
            elevatorActive = true;
        }
        else if (nbPressurePlatesActivated >= PLATE_FOR_FIRST_DOOR)
        {
            Finder.Events.RoomCompleted(RoomType.PressureRoomFirstDoor);
        }
    }

    private void RaiseElevator()
    {
        elevator.transform.Translate(Vector3.up * Time.deltaTime);
    }
}
