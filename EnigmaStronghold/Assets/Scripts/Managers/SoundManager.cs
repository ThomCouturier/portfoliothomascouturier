using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager SoundInstance;

    /*********************************************************************************************************************
    **********************************************************************************************************************
    *********************************************************  CLIPS  ****************************************************
    **********************************************************************************************************************
    *********************************************************************************************************************/

    //---------------------------------------------------------------
    //----------------------BACKGROUND MUSIC-------------------------
    //---------------------------------------------------------------
    [SerializeField] private AudioClip tutorialNatureSFX;
    [SerializeField] private AudioClip mainMenuMusic;
    [SerializeField] private AudioClip finalBossRoomMusic;
    [SerializeField] private AudioClip khrallidMusic;
    [SerializeField] private AudioClip khalledMusic;
    [SerializeField] private AudioClip khromMusic;
    [SerializeField] private AudioClip khrenMusic;
    [SerializeField] private AudioClip stoneKnightEncounterMusic;
    [SerializeField] private AudioClip skeletonEncounterMusic;

    //---------------------------------------------------------------
    //-------------------------PLAYER SFX----------------------------
    //---------------------------------------------------------------
    [SerializeField] private AudioClip[] playerHurtSFX;
    [SerializeField] private AudioClip[] playerFootstepsSFX;
    [SerializeField] private AudioClip[] playerDeathSFX;
    [SerializeField] private AudioClip[] playerAttackSFX;
    [SerializeField] private AudioClip playerGhostInteraction;
    [SerializeField] private AudioClip potionDrink;

    //---------------------------------------------------------------
    //-------------------------SWORD SFX------------------------------
    //---------------------------------------------------------------
    [SerializeField] private AudioClip[] impactSFX;
    [SerializeField] private AudioClip[] swingSFX;

    //---------------------------------------------------------------
    //------------------------KHAN'BAN SFX---------------------------
    //---------------------------------------------------------------
    [SerializeField] private AudioClip khanbanSpeech;
    [SerializeField] private AudioClip khanbanDeathSFX;
    [SerializeField] private AudioClip[] khanbanHurtSFX;
    [SerializeField] private AudioClip khanbanMidFightSpeech;
    [SerializeField] private AudioClip[] khanbanPlayerHit;

    //---------------------------------------------------------------
    //------------------------KHRALLID SFX---------------------------
    //---------------------------------------------------------------
    [SerializeField] private AudioClip khrallidSpeech;
    [SerializeField] private AudioClip khrallidDeathSFX;

    //---------------------------------------------------------------
    //------------------------KHROM SFX------------------------------
    //---------------------------------------------------------------
    [SerializeField] private AudioClip khromSpeech;
    [SerializeField] private AudioClip khromDeathSFX;

    //---------------------------------------------------------------
    //------------------------KHALLED SFX----------------------------
    //---------------------------------------------------------------
    [SerializeField] private AudioClip khalledSpeech;
    [SerializeField] private AudioClip khalledDeathSFX;

    //---------------------------------------------------------------
    //------------------------KHREN SFX------------------------------
    //---------------------------------------------------------------
    [SerializeField] private AudioClip khrenSpeech;
    [SerializeField] private AudioClip khrenDeathSFX;

    //---------------------------------------------------------------
    //------------------------STONEKNIGHT SFX------------------------
    //---------------------------------------------------------------
    [SerializeField] private AudioClip knightSpeech;
    [SerializeField] private AudioClip knightDeathSFX;
    [SerializeField] private AudioClip[] knightAttackSFX;
    [SerializeField] private AudioClip[] knightWalkSFX;
    [SerializeField] private AudioClip[] knightWhirlwindSFX;

    //---------------------------------------------------------------
    //------------------------SKELETON SFX---------------------------
    //---------------------------------------------------------------
    [SerializeField] private AudioClip[] skeletonDeathSFX;
    [SerializeField] private AudioClip[] skeletonAttackSFX;
    [SerializeField] private AudioClip[] skeletonWalkSFX;

    //---------------------------------------------------------------
    //------------------------CHESSROOM SFX--------------------------
    //---------------------------------------------------------------
    [SerializeField] private AudioClip pieceSelectedSFX;
    [SerializeField] private AudioClip coordinateSelectedSFX;
    [SerializeField] private AudioClip puzzleFailedSFX;

    //---------------------------------------------------------------
    //------------------------SKULLROOM SFX--------------------------
    //---------------------------------------------------------------
    [SerializeField] private AudioClip rightSkullPlacementSFX;
    [SerializeField] private AudioClip wrongSkullPlacementSFX;
    [SerializeField] private AudioClip skullRespawnSFX;

    //---------------------------------------------------------------
    //------------------------GHOSTROOM SFX--------------------------
    //---------------------------------------------------------------
    [SerializeField] private AudioClip ghostSpeech;
    [SerializeField] private AudioClip[] ghostHurt;

    //---------------------------------------------------------------
    //------------------------PLATEROOM SFX--------------------------
    //---------------------------------------------------------------
    [SerializeField] private AudioClip boxPlacedSFX;
    [SerializeField] private AudioClip boxSlidingSFX;
    [SerializeField] private AudioClip lavaSFX;
    [SerializeField] private AudioClip elevatorRiseSFX;
    [SerializeField] private AudioClip elevatorArrivedSFX;

    //---------------------------------------------------------------
    //------------------------TUTORIAL SFX-----------------------------
    //---------------------------------------------------------------
    [SerializeField] private AudioClip paternalBrossardSpeech;
    [SerializeField] private AudioClip potatoLauncherSFX;

    //---------------------------------------------------------------
    //------------------------GLOBAL SFX-----------------------------
    //---------------------------------------------------------------
    [SerializeField] private AudioClip obtainedKeySFX;
    [SerializeField] private AudioClip metalDoorOpenSFX;
    [SerializeField] private AudioClip metalDoorCloseSFX;
    [SerializeField] private AudioClip fireCrepitSFX;
    [SerializeField] private AudioClip itemPickedUpSFX;
    [SerializeField] private AudioClip leverActivatedSFX;
    [SerializeField] private AudioClip puzzleSolvedSFX;
    [SerializeField] private AudioClip achievementObtainedSFX;
    [SerializeField] private AudioClip deathBellSFX;
    [SerializeField] private AudioClip woodBreakSFX;

    //---------------------------------------------------------------
    //------------------------GLOBAL BOSSES--------------------------
    //---------------------------------------------------------------
    [SerializeField] private AudioClip[] dragonWalkSFX;
    [SerializeField] private AudioClip[] dragonWingFlapSFX;
    [SerializeField] private AudioClip preparingAttackSFX;

    //---------------------------------------------------------------
    //--------------------------PROJECTILE---------------------------
    //---------------------------------------------------------------
    [SerializeField] private AudioClip airAttackSFX;
    [SerializeField] private AudioClip fireAttackSFX;
    [SerializeField] private AudioClip iceAttackSFX;
    [SerializeField] private AudioClip poisonAttackSFX;
    [SerializeField] private AudioClip obsidianAttackSFX;
    [SerializeField] private AudioClip attackTravellingSFX;
    [SerializeField] private AudioClip projectileDeflectedSFX;

    //---------------------------------------------------------------
    //------------------------SPEC ATT SFX---------------------------
    //---------------------------------------------------------------
    [SerializeField] private AudioClip airSpecialAttackSFX;
    [SerializeField] private AudioClip fireSpecialAttackSFX;
    [SerializeField] private AudioClip[] iceSpecialAttackSFX;
    [SerializeField] private AudioClip iceAttackSpawnSFX;
    [SerializeField] private AudioClip poisonSpecialAttackSFX;

    //---------------------------------------------------------------
    //----------------------------UI SFX-----------------------------
    //---------------------------------------------------------------
    [SerializeField] private AudioClip hoverSFX;
    [SerializeField] private AudioClip clickSFX;


    /********************************************************************************************************************
    *********************************************************************************************************************
    ********************************************************  GETTERS  **************************************************
    *********************************************************************************************************************
    ********************************************************************************************************************/

    //---------------------------------------------------------------
    //------------------BACKGROUND MUSIC GETTERS---------------------
    //---------------------------------------------------------------
    public AudioClip TutorialNatureSFX { get { return tutorialNatureSFX; } }
    public AudioClip MainMenuMusic { get { return mainMenuMusic; } }
    public AudioClip FinalBossRoomMusic { get { return finalBossRoomMusic; } }
    public AudioClip KhrallidMusic { get { return khrallidMusic; } }
    public AudioClip KhalledMusic { get { return khalledMusic; } }
    public AudioClip KhromMusic { get { return khromMusic; } }
    public AudioClip KhrenMusic { get { return khrenMusic; } }
    public AudioClip StoneKnightEncounterMusic { get { return stoneKnightEncounterMusic; } }
    public AudioClip SkeletonEncounterMusic { get { return skeletonEncounterMusic; } }

    //---------------------------------------------------------------
    //------------------------PLAYER GETTERS----------------------
    //---------------------------------------------------------------
    public AudioClip PlayerHurtSFX { get { return playerHurtSFX[Random.Range(0, playerHurtSFX.Length)]; } } //ENUM
    public AudioClip PlayerFootstepsSFX { get { return playerFootstepsSFX[Random.Range(0, playerFootstepsSFX.Length)]; } } //ENUM
    public AudioClip PlayerDeathSFX { get { return playerDeathSFX[Random.Range(0, playerDeathSFX.Length)]; } }
    public AudioClip PlayerAttackSFX { get { return playerAttackSFX[Random.Range(0, playerAttackSFX.Length)]; } } //ENUM
    public AudioClip PlayerGhostInteraction { get { return playerGhostInteraction; } }
    public AudioClip PotionDrink { get { return potionDrink; } }

    //---------------------------------------------------------------
    //--------------------------SWORD GETTERS------------------------
    //---------------------------------------------------------------
    public AudioClip ImpactSFX { get { return impactSFX[Random.Range(0, impactSFX.Length)]; } } //ENUM
    public AudioClip SwingSFX { get { return swingSFX[Random.Range(0, swingSFX.Length)]; } } //ENUM

    //---------------------------------------------------------------
    //----------------------KHAN'BAN GETTERS-------------------------
    //---------------------------------------------------------------
    public AudioClip KhanbanSpeech { get { return khanbanSpeech; } }
    public AudioClip KhanbanDeathSFX { get { return khanbanDeathSFX; } }
    public AudioClip KhanbanMidFightSpeech { get { return khanbanMidFightSpeech; } }
    public AudioClip KhanbanHurtSFX { get { return khanbanHurtSFX[Random.Range(0, khanbanHurtSFX.Length)]; } }
    public AudioClip KhanbanPlayerHit { get { return khanbanHurtSFX[Random.Range(0, khanbanPlayerHit.Length)]; } }


    //---------------------------------------------------------------
    //-----------------------KHRALLID GETTERS------------------------
    //---------------------------------------------------------------
    public AudioClip KhrallidSpeech { get { return khrallidSpeech; } }
    public AudioClip KhrallidDeathSFX { get { return khrallidDeathSFX; } }

    //---------------------------------------------------------------
    //-----------------------KHALLED GETTERS-------------------------
    //---------------------------------------------------------------
    public AudioClip KhalledSpeech { get { return khalledSpeech; } }
    public AudioClip KhalledDeathSFX { get { return khalledDeathSFX; } }


    //---------------------------------------------------------------
    //------------------------KHROM GETTERS--------------------------
    //---------------------------------------------------------------
    public AudioClip KhromSpeech { get { return khromSpeech; } }
    public AudioClip KhromDeathSFX { get { return khromDeathSFX; } }


    //---------------------------------------------------------------
    //-----------------------KHREN GETTERS---------------------------
    //---------------------------------------------------------------
    public AudioClip KhrenSpeech { get { return khrenSpeech; } }
    public AudioClip KhrenDeathSFX { get { return khrenDeathSFX; } }


    //---------------------------------------------------------------
    //---------------------STONEKNIGHT GETTERS-----------------------
    //---------------------------------------------------------------
    public AudioClip KnightSpeech { get { return knightSpeech; } }
    public AudioClip KnightDeathSFX { get { return knightDeathSFX; } }
    public AudioClip KnightAttackSFX { get { return knightAttackSFX[Random.Range(0, knightAttackSFX.Length)]; } } //ENUM
    public AudioClip KnightWalkSFX { get { return knightWalkSFX[Random.Range(0, knightWalkSFX.Length)]; } } //ENUM
    public AudioClip KnightWhirlwindSFX { get { return knightWhirlwindSFX[Random.Range(0, knightWhirlwindSFX.Length)]; } }


    //---------------------------------------------------------------
    //----------------------SKELETON GETTERS-------------------------
    //---------------------------------------------------------------
    public AudioClip SkeletonDeathSFX { get { return skeletonDeathSFX[Random.Range(0, skeletonDeathSFX.Length)]; } }
    public AudioClip SkeletonAttackSFX { get { return skeletonAttackSFX[Random.Range(0, skeletonAttackSFX.Length)]; } } //ENUM
    public AudioClip SkeletonWalkSFX { get { return skeletonWalkSFX[Random.Range(0, skeletonWalkSFX.Length)]; } } //ENUM


    //---------------------------------------------------------------
    //----------------------CHESSROOM GETTERS------------------------
    //---------------------------------------------------------------
    public AudioClip PieceSelectedSFX { get { return pieceSelectedSFX; } }
    public AudioClip CoordinateSelectedSFX { get { return coordinateSelectedSFX; } }
    public AudioClip PuzzleFailedSFX { get { return puzzleFailedSFX; } }


    //---------------------------------------------------------------
    //----------------------SKULLROOM GETTERS------------------------
    //---------------------------------------------------------------
    public AudioClip RightSkullPlacementSFX { get { return rightSkullPlacementSFX; } }
    public AudioClip WrongSkullPlacementSFX { get { return wrongSkullPlacementSFX; } }
    public AudioClip SkullRespawnSFX { get { return skullRespawnSFX; } }

    //---------------------------------------------------------------
    //----------------------GHOSTROOM GETTERS------------------------
    //---------------------------------------------------------------
    public AudioClip GhostSpeech { get { return ghostSpeech; } }
    public AudioClip GhostHurtSFX { get { return ghostHurt[Random.Range(0, ghostHurt.Length)]; } } //ENUM


    //---------------------------------------------------------------
    //----------------------PLATEROOM GETTERS------------------------
    //---------------------------------------------------------------
    public AudioClip BoxPlacedSFX { get { return boxPlacedSFX; } }
    public AudioClip BoxSlidingSFX { get { return boxSlidingSFX; } }
    public AudioClip LavaSFX { get { return lavaSFX; } }
    public AudioClip ElevatorRiseSFX { get { return elevatorRiseSFX; } }
    public AudioClip ElevatorArrivedSFX { get { return elevatorArrivedSFX; } }

    //---------------------------------------------------------------
    //----------------------MAIN MENU GETTERS------------------------
    //---------------------------------------------------------------
    public AudioClip FireCrepitSFX { get { return fireCrepitSFX; } }

    //---------------------------------------------------------------
    //----------------------TUTORIAL GETTERS-------------------------
    //---------------------------------------------------------------
    public AudioClip PaternalBrossardSpeech { get { return paternalBrossardSpeech; } }
    public AudioClip PotatoLauncherSFX { get { return potatoLauncherSFX; } }

    //---------------------------------------------------------------
    //-----------------------GLOBAL GETTERS--------------------------
    //---------------------------------------------------------------
    public AudioClip ObtainedKeySFX { get { return obtainedKeySFX; } }
    public AudioClip MetalDoorOpenSFX { get { return metalDoorOpenSFX; } }
    public AudioClip MetalDoorCloseSFX { get { return metalDoorCloseSFX; } }
    public AudioClip ItemPickedUpSFX { get { return itemPickedUpSFX; } }
    public AudioClip LeverActivatedSFX { get { return leverActivatedSFX; } }
    public AudioClip PuzzleSolvedSFX { get { return puzzleSolvedSFX; } }
    public AudioClip AchievementObtained { get { return achievementObtainedSFX; } }
    public AudioClip DeathBell { get { return deathBellSFX; } }
    public AudioClip WoodBreakSFX { get { return woodBreakSFX; } }

    //---------------------------------------------------------------
    //--------------------GLOBAL BOSSES GETTERS----------------------
    //---------------------------------------------------------------
    public AudioClip DragonWalkSFX { get { return dragonWalkSFX[Random.Range(0, dragonWalkSFX.Length)]; } }
    public AudioClip DragonWingFlapSFX { get { return dragonWingFlapSFX[Random.Range(0, dragonWingFlapSFX.Length)]; } }
    public AudioClip PreparingAttackSFX { get { return preparingAttackSFX; } }

    //---------------------------------------------------------------
    //----------------------PROJECTILE GETTERS-----------------------
    //---------------------------------------------------------------
    public AudioClip AirAttackSFX { get { return airAttackSFX; } }
    public AudioClip FireAttackSFX { get { return fireAttackSFX; } }
    public AudioClip IceAttackSFX { get { return iceAttackSFX; } }
    public AudioClip PoisonAttackSFX { get { return poisonAttackSFX; } }
    public AudioClip ObsidianAttackSFX { get { return obsidianAttackSFX; } }
    public AudioClip AttackTravellingSFX { get { return attackTravellingSFX; } }
    public AudioClip ProjectileDeflectedSFX { get { return projectileDeflectedSFX; } }

    //---------------------------------------------------------------
    //------------------------SPEC ATT SFX---------------------------
    //---------------------------------------------------------------
    public AudioClip AirSpecialAttackSFX { get { return airSpecialAttackSFX; } }
    public AudioClip FireSpecialAttackSFX { get { return fireSpecialAttackSFX; } }
    public AudioClip IceSpecialAttackSFX { get { return iceSpecialAttackSFX[Random.Range(0, iceSpecialAttackSFX.Length)]; } }
    public AudioClip IceAttackSpawnSFX { get { return iceAttackSpawnSFX;; } }
    public AudioClip PoisonSpecialAttackSFX { get { return poisonSpecialAttackSFX; } }

    //---------------------------------------------------------------
    //----------------------------UI SFX-----------------------------
    //---------------------------------------------------------------
    public AudioClip HoverSFX { get { return hoverSFX; } }
    public AudioClip ClickSFX { get { return clickSFX; } }


    private void Awake()
    {
        if (SoundInstance == null)
        {
            SoundInstance = this;
        }
        else if (SoundInstance != this)
            Destroy(gameObject);
    }
}