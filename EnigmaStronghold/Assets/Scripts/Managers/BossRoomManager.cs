using UnityEngine;

public class BossRoomManager : MonoBehaviour
{
    [SerializeField] public AudioClip finalBossSong;

    private void Awake()
    {
        gameObject.DontDestroyOnLoad();
    }

    private void Start()
    {
        GameManager.Instance.FetchPlayer();
        GlobalSFX.Instance.SetGlobalAudioSource(finalBossSong);
        GlobalSFX.Instance.PlayGlobalAudioSource();
    }
}
