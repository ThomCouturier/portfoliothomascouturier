using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] private float mouseSensitivity = 0.5f;

    private GameObject playerBody;

    void Start()
    {
        playerBody = transform.parent.gameObject;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void LateUpdate()
    {
        RotateCamera();
    }

    private void RotateCamera()
    {
        Vector2 inputValues = InputManager.Instance.Inputs.Player.Look.ReadValue<Vector2>();
        Vector2 rotation = new Vector2(inputValues.y, inputValues.x) * mouseSensitivity;

        Vector3 currentRotation = transform.rotation.eulerAngles;
        currentRotation.x -= rotation.x;

        transform.localRotation = Quaternion.Euler(new Vector3(currentRotation.x, 0, 0));
        playerBody.transform.eulerAngles += new Vector3(0, rotation.y, 0);
    }

    public void SetCameraPosition(float yCameraPosition)
    {
        playerBody.transform.eulerAngles = new Vector3(0, yCameraPosition, 0);
    }
}
