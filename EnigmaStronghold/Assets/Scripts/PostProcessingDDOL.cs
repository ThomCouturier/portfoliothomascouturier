using Harmony;
using UnityEngine;
using UnityEngine.UI;

public class PostProcessingDDOL : MonoBehaviour
{
    [SerializeField] private GameObject postProcessingCheckbox;
    void Start()
    {
        gameObject.DontDestroyOnLoad();
        UserSettingsManager userSettings = GameObject.Find(GameObjects.UserSettings).GetComponent<UserSettingsManager>();
        postProcessingCheckbox.GetComponent<Toggle>().isOn = userSettings.IsPostProcessing();
    }
}
