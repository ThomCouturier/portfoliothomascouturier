using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private List<Vector3> cameraPositions;
    [SerializeField] private List<Vector3> cameraAngles;
    [SerializeField] private float duration = 3f;

    public void TransitionCamera(int index)
    {
        StartCoroutine(StartTransition(index));
    }

    public void ReturnCameraToInitialPosition()
    {
        StartCoroutine(TransitionToInitialPosition());
    }

    public float GetDuration()
    {
        return duration;
    }

    private IEnumerator StartTransition(int index)
    {
        gameObject.transform.DORotate(cameraAngles[index], duration);
        yield return new DOTweenCYInstruction.WaitForCompletion(gameObject.transform.DOMove(cameraPositions[index], duration));
    }

    private IEnumerator TransitionToInitialPosition()
    {
        gameObject.transform.DORotate(cameraAngles[(int)MainMenuCameraAngles.InitialPosition], duration);
        yield return new DOTweenCYInstruction.WaitForCompletion(gameObject.transform.DOMove(cameraPositions[0], duration));
    }
}
