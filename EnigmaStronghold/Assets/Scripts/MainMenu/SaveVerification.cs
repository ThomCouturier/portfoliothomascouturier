using Harmony;
using TMPro;
using UnityEngine;

public class SaveVerification : MonoBehaviour
{
    private const string NEW_GAME_TEXT = "New Game";

    [SerializeField] private TextMeshProUGUI text;
    [SerializeField] private SaveFilesEnum fileAssociated;

    private GameObject deleteButton;
    private GameObject achievementButton;
    private UIManager uiManager;

    private string AssociatedFile
    {
        get => fileAssociated.ToString().ToLower() + SaveManager.FILE_EXTENSION;
    }

    private void Start()
    {
        deleteButton = transform.parent.parent.Find(GameObjects.DeleteButton).gameObject;
        achievementButton = transform.parent.parent.Find(GameObjects.AchievementButton).gameObject;
        uiManager = GameObject.Find(GameObjects.UIManager).GetComponent<UIManager>();
        if (FileSaver.FileExist(AssociatedFile))
        {
            text.text = fileAssociated.ToString();
            deleteButton.SetActive(true);
            achievementButton.SetActive(true);
        }
        else
        {
            deleteButton.SetActive(false);
            achievementButton.SetActive(false);
            text.text = NEW_GAME_TEXT;
        }
    }

    public void OnDeleteButtonPressed()
    {
        text.text = NEW_GAME_TEXT;
        ToggleButtons();
        FileSaver.DeleteFile(AssociatedFile);
    }

    public void OnAchievementButtonPressed()
    {
        SaveObject save = FileSaver.ReadFile<SaveObject>(AssociatedFile);
        uiManager.OnAchievementButtonPressed(save);
    }

    private void ToggleButtons()
    {
        deleteButton.SetActive(!deleteButton.activeSelf);
        achievementButton.SetActive(!achievementButton.activeSelf);
    }
}
