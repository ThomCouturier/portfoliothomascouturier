using System.Collections;
using Harmony;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    [SerializeField] private GameObject mainMenu;
    [SerializeField] private GameObject playMenu;
    [SerializeField] private GameObject optionsMenu;
    [SerializeField] private GameObject achievementMenu;
    [SerializeField] private GameObject creditsMenu;

    private CameraController cameraController;
    private AchievementMenu achievementMenuController;

    private bool transitionStarted = false;
    private bool isOnInitial = true;

    private InputActions inputs;

    private void Start()
    {
        cameraController = GameObject.Find(GameObjects.MainCamera).GetComponent<CameraController>();
        achievementMenuController = achievementMenu.GetComponent<AchievementMenu>();

        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        inputs = new InputActions();
        inputs.Enable();
    }

    private void Update()
    {
        if (!inputs.UI.Back.WasReleasedThisFrame() || isOnInitial || transitionStarted) return;

        if (achievementMenu.activeSelf)
        {
            OnPlayButtonClicked();
        }
        else if (creditsMenu.activeSelf)
        {
            QuitCredits();
        }
        else
        {
            OnBackButtonClicked();
        }
    }

    public void OnPlayButtonClicked()
    {
        if (transitionStarted) return;
        
        transitionStarted = true;
        StartCoroutine(PlayTransition());
    }
    
    public void OnOptionsButtonClicked()
    {
        if (transitionStarted) return;
        
        transitionStarted = true;
        StartCoroutine(OptionsTransition());
    }

    public void OnExitButtonClicked()
    {
        transitionStarted = true;
        Application.Quit();
    }

    public void OnCreditsButtonClicked()
    {
        creditsMenu.SetActive(true);
        mainMenu.SetActive(false);
        isOnInitial = false;
    }

    public void QuitCredits()
    {
        creditsMenu.SetActive(false);
        mainMenu.SetActive(true);
        isOnInitial = true;
    }

    public void OnBackButtonClicked()
    {
        if (achievementMenu.activeSelf)
        {
            OnPlayButtonClicked();
        }
        else
        {
            transitionStarted = true;
            StartCoroutine(BackTransition());
        }
    }
    
    public void OnAchievementButtonPressed(SaveObject save)
    {
        IEnumerator AchievementTransition()
        {
            playMenu.SetActive(false);
            cameraController.TransitionCamera((int)MainMenuCameraAngles.Achievement);
            transitionStarted = true;
            yield return new WaitForSeconds(cameraController.GetDuration());
            achievementMenu.SetActive(true);
            achievementMenuController.SetAchievementState(save);
            transitionStarted = false;
        }
        StartCoroutine(AchievementTransition());
    }

    private IEnumerator PlayTransition()
    {
        achievementMenu.SetActive(false);
        mainMenu.SetActive(false);
        cameraController.TransitionCamera((int)MainMenuCameraAngles.Play);
        yield return new WaitForSeconds(cameraController.GetDuration());
        playMenu.SetActive(true);
        transitionStarted = false;
        isOnInitial = false;
    }
    
    private IEnumerator OptionsTransition()
    {
        mainMenu.SetActive(false);
        cameraController.TransitionCamera((int)MainMenuCameraAngles.Options);
        yield return new WaitForSeconds(cameraController.GetDuration());
        optionsMenu.SetActive(true);
        transitionStarted = false;
        isOnInitial = false;
    }

    private IEnumerator BackTransition()
    {
        playMenu.SetActive(false);
        optionsMenu.SetActive(false);
        cameraController.ReturnCameraToInitialPosition();
        yield return new WaitForSeconds(cameraController.GetDuration());
        mainMenu.SetActive(true);
        transitionStarted = false;
        isOnInitial = true;
    }

    [UsedImplicitly]
    public void StartGame(string saveName)
    {
        SaveManager.FileNameComplete = saveName;
        SceneManager.LoadScene(Scenes.Main);
    }
}
