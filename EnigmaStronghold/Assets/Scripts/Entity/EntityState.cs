using UnityEngine;

public abstract class EntityState<T> : MonoBehaviour
    where T : struct
{
    protected EntityAI<T> entityAI;

    void Start()
    {
        entityAI = GetComponent<EntityAI<T>>();
    }

    public void SetEntityAI(EntityAI<T> entityAI)
    {
        this.entityAI = entityAI;
    }
}
