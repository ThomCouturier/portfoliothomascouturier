using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class EntityAI<T> : LivingEntity
     where T : struct
{
    private const int HURT_BLINKS = 7;
    private const float HURT_TIME = 0.5f;

    [SerializeField] protected EntityState<T> state;
    [SerializeField] protected Color baseColor = Color.white;

    protected Dictionary<T, EntityState<T>> states;
    protected Collider stimuli;

    public EntityAI()
    {
        // Code pris de https://stackoverflow.com/questions/79126/create-generic-method-constraining-t-to-an-enum
        // On prend ce code pour s'assurer qu'une programmeur ne cree pas une nouvelle entite sans un Enum de ses etats.
        if (!typeof(T).IsEnum) throw new ArgumentException("T must be an enum");
    }

    protected void SetupStates()
    {
        foreach (var state in states)
        {
            state.Value.SetEntityAI(this);
            state.Value.enabled = false;
        }

        state = states.First().Value;
        state.enabled = true;
    }


    public void ChangeState(T newState)
    {
        state.enabled = false;
        state = states[newState];
        state.enabled = true;
    }

    protected bool IsState(EntityState<T> state)
    {
        return (this.state.Equals(state));
    }

    protected virtual IEnumerator Hurt(Renderer renderer, int blinks = HURT_BLINKS, float hurtTime = HURT_TIME)
    {
        if (stimuli != null) stimuli.enabled = false;
        for (int i = 0; i < blinks; i++)
        {
            yield return new WaitForSeconds(hurtTime / blinks);
            if (i % 2 == 0)
            {
                renderer.material.color = Color.red;
            }
            else
            {
                renderer.material.color = baseColor;
            }
        }
        renderer.material.color = baseColor;
        if (stimuli != null) stimuli.enabled = true;
    }
}
