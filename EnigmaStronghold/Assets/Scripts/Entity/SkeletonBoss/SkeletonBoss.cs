using System.Collections.Generic;

public class SkeletonBoss : Skeleton, RoomCompleted
{
    private void Awake()
    {
        Events.RoomCompletedEvent += OnRoomCompleted;
    }

    private new void Start()
    {
        base.Start();
        
        states = new Dictionary<SkeletonState, EntityState<SkeletonState>>()
        {
            [SkeletonState.Idle] = GetComponent<SkeletonIdle>(),
            [SkeletonState.Walk] = GetComponent<SkeletonWalk>(),
            [SkeletonState.Attack] = GetComponent<SkeletonAttack>(),
            [SkeletonState.Death] = GetComponent<SkeletonDeath>()
        };
        SetupStates();
    }

    public void OnRoomCompleted(RoomType roomType)
    {
        if(roomType.Equals(RoomType.Skull))
            gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        Events.RoomCompletedEvent -= OnRoomCompleted;
    }
}

