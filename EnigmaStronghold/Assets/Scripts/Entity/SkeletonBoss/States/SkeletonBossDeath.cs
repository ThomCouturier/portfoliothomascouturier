using Harmony;
using UnityEngine;

public class SkeletonBossDeath : SkeletonDeath
{
    [SerializeField] private ElementType skullType;

    private new void OnEnable()
    {
        base.OnEnable();
        Finder.SkullPuzzleManager.SkeletonKillTracker();
    }

    private void Update()
    {
        gameObject.transform.Find(GameObjects.SkeletonStimuli).GetComponent<Collider>().enabled = false;
        deathTime = Mathf.Max(0, deathTime -= Time.deltaTime);
        if (deathTime == 0)
        {
            entityAI.gameObject.SetActive(false);
            SkullSpawner.Instance.Spawn(skullType, transform.position);
        }
    }
}
