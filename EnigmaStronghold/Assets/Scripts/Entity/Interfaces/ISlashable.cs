using UnityEngine;

public interface ISlashable
{
    public abstract void Slash(int damage, GameObject sender);
}
