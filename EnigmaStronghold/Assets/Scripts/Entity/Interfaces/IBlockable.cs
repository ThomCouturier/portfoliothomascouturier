using UnityEngine;

public interface IBlockable
{
    public abstract void Block(GameObject sender);
}
