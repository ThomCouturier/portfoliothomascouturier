using UnityEngine;

public abstract class LivingEntity : MonoBehaviour
{
    [SerializeField] private int maxHealth = 20;
    [SerializeField] private float speed;
    [SerializeField] private float invulnerableMaxTime = 0.3f;
    protected float invulnerableTime;
    public int Health { get; set; }

    protected void Start()
    {
        Health = maxHealth;
    }
    public int GetMaxHealth()
    {
        return maxHealth;
    }

    public float GetSpeed()
    {
        return speed;
    }

    public virtual void Hit(int damage)
    {
        if(invulnerableTime == 0)
        {
            invulnerableTime = invulnerableMaxTime;
            Health = Mathf.Max(0, Health - damage);
            if (!IsAlive())
            {
                Die();
            }
        }
    }

    public void Heal(int heal)
    {
        Health = Mathf.Min(maxHealth, Health + heal);
    }

    public bool IsAlive()
    {
        return (Health > 0);
    }

    protected void UpdateInvulnerabilityTime()
    {
        invulnerableTime = Mathf.Max(0, invulnerableTime - Time.deltaTime);
    }

    public void SetInvulnerabilityTime(float time)
    {
        invulnerableTime = time;
    }

    protected abstract void Die();

    public void Reset()
    {
        Health = maxHealth;
    }
}
