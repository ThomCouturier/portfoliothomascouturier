using System.Collections.Generic;
using UnityEngine;
using Harmony;

public class StoneKnight : EntityAI<StoneKnight.StoneKnightState>, ISlashable, RoomCompleted
{
    public enum StoneKnightState { Idle, Walk, Kneel, Attack, WhirlWind }
    private Renderer meshRenderer;
    private StoneKnightSFX knightSFX;
    private Sword sword;


    private new void Start()
    {
        base.Start();
        states = new Dictionary<StoneKnightState, EntityState<StoneKnightState>>()
        {
            [StoneKnightState.Idle] = GetComponent<StoneKnightIdle>(),
            [StoneKnightState.Walk] = GetComponent<StoneKnightWalk>(),
            [StoneKnightState.Attack] = GetComponent<StoneKnightAttack>(),
            [StoneKnightState.Kneel] = GetComponent<StoneKnightKneel>(),
            [StoneKnightState.WhirlWind] = GetComponent<StoneKnightWhirlwind>()
        };

        
        knightSFX = GetComponent<StoneKnightSFX>();
        meshRenderer = transform.Find(GameObjects.StoneKnightMeshRenderer).GetComponent<Renderer>();
        sword = GameObject.Find(GameObjects.StoneSword).GetComponent<Sword>();
        SetupStates();
        Events.RoomCompletedEvent += OnRoomCompleted;
    }

    private void FixedUpdate()
    {
        UpdateInvulnerabilityTime();
    }

    public void Slash(int damage, GameObject sender)
    {
        if (IsState(states[StoneKnightState.WhirlWind]) || IsState(states[StoneKnightState.Kneel])) return;
        base.Hit(damage);
        StartCoroutine(Hurt(meshRenderer));
        if (!IsAlive())
        {
            ChangeState(StoneKnightState.Kneel);
            return;
        }
    }

    public void Revive()
    {
        base.Heal(GetMaxHealth());
        sword.SetDangerStatus(true);
        ChangeState(StoneKnightState.Idle);
    }

    protected override void Die()
    {
        knightSFX.OnDeathSFX();
        sword.SetDangerStatus(false);
        ChessRoomManager.Instance.FightOver();
        ChangeState(StoneKnightState.Kneel);
        
    }

    public void OnRoomCompleted(RoomType roomType)
    {
        if (roomType.Equals(RoomType.Chess))
            ChangeState(StoneKnightState.Kneel);
    }

    private void OnDestroy()
    {
        Events.RoomCompletedEvent -= OnRoomCompleted;
    }
}
