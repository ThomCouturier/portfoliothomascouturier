using Game;
using Harmony;
using UnityEngine;
using UnityEngine.AI;

public class StoneKnightWalk : EntityState<StoneKnight.StoneKnightState>
{
    public const float SLAM_ATTACK_DISTANCE = 4f;
    public const float WHIRL_ATTACK_DISTANCE = 9f;
    public const float SECOND_PHASE_HEALTH = 0.5f;
    private const float WALKING_RYTHM = 0.7f;

    private ISensor<Player> playerSensor;
    private Player player;
    private Animator animator;
    private NavMeshAgent agent;
    private StoneKnightSFX knightSFX;

    private bool initialize = true;
    private bool hasSlammed = false;
    private float timer = 0f;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        knightSFX = GetComponent<StoneKnightSFX>();
    }
    private void OnEnable()
    {
        if (initialize)
        {
            Sensor sensor = transform.Find(GameObjects.PlayerSensor).GetComponent<Sensor>();
            playerSensor = sensor.For<Player>();
            playerSensor.OnUnsensedObject += HandlePlayerLeave;
            initialize = false;
            hasSlammed = false;
        }

        if (playerSensor.SensedObjects.Count == 0)
        {
            entityAI.ChangeState(StoneKnight.StoneKnightState.Idle);
            return;
        }

        player = playerSensor.SensedObjects[0];
        agent.isStopped = false;
        animator.SetFloat(AnimatorParameters.Speed, 1);
    }

    protected void Update()
    {
        bool willFight = false;
        if (entityAI.Health > (entityAI.GetMaxHealth() * SECOND_PHASE_HEALTH))
        {
            if (FirstPhase()) willFight = true;
        }
        else
        {
            if (SecondPhase()) willFight = true;
        }

        if (!willFight)
        {
            agent.destination = player.transform.position;
            MovementRythm();
        }
    }

    void HandlePlayerLeave(Player playerMovement)
    {
        if (!enabled) return;
        entityAI.ChangeState(StoneKnight.StoneKnightState.Idle);
    }

    private bool FirstPhase()
    {
        if (transform.position.IsCloseOf(player.transform.position, SLAM_ATTACK_DISTANCE))
        {
            hasSlammed = true;
            entityAI.ChangeState(StoneKnight.StoneKnightState.Attack);
            return true;
        }
        return false;
    }

    private bool SecondPhase()
    {
        if (hasSlammed && transform.position.IsCloseOf(player.transform.position, WHIRL_ATTACK_DISTANCE))
        {
            hasSlammed = false;
            entityAI.ChangeState(StoneKnight.StoneKnightState.WhirlWind);
            return true;
        }
        else if (transform.position.IsCloseOf(player.transform.position, SLAM_ATTACK_DISTANCE))
        {
            hasSlammed = true;
            entityAI.ChangeState(StoneKnight.StoneKnightState.Attack);
            return true;
        }
        return false;
    }

    private void MovementRythm()
    {
        timer -= Time.deltaTime;
        if (timer < 0)
        {
            knightSFX.OnMovementSFX();
            timer = WALKING_RYTHM;
        }
    }
}
