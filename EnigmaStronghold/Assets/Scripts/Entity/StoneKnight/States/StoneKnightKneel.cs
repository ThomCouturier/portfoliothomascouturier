using Harmony;
using UnityEngine;
using UnityEngine.AI;

public class StoneKnightKneel : EntityState<StoneKnight.StoneKnightState>
{
    private Animator animator;
    protected float deathTime;
    private bool initialize = true;
    private NavMeshAgent agent;

    private void OnEnable()
    {
        if (initialize)
        {
            agent = GetComponent<NavMeshAgent>();
            animator = GetComponent<Animator>();
            initialize = false;
        }
        agent.isStopped = true;
        animator.SetFloat(AnimatorParameters.Speed, 0);
    }

    // doit être fait de cette manière sinon le knight retourne en idle (animation seulement)
    private void FixedUpdate()
    {
        animator.SetTrigger(AnimatorParameters.Death);
    }
}
