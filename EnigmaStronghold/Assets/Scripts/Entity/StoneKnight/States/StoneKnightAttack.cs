using Game;
using UnityEngine;
using UnityEngine.AI;
using Harmony;
using JetBrains.Annotations;

public class StoneKnightAttack : EntityState<StoneKnight.StoneKnightState>
{
    private const float MAX_COOLDOWN = 2.1f;
    private const float ROTATION_SPEED = 1.7f;
    private const float TIME_BEFORE_ROTATION = 0.7f;

    private ISensor<Player> playerSensor;
    private Animator animator;
    private NavMeshAgent agent;
    private Player player;
    private Sword sword;
    private StoneKnightSFX knightSFX;

    private bool initialize = true;
    private float cooldown;
    private int slamDamage;

    private void OnEnable()
    {
        if (initialize)
        {
            Sensor sensor = transform.Find(GameObjects.PlayerSensor).GetComponent<Sensor>();
            agent = GetComponent<NavMeshAgent>();
            knightSFX = GetComponent<StoneKnightSFX>();
            playerSensor = sensor.For<Player>();
            animator = GetComponent<Animator>();
            sword = transform.GetComponentInChildren<Sword>();
            initialize = false;
            slamDamage = sword.GetDamage();
        }
        animator.SetFloat(AnimatorParameters.Speed, 0);
        sword.SetDamage(slamDamage);
        cooldown = MAX_COOLDOWN;
        player = playerSensor.SensedObjects[0];
        agent.isStopped = true;
        RotateTowardsPlayer();
        Attack();
    }

    protected void Update()
    {
        if (cooldown < TIME_BEFORE_ROTATION)
        {
            if (cooldown == 0)
            {
                entityAI.ChangeState(StoneKnight.StoneKnightState.Walk);
                return;
            }

            RotateTowardsPlayer();
        }
        cooldown = Mathf.Max(0, cooldown -= Time.deltaTime);
    }

    private void Attack()
    {
        animator.SetTrigger(AnimatorParameters.Attack);
        knightSFX.OnAttackSFX();
    }

    [UsedImplicitly]
    public void SetSwordDanger(int boolean)
    {
        sword.SetDangerStatus(boolean != 0);
    }

    private void RotateTowardsPlayer()
    {
        Vector3 direction = (player.transform.position - transform.position).normalized;
        Quaternion rotation = Quaternion.LookRotation(direction);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * ROTATION_SPEED);
    }
}
