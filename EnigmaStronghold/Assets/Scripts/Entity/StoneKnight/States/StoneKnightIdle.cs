using Game;
using UnityEngine;
using UnityEngine.AI;
using Harmony;

public class StoneKnightIdle : EntityState<StoneKnight.StoneKnightState>
{
    private const string STIMULI_COLLIDER_NAME = "StoneKnightStimuli";
    private ISensor<Player> playerSensor;
    private NavMeshAgent agent;
    private Animator animator;
    private bool initialize = true;
    private Collider entityCollider;

    private void OnEnable()
    {
        if (initialize)
        {
            animator = GetComponent<Animator>();
            agent = GetComponent<NavMeshAgent>();
            Sensor sensor = transform.Find(GameObjects.PlayerSensor).GetComponent<Sensor>();
            entityCollider = transform.Find(STIMULI_COLLIDER_NAME).GetComponent<Collider>();
            playerSensor = sensor.For<Player>();
            playerSensor.OnSensedObject += HandlePlayerContact;
            
            initialize = false;
        }

        agent.isStopped = false;
        entityCollider.enabled = true;
        animator.SetTrigger(AnimatorParameters.Activate);
        animator.SetFloat(AnimatorParameters.Speed, 0);
        agent.destination = transform.position;

        if (playerSensor.SensedObjects.Count > 0)
            entityAI.ChangeState(StoneKnight.StoneKnightState.Walk);
    }

    void HandlePlayerContact(Player player)
    {
        if (!enabled) return;
        entityAI.ChangeState(StoneKnight.StoneKnightState.Walk);
    }
}
