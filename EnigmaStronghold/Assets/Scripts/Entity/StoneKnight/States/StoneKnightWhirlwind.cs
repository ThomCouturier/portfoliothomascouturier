using Game;
using Harmony;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class StoneKnightWhirlwind : EntityState<StoneKnight.StoneKnightState>
{
    private const float WORLDWIND_COOLDOWN = 2f;
    private const float WORLDWIND_RYTHM = 0.2f;
    private const float PREPARE_COOLDOWN = 0.75f;
    private float SPEED_BOOST = 40f;

    private ISensor<Player> playerSensor;
    private Animator animator;
    private NavMeshAgent agent;
    private Rigidbody rb;
    private Player player;
    private Sword sword;
    private StoneKnightSFX knightSFX;

    private bool preparing;
    private bool spinning = false;
    private bool initialize = true;
    private Vector3 directionWhirl;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        sword = transform.GetComponentInChildren<Sword>();
        rb = GetComponentInChildren<Rigidbody>();
    }

    private void OnEnable()
    {
        if (initialize)
        {
            Sensor sensor = transform.Find(GameObjects.PlayerSensor).GetComponent<Sensor>();
            knightSFX = GetComponent<StoneKnightSFX>();
            playerSensor = sensor.For<Player>();
            initialize = false;
        }

        animator.SetFloat(AnimatorParameters.Speed, 0);
        player = playerSensor.SensedObjects[0];
        agent.isStopped = true;
        StartCoroutine(Prepare());
    }

    private IEnumerator Prepare()
    {
        preparing = true;
        animator.SetTrigger(AnimatorParameters.Prepare);
        yield return new WaitForSeconds(PREPARE_COOLDOWN);

        sword.SetDamage(1);
        sword.SetDangerStatus(true);
        directionWhirl = transform.position.DirectionTo(player.transform.position);
        yield return StartCoroutine(SpinAttack());
    }

    private IEnumerator SpinAttack()
    {
        rb.isKinematic = false;
        preparing = false;
        spinning = true;
        animator.SetBool(AnimatorParameters.WhirlWind, true);
        StartCoroutine(WhirlwindSFXRythm());
        yield return new WaitForSeconds(WORLDWIND_COOLDOWN);

        sword.SetDangerStatus(false);
        agent.isStopped = false;
        spinning = false;
        rb.isKinematic = true;
        animator.SetBool(AnimatorParameters.WhirlWind, false);
        entityAI.ChangeState(StoneKnight.StoneKnightState.Walk);
    }

    private IEnumerator WhirlwindSFXRythm()
    {
        while(spinning)
        {
            knightSFX.OnWhirlwindSFX();
            yield return new WaitForSeconds(WORLDWIND_RYTHM);
        }
        StopCoroutine(WhirlwindSFXRythm());
    }

    private void FixedUpdate()
    {
        if (!preparing)
            rb.velocity = SPEED_BOOST * directionWhirl * Time.deltaTime;
    }
}
