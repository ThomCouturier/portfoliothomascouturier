using Game;
using Harmony;
using UnityEngine;
using UnityEngine.AI;

public class SkeletonIdle : EntityState<Skeleton.SkeletonState>
{
    private ISensor<Player> playerSensor;
    private NavMeshAgent agent;
    private Animator animator;
    private bool initialize = true;

    private void OnEnable()
    {
        if (initialize)
        {
            animator = GetComponent<Animator>();
            agent = GetComponent<NavMeshAgent>();
            Sensor sensor = transform.Find(GameObjects.PlayerSensor).GetComponent<Sensor>();
            playerSensor = sensor.For<Player>();
            playerSensor.OnSensedObject += HandlePlayerContact;
            initialize = false;
        }
        agent.destination = transform.position;
        animator.SetFloat(AnimatorParameters.Speed, 0);
    }

    void HandlePlayerContact(Player player)
    {
        if (!enabled) return;
        entityAI.ChangeState(Skeleton.SkeletonState.Walk);
    }
}
