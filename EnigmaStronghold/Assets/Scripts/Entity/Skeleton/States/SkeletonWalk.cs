using Game;
using Harmony;
using UnityEngine;
using UnityEngine.AI;

public class SkeletonWalk : EntityState<Skeleton.SkeletonState>
{
    public const float ATTACK_DISTANCE = 2.75f;
    private const float WALKING_RYTHM = 0.6f;

    private ISensor<Player> playerSensor;
    private Player player;
    private Animator animator;
    private NavMeshAgent agent;
    private SkeletonSFX skeletonSFX;

    private bool initialize = true;
    private float timer = 0f;

    private void OnEnable()
    {
        if(initialize)
        {
            animator = GetComponent<Animator>();
            agent = GetComponent<NavMeshAgent>();
            skeletonSFX = GetComponent<SkeletonSFX>();
            Sensor sensor = transform.Find(GameObjects.PlayerSensor).GetComponent<Sensor>();
            playerSensor = sensor.For<Player>();
            playerSensor.OnUnsensedObject += HandlePlayerLeave;
            initialize = false;
        }
        if (playerSensor.SensedObjects.Count == 0)
        {
            entityAI.ChangeState(Skeleton.SkeletonState.Idle);
            return;
        }
        player = playerSensor.SensedObjects[0];
        agent.isStopped = false;
        animator.SetFloat(AnimatorParameters.Speed, 1);
    }

    protected void Update()
    {
        if (transform.position.IsCloseOf(player.transform.position, ATTACK_DISTANCE))
        {
            entityAI.ChangeState(Skeleton.SkeletonState.Attack);
        } else
        {
            agent.destination = player.transform.position;
            MovementRythm();
        }
    }

    void HandlePlayerLeave(Player playerMovement)
    {
        if (!enabled) return;
        entityAI.ChangeState(Skeleton.SkeletonState.Idle);
    }

    private void MovementRythm()
    {
        timer -= Time.deltaTime;
        if (timer < 0)
        {
            skeletonSFX.OnMovementSFX();
            timer = WALKING_RYTHM;
        }
    }
}
