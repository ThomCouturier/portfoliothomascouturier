using Game;
using Harmony;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.AI;

public class SkeletonAttack : EntityState<Skeleton.SkeletonState>
{
    private const float MAX_COOLDOWN = 1.5f;
    private const float ROTATION_SPEED = 2.1f;

    private ISensor<Player> playerSensor;

    private Animator animator;
    private NavMeshAgent agent;
    private Player player;
    private Sword sword;
    private SkeletonSFX skeletonSFX;

    private float cooldown;
    private bool initialize = true;

    private void OnEnable()
    {
        if (initialize)
        {
            Sensor sensor = transform.Find(GameObjects.PlayerSensor).GetComponent<Sensor>();
            agent = GetComponent<NavMeshAgent>();
            skeletonSFX = GetComponent<SkeletonSFX>();
            playerSensor = sensor.For<Player>();
            animator = GetComponent<Animator>();
            sword = transform.GetComponentInChildren<Sword>();
            initialize = false;
        }
        cooldown = 0;
        player = playerSensor.SensedObjects[0];
        agent.isStopped = true;
        animator.SetFloat(AnimatorParameters.Speed, 0);
    }

    protected void Update()
    {
        if (!transform.position.IsCloseOf(player.transform.position, SkeletonWalk.ATTACK_DISTANCE))
        {
            entityAI.ChangeState(Skeleton.SkeletonState.Walk);
        } else
        {
            if (cooldown < MAX_COOLDOWN / 2)
            {
                Vector3 direction = (player.transform.position - transform.position).normalized;
                Quaternion rotation = Quaternion.LookRotation(direction);
                // J'utilise Slerp pour rotate smoothly (lentement) vers la target
                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * ROTATION_SPEED);
            }

            cooldown = Mathf.Max(0, cooldown -= Time.deltaTime);
            if (cooldown == 0)
            {
                Attack();
            }
        }
    }

    private void Attack()
    {
        cooldown = MAX_COOLDOWN;
        animator.SetTrigger(AnimatorParameters.Attack);
        skeletonSFX.OnAttackSFX();
    }

    [UsedImplicitly]
    public void SetSwordDanger(int boolean) // Don't ask... Unity doesn't like boolean in animations
    {
        sword.SetDangerStatus(boolean != 0);
    }
}
