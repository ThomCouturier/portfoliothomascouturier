using Harmony;
using System;
using UnityEngine;
using UnityEngine.AI;

public class SkeletonDeath : EntityState<Skeleton.SkeletonState>
{
    protected const float DEATH_TIME = 5;
    private Animator animator;
    private NavMeshAgent agent;
    [SerializeField] private Collider masterCollider;
    [SerializeField] protected Renderer meshRenderer;
    [SerializeField] private Collider[] colliders;
    protected float deathTime;
    private bool initialize = true;
    private SkeletonSFX skeletonSFX;

    protected void OnEnable()
    {
        if (initialize)
        {
            animator = GetComponent<Animator>();
            agent = GetComponent<NavMeshAgent>();
            skeletonSFX = GetComponent<SkeletonSFX>();
            transform.GetComponentsInChildren<Collider>(true);
            initialize = false;
        }
        Array.ForEach(colliders, collider => collider.enabled = false);
        masterCollider.enabled = true;
        deathTime = DEATH_TIME;
        agent.enabled = false;
        animator.SetFloat(AnimatorParameters.Speed, 0);
        animator.SetTrigger(AnimatorParameters.Death);
        animator.SetBool(AnimatorParameters.Dead, true);
        skeletonSFX.OnDeathSFX();
    }

    private void Update()
    {
        deathTime = Mathf.Max(0, deathTime -= Time.deltaTime);
        meshRenderer.material.color = new Color32(255, 255, 255, (byte) (255 * (deathTime / DEATH_TIME)));
        if (deathTime == 0)
        {
            entityAI.gameObject.SetActive(false);
        }
    }
}
