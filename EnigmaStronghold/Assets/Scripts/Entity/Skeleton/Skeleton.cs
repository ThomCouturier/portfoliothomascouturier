using System.Collections.Generic;
using UnityEngine;

public class Skeleton : EntityAI<Skeleton.SkeletonState>, ISlashable
{
    public enum SkeletonState { Idle, Walk, Attack, Death }
    [SerializeField] protected Renderer meshRenderer;

    new void Start()
    {
        base.Start();

        states = new Dictionary<SkeletonState, EntityState<SkeletonState>>()
        {
            [SkeletonState.Idle] = GetComponent<SkeletonIdle>(),
            [SkeletonState.Walk] = GetComponent<SkeletonWalk>(),
            [SkeletonState.Attack] = GetComponent<SkeletonAttack>(),
            [SkeletonState.Death] = GetComponent<SkeletonDeath>()
        };      
        SetupStates();
    }

    private void FixedUpdate()
    {
        UpdateInvulnerabilityTime();
    }

    public void Slash(int damage, GameObject sender)
    {
        base.Hit(damage);
        StartCoroutine(Hurt(meshRenderer));
    }

    protected override void Die()
    {
        if (IsState(states[SkeletonState.Death])) return;
        ChangeState(SkeletonState.Death);
    }
}
