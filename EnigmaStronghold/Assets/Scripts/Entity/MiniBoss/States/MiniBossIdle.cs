using Game;
using Harmony;
using UnityEngine;

public class MiniBossIdle : EntityState<MiniBoss.MiniBossStates>
{
    [SerializeField] public MiniBossSFX sfx;
    [SerializeField] public float chargingRythm = 1f;

    public const float ROTATION_SPEED = 2.1f;
    private const float MAX_ATTACK_COOLDOWN = 2f;
    private const float PLAYER_RADIUS_EXPAND = 3.5f;

    private ISensor<Player> playerSensor;
    private Animator animator;

    public float idleCooldown = MAX_ATTACK_COOLDOWN / 2;
    private bool hasShot;
    private bool initialize = true;
    private float timer = 0f;

    private void OnEnable()
    {
        if (initialize)
        {
            GameObject playerSensorGameObject = transform.Find(GameObjects.PlayerSensor).gameObject;
            Sensor sensor = playerSensorGameObject.GetComponent<Sensor>();
            playerSensorGameObject.GetComponent<SphereCollider>().radius *= PLAYER_RADIUS_EXPAND;
            playerSensor = sensor.For<Player>();
            animator = GetComponent<Animator>();
            initialize = false;
            hasShot = false;
            animator.SetTrigger(AnimatorParameters.Entrance);
            Finder.Events.BossFightBegin(GetComponent<MiniBoss>().GetElementType());
        } 
        else
        {
            animator.SetTrigger(AnimatorParameters.Idle);
        }
        idleCooldown = MAX_ATTACK_COOLDOWN;
    }

    private void Update()
    {
        if(gameObject.name == GameObjects.YellowMiniBoss)
        {
            MovementRythm();
        }

        idleCooldown = Mathf.Max(0, idleCooldown - Time.deltaTime);
        if (idleCooldown == 0)
        {
            if (!hasShot)
            {
                entityAI.ChangeState(MiniBoss.MiniBossStates.Attack);
            }
            else
            {
                entityAI.ChangeState(MiniBoss.MiniBossStates.Charge);
            }
            hasShot = !hasShot;
        }

        if (playerSensor.SensedObjects.Count > 0) 
        {
            GameObject target = playerSensor.SensedObjects[0].gameObject;
            Vector3 targetPos = new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z);
            Vector3 direction = (targetPos - transform.position).normalized;
            Quaternion rotation = Quaternion.LookRotation(direction);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * ROTATION_SPEED);
        }
    }

    private void MovementRythm()
    {
        timer -= Time.deltaTime;
        if (timer < 0)
        {
            sfx.OnMoveSFX();
            timer = chargingRythm;
        }
    }
}
