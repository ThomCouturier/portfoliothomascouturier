using Game;
using Harmony;
using UnityEngine;

public class MiniBossEntrance : EntityState<MiniBoss.MiniBossStates>
{
    [SerializeField] public MiniBossSFX sfx;
    private ISensor<Player> playerSensor;

    private void Start()
    {
        Sensor sensor = transform.Find(GameObjects.PlayerSensor).GetComponent<Sensor>();
        playerSensor = sensor.For<Player>();
        playerSensor.OnSensedObject += HandlePlayerContact;
    }

    private void HandlePlayerContact(Player player)
    {
        sfx.PlaySpeech();
        entityAI.ChangeState(MiniBoss.MiniBossStates.Idle);
    }
}
