using Harmony;
using System.Collections;
using UnityEngine;

public class MiniBossDead : EntityState<MiniBoss.MiniBossStates>
{
    private const float DEATH_TIME = 3;

    [SerializeField] public MiniBossSFX sfx;

    private Animator animator;
    private Collider sensorCollider;

    private bool initialize = true;

    private void OnEnable()
    {
        if (initialize)
        {
            animator = GetComponent<Animator>();
            sensorCollider = gameObject.transform.Find(GameObjects.Stimuli).GetComponent<Collider>();
            initialize = false;
        }
        sensorCollider.enabled = false;
        sfx.OnDeathSFX();
        GlobalSFX.Instance.StopGlobalAudioSource();
        animator.SetTrigger(AnimatorParameters.Dead);
        StartCoroutine(Despawn());
    }

    private IEnumerator Despawn()
    {
        yield return new WaitForSeconds(DEATH_TIME);
        gameObject.SetActive(false);
    }

}
