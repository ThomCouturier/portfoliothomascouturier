using Game;
using Harmony;
using UnityEngine;

public class MiniBossCharge : EntityState<MiniBoss.MiniBossStates>
{
    [SerializeField] public MiniBossSFX sfx;
    [SerializeField] public float chargingRythm = 0.3f;

    private const float MAX_ATTACK_COOLDOWN = 0.5f;
    private const float MAX_RUNNING_TIME = 2.5f;
    private const float PUSH_RANGE = 2.5f;
    private const int PUSH_DAMAGE = 3;
    

    private ISensor<Player> playerSensor;
    private Player player;
    private Rigidbody rb;
    private Animator animator;

    private bool initialize = true;
    public float attackCooldown;
    public float runningTime;
    private float timer = 0f;

    private void OnEnable()
    {
        if (initialize)
        {
            Sensor sensor = transform.Find(GameObjects.PlayerSensor).GetComponent<Sensor>();
            playerSensor = sensor.For<Player>();
            animator = GetComponent<Animator>();
            rb = GetComponent<Rigidbody>();
            initialize = false;
        }
        runningTime = 0;
        attackCooldown = MAX_ATTACK_COOLDOWN;
        player = playerSensor.SensedObjects[0];
    }

    private void Update()
    {
        if (runningTime == 0)
        {
            if (attackCooldown == 0) entityAI.ChangeState(MiniBoss.MiniBossStates.Idle);
            else
            {
                Vector3 targetPos = new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z);
                Vector3 direction = (targetPos - transform.position).normalized;
                Quaternion rotation = Quaternion.LookRotation(direction);
                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * MiniBossIdle.ROTATION_SPEED);

                attackCooldown = Mathf.Max(0, attackCooldown - Time.deltaTime);
                if (attackCooldown == 0)
                {
                    animator.SetTrigger(AnimatorParameters.Charge);
                    runningTime = MAX_RUNNING_TIME;
                }
            }
        }
    }

    private void FixedUpdate()
    {
        if (runningTime > 0)
        {
            MovementRythm();
            if (transform.position.IsCloseOf(player.transform.position, PUSH_RANGE))
                player.Hit(PUSH_DAMAGE);

            rb.velocity = transform.forward * entityAI.GetSpeed();
            runningTime = Mathf.Max(0, runningTime - Time.deltaTime);
        }
    }
    private void MovementRythm()
    {
        timer -= Time.deltaTime;
        if (timer < 0)
        {
            sfx.OnMoveSFX();
            timer = chargingRythm;
        }
    }
}
