using Game;
using Harmony;
using UnityEngine;

public class MiniBossAttack : EntityState<MiniBoss.MiniBossStates>
{
    private const float SERIES_OF_DRAGON_BALLS = 1;
    private const float MAX_ATTACK_COOLDOWN = 0.3f;
    private const float ROTATION_OFFSET_SHOOT = 0;
    protected const float DRAGONBALL_DELAY_TIME = 0.175f;

    [SerializeField] public MiniBossSFX sfx;
    [SerializeField] protected float dragonBallSummonTime = 0.225f;
    [SerializeField] private GameObject mouth;

    private ISensor<Player> playerSensor;
    private Player player;
    private Animator animator;
    private DragonBallSpawner dragonBallManager;

    private bool initialize = true;
    public float attackCooldown;
    private int shots;

    private void OnEnable()
    {
        if (initialize)
        {
            Sensor sensor = transform.Find(GameObjects.PlayerSensor).GetComponent<Sensor>();
            playerSensor = sensor.For<Player>();
            animator = GetComponent<Animator>();
            initialize = false;
        }
        attackCooldown = MAX_ATTACK_COOLDOWN;
        if(playerSensor.SensedObjects.Count == 0)
            entityAI.ChangeState(MiniBoss.MiniBossStates.Entrance);
        player = Finder.Player;
        shots = 0;
    }

    private void Start()
    {
        dragonBallManager = DragonBallSpawner.instance;
    }

    private void Update()
    {
        if (shots >= SERIES_OF_DRAGON_BALLS)
            entityAI.ChangeState(MiniBoss.MiniBossStates.Idle);

        attackCooldown = Mathf.Max(0, attackCooldown - Time.deltaTime);
        if (attackCooldown == 0 && shots < SERIES_OF_DRAGON_BALLS)
        {
            sfx.OnAttackPreparationSFX();
            animator.SetTrigger(AnimatorParameters.Shoot);
            MiniBoss miniBoss = (MiniBoss)entityAI;
            dragonBallManager.Spawn(mouth, player.gameObject, miniBoss.GetElementType(), ROTATION_OFFSET_SHOOT, dragonBallSummonTime, DRAGONBALL_DELAY_TIME);
            shots++;
            attackCooldown = MAX_ATTACK_COOLDOWN;
        }
    }
}
