using Harmony;
using System.Collections.Generic;
using UnityEngine;

public class MiniBoss : EntityAI<MiniBoss.MiniBossStates>, ISlashable, IUIBar
{
    public enum MiniBossStates { Entrance, Idle, Attack, Charge, Special, Death }

    [SerializeField] private ElementType type;
    [SerializeField] private Key key;
    [SerializeField] protected Renderer meshRenderer;
    [SerializeField] private AudioClip globalClip;

    private new void Start()
    {
        base.Start();

        states = new Dictionary<MiniBossStates, EntityState<MiniBossStates>>()
        {
            [MiniBossStates.Entrance] = GetComponent<MiniBossEntrance>(),
            [MiniBossStates.Idle] = GetComponent<MiniBossIdle>(),
            [MiniBossStates.Attack] = GetComponent<MiniBossAttack>(),
            [MiniBossStates.Charge] = GetComponent<MiniBossCharge>(),
            [MiniBossStates.Death] = GetComponent<MiniBossDead>(),
        };
        
        var hpBar = Finder.PlayerUI.transform.Find(GameObjects.BossHealthBar);
        hpBar.GetComponent<UIBarManager>().SetData(gameObject);
        meshRenderer = transform.Find(GameObjects.MiniBossMesh).GetComponent<Renderer>();
        SetupStates();
    }

    public ElementType GetElementType()
    {
        return type;
    }

    private void FixedUpdate()
    {
        UpdateInvulnerabilityTime();
    }

    public void Slash(int damage, GameObject sender)
    {
        base.Hit(damage);
        if (IsAlive()) StartCoroutine(Hurt(meshRenderer));
    }

    protected override void Die()
    {
        key.SpawnKey(gameObject.transform.position);
        GlobalSFX.Instance.StopGlobalAudioSource();
        GlobalSFX.Instance.SetGlobalAudioSource(globalClip);
        ChangeState(MiniBossStates.Death);
        Finder.Events.BossFightEnd();
    }

    public float GetMax()
    {
        return GetMaxHealth();
    }

    public float GetCurrent()
    {
        return Health;
    }
}
