using Harmony;
using System.Collections.Generic;
using UnityEngine;

public class Dragon : EntityAI<Dragon.DragonState>, ISlashable, IUIBar
{
    public const float SECOND_PHASE_HEALTH_PERCENTAGE = 0.67f;
    public enum DragonState { Entrance, Idle, Attack, Hurt, Fly, Death }
    public event EventHandler OnDeath;
    private Renderer meshRenderer;
    private KhanbanSFX sfx;

    private new void Start()
    {
        base.Start();

        states = new Dictionary<DragonState, EntityState<DragonState>>()
        {
            [DragonState.Entrance] = GetComponent<DragonEntrance>(),
            [DragonState.Idle] = GetComponent<DragonIdle>(),
            [DragonState.Attack] = GetComponent<DragonAttack>(),
            [DragonState.Hurt] = GetComponent<DragonHurt>(),
            [DragonState.Fly] = GetComponent<DragonFly>(),
            [DragonState.Death] = GetComponent<DragonDeath>()
        };
        
        SetupStates();
        meshRenderer = transform.Find(GameObjects.DragonBody).GetComponent<Renderer>();
        GlobalSFX.Instance.PlayGlobalAudioSource();
        sfx = GetComponent<KhanbanSFX>();
    }

    private void FixedUpdate()
    {
        UpdateInvulnerabilityTime();
    }

    public void Slash(int damage, GameObject sender)
    {
        base.Hit(damage);
        if (IsAlive())
        {
            StartCoroutine(Hurt(meshRenderer));
            if(!IsSecondPhase()) ChangeState(DragonState.Hurt);
            else sfx.OnHurtSFX();
        }
    }

    protected override void Die()
    {
        StartCoroutine(Hurt(meshRenderer));
        ChangeState(DragonState.Death);
        Finder.Events.BossFightEnd();
    }

    public float GetMax()
    {
        return GetMaxHealth();
    }

    public float GetCurrent()
    {
        return Health;
    }

    public bool IsSecondPhase()
    {
        return (((float)Health / GetMaxHealth()) < SECOND_PHASE_HEALTH_PERCENTAGE);
    }
}
