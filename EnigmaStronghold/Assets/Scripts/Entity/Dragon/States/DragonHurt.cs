using Harmony;
using UnityEngine;

public class DragonHurt : EntityState<Dragon.DragonState>
{
    private const float MAX_HURT_COOLDOWN = 2.5f;
    private Animator animator;
    private FollowAt follower;
    private KhanbanSFX sfx;
    private bool initialize = true;
    private float hurtCooldown;

    private void OnEnable()
    {
        if (initialize)
        {
            sfx = GetComponent<KhanbanSFX>();
            animator = GetComponent<Animator>();
            follower = transform.GetComponentInChildren<FollowAt>();
            initialize = false;
        }
        hurtCooldown = MAX_HURT_COOLDOWN;
        follower.enabled = false;
        sfx.OnHurtSFX();
        animator.SetTrigger(AnimatorParameters.Hurt);
    }

    private void Update()
    {
        hurtCooldown = Mathf.Max(0, hurtCooldown - Time.deltaTime);
        if (hurtCooldown == 0)
        {
            follower.enabled = true;
            entityAI.ChangeState(Dragon.DragonState.Idle);
        }
    }
}
