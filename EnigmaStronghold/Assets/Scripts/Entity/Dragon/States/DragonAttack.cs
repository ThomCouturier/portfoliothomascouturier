using Harmony;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonAttack : EntityState<Dragon.DragonState>
{
    public const float DRAGONBALL_SUMMON_TIME = 0.7f;
	public const float DRAGON_MOUTH_OFFSET = 2.5f;
	public const float INVULNERABLE_SHOOT_TIME = 0.8f;

    private const int DRAGONBALLS_BEFORE_ELEMENTAL = 3;
    private const float MAX_ATTACK_COOLDOWN = 1.3f;
    private const float ROTATION_OFFSET_SHOOT = 25;
    private const float STATE_TRANSITION_TIME = 1.2f;
    private const float FLAMES_TIME_MULTIPLIER = 0.75f;

    [SerializeField] private GameObject mouth;
    
    private GameObject player;
    private List<ElementType> allSpecialShots;
    private List<ElementType> remainingSpecialShots;
    private DragonBallSpawner dragonBallManager;
    private FlameThrower flameThrower;
    private Animator animator;
    private Rigidbody rb;
    private FollowAt follower;
    private KhanbanSFX sfx;

    private bool initialize = true;
    private float attackCooldown;
    private int shots;

    private void Awake()
    {
        sfx = GetComponent<KhanbanSFX>();
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        flameThrower = GetComponentInChildren<FlameThrower>();
        follower = GetComponentInChildren<FollowAt>();
        allSpecialShots = new List<ElementType>
        {
            ElementType.Fire,
            ElementType.Ice,
            ElementType.Poison,
            ElementType.Air,
        };
        remainingSpecialShots = new List<ElementType>(allSpecialShots);
    }

    private void OnEnable()
    {
        if (initialize)
        {
            player = Finder.Player.gameObject;
            initialize = false;
        }
        follower.enabled = true;
        attackCooldown = MAX_ATTACK_COOLDOWN;
        animator.SetBool(AnimatorParameters.MouthClose, false);
        shots = 0;
    }

    private void Start()
    {
        dragonBallManager = DragonBallSpawner.instance;
    }

    private void Update()
    {
        rb.velocity = Vector3.zero;
        attackCooldown = Mathf.Max(0, attackCooldown - Time.deltaTime);
        if (attackCooldown == 0) Attack();
        else follower.LookAt(player.transform.position);
    }

    private void Attack()
    {
        if (remainingSpecialShots.Count == 0)
            remainingSpecialShots = new List<ElementType>(allSpecialShots);

        shots++;
        attackCooldown = MAX_ATTACK_COOLDOWN;
        animator.SetTrigger(AnimatorParameters.Shoot);
        float rotationOffset = ROTATION_OFFSET_SHOOT;

        if (Random.Range(0, 2) == 0)
            rotationOffset *= -1;

        if (shots <= DRAGONBALLS_BEFORE_ELEMENTAL)
        {
            Dragon dragon = (Dragon)entityAI;
            if (dragon.IsSecondPhase())
            {
                attackCooldown = MAX_ATTACK_COOLDOWN * DRAGONBALLS_BEFORE_ELEMENTAL;
                flameThrower.Shoot(attackCooldown * FLAMES_TIME_MULTIPLIER);
                shots = DRAGONBALLS_BEFORE_ELEMENTAL;
            }
            else
            {
                sfx.OnAttackPreparationSFX();
                dragonBallManager.Spawn(mouth, player, ElementType.Obsidian, rotationOffset, DRAGONBALL_SUMMON_TIME);
            }
        }
        else
        {
            int random = Random.Range(0, remainingSpecialShots.Count);
            ElementType type = remainingSpecialShots[random];
            remainingSpecialShots.Remove(type);
            dragonBallManager.Spawn(mouth, player, type, rotationOffset, DRAGONBALL_SUMMON_TIME);
            sfx.OnAttackPreparationSFX();
            shots = 0;
            StartCoroutine(ChangeState());
            return;
        }
    }

    private IEnumerator ChangeState()
    {
        yield return new WaitForSeconds(STATE_TRANSITION_TIME);
        follower.enabled = false;
        entityAI.ChangeState(Dragon.DragonState.Idle);
    }
}
