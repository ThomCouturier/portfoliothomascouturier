using Harmony;
using System.Collections;
using UnityEngine;

public class DragonDeath : EntityState<Dragon.DragonState>
{
    private const float DEATH_COOLDOWN = 4f;

    private KhanbanSFX sfx;
    private Animator animator;
    private FollowAt follower;
    private Rigidbody rb;
    private Collider stimuli;

    private void Awake()
    {
        sfx= GetComponent<KhanbanSFX>();
        follower = transform.GetComponentInChildren<FollowAt>();
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        GlobalSFX.Instance.StopGlobalAudioSource();
        sfx.OnDeathSFX();
        stimuli = GameObject.Find(GameObjects.DragonStimuli).GetComponent<Collider>(); // Sensor is created after Awake()
        rb.velocity = Vector3.zero;
        follower.enabled = false;
        animator.SetTrigger(AnimatorParameters.Death);
        stimuli.enabled = false;
        StartCoroutine(Dead());
    }

    private IEnumerator Dead()
    {
        yield return new WaitForSeconds(DEATH_COOLDOWN);
        gameObject.SetActive(false);
        Finder.Events.Victory();
    }
}
