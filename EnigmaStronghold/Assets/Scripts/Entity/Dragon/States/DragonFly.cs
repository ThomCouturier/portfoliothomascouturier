using DG.Tweening;
using Harmony;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DragonFly : EntityState<Dragon.DragonState>
{
    private const float NEAR_BIAS = 6f;
    private const float REPOSITION_TIME = 1.5f;
    private const float ROTATION_TIME = 0.5f;
    private const float SPECIAL_ATTACK_COOLDOWN = 2.5f;

    public const float ROTATION_SPEED = 1.1f;
    public const float FLY_SPEED = 14;

    [SerializeField] private GameObject[] flyingPoints;
    [SerializeField] public float flyingRythm = 1.7f;

    private List<ElementType> remainingSpecialShots;
    private List<ElementType> allSpecialShots;
    private SpecialsSpawner specialAttacksSpawner;

    private KhanbanSFX sfx;
    private Animator animator;
    private GameObject mouth;
    private FollowAt follower;
    private Player player;
    private Rigidbody rb;
    private int perchingIndex;
    private float timer = 0.5f;

    private void Awake()
    {
        sfx = GetComponent<KhanbanSFX>();
        rb = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        follower = transform.GetComponentInChildren<FollowAt>();
        flyingPoints = GameObject.FindGameObjectsWithTag(Tags.FlyingPoint);
        mouth = GameObject.Find(GameObjects.DragonMouth);
        allSpecialShots = new List<ElementType>
        {
            ElementType.Poison,
            ElementType.Fire,
            ElementType.Ice,
            ElementType.Poison,
            ElementType.Ice,
            ElementType.Poison
        };
    }

    private void Start()
    {
        specialAttacksSpawner = SpecialsSpawner.Instance;
        player = Finder.Player;
    }

    private void OnEnable()
    {
        remainingSpecialShots = new List<ElementType>(allSpecialShots);
        follower.enabled = false;
        perchingIndex = 0;
        Vector3 direction = transform.position.DirectionTo(flyingPoints[perchingIndex].transform.position).normalized;
        transform.DORotate(direction, ROTATION_TIME);
        animator.SetBool(AnimatorParameters.MouthClose, false);
        animator.SetBool(AnimatorParameters.Fly, true);
        animator.SetBool(AnimatorParameters.Hover, false);
        StartCoroutine(SpecialAttack());
    }

    private void FixedUpdate()
    {
        MovementRythm();
        if (perchingIndex >= flyingPoints.Count())
        {
            perchingIndex = 0;
            transform.DOMove(flyingPoints[flyingPoints.Count() - 1].transform.position, REPOSITION_TIME);
            transform.DORotate(new Vector3(0, DragonEntrance.FORWARD_ROTATION, 0), REPOSITION_TIME);
            animator.SetBool(AnimatorParameters.Hover, true);
            entityAI.ChangeState(Dragon.DragonState.Attack);
            return;
        }
        
        Vector3 targetPosition = flyingPoints[perchingIndex].transform.position;

        if ((targetPosition - transform.position).magnitude < NEAR_BIAS)
        {
            if (flyingPoints.Count() <= ++perchingIndex) return;
            targetPosition = flyingPoints[perchingIndex].transform.position;
        }

        rb.velocity = transform.forward * FLY_SPEED;

        RotationPath(targetPosition);
    }

    private void RotationPath(Vector3 targetPosition)
    {
        Vector3 direction = transform.position.DirectionTo(targetPosition).normalized;
        Quaternion rotation = Quaternion.LookRotation(direction);
        transform.rotation = Quaternion.Lerp(transform.rotation, rotation, Time.deltaTime * ROTATION_SPEED);
    }

    private IEnumerator SpecialAttack()
    {
        while (remainingSpecialShots.Count > 0)
        {
            yield return new WaitForSeconds(SPECIAL_ATTACK_COOLDOWN);
            if (remainingSpecialShots.Count > 0)
            {
                ElementType type = remainingSpecialShots[0];

                switch (type)
                {
                    case ElementType.Ice:
                        specialAttacksSpawner.Spawn(gameObject, player.gameObject, type);
                        break;
                    case ElementType.Poison:
                        specialAttacksSpawner.Spawn(gameObject, player.gameObject, type);
                        break;
                    case ElementType.Fire:
                        DragonBallSpawner.instance.Spawn(mouth, player.gameObject, type, 0, DragonAttack.DRAGONBALL_SUMMON_TIME);
                        break;
                }
                remainingSpecialShots.RemoveAt(0);
            }
        }
    }
    private void MovementRythm()
    {
        timer -= Time.deltaTime;
        if (timer < 0)
        {
            sfx.OnMoveSFX();
            timer = flyingRythm;
        }
    }
}
