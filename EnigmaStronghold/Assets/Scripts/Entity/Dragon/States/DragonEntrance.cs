using DG.Tweening;
using Harmony;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonEntrance : EntityState<Dragon.DragonState>
{
    private const float TILT_NERF = 3;
    private const float NEAR_BIAS = 3.5f;
    private const float LANDING_TIME = 1.5f;

    public const float FORWARD_ROTATION = 180f;
    [SerializeField] private List<GameObject> perchingPoints;
    [SerializeField] public float flyingRythm = 1.9f;

    private KhanbanSFX sfx;
    private Animator animator;
    private Rigidbody rb;
    private FollowAt follower;
    private int perchingIndex;
    private float timer = 0f;

    private void Awake()
    {
        sfx = GetComponent<KhanbanSFX>();
        rb = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        follower = transform.GetComponentInChildren<FollowAt>();
    }

    private void OnEnable()
    {
        perchingIndex = 0;
        follower.enabled = false;
        animator.SetBool(AnimatorParameters.MouthClose, true);
    }

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(2f);
        sfx.PlaySpeech();
    }

    private void FixedUpdate()
    {
        if (perchingIndex >= perchingPoints.Count)
        {
            
            rb.velocity = Vector3.zero;
            transform.DOMove(perchingPoints[perchingPoints.Count - 1].transform.position, LANDING_TIME);
            animator.SetBool(AnimatorParameters.Fly, false);
            transform.DORotate(new Vector3(0, FORWARD_ROTATION, 0), LANDING_TIME);
            sfx.OnMoveSFX();
            entityAI.ChangeState(Dragon.DragonState.Idle);
        } else
        {
            MovementRythm();
            Vector3 targetPosition = perchingPoints[perchingIndex].transform.position;

            if ((targetPosition - transform.position).magnitude < NEAR_BIAS)
            {
                if (perchingPoints.Count <= ++perchingIndex) return;
                targetPosition = perchingPoints[perchingIndex].transform.position;
            }

            rb.velocity = transform.forward * DragonFly.FLY_SPEED;

            Vector3 direction = transform.position.DirectionTo(targetPosition).normalized;
            Quaternion rotation = Quaternion.LookRotation(direction);
            transform.rotation = Quaternion.Lerp(transform.rotation, rotation, Time.deltaTime * DragonFly.ROTATION_SPEED);

            RotateToTarget(direction, targetPosition);
        }
    }

    private void RotateToTarget(Vector3 direction, Vector3 target)
    {
        float angle = Vector3.Angle(direction, transform.forward);
        if (transform.InverseTransformPoint(target).x < 0)
        {
            angle = -angle;
        }
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, -angle / TILT_NERF);
    }

    private void MovementRythm()
    {
        timer -= Time.deltaTime;
        if (timer < 0)
        {
            sfx.OnMoveSFX();
            timer = flyingRythm;
        }
    }
}
