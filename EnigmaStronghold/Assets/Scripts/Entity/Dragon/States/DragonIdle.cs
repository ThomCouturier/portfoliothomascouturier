using Harmony;
using UnityEngine;

public class DragonIdle : EntityState<Dragon.DragonState>
{
    
    private const float MAX_IDLE_COOLDOWN = 1f;

    private Animator animator;
    private FollowAt follower;
    private float idleCooldown = MAX_IDLE_COOLDOWN / 2;

    private void Awake()
    {
        follower = transform.GetComponentInChildren<FollowAt>();
        animator = GetComponent<Animator>();
    }

    private void OnEnable()
    {
        Dragon dragon = (Dragon)entityAI;
        if (dragon.IsSecondPhase())
        {
            entityAI.ChangeState(Dragon.DragonState.Fly);
            return;
        }
        idleCooldown = MAX_IDLE_COOLDOWN;
        follower.enabled = true;
        animator.SetBool(AnimatorParameters.MouthClose, true);
        GameObject player = Finder.Player.gameObject;
        Finder.Events.BossFightBegin(ElementType.Obsidian);
    }

    private void Update()
    {
        follower.LookAt(Finder.Player.transform.position);
        idleCooldown = Mathf.Max(0, idleCooldown - Time.deltaTime);
        if (idleCooldown == 0) entityAI.ChangeState(Dragon.DragonState.Attack);
    }
}
