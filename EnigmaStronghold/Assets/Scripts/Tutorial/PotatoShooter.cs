using System.Collections.Generic;
using Harmony;
using UnityEngine;

public class PotatoShooter : MonoBehaviour
{
    AudioSource audioSource;
    [SerializeField] private int potatoCount;
    [SerializeField] private GameObject potatoPrefab;
    private float timer = 0;

    private bool canShoot;

    WoodSFX sfx;
    private List<GameObject> potatoes;

    private void Awake()
    {
        sfx = GetComponent<WoodSFX>();
    }
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        potatoes = new List<GameObject>();
        Events.TutorialStepCompletedEvent += OnStepCompleted;

        for (int i = 0; i < potatoCount; i++)
        {
            GameObject newPotato = Instantiate(potatoPrefab, transform.position, Quaternion.identity);
            newPotato.SetActive(false);
            potatoes.Add(newPotato);
        }
    }

    private void Update()
    {
        if (!canShoot) return;
        
        timer += Time.deltaTime;

        if (timer < 3f) return;

        timer = 0;
        Shoot();
    }

    private GameObject GetFirstActivePotato()
    {
        foreach (var potato in potatoes)
        {
            if (!potato.activeSelf)
                return potato;
        }
        
        GameObject newPotato = Instantiate(potatoPrefab, transform.position, Quaternion.identity);
        potatoes.Add(newPotato);
        return potatoes[potatoes.Count - 1];
    }
    
    private void Shoot()
    {
        GameObject potato = GetFirstActivePotato();
        potato.transform.position = transform.position;
        
        potato.SetActive(true);
        potato.GetComponent<Potato>().Summon(Finder.Player.gameObject, gameObject);
        audioSource.PlayOneShot(SoundManager.SoundInstance.PotatoLauncherSFX);
    }

    private void OnStepCompleted(TutorialStepType stepType)
    {
        switch (stepType)
        {
            case TutorialStepType.Heal:
                canShoot = true;
                break;
            case TutorialStepType.Reflect:
                DeactivatePotatoLauncher();
                break;
        }
    }

    private void DeactivatePotatoLauncher()
    {
        canShoot = false;
        foreach (GameObject potato in potatoes)
        {
            potato.SetActive(false);
        }

        var ballista = transform.Find(GameObjects.Ballista);
        for(int i = 0; i < ballista.transform.childCount; i++)
        {
            var rigidbody = ballista.transform.GetChild(i).GetComponent<Rigidbody>();
            rigidbody.isKinematic = false;
            rigidbody.useGravity = true;
        }
        sfx.PlayWoodBreakSFX();
        Events.TutorialStepCompletedEvent -= OnStepCompleted;
    }

    private void OnDestroy()
    {
        Events.TutorialStepCompletedEvent -= OnStepCompleted;
    }
}
