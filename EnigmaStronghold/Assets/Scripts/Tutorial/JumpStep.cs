using Harmony;
using UnityEngine;

public class JumpStep : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.CompareTag(Tags.Player)) return;
        
        Finder.Events.CompleteStep(TutorialStepType.Jump);
        gameObject.SetActive(false);
        enabled = false;
    }
}