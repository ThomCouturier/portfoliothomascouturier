using Harmony;
using UnityEngine;


public class ExitTutorial : MonoBehaviour
{
    private Collider trigger;
    void Start()
    {
        trigger = GetComponent<Collider>();
        trigger.enabled = false;
        Events.TutorialStepCompletedEvent += OnTutorialCompletion;
    }

    private void OnTutorialCompletion(TutorialStepType step)
    {
        if (!step.Equals(TutorialStepType.Reflect)) return;
        Events.TutorialStepCompletedEvent -= OnTutorialCompletion;
        trigger.enabled = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Tags.Player))
        {
            TutorialSFX.TutorialInstance.PlayTutorialCompleted();
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            Finder.PlayerUI.DisplayGameHint();
            SceneLoadingManager.Instance.LoadMainGame();
            RespawnManager.Instance.MovePlayerToHub();
        }
    }

    private void OnDestroy()
    {
        Events.TutorialStepCompletedEvent -= OnTutorialCompletion;
    }
}
