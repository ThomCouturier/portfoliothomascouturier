using Harmony;
using UnityEngine;

public class MovementStep : MonoBehaviour
{

    void Update()
    {
        if(!InputManager.Instance.Inputs.Player.Move.WasPressedThisFrame()) return;
        
        Finder.Events.CompleteStep(TutorialStepType.Movement);
        gameObject.SetActive(false);
        enabled = false;
    }
}
