using Harmony;
using UnityEngine;

public class SliceStep : MonoBehaviour
{
    private const int POTATOES_TO_SLICE = 3;

    private bool canSlice = false;

    private int potatoesSliced;

    void Start()
    {
        Events.PotatoSlicedEvent += OnPotatoSliced;
        Events.TutorialStepCompletedEvent += OnStepCompleted;
    }

    private void OnStepCompleted(TutorialStepType stepType)
    {
        if (stepType == TutorialStepType.Block)
        {
            canSlice = true;
        }
    }

    private void OnPotatoSliced() 
    {
        if(canSlice)
            potatoesSliced++;
        
        if (potatoesSliced < POTATOES_TO_SLICE) return;
        
        Finder.Events.CompleteStep(TutorialStepType.Reflect);
        Events.PotatoSlicedEvent -= OnPotatoSliced;
        enabled = false;
    }

    private void OnDestroy()
    {
        Events.PotatoSlicedEvent -= OnPotatoSliced;
    }
}
