using Harmony;
using UnityEngine;

public class HealStep : MonoBehaviour
{
    private bool healPickedUp;

    private void Start()
    {
        Events.PickUpHealEvent += OnHealPickup;
    }

    private void OnHealPickup()
    {
        healPickedUp = true;
        Finder.Player.Health /= 2;
        Events.PickUpHealEvent -= OnHealPickup;
    }

    void Update()
    {
        if (!InputManager.Instance.Inputs.Player.Potion.WasPressedThisFrame() || !healPickedUp) return;

        Finder.Player.Health = Finder.Player.GetMaxHealth();
        Finder.Events.CompleteStep(TutorialStepType.Heal);
        gameObject.SetActive(false);
        enabled = false;
    }

    private void OnDestroy()
    {
        Events.PickUpHealEvent -= OnHealPickup;
    }
}
