using Harmony;
using UnityEngine;

public class Wall : MonoBehaviour
{
    [SerializeField] private TutorialStepType stepType;
    private void Start()
    {
        Events.TutorialStepCompletedEvent += OnStepCompleted;
    }
    
    private void OnStepCompleted(TutorialStepType stepType)
    {
        if (stepType == this.stepType)
        {
            if (this.stepType == TutorialStepType.Movement)
            {
                GameObject.Find(GameObjects.JumpTutorial).GetComponent<JumpStep>().enabled = true;
            }

            TutorialSFX.TutorialInstance.PlayStepCompleted();
            gameObject.SetActive(false);
            Events.TutorialStepCompletedEvent -= OnStepCompleted;
        }
    }

    private void OnDestroy()
    {
        Events.TutorialStepCompletedEvent -= OnStepCompleted;
    }
}
