public enum TutorialStepType
{
    Movement,
    Jump,
    Pickup,
    Attack,
    Heal,
    Block,
    Reflect,
}
