using Harmony;
using System.Collections;
using UnityEngine;

public class Dummy : MonoBehaviour, ISlashable
{
    private const int MAX_HITS = 3;

    [SerializeField] private float cooldownLength;
    private Collider dummyCollider;

    private int hitCount = 0;
    private bool completed;

    private Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
        dummyCollider = GetComponentInChildren<Collider>();
    }

    private void Update()
    {
        if (hitCount < MAX_HITS || completed) return;

        Finder.Events.CompleteStep(TutorialStepType.Attack);
        completed = true;
    }

    public void Slash(int damage, GameObject sender)
    {
        animator.SetTrigger(AnimatorParameters.Hit);
        dummyCollider.enabled = false;
        hitCount++;

        IEnumerator Cooldown()
        {
            yield return new WaitForSeconds(cooldownLength);
            dummyCollider.enabled = true;
        }
        StartCoroutine(Cooldown());
    }
}
