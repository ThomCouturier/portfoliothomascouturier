using Harmony;
using UnityEngine;

public class BlockStep : MonoBehaviour
{
    private const int POTATOES_TO_BLOCK = 3;
    
    private int potatoesBlocked = 0;

    void Start()
    {
        Events.PotatoBlockedEvent += OnPotatoBlocked;
    }

    private void OnPotatoBlocked() 
    {
        potatoesBlocked++;
        if (potatoesBlocked < POTATOES_TO_BLOCK) return;
        
        Finder.Events.CompleteStep(TutorialStepType.Block);
        Events.PotatoBlockedEvent -= OnPotatoBlocked;
        enabled = false;
    }

    private void OnDestroy()
    {
        Events.PotatoBlockedEvent -= OnPotatoBlocked;
    }
}
