using Harmony;
using UnityEngine;

public class PickupStep : MonoBehaviour, IPickable
{
    public void PickUp()
    {
        gameObject.SetActive(false);
        Finder.Events.CompleteStep(TutorialStepType.Pickup);
    }
}
