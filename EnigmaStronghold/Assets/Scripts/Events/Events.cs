using Harmony;
using UnityEngine;

[Findable(Tags.Events)]
public class Events : MonoBehaviour
{
    public static event EventHandler<RoomType> RoomCompletedEvent;
    public static event EventHandler<ElementType> KeyPickedUpEvent;
    public static event EventHandler CompletedTutorialEvent;
    public static event EventHandler<RoomType> PressurePlateRoomDoneEvent;
    public static event EventHandler<TutorialStepType> TutorialStepCompletedEvent;
    public static event EventHandler PotatoBlockedEvent;
    public static event EventHandler PotatoSlicedEvent;
    public static event EventHandler<ElementType> KeyCollectedVerificationEvent;
    public static event EventHandler<AchievementsEnum> AchievementUnlockedEvent;
    public static event EventHandler PickUpHealEvent;
    public static event EventHandler VictoryEvent;
    public static event EventHandler GhostRespawnEvent;
    public static event EventHandler PlayerPickedUpPendantEvent;
    public static event EventHandler PlayerIsTooFarFromGhostEvent;
    public static event EventHandler PlayerIsNearGhostEvent;
    public static event EventHandler<GameObject> SkullPickedUpEvent;
    public static event EventHandler PlayerRespawnEvent;
    public static event EventHandler<ElementType> BossFightBeginEvent;
    public static event EventHandler BossFightEndEvent;

    private void Awake()
    {
        gameObject.DontDestroyOnLoad();
    }

    public void KeyPickedUp(ElementType elementType)
    {
        KeyPickedUpEvent?.Invoke(elementType);
    }

    public void RoomCompleted(RoomType roomType)
    {
        RoomCompletedEvent?.Invoke(roomType);
    }
    
    public void KeyCollectedVerification(ElementType elementType)
    {
        KeyCollectedVerificationEvent?.Invoke(elementType);
    }

    public void CompleteTutorial()
    {
        CompletedTutorialEvent?.Invoke();
    }

    public void CompleteStep(TutorialStepType tutorialStepType)
    {
        if(tutorialStepType == TutorialStepType.Reflect)
        {
            Finder.Events.CompleteTutorial();
        }
        TutorialStepCompletedEvent?.Invoke(tutorialStepType);
    }

    public void PressurePlateRoomDone(RoomType roomType)
    {
        PressurePlateRoomDoneEvent?.Invoke(roomType);
    }

    public void PotatoBlocked()
    {
        PotatoBlockedEvent?.Invoke();
    }
    
    public void PotatoSliced()
    {
        PotatoSlicedEvent?.Invoke();
    }

    public void AchievementUnlocked(AchievementsEnum achievement)
    {
        AchievementUnlockedEvent?.Invoke(achievement);
    }

    public void PickUpHeal()
    {
        PickUpHealEvent?.Invoke();
    }

    public void Victory()
    {
        VictoryEvent?.Invoke();
    }
    
    public void GhostRespawn()
    {
        GhostRespawnEvent?.Invoke();
    }
    
    public void PlayerPickedUpPendant()
    {
        PlayerPickedUpPendantEvent?.Invoke();
    }
    
    public void PlayerIsTooFarFromGhost()
    {
        PlayerIsTooFarFromGhostEvent?.Invoke();
    }
    
    public void PlayerIsNearGhost()
    {
        PlayerIsNearGhostEvent?.Invoke();
    }
    
    public void SkullPickedUp(GameObject skull)
    {
        SkullPickedUpEvent?.Invoke(skull);
    }
    
    public void PlayerRespawn()
    {
        PlayerRespawnEvent?.Invoke();
    }
    
    public void BossFightBegin(ElementType elementType)
    {
        BossFightBeginEvent?.Invoke(elementType);
    }
    
    public void BossFightEnd()
    {
        BossFightEndEvent?.Invoke();
    }
}
