﻿
public interface RoomCompleted
{
    void OnRoomCompleted(RoomType roomType);
}
