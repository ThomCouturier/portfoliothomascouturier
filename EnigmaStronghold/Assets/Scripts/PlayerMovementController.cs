using Harmony;
using UnityEngine;

public class PlayerMovementController : MonoBehaviour
{
    private const float BLOCK_SPEED_NERF = 0.5f;
    private const float MOVE_STOP_NERF = 0.8f;
    private const float MOMENTUM_TIME = 0.34f;
    private const float WALKING_RYTHM = 0.45f;

    [SerializeField] private float jumpForce;
    [SerializeField] private float raycastGroundLength = 0.1f;
    [SerializeField] private float footOffset = 0.8f;
    [SerializeField] private StaminaManager staminaManager;

    private PlayerSFX playerSFX;
    private Rigidbody rigidbody;
    private InputManager inputManager;
    private PlayerAttack melee;
    private Animator animator;

    private float speed;
    private bool grounded;
    private float walkTimer = 0f;
    private float acceleration = 0f;

    void Start()
    {
        playerSFX = GetComponent<PlayerSFX>();
        animator = transform.GetComponentInChildren< Animator>();
        melee = transform.GetComponentInChildren< PlayerAttack>();
        rigidbody = GetComponent<Rigidbody>();
        inputManager = InputManager.Instance;
    }

    private void Update()
    {
        Vector3 footPosition = transform.position - new Vector3(0, footOffset, 0);
        Debug.DrawLine(footPosition, footPosition + (Vector3.down * raycastGroundLength), Color.red);
        if (Physics.Raycast(footPosition, Vector3.down, out RaycastHit hit, raycastGroundLength))
        {
            grounded = (hit.transform.CompareTag(Tags.Ground));
        }
        else
        {
            grounded = false;
        }

        if (inputManager.Inputs.Player.Jump.ReadValue<float>() > 0 && inputManager.Inputs.Player.Jump.WasPressedThisFrame())
        {
            if (grounded)
            {
                Jump();
            }
        }
    }

    void FixedUpdate()
    {
        Vector2 movement = inputManager.Inputs.Player.Move.ReadValue<Vector2>();
        Vector3 rbVelocity = rigidbody.velocity;
        if (movement == Vector2.zero)
        {
            animator.SetBool(AnimatorParameters.Walk, false);
            acceleration = 0;
            staminaManager.FastRestoreSpeed();
            walkTimer = 0f;
            rigidbody.velocity = new Vector3(rbVelocity.x * MOVE_STOP_NERF, rbVelocity.y, rbVelocity.z * MOVE_STOP_NERF);
        }
        else
        {
            acceleration = Mathf.Min(MOMENTUM_TIME, acceleration + Time.deltaTime);
            animator.SetBool(AnimatorParameters.Walk, true);
            MovementRythm();
            staminaManager.SlowRestoreSpeed();

            Vector3 direction = Vector3.Normalize(new Vector3(movement.x, 0, movement.y));
            Vector3 newVelocity = rigidbody.transform.TransformDirection(direction) * speed * (acceleration / MOMENTUM_TIME);
            if (melee.Blocking) newVelocity *= BLOCK_SPEED_NERF;
            rigidbody.velocity = new Vector3(newVelocity.x, rbVelocity.y, newVelocity.z);
        }
    }

    private void Jump()
    {
        Vector3 velocity = rigidbody.velocity;
        velocity = new Vector3(velocity.x, 0, velocity.z);
        rigidbody.velocity = velocity;
        rigidbody.AddForce(0, jumpForce, 0, ForceMode.Impulse);
        grounded = false;
    }

    public void SetSpeed(float newSpeed)
    {
        speed = newSpeed;
    }

    public float GetSpeed()
    {
        return speed;
    }

    private void MovementRythm()
    {
        if (grounded)
        {
            float rate = Time.deltaTime;
            float volume = 1;
            if (melee.Blocking)
            {
                rate *= BLOCK_SPEED_NERF;
                volume = BLOCK_SPEED_NERF;
            }
            walkTimer -= rate;
            if (walkTimer < 0)
            {
                playerSFX.OnPlayerWalkSFX(volume);
                walkTimer = WALKING_RYTHM;
            }
        }
        else
        {
            walkTimer = 0f;
        }
    }
}