using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Harmony;

public class SceneLoadingManager : MonoBehaviour
{
    public static SceneLoadingManager Instance;
    
    [SerializeField] private Material skybox;
    [SerializeField] private Material bossSkybox;
    [SerializeField] private List<string> loadedScenes;
    [SerializeField] private List<string> currentlyLoadingScenes;

    private void Start()
    {
        Events.KeyPickedUpEvent += OnKeyPickedUp;
        loadedScenes = new List<string>();
        currentlyLoadingScenes = new List<string>();
        HandleLoading();
    }

    private void OnKeyPickedUp(ElementType elementType)
    {
        LoadScene(Scenes.RightHallway);
        LoadScene(Scenes.LeftHallway);
    }

    private void Awake()
    {
        if (Instance == null)
            Instance = this;

        else if (Instance != this)
            Destroy(gameObject);

        gameObject.DontDestroyOnLoad();
    }
    
    private void HandleLoading()
    {
        if (Finder.SaveManager.HasReachedBoss())
            LoadBossScene();
        else if (Finder.SaveManager.IsTutorialDone())
            LoadMainGame();
        else
            LoadTutorial();
    }

    public Coroutine LoadScene(string scene)
    {
        if (loadedScenes.Contains(scene)) return null;
        loadedScenes.Add(scene);
        if (!currentlyLoadingScenes.Contains(scene))
            return StartCoroutine(LoadSceneAsync(scene));
        return null;
    }

    private IEnumerator LoadSceneAsync(string scene)
    {
        currentlyLoadingScenes.Add(scene);
        yield return SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive);
        if (!loadedScenes.Contains(scene))
        {
            yield return SceneManager.UnloadSceneAsync(scene);
        }
        currentlyLoadingScenes.Remove(scene);
    }

    public void UnloadScene(string scene)
    {
        if (!loadedScenes.Contains(scene)) return;
        loadedScenes.Remove(scene);
        for (int i = 0; i < SceneManager.sceneCount; i++)
        {
            if (SceneManager.GetSceneAt(i).name == scene)
                SceneManager.UnloadSceneAsync(scene);
        }
    }

    public void ReloadScene(string scene)
    {
        IEnumerator Reload()
        {
            Finder.PlayerUI.ActivateLoadingScreen();
            Time.timeScale = 0;
            UnloadScene(Scenes.ChessScene);
            UnloadScene(Scenes.SkullScene);
            UnloadScene(Scenes.PressurePlateRoom);
            UnloadScene(Scenes.GhostRoom);
            UnloadScene(scene);
            yield return LoadScene(scene);
            Finder.PlayerUI.DeactivateLoadingScreen();
            Time.timeScale = 1;
        }
        StartCoroutine(Reload());
    }

    public void LoadBossScene()
    {
        IEnumerator Routine()
        {
            Time.timeScale = 0;
            Finder.PlayerUI.ActivateLoadingScreen();
            RenderSettings.skybox = bossSkybox;
            UnloadScene(Scenes.Hub);
            UnloadScene(Scenes.LeftHallway);
            UnloadScene(Scenes.RightHallway);
            yield return LoadScene(Scenes.BossRoom);
            Finder.PlayerUI.DeactivateLoadingScreen();
            Time.timeScale = 1;
        }

        StartCoroutine(Routine());
    }

    public void LoadMainGame()
    {
        IEnumerator Routine()
        {
            Time.timeScale = 0;
            Finder.PlayerUI.ActivateLoadingScreen();
            UnloadScene(Scenes.Tutoriel);
            RenderSettings.skybox = null;
            yield return LoadScene(Scenes.Hub);
            yield return LoadScene(Scenes.RightHallway);
            yield return LoadScene(Scenes.LeftHallway);
            Finder.PlayerUI.DeactivateLoadingScreen();
            Time.timeScale = 1;
        }

        StartCoroutine(Routine());
    }

    private void LoadTutorial()
    {
        IEnumerator Routine()
        {
            Time.timeScale = 0;
            Finder.PlayerUI.ActivateLoadingScreen();
            RenderSettings.skybox = skybox;
            yield return LoadScene(Scenes.Tutoriel);
            Finder.PlayerUI.DeactivateLoadingScreen();
            Time.timeScale = 1;
        }

        StartCoroutine(Routine());
    }

    private void OnDestroy()
    {
        Events.KeyPickedUpEvent -= OnKeyPickedUp;
    }
}
