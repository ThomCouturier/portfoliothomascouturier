using System.Collections.Generic;
using Harmony;
using UnityEngine;

public class LoadingTriggerController : MonoBehaviour
{
    [SerializeField] private List<SceneReference> scenesToLoad;
    [SerializeField] private bool loader;

    private void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.CompareTag(Tags.Player)) 
            return;
        if (loader)
            LoadScenes();
        else
            UnloadScenes();
    }
    
    private void LoadScenes()
    {
        foreach (SceneReference sceneReference in scenesToLoad)
        {
            SceneLoadingManager.Instance.LoadScene(sceneReference.Name);
        }
    }
    
    private void UnloadScenes()
    {
        foreach (SceneReference sceneReference in scenesToLoad)
        {
            SceneLoadingManager.Instance.UnloadScene(sceneReference.Name);
        }
    }
}
