using Harmony;
using UnityEngine;

public class RespawnTriggerController : MonoBehaviour
{
    [SerializeField] private bool defaultSpawn;
    [SerializeField] private SceneReference assignedScene;
    [SerializeField] private ElementType affectedKey;

    private void Start()
    {
        Events.KeyPickedUpEvent += OnKeyPickedUp;
        if(defaultSpawn)
            RespawnManager.Instance.SelectSpawnPoint(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == GameObjects.Player)
            RespawnManager.Instance.SelectSpawnPoint(gameObject);
    }

    public SceneReference GetAssignedScene()
    {
        return assignedScene;
    }
    
    private void OnKeyPickedUp(ElementType elementType)
    {
        if(affectedKey.Equals(elementType))
            gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        Events.KeyPickedUpEvent -= OnKeyPickedUp;
    }
}
