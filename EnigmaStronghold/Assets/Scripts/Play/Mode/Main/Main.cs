using System.Collections;
using Harmony;
using UnityEngine;

namespace Game
{
    [Findable(Tags.MainController)]
    public class Main : MonoBehaviour
    {
        [Header("Scenes")] [SerializeField] private SceneBundle homeScenes;
        [SerializeField] private SceneBundle gameScenes;

        private SceneBundleLoader loader;

        private void Awake()
        {
            loader = Finder.SceneBundleLoader;
        }

        private IEnumerator Start()
        {
#if UNITY_EDITOR
            if (gameScenes.IsLoaded)
                yield return loader.Load(gameScenes);
            else
#endif
                yield return loader.Load(homeScenes);
        }

        public Coroutine LoadHomeScenes()
        {
            return loader.Load(homeScenes);
        }
        
        public Coroutine UnloadHomeScenes()
        {
            return loader.Unload(homeScenes);
        }

        public Coroutine LoadGameScenes()
        {
            return loader.Load(gameScenes);
        }
        
        public Coroutine UnloadGameScenes()
        {
            return loader.Unload(homeScenes);
        }
    }
}