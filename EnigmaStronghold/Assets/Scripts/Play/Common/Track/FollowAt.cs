using UnityEngine;

public class FollowAt : MonoBehaviour
{
    [SerializeField] private Vector3 target;
    [SerializeField] private Vector3 rotationOffset;
    private Vector3 initialRotation;

    private void Awake()
    {
        initialRotation = transform.eulerAngles;
    }

    public void LookAt(Vector3 position)
    {
        this.target = position;
    }

    public void Reset()
    {
        transform.eulerAngles = initialRotation;
        transform.Rotate(rotationOffset);
    }

    void Update()
    {
        if (target == null) return;
        transform.LookAt(target); 
        transform.Rotate(rotationOffset);
    }
}
