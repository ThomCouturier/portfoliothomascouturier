using System;
using Harmony;
using UnityEngine;

namespace Game
{
    public sealed class Stimuli : MonoBehaviour
    {
        [SerializeField] private Shape shape = Shape.Sphere;
        [SerializeField] [Range(0.01f, 100)] private float radius = 1f;
        [SerializeField] [Range(0.01f, 100)] private float height = 1f;

        public event StimuliEventHandler OnDestroyed;

        private void Awake()
        {
            CreateCollider();
            SetSensorLayer();
        }

        private void OnDisable()
        {
            NotifyDestroyed();
        }

        private void CreateCollider()
        {
            switch (shape)
            {
                case Shape.Cube:
                    BoxCollider cubeCollider = gameObject.AddComponent<BoxCollider>();
                    cubeCollider.isTrigger = true;
                    cubeCollider.size = Vector3.one * radius;
                    break;
                case Shape.Sphere:
                    SphereCollider sphereCollider = gameObject.AddComponent<SphereCollider>();
                    sphereCollider.isTrigger = true;
                    sphereCollider.radius = radius / 2;
                    break;
                case Shape.Capsule:
                    CapsuleCollider capsuleCollider = gameObject.AddComponent<CapsuleCollider>();
                    capsuleCollider.isTrigger = true;
                    capsuleCollider.radius = radius / 2;
                    capsuleCollider.height = height;
                    break;
                default:
                    throw new Exception("Unknown shape named \"" + shape + "\".");
            }
        }

        private void SetSensorLayer()
        {
            gameObject.layer = Layers.Sensor.Index;
        }

        private void NotifyDestroyed()
        {
            if (OnDestroyed != null) OnDestroyed(transform.parent.gameObject);
        }

        private enum Shape
        {
            Cube,
            Sphere,
            Capsule
        }
    }

    public delegate void StimuliEventHandler(GameObject otherObject);
}