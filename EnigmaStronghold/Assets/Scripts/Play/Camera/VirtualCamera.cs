using Harmony;
using UnityEngine;

namespace Game
{
    [Findable(Tags.GameCamera)]
    public class VirtualCamera : MonoBehaviour
    {
        private new Camera camera;

        private Vector3 position;
        private Quaternion rotation;
        private bool isOrthographic;
        private float orthographicSize;
        private bool isDirty;

        public Vector3 Position
        {
            get => position;
            set
            {
                position = value;
                isDirty = true;
            }
        }

        public Quaternion Rotation
        {
            get => rotation;
            set
            {
                rotation = value;
                isDirty = true;
            }
        }

        public bool IsOrthographic
        {
            get => isOrthographic;
            set
            {
                isOrthographic = value;
                isDirty = true;
            }
        }

        public float OrthographicSize
        {
            get => orthographicSize;
            set
            {
                orthographicSize = value;
                isDirty = true;
            }
        }

        private void Awake()
        {
            camera = Camera.main;
        }

        private void LateUpdate()
        {
            if (!camera || !isDirty) return; //Skip if not dirty or destroyed.

            var cameraTransform = camera.transform;
            cameraTransform.position = position;
            cameraTransform.rotation = rotation;
            camera.orthographic = isOrthographic;
            camera.orthographicSize = orthographicSize;

            isDirty = false;
        }

        public Vector2 WorldToViewport(Vector3 position)
        {
            return camera.WorldToViewportPoint(position);
        }
        
        public Vector3 ViewportToWorld(Vector2 position)
        {
            return camera.ViewportToWorld(position);
        }
    }
}