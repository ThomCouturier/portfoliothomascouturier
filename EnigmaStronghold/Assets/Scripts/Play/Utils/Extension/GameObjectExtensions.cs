using Harmony;
using UnityEngine;


public static class EntityAIExtensions
    {
    /// <summary>
    ///  Search for the T component in parent and first sons only
    /// </summary>
    public static T FindComponentInDirectChildren<T>(this GameObject parent) where T : class
    {
        T sonComponent = parent.GetComponent<T>();
        if (sonComponent != null) return sonComponent;

        foreach (GameObject gameObject in parent.Children())
        {
            sonComponent = gameObject.GetComponent<T>();
            if (sonComponent != null) return sonComponent;
        }

        return null;
    }
}