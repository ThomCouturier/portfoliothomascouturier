using DG.Tweening;
using Harmony;
using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class WoodBox : MonoBehaviour, ISlashable
{
    private const float BREAK_PUSH_FORCE = 8;
    private const float BREAK_JUMP_FORCE = 1.5f;
    private const int DAMAGE = 1;
    private const float DESPAWN_TIME = 4.5f;
    private const float DESPAWN_SCALE = 0.01f;
    private const float SHAKE_TIME = 0.15f;
    private const float SHAKE_FORCE = 10f; // angle

    [SerializeField] private int maxHealth = 2;
    [SerializeField] private float potionDropChance = 0.5f;

    private WoodSFX sfx;
    private Rigidbody[] rigidbodies;
    public Collider masterCollider;
    private Vector3 initialRotation;
    private int life;
    private bool destroyed;

    private void Awake()
    {
        sfx = GetComponent<WoodSFX>();
        masterCollider = GetComponent<Collider>();
        rigidbodies = GetComponentsInChildren<Rigidbody>();
        Rigidbody masterRigidbody = GetComponent<Rigidbody>();
        rigidbodies = rigidbodies.Where(rigidBody => (masterRigidbody != rigidBody)).ToArray();
        initialRotation = transform.eulerAngles;
        life = maxHealth;
        destroyed = false;
    }

    private IEnumerator Despawn()
    {
        Array.ForEach(rigidbodies, rigidbody => rigidbody.gameObject.transform.DOScale(Vector3.one * DESPAWN_SCALE, DESPAWN_TIME));
        float randomChance = Random.Range(0.0f, 1.0f);
        if (randomChance < potionDropChance)
        {
            PotionSpawner.Instance.Spawn(transform.position);
        }
        yield return new WaitForSeconds(DESPAWN_TIME);
        Destroy(gameObject);
    }

    private IEnumerator Shake()
    {
        float xAngle = Random.Range(0.0f, 1.0f);
        float yAngle = Random.Range(0.0f, 1.0f);
        float zAngle = Random.Range(0.0f, 1.0f);

        Vector3 direction = new Vector3(xAngle, yAngle, zAngle).normalized;
        Vector3 force = (direction * SHAKE_FORCE);

        transform.DORotate(initialRotation + force, SHAKE_TIME);
        yield return new WaitForSeconds(SHAKE_TIME);
        transform.DORotate(initialRotation - force, SHAKE_TIME);
        yield return new WaitForSeconds(SHAKE_TIME);
        transform.DORotate(initialRotation, SHAKE_TIME);
    }

    public void Slash(int damage, GameObject sender)
    {
        if (destroyed) return;
        life = Mathf.Max(0, life - DAMAGE);
        if (life == 0)
        {
            sfx.PlayWoodBreakSFX();
            destroyed = true;
            masterCollider.enabled = false;
            Array.ForEach(rigidbodies, rigidbody => {
                rigidbody.isKinematic = false;
                rigidbody.velocity = (Vector3.up * BREAK_JUMP_FORCE) + (transform.position.DirectionTo(rigidbody.transform.position) * BREAK_PUSH_FORCE);
            });
            StartCoroutine(Despawn());
        }
        else
        {
            StartCoroutine(Shake());
        }
    }
}
