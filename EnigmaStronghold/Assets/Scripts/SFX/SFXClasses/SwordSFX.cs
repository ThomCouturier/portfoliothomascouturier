using UnityEngine;

public class SwordSFX : MonoBehaviour
{
    [SerializeField] public float impactVolume = 0.4f;

    private AudioSource audioSource;
    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void OnSwordImpactSFX()
    {
        audioSource.PlayOneShot(SoundManager.SoundInstance.ImpactSFX, impactVolume);
    }

    public void OnSwordSwingSFX()
    {
        audioSource.PlayOneShot(SoundManager.SoundInstance.SwingSFX);
    }
}
