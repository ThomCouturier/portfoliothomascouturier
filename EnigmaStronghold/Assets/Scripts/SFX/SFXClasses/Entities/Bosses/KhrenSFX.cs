using UnityEngine;

public class KhrenSFX : MiniBossSFX, IBossesSFX
{
    private AudioSource audioSource;
    private void Awake()
    {
        audioSource = GetComponentInParent<AudioSource>();
    }


    public override void OnDeathSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.KhrenDeathSFX);
        }
    }

    public override void PlaySpeech()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.KhrenSpeech);
        }
    }

    public override void OnMoveSFX()
    {
        audioSource.PlayOneShot(SoundManager.SoundInstance.DragonWingFlapSFX);
    }

    public override void OnAttackPreparationSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.PreparingAttackSFX);
        }
    }
}
