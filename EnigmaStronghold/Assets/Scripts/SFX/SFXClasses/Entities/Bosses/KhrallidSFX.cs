using UnityEngine;

public class KhrallidSFX : MiniBossSFX,  IBossesSFX
{
    private AudioSource audioSource;
    private void Awake()
    {
        audioSource = GetComponentInParent<AudioSource>();
    }

    public override void OnDeathSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.KhrallidDeathSFX);
        }
    }

    public override void PlaySpeech()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.KhrallidSpeech);
        }
    }

    public override void OnMoveSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.DragonWalkSFX);
        }
    }

    public override void OnAttackPreparationSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.PreparingAttackSFX);
        }
    }
}
