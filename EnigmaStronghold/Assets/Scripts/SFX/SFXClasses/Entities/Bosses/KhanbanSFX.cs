using UnityEngine;

public class KhanbanSFX : MonoBehaviour, IBossesSFX
{
    [SerializeField] private float wingFlapVolume;
    private AudioSource audioSource;
    public static KhanbanSFX Instance;


    private void Awake()
    {
        if (Instance == null)
            Instance = this;

        else if (Instance != this)
            Destroy(gameObject);

        audioSource = GetComponent<AudioSource>();
    }

    public void OnDeathSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.KhanbanDeathSFX);
        }
    }

    public void OnHurtSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.KhanbanHurtSFX);
        }
    }

    public void PlaySpeech()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.KhanbanSpeech);
        }
    }

    public void PlayMidFightSpeech()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.KhanbanMidFightSpeech);
        }
    }

    public void OnMoveSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.DragonWingFlapSFX, wingFlapVolume);
        }
    }

    public void OnAttackLaunchedSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.AttackTravellingSFX);
        }
    }

    public void OnAttackPreparationSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.PreparingAttackSFX);
        }
    }
}
