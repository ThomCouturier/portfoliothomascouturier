using UnityEngine;

public abstract class MiniBossSFX : MonoBehaviour
{
    public abstract void OnDeathSFX();
    public abstract void PlaySpeech();
    public abstract void OnMoveSFX();
    public abstract void OnAttackPreparationSFX();
}
