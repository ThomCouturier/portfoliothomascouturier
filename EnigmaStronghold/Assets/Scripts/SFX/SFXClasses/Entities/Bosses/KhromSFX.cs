using UnityEngine;

public class KhromSFX : MiniBossSFX, IBossesSFX
{
    private AudioSource audioSource;
    private void Awake()
    {
        audioSource = GetComponentInParent<AudioSource>();
    }

    public override void OnDeathSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.KhromDeathSFX);
        }
    }

    public override void PlaySpeech()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.KhromSpeech);
        }
    }

    public override void OnMoveSFX()
    {
        audioSource.PlayOneShot(SoundManager.SoundInstance.DragonWalkSFX);
    }

    public override void OnAttackPreparationSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.PreparingAttackSFX);
        }
    }
}
