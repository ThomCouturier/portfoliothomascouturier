using UnityEngine;

public class GhostSFX : MonoBehaviour
{
    private AudioSource audioSource;
    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlayGhostSpeech()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.GhostSpeech);
        }
    }

    public void OnGhostHurt()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.GhostHurtSFX);
        }
    }

    public void OnGhostRespawn()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.SkullRespawnSFX);
        }
    }
}
