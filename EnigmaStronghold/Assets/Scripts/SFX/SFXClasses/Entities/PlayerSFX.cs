using UnityEngine;

public class PlayerSFX : MonoBehaviour
{
    [SerializeField] public float movementVolume = 0.4f;
    [SerializeField] public float playerAttackVolume = 0.4f;

    private AudioSource audioSource;
    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void OnPlayerHurtSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.PlayerHurtSFX);
        }
    }

    public void OnPlayerWalkSFX(float speed = 1)
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.PlayerFootstepsSFX, movementVolume * speed);
        }
    }

    public void OnPlayerDeathSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.PlayerDeathSFX);
        }
    }

    public void OnPlayerAttackSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.PlayerAttackSFX, playerAttackVolume);
        }
    }

    public void OnPotionDrank()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.PotionDrink);
        }
    }

    public void PlayGhostInteractionSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.PlayerGhostInteraction);
        }
    }

    public void OnPendantInteraction()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.ItemPickedUpSFX);
        }
    }
}
