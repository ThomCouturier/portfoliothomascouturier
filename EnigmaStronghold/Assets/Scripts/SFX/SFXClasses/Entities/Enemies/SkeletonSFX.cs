using System.Collections;
using UnityEngine;

public class SkeletonSFX : MonoBehaviour, IEnemySFX
{
    [SerializeField] public float movementVolume = 0.05f;
    [SerializeField] public float attackVolume = 0.7f;

    private const float DELAY = 0.5f;
    private AudioSource audioSource;
    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }
    public void OnAttackSFX()
    {
        if (audioSource != null)
        {
            StartCoroutine(AttackDelay());
        }
    }

    public void OnMovementSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.SkeletonWalkSFX, movementVolume);
        }
    }

    public void OnDeathSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.SkeletonDeathSFX);
        }
    }
    private IEnumerator AttackDelay()
    {
        yield return new WaitForSecondsRealtime(DELAY);
        audioSource.PlayOneShot(SoundManager.SoundInstance.SkeletonAttackSFX, attackVolume);
        StopCoroutine(AttackDelay());
    }
}
