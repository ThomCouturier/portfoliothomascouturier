using System.Collections;
using UnityEngine;

public class StoneKnightSFX : MonoBehaviour, IEnemySFX
{
    [SerializeField] public float movementVolume = 0.5f;
    [SerializeField] public float deathVolume = 0.9f;
    [SerializeField] public float attackVolume = 0.7f;

    private const float DELAY = 0.5f;
    private AudioSource audioSource;
    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }
    public void OnAttackSFX()
    {
        if (audioSource != null)
        {
            StartCoroutine(AttackDelay());
        }
    }

    public void OnMovementSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.KnightWalkSFX, movementVolume);
        }
    }

    public void OnDeathSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.KnightDeathSFX, deathVolume);
        }
    }

    public void OnWhirlwindSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.KnightWhirlwindSFX);
        }
    }

    public void PlaySpeech()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.KnightSpeech);
        }
    }

    private IEnumerator AttackDelay()
    {
        yield return new WaitForSecondsRealtime(DELAY);
        audioSource.PlayOneShot(SoundManager.SoundInstance.KnightAttackSFX, attackVolume);
        StopCoroutine(AttackDelay());
    }
}
