using UnityEngine;

public class MetalDoorSFX : MonoBehaviour
{
    [SerializeField] public float metalDoorOpenVolume = 0.2f;
    private AudioSource audioSource;
    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }
    public void OnMetalDoorOpenSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.MetalDoorOpenSFX, metalDoorOpenVolume);
        }
    }

    public void OnMetalDoorCloseSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.MetalDoorCloseSFX, metalDoorOpenVolume);
        }
    }
}
