using UnityEngine;

public class FireSpecialAttackSFX : MonoBehaviour, ISpecialAttackSFX
{
    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponentInParent<AudioSource>();
    }

    public void PlaySpecialSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.FireSpecialAttackSFX);
        }
    }
}
