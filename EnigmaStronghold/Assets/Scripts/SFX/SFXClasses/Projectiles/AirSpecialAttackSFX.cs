using UnityEngine;

public class AirSpecialAttackSFX : MonoBehaviour, ISpecialAttackSFX
{
    private AudioSource audioSource;
    private void Awake()
    {
        audioSource = transform.parent.GetComponent<AudioSource>();
    }

    public void PlaySpecialSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.AirSpecialAttackSFX);
        }
    }
}
