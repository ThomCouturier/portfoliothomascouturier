using UnityEngine;

public class PoisonSpecialAttackSFX : MonoBehaviour, ISpecialAttackSFX
{
    private AudioSource audioSource;
    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlaySpecialSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.PoisonSpecialAttackSFX);
        }
    }
}
