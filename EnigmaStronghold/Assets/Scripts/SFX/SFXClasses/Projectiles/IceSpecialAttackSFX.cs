using UnityEngine;

public class IceSpecialAttackSFX : MonoBehaviour, ISpecialAttackSFX
{
    private AudioSource audioSource;
    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlaySpecialSpawnSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.IceAttackSpawnSFX, 0.2f);
        }
    }

    public void PlaySpecialSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.IceSpecialAttackSFX, 0.2f);
        }
    }
}
