using UnityEngine;

public class ProjectilesSFX : MonoBehaviour
{
    [SerializeField] public float IMPACT_VOLUME = 100.0f;
    private AudioSource audioSource;
    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void OnProjectileDeflectedSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.ProjectileDeflectedSFX);
        }
    }

    public void PlayTravelSFX()
    {
        audioSource.Play();
    }

    public void StopTravelSFX()
    {
        audioSource.Stop();
    }
}
