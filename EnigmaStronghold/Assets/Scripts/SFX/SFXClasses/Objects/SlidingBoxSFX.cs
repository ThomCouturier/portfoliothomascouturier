using UnityEngine;

public class SlidingBoxSFX : MonoBehaviour
{
    [SerializeField] public float boxSlidingVolume = 0.2f;
    private AudioSource audioSource;
    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlaySlidingSound()
    {
        audioSource.Play();
    }

    public void StopSlidingSound()
    {
        audioSource.Stop();
    }

    public void OnBoxInPlaceSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.BoxPlacedSFX, boxSlidingVolume);
        }
    }
}
