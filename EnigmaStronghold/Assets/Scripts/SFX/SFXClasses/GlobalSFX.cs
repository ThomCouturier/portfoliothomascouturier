using UnityEngine;

public class GlobalSFX : MonoBehaviour
{
    public static GlobalSFX Instance;
    private AudioSource audioSource;
    private void Awake()
    {
        if (Instance == null)
            Instance = this;

        else if (Instance != this)
            Destroy(gameObject);

        gameObject.DontDestroyOnLoad();

        audioSource = transform.parent.GetComponent<AudioSource>();
    }

    public void SetGlobalAudioSource(AudioClip clip)
    {
        if (audioSource != null)
        {
            audioSource.clip = clip;
        }
    }

    public void PlayGlobalAudioSource()
    {
        if (audioSource != null)
        {
            audioSource.Play();
        }
    }
    public void StopGlobalAudioSource()
    {
        if (audioSource != null)
        {
            if (audioSource.isPlaying)
            {
                audioSource.Stop();
            }
        }
    }

    public void OnKeyObtainedSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.ObtainedKeySFX);
        }
    }

    public void OnItemPickedUpSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.ItemPickedUpSFX);
        }
    }

    public void OnPuzzleSolvedSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.PuzzleSolvedSFX);
        }
    }

    public void OnAchievementUnlocked()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.AchievementObtained);
        }
    }

    public void OnPlayerDeath()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.DeathBell);
        }
    }

    public void OnProjectileImpactSFX(ElementType type)
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.IceAttackSFX);
            switch (type)
            {
                case ElementType.Fire:
                    audioSource.PlayOneShot(SoundManager.SoundInstance.FireAttackSFX);
                    break;
                case ElementType.Ice:
                    audioSource.PlayOneShot(SoundManager.SoundInstance.IceAttackSFX);
                    break;
                case ElementType.Air:
                    audioSource.PlayOneShot(SoundManager.SoundInstance.AirAttackSFX);
                    break;
                case ElementType.Poison:
                    audioSource.PlayOneShot(SoundManager.SoundInstance.PoisonAttackSFX);
                    break;
                case ElementType.Obsidian:
                    audioSource.PlayOneShot(SoundManager.SoundInstance.ObsidianAttackSFX);
                    break;
            }
        }
    }
}
