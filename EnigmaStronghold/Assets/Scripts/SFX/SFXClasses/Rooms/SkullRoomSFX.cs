using System.Collections;
using UnityEngine;

public class SkullRoomSFX : MonoBehaviour
{
    [SerializeField] public float rightSkullPlacementVolume = 0.5f;

    private const float MAX_VOLUME = 0.4f;
    private const float VOLUME_FADE = 0.05f;
    private const int VOLUME_FADE_TIMER = 1;
    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlayGlobalSound()
    {
        if (audioSource != null)
        {
            audioSource.volume = 0.0f;
            audioSource.Play();
            StartCoroutine(FadeIn());
        }
    }

    public void StopGlobalSound()
    {
        if (audioSource != null)
        {
            StartCoroutine(FadeOut());
        }
    }

    public void OnRightSkullPlacementSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.RightSkullPlacementSFX, rightSkullPlacementVolume);
        }
    }

    public void OnWrongSkullPlacementSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.WrongSkullPlacementSFX);
        }
    }

    public void OnSkullRespawnSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.SkullRespawnSFX);
        }
    }

    private IEnumerator FadeIn()
    {
        while (audioSource.volume <= MAX_VOLUME)
        {
            audioSource.volume += VOLUME_FADE;
            yield return new WaitForSecondsRealtime(VOLUME_FADE_TIMER);
        }
    }

    private IEnumerator FadeOut()
    {
        while (audioSource.volume > 0.0f)
        {
            audioSource.volume -= VOLUME_FADE;
            yield return new WaitForSecondsRealtime(VOLUME_FADE_TIMER);
        }
        audioSource.Stop();
        audioSource.volume = MAX_VOLUME;
    }
}
