using Harmony;
using UnityEngine;

public class TutorialSFX : MonoBehaviour
{
    public static TutorialSFX TutorialInstance;
    private AudioSource audioSource;
    private void Awake()
    {
        if (TutorialInstance == null)
            TutorialInstance = this;

        else if (TutorialInstance != this)
            Destroy(gameObject);

        audioSource = Finder.Player.GetComponent<AudioSource>();
    }

    public void OnPotatoLauncherActivatedSFX()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.PotatoLauncherSFX);
        }
    }

    public void PlayStepCompleted()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.AchievementObtained);
        }
    }

    public void PlayTutorialCompleted()
    {
        if (audioSource != null)
        {
            audioSource.PlayOneShot(SoundManager.SoundInstance.PuzzleSolvedSFX);
        }
    }

}
