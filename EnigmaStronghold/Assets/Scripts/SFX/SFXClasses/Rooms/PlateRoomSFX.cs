using UnityEngine;

public class PlateRoomSFX : MonoBehaviour
{
    private AudioSource audioSource;
    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }
    public void PlayElevatorRise()
    {
        audioSource.Play();
    }

    public void StopElevatorRise()
    {
        if (audioSource != null)
        {
            audioSource.Stop();
            OnElevatorStop();
        }
    }

    public void OnElevatorStop()
    {
        audioSource.PlayOneShot(SoundManager.SoundInstance.ElevatorArrivedSFX);
    }

}
