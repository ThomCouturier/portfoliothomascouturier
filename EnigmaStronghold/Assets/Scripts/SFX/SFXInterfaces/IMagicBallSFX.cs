public interface IMagicBallSFX
{
    void OnProjectileLaunchedSFX();
    void OnProjectileImpactSFX();
    void OnProjectileDeflectedSFX();
}
