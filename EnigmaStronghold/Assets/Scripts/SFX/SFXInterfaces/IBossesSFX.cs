public interface IBossesSFX
{
    void PlaySpeech();

    void OnAttackPreparationSFX();
    void OnDeathSFX();
    void OnMoveSFX();
}
