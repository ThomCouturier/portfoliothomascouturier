public interface IEnemySFX
{
    void OnMovementSFX();
    void OnDeathSFX();
}
