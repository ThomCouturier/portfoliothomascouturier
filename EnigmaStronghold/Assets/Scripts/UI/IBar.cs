public interface IUIBar
{
    public float GetMax();

    public float GetCurrent();
}
