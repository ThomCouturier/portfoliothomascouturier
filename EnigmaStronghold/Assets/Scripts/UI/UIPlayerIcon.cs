using Harmony;
using System;
using UnityEngine;
using UnityEngine.UI;

public class UIPlayerIcon : MonoBehaviour
{
    [SerializeField] private float speed;

    private GameObject playerIcon;
    private GameObject deadPlayerIcon;

    private Player player;

    private float currentPercentage;
    private float percentage;

    void Start()
    {
        player = GameObject.Find(GameObjects.Player).GetComponent<Player>();
        playerIcon = GameObject.Find(GameObjects.PlayerIcon).gameObject;
        deadPlayerIcon = GameObject.Find(GameObjects.DeadPlayerIcon).gameObject;
        currentPercentage = 1;
    }

    void Update()
    {
        percentage = player.GetCurrent() / player.GetMaxHealth();

        if (speed == 0)
        {
            playerIcon.GetComponent<Image>().color = new Color(1, 1, 1, percentage);
            deadPlayerIcon.GetComponent<Image>().color = new Color(1, 1, 1, 1 - percentage);
            return;
        }

        if (percentage < currentPercentage)
        {
            currentPercentage = Math.Max(percentage, currentPercentage - speed);
        }
        else if (percentage > currentPercentage)
        {
            currentPercentage = Math.Min(percentage, currentPercentage + speed);
        }

        playerIcon.GetComponent<Image>().color = new Color(1, 1, 1, currentPercentage);
        deadPlayerIcon.GetComponent<Image>().color = new Color(1, 1, 1, 1 - currentPercentage);
    }
}
