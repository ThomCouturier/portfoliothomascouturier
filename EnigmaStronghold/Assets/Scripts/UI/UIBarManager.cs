using System.Collections;
using DG.Tweening;
using Harmony;
using UnityEngine;
using UnityEngine.UI;

public class UIBarManager : MonoBehaviour
{
    private const float BAR_OFFSET = 1f;
    
    [SerializeField] private GameObject data;
    [SerializeField] private float duration;
    private Image primaryBar;
    private Image secondaryBar;
    private IUIBar iBar;
    private float percentage;

    private void Start()
    {
        primaryBar = transform.Find(GameObjects.PrimaryBar).GetComponent<Image>();
        secondaryBar = transform.Find(GameObjects.SecondaryBar).GetComponent<Image>();
        if(data != null)
            iBar = data.GetComponent<IUIBar>();
    }

    private void Update()
    {
        if(iBar == null) return;
        if (Time.timeScale != 0 || !(iBar.GetCurrent() <= 0)) return;
        
        primaryBar.fillAmount = 0;
        secondaryBar.fillAmount = 0;
    }

    private void FixedUpdate()
    {
        if(iBar == null || !gameObject.activeSelf) return;
        percentage = iBar.GetCurrent() / iBar.GetMax();
        primaryBar.fillAmount = percentage;
        
        if(duration == 0)
        {
            secondaryBar.fillAmount = percentage;
            return;
        }

        IEnumerator Cooldown()
        {
            yield return new WaitForSeconds(BAR_OFFSET);
        }

        StartCoroutine(Cooldown());

        secondaryBar.DOFillAmount(percentage, duration);
    }
    
    public void SetData(GameObject data)
    {
        this.data = data;
        iBar = data.GetComponent<IUIBar>();
    }
}
