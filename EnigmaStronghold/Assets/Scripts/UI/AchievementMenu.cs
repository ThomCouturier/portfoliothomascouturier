using System.Linq;
using TMPro;
using UnityEngine;

public class AchievementMenu : MonoBehaviour
{
    private Color doneColor = new Color32(0, 255, 0, 255);
    private Color notDoneColor = new Color32(175, 175, 175, 150);

    private TextMeshProUGUI[] achievements;

    private void Awake()
    {
        achievements = transform.GetChild(0).GetComponentsInChildren<TextMeshProUGUI>();
        AchievementsUIManager.GenerateDict();
        for (int i = 0; i < achievements.Length; i++)
        {
            string name = AchievementsUIManager.achievementTexts.ElementAt(i).Value;
            achievements[i].name = name;
            achievements[i].text = name;
        }
    }

    public void SetAchievementState(SaveObject save)
    {
        foreach (TextMeshProUGUI text in achievements)
        {
            if (IsAchievementUnlocked(text.text, save))
            {
                text.color = doneColor;
            }
            else
            {
                text.color = notDoneColor;
            }
        }
    }

    private bool IsAchievementUnlocked(string text, SaveObject save)
    {
        switch (text)
        {
            case AchievementsUIManager.MAGNUS_CARLSEN_TEXT:
                return save.MagnusCarlsenDone;
            case AchievementsUIManager.INVISIBLE_FRIEND_TEXT:
                return save.InvisibleFriendDone;
            case AchievementsUIManager.DIAGNOSTIC_NOT_COLOR_BLIND_TEXT:
                return save.DiagnosticNotColorBlindDone;
            case AchievementsUIManager.NO_PRESSURE_TEXT:
                return save.NoPressureDone;
            case AchievementsUIManager.ICE_TO_MEET_YOU_TEXT:
                return save.IceToMeetYouDone;
            case AchievementsUIManager.PEPTO_BISMOL_TEXT:
                return save.PeptoBismolDone;
            case AchievementsUIManager.WINDYS_TEXT:
                return save.WindysDone;
            case AchievementsUIManager.BLAST_FLAME_OUS_TEXT:
                return save.BlastFlameOusDone;
            case AchievementsUIManager.JANITOR_TEXT:
                return save.JanitorDone;
            case AchievementsUIManager.YOU_SLAYIN_TEXT:
                return save.YouSlayinDone;
            case AchievementsUIManager.NO_HEAL_TEXT:
                return save.NoHealDone;
            case AchievementsUIManager.DOOM_GUY_TEXT:
                return save.DoomGuyDone;
            default:
                return false;
        }
    }
}
