using Harmony;
using UnityEngine;

public class TutorialSteps : MonoBehaviour
{
    private const string MOVEMENT_STEP_COMPLETED_TXT = "Williaume! Use the space bar on your keyboard to jump over the broken fence!";
    private const string JUMP_STEP_COMPLETED_TXT = "Good! Now, pick up the diamond in the well's bucket by pressing the E key on your keyboard!";
    private const string PICKUP_STEP_COMPLETED_TXT = "Perfect! Now that you can fight, slash the dummy three times by pressing the left mouse button!";
    private const string ATTACK_STEP_COMPLETED_TXT = "Great! Now go break the box in front of you to heal yourself with the potion inside! Heal by pressing B";
    private const string HEAL_STEP_COMPLETED_TXT = "Watch out! Block the incoming potatoes three times by pressing the right mouse button!";
    private const string BLOCK_STEP_COMPLETED_TXT = "Watch out! Reflect the incoming potatoes three times by pressing the left mouse button!";
    private const string SUCCESS_TXT = "Congratulations, Knight! You have completed the tutorial! Head to the end of the cave to start game!";
    
    private PlayerUI playerUI;

    void Start()
    {
        playerUI = GameObject.Find(GameObjects.PlayerUI).GetComponent<PlayerUI>();

        Events.TutorialStepCompletedEvent += OnStepCompleted;
        Events.CompletedTutorialEvent += OnTutorialCompletion;
    }

    private void OnStepCompleted(TutorialStepType stepType)
    {
        switch (stepType)
        {
            case TutorialStepType.Movement:
                playerUI.SetTaskText(MOVEMENT_STEP_COMPLETED_TXT);
                break;
            case TutorialStepType.Jump:
                playerUI.SetTaskText(JUMP_STEP_COMPLETED_TXT);
                break;
            case TutorialStepType.Pickup:
                playerUI.SetTaskText(PICKUP_STEP_COMPLETED_TXT);
                break;
            case TutorialStepType.Attack:
                playerUI.SetTaskText(ATTACK_STEP_COMPLETED_TXT);
                break;
            case TutorialStepType.Block:
                playerUI.SetTaskText(BLOCK_STEP_COMPLETED_TXT);
                break;
            case TutorialStepType.Heal:
                playerUI.SetTaskText(HEAL_STEP_COMPLETED_TXT);
                break;
        }
    }

    private void OnTutorialCompletion()
    {
        playerUI.SetTaskText(SUCCESS_TXT);
    }

    private void OnDestroy()
    {
        Events.TutorialStepCompletedEvent -= OnStepCompleted;
        Events.CompletedTutorialEvent -= OnTutorialCompletion;
    }
}
