using Harmony;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class AudioSlider : MonoBehaviour
{
    [SerializeField] private AudioMixer mixer;
    [SerializeField] private AudioSource source;

    public void OnChangeSlider(float value)
    {
        mixer.SetFloat("Volume", Mathf.Log10(value) * 20);
    }

    private void Start()
    {
        UserSettingsManager userSettings = GameObject.Find(GameObjects.UserSettings).GetComponent<UserSettingsManager>();
        Slider slider = gameObject.GetComponent<Slider>();
        slider.value = userSettings.GetVolume();
        transform.parent.gameObject.SetActive(false);
    }
}
