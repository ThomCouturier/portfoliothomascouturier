using Harmony;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[Findable(Tags.PlayerUI)]
public class PlayerUI : MonoBehaviour
{
    [SerializeField] public List<Sprite> potionsSprites;
    
    private const string HINT_TITLE = "Hint";
    private const string NO_CURRENT_TASK = "There is no current objective. Enter one of the enigmas to find one.";
    private const string SOUL_CONNECTION = "Soul Connection";
    private const string SOUL_CONNECTION_HINT = "The maze's prisoner will now follow your every move when the pendant is equipped. Press R to equip/unequip it.";

    private const float POP_UP_TIME = 2f;
    private const float HINT_DURATION = 4.5f;
    private const int MAX_POTIONS_QUARTER_FULL = 2;
    private const int MAX_POTIONS_HALF_FULL = 6;
    private const int MAX_POTIONS_THREE_QUARTER_FULL = 8;
    private readonly Vector2 HintSizeTutorial = new Vector2(200, 50);
    private readonly Vector2 HintSizeGame = new Vector2(400, 50);
    
    private GameObject victoryMenu;
    private GameObject defeatMenu;
    private GameObject pauseMenu;
    private GameObject keybindsMenu;
    private GameObject popUp;
    private TextMeshProUGUI taskLabelText;
    private TextMeshProUGUI taskText;
    private TextMeshProUGUI potionCountText;
    private GameObject taskMenu;
    private GameObject taskIcon;
    private Image potionImage;
    private GameObject ghostRespawnHint;
    private GameObject ghostTooFarHint;
    private GameObject crosshair;
    private GameObject pickupCrosshair;

    private GameObject loadingScreen;

    private bool popUpActive;

    private void Init()
    {
        victoryMenu = transform.Find(GameObjects.Victory).gameObject;
        defeatMenu = transform.Find(GameObjects.Defeat).gameObject;
        pauseMenu = transform.Find(GameObjects.Pause).gameObject;
        keybindsMenu = transform.Find(GameObjects.KeybindsMenu).gameObject;
        popUp = transform.Find(GameObjects.CannotEnterMessage).gameObject;
        taskText = GameObject.Find(GameObjects.TaskText).GetComponent<TextMeshProUGUI>();
        taskLabelText = GameObject.Find(GameObjects.TaskLabelText).GetComponent<TextMeshProUGUI>();
        potionCountText = GameObject.Find(GameObjects.PotionText).GetComponent<TextMeshProUGUI>();
        taskMenu = transform.Find(GameObjects.TaskMenu).gameObject;
        taskIcon = transform.Find(GameObjects.TaskIcon).gameObject;
        potionImage = GameObject.Find(GameObjects.PotionsIcon).GetComponent<Image>();
        ghostRespawnHint = transform.Find(GameObjects.GhostRespawnHint).gameObject;
        ghostTooFarHint = transform.Find(GameObjects.GhostTooFarHint).gameObject;
        crosshair = transform.Find(GameObjects.Crosshair).gameObject;
        pickupCrosshair = transform.Find(GameObjects.PickupCrosshair).gameObject;
        loadingScreen = transform.Find(GameObjects.Loading).gameObject;
    }
    
    private void Awake()
    {
        DOTween.SetTweensCapacity(500, 50);
        Init();
    }

    private void OnEnable()
    {
        Init();
        Events.VictoryEvent += ActivateVictoryMenu;
        Events.PlayerIsTooFarFromGhostEvent += ActivateGhostTooFarHint;
        Events.PlayerIsNearGhostEvent += DeactivateGhostTooFarHint;
        Events.GhostRespawnEvent += ActivateGhostRespawnHint;
        Events.RoomCompletedEvent += ResetGameHint;
    }

    private void Start()
    {
        if (Finder.SaveManager.HasReachedBoss())
        {
            DisplayBossHint();
        }
        else if (!Finder.SaveManager.IsTutorialDone())
        {
            DisplayTutorialHint();
        }
        else
        {
            DisplayGameHint();
        }
    }

    public void DisplayPickupCrosshair()
    {
        crosshair.SetActive(false);
        pickupCrosshair.SetActive(true);
    }
    
    public void DisplayCrosshair()
    {
        crosshair.SetActive(true);
        pickupCrosshair.SetActive(false);
    }

    private void ResetGameHint(RoomType type)
    {
        taskLabelText.text = HINT_TITLE;
        taskText.text = NO_CURRENT_TASK;
        taskMenu.SetActive(false);
        taskIcon.SetActive(true);
    }

    private void ActivateGhostTooFarHint()
    {
        if(!ghostRespawnHint.activeSelf)
            ghostTooFarHint.SetActive(true);
    }

    private void DeactivateGhostTooFarHint()
    {
        ghostTooFarHint.SetActive(false);
    }
    
    private void ActivateGhostRespawnHint()
    {
        IEnumerator Cooldown()
        {
            yield return new WaitUntil(() => ghostTooFarHint.activeSelf == false);
            ghostRespawnHint.SetActive(true);
            yield return new WaitForSeconds(HINT_DURATION);
            ghostRespawnHint.SetActive(false);
        }
        StartCoroutine(Cooldown());
    }

    private void DisplayBossHint()
    {
        taskLabelText.text = HINT_TITLE;
        taskText.text = "Survive...";
        taskMenu.SetActive(true);
        taskIcon.SetActive(false);
    }

    private void DisplayTutorialHint()
    {
        taskLabelText.text = "Tutorial";
        taskLabelText.gameObject.GetComponent<RectTransform>().sizeDelta = HintSizeTutorial;
        taskText.text = "Williaume! Use the W, A, S and D keys on your keyboard to move!";
        taskMenu.SetActive(true);
        taskIcon.SetActive(false);
    }

    private void Update()
    {
        if (InputManager.Instance.Inputs.Player.Hint.WasPressedThisFrame()) 
        {
            ToggleHint();
        }
        
        if(ghostRespawnHint.activeSelf)
            ghostTooFarHint.SetActive(false);
    }

    private void ToggleHint()
    {
        taskMenu.SetActive(!taskMenu.activeSelf);
        taskIcon.SetActive(!taskIcon.activeSelf);
    }

    private void ActivateVictoryMenu()
    {
        InputManager.Instance.ActivateUIInputs();
        victoryMenu.SetActive(true);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
    }

    public void SetDefeatMenuState(bool state)
    {
        defeatMenu.SetActive(state);
    }

    public void TogglePauseMenu()
    {
        pauseMenu.SetActive(!pauseMenu.activeSelf);
    }

    public void ToggleKeybindsMenu()
    {
        keybindsMenu.SetActive(!keybindsMenu.activeSelf);
    }

    public void SetTaskText(string text)
    {
        taskText.text = text;
    }

    public void SetPotionCount(int nbPotions)
    {
        potionCountText.text = nbPotions.ToString();
        AdjustPotionSprite(nbPotions);
    }

    public bool IsInKeybindsMenu()
    {
        return keybindsMenu.activeSelf;
    }

    public void ActivatePopUpMessage()
    {
        if (popUpActive)
            return;

        popUpActive = true;
        StartCoroutine(PopUpMessage());
    }

    public void ReturnMenu()
    {
        DDOLManager.DestroyAll();
        Time.timeScale = 1;
        SceneManager.LoadScene(Scenes.MainMenu);
    }

    public void DisplayGameHint()
    {
        taskLabelText.text = HINT_TITLE;
        taskLabelText.gameObject.GetComponent<RectTransform>().sizeDelta = HintSizeGame;
        taskText.text = NO_CURRENT_TASK;
        taskMenu.SetActive(false);
        taskIcon.SetActive(true);
    }
    
    public void DisplayGhostRoomHint()
    {
        taskLabelText.text = SOUL_CONNECTION;
        taskLabelText.gameObject.GetComponent<RectTransform>().sizeDelta = HintSizeGame;
        taskText.text = SOUL_CONNECTION_HINT;
        taskMenu.SetActive(true);
        taskIcon.SetActive(false);
    }

    private IEnumerator PopUpMessage()
    {
        popUp.SetActive(true);
        yield return new WaitForSeconds(POP_UP_TIME);
        popUp.SetActive(false);
        popUpActive = false;
    }

    private void AdjustPotionSprite(int nbPotions)
    {
        switch (nbPotions)
        {
            case 0:
                potionImage.sprite = potionsSprites[4];
                break;
            case <= MAX_POTIONS_QUARTER_FULL:
                potionImage.sprite = potionsSprites[3];
                break;
            case <= MAX_POTIONS_HALF_FULL:
                potionImage.sprite = potionsSprites[2];
                break;
            case <= MAX_POTIONS_THREE_QUARTER_FULL:
                potionImage.sprite = potionsSprites[1];
                break;
            default:
                potionImage.sprite = potionsSprites[0];
                break;
        }
    }

    private void OnDestroy()
    {
        Events.VictoryEvent -= ActivateVictoryMenu;
        Events.PlayerIsTooFarFromGhostEvent -= ActivateGhostTooFarHint;
        Events.PlayerIsNearGhostEvent -= DeactivateGhostTooFarHint;
        Events.GhostRespawnEvent -= ActivateGhostRespawnHint;
        Events.GhostRespawnEvent -= ActivateGhostRespawnHint;
        Events.RoomCompletedEvent -= ResetGameHint;
    }

    public void ShowHint(string message)
    {
        taskLabelText.gameObject.GetComponent<RectTransform>().sizeDelta = HintSizeGame;
        taskText.text = message;
        taskMenu.SetActive(true);
        taskIcon.SetActive(false);
    }

    public void ActivateLoadingScreen()
    {
        loadingScreen.SetActive(true);
    }

    public void DeactivateLoadingScreen()
    {
        loadingScreen.SetActive(false);
    }
}
