using System.Collections;
using Harmony;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BossBar : MonoBehaviour
{
    private const float DELAY = 2f;
    
    private const string FIRE_BOSS_NAME = "Khrom, the Ember of Fear";
    private const string AIR_BOSS_NAME = "Khren, the Deathbreather";
    private const string ICE_BOSS_NAME = "Khralid, the Icebringer";
    private const string POISON_BOSS_NAME = "Khalled, the Plaguebearer";
    private const string OBSIDIAN_BOSS_NAME = "Khan'Ban, the Cruel";
    
    private TextMeshProUGUI bossName;
    private UIBarManager bossBarManager;
    private Image frame;
    void Start()
    {
        bossName = GetComponentInChildren<TextMeshProUGUI>();
        bossBarManager = GetComponent<UIBarManager>();
        frame = GetComponent<Image>();
        bossName.gameObject.SetActive(false);
        bossBarManager.gameObject.transform.Find(GameObjects.PrimaryBar).gameObject.SetActive(false);
        bossBarManager.gameObject.transform.Find(GameObjects.SecondaryBar).gameObject.SetActive(false);
        Events.BossFightBeginEvent += OnBossFightBegin;
        Events.BossFightEndEvent += OnBossFightEnd;
    }

    private void OnBossFightEnd()
    {
        IEnumerator Cooldown()
        {
            yield return new WaitForSeconds(DELAY);
            DeactivateBossBar();
        }
        StartCoroutine(Cooldown());
    }

    private void OnBossFightBegin(ElementType element)
    {
        ActivateBossBar();
        switch (element)
        {
            case ElementType.Fire:
                bossName.text = FIRE_BOSS_NAME;
                bossBarManager.SetData(GameObject.Find(GameObjects.RedMiniBoss));
                break;
            case ElementType.Air:
                bossName.text = AIR_BOSS_NAME;
                bossBarManager.SetData(GameObject.Find(GameObjects.YellowMiniBoss));
                break;
            case ElementType.Ice:
                bossName.text = ICE_BOSS_NAME;
                bossBarManager.SetData(GameObject.Find(GameObjects.BlueMiniBoss));
                break;
            case ElementType.Poison:
                bossName.text = POISON_BOSS_NAME;
                bossBarManager.SetData(GameObject.Find(GameObjects.GreenMiniBoss));
                break;
            case ElementType.Obsidian:
                bossName.text = OBSIDIAN_BOSS_NAME;
                bossBarManager.SetData(GameObject.Find(GameObjects.Dragon));
                break;
        }
    }

    private void OnDestroy()
    {
        Events.BossFightBeginEvent -= OnBossFightBegin;
        Events.BossFightEndEvent -= OnBossFightEnd;
    }
    
    private void ActivateBossBar()
    {
        foreach (var child in gameObject.Children())
        {
            child.SetActive(true);
        }

        frame.enabled = true;
    }
    
    private void DeactivateBossBar()
    {
        foreach (var child in gameObject.Children())
        {
            child.SetActive(false);
        }
        
        frame.enabled = false;
    }
}
