using Harmony;
using UnityEngine;

public class CreditsTExt : MonoBehaviour
{
    private UIManager uiManager;

    void Start()
    {
        uiManager = GameObject.Find(GameObjects.UIManager).GetComponent<UIManager>();
    }

    public void OnCreditEnded()
    {
        uiManager.QuitCredits();
    }
}
