using Harmony;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    private PlayerUI playerUI;

    private void Start()
    {
        playerUI = GameObject.Find(GameObjects.PlayerUI).GetComponent<PlayerUI>();
    }

    private void Update()
    {
        if(InputManager.Instance.Inputs.Player.Pause.WasPressedThisFrame())
        {
            PauseGame();
        }
        else if(InputManager.Instance.Inputs.UI.Back.WasPressedThisFrame())
        {
            Back();
        }
    }

    private void PauseGame()
    {
        playerUI.TogglePauseMenu();
        InputManager.Instance.ActivateUIInputs();
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        Time.timeScale = 0;
    }

    public void Back()
    {
        if(playerUI.IsInKeybindsMenu())
        {
            GoBackToPauseMenuFromKeybinds();
        }
        else
        {
            ResumeGame();
        }
    }

    private void GoBackToPauseMenuFromKeybinds()
    {
        playerUI.ToggleKeybindsMenu();
        playerUI.TogglePauseMenu();
    }

    private void ResumeGame()
    {
        playerUI.TogglePauseMenu();
        InputManager.Instance.ActivatePlayerInputs();
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        Time.timeScale = 1;
    }
}
