using Harmony;
using UnityEngine;

public class BossLoadTrigger : MonoBehaviour
{
    private const int ACQUIRED_ALL_KEYS = 4;

    private void OnTriggerEnter(Collider other)
    {
        if (Finder.SaveManager.GetNumberCollectedKeys() >= ACQUIRED_ALL_KEYS)
        {
            SceneLoadingManager.Instance.LoadBossScene();
            Finder.SaveManager.WentToBossRoomTrigger();
            RespawnManager.Instance.MovePlayerToBoss();
        }
        else
        {
            GameObject.Find(GameObjects.PlayerUI).GetComponent<PlayerUI>().ActivatePopUpMessage();
        }
    }
}
