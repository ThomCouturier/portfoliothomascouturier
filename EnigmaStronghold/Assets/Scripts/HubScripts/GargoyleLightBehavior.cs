using System.Collections.Generic;
using Harmony;
using JetBrains.Annotations;
using UnityEngine;

public class GargoyleLightBehavior : MonoBehaviour
{
    private const int LEVEL = 2;
    [SerializeField] ElementType lightType;
    private List<ElementType> obtainedKeyTypes;

    private void OnEnable()
    {
        gameObject.SetActive(true);
        ManageLight();
    }

    [UsedImplicitly]
    private void OnLevelWasLoaded(int level)
    {
        if (level== LEVEL)
        {
            ManageLight();
        }
    }

    private void ManageLight()
    {
        if(Finder.SaveManager.BossIsCompleted(lightType))
            gameObject.SetActive(false);
    }
}