using System.Collections.Generic;
using Harmony;
using JetBrains.Annotations;
using UnityEngine;

public class DoorLightBehavior : MonoBehaviour
{
    private const float ACTIVATED_INTENSITY = 0.77f;
    private const int LEVEL = 2;
    [SerializeField] ElementType lightType;
    private List<ElementType> obtainedKeyTypes;
    private Light light;

    private void OnEnable()
    {
        light = gameObject.GetComponent<Light>();
        light.intensity = 0.0f;
        ManageLight();
    }

    [UsedImplicitly]
    private void OnLevelWasLoaded(int level)
    {
        if (level == LEVEL)
        {
            ManageLight();
        }
    }

    private void ManageLight()
    {
        if(Finder.SaveManager.BossIsCompleted(lightType))
            light.intensity = ACTIVATED_INTENSITY;
    }
}
