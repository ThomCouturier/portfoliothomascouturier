using Harmony;
using UnityEngine;

public class HintTrigger : MonoBehaviour
{
    private const string SKULL_HINT = "Harness the power of the skulls to open the door";
    private const string PRESSURE_PLATE_HINT = "Hit the boxes to move them onto the pressure plates";
    private enum HintState { Skull, PressurePlate }

    private bool hasBeenTriggered;
    [SerializeField] private HintState hintState;

    private void OnTriggerEnter(Collider other)
    {
        if (hasBeenTriggered) return;
        if (!other.gameObject.CompareTag(Tags.Player)) return;
        
        switch (hintState)
        {
            case HintState.Skull:
                Finder.PlayerUI.ShowHint(SKULL_HINT);
                break;
            case HintState.PressurePlate:
                Finder.PlayerUI.ShowHint(PRESSURE_PLATE_HINT);
                break;
        }

        hasBeenTriggered = true;
    }
}
