using Game;
using Harmony;
using UnityEngine;

public class Sword : MonoBehaviour
{    
    [SerializeField] private int damage;
    [SerializeField] private LivingEntity holder;
    [SerializeField] private StaminaManager staminaManager;
    [SerializeField] private bool hasBlock = false;

    private bool blocking;
    private ISensor<ISlashable> slashableSensor;
    private ISensor<IBlockable> blockableSensor;
    private GameObject slashSensorGameObject;
    private GameObject blockSensorGameObject;
    private SwordSFX swordSFX;
    

    private void Start()
    {
        Sensor slashSensor = transform.Find(GameObjects.SlashableSensor).GetComponent<Sensor>();
        
        swordSFX = GetComponent<SwordSFX>();

        slashSensorGameObject = slashSensor.gameObject;
        slashableSensor = slashSensor.For<ISlashable>();
        slashableSensor.OnSensedObject += Attack;

        if (hasBlock)
        {
            Sensor blockSensor = transform.Find(GameObjects.BlockableSensor).GetComponent<Sensor>();
            blockSensorGameObject = blockSensor.gameObject;
            blockableSensor = blockSensor.For<IBlockable>();
            blockableSensor.OnSensedObject += Block;
        }

        SetDangerStatus(false);
    }

    public void SetDamage(int damage)
    {
        this.damage = damage;
    }

    public int GetDamage() 
    {
        return damage;    
    }

    public void SetBlockStatus(bool blocking)
    {
        this.blocking = blocking;
    }

    public void SetDangerStatus(bool canHurt)
    {
        slashSensorGameObject.SetActive(canHurt);
    }

    private void Attack(ISlashable target)
    {
        LivingEntity entity = target as LivingEntity;
        if(entity != null)
            if (entity == holder) return;

        if (!blocking)
        {
            target.Slash(damage, gameObject);
        }
    }

    private void Block(IBlockable target)
    {
        LivingEntity entity = target as LivingEntity;
        if (entity != null)
            if (entity == holder) return;

        if (blocking)
        {
            target.Block(gameObject);
            staminaManager?.LoseStamina(Player.STAMINA_BLOCK_COST);
            swordSFX.OnSwordImpactSFX();
        }
    }
}
