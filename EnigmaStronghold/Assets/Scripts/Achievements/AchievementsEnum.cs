public enum AchievementsEnum
{
    NoPressure,
    DiagnosticNotColorBlind,
    MagnusCarlsen,
    InvisibleFriend,
    Janitor,
    IceToMeetYou,
    PeptoBismol,
    Windys,
    BlastFlameOus,
    YouSlayin,
    NoHeal,
    DoomGuy
}
