using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using Harmony;
using TMPro;
using UnityEngine;

public class AchievementsUIManager : MonoBehaviour
{
    #region consts

    public const string MAGNUS_CARLSEN_TEXT = "Magnus Carlsen";
    public const string INVISIBLE_FRIEND_TEXT = "Invisible Friend";
    public const string DIAGNOSTIC_NOT_COLOR_BLIND_TEXT = "Diagnostic : Not Color Blind";
    public const string NO_PRESSURE_TEXT = "No Pressure...";
    public const string ICE_TO_MEET_YOU_TEXT = "Ice To Meet You";
    public const string PEPTO_BISMOL_TEXT = "Pepto du Bismol";
    public const string WINDYS_TEXT = "Windy's Baconator";
    public const string BLAST_FLAME_OUS_TEXT = "Blast-flame-ous";
    public const string JANITOR_TEXT = "Janitor";
    public const string YOU_SLAYIN_TEXT = "You Slayin gurl!";
    public const string NO_HEAL_TEXT = "I don't need no heal";
    public const string DOOM_GUY_TEXT = "Doom Guy";

    private const int SHOW_TIME = 5;
    private const int ACHIEVEMENT_MENU_SHOWING_OFFSET = 400;
    private const float ACHIEVEMENT_MENU_ANIMATION_DURATION = 0.5f;
    #endregion
    
    private List<AchievementsEnum> achievementsToShow;
    public static Dictionary<AchievementsEnum, string> achievementTexts;
    private bool coroutineStarted;
    private GameObject popup;
    private TextMeshProUGUI popupText;
    private float initialPos;

    private void Awake()
    {
        achievementsToShow = new List<AchievementsEnum>();
        coroutineStarted = false;
        popup = gameObject;
        popupText = transform.Find(GameObjects.AchievementName).GetComponent<TextMeshProUGUI>();
        initialPos = popup.transform.position.x;
        GenerateDict();
        Events.AchievementUnlockedEvent += ShowAchievement;
    }

    public static void GenerateDict()
    {
        achievementTexts = new Dictionary<AchievementsEnum, string>()
        {
            { AchievementsEnum.MagnusCarlsen, MAGNUS_CARLSEN_TEXT },
            { AchievementsEnum.InvisibleFriend, INVISIBLE_FRIEND_TEXT },
            { AchievementsEnum.DiagnosticNotColorBlind, DIAGNOSTIC_NOT_COLOR_BLIND_TEXT},
            { AchievementsEnum.NoPressure, NO_PRESSURE_TEXT },
            { AchievementsEnum.IceToMeetYou, ICE_TO_MEET_YOU_TEXT },
            { AchievementsEnum.PeptoBismol, PEPTO_BISMOL_TEXT },
            { AchievementsEnum.Windys, WINDYS_TEXT },
            { AchievementsEnum.BlastFlameOus, BLAST_FLAME_OUS_TEXT },
            { AchievementsEnum.Janitor, JANITOR_TEXT },
            { AchievementsEnum.YouSlayin, YOU_SLAYIN_TEXT },
            { AchievementsEnum.NoHeal, NO_HEAL_TEXT },
            { AchievementsEnum.DoomGuy, DOOM_GUY_TEXT }
        };
    }

    public Dictionary<AchievementsEnum, string> GetAchievementTexts()
    {
        return achievementTexts;
    }

    private void ShowAchievement(AchievementsEnum achievement)
    {
        achievementsToShow.Add(achievement);
        if (!coroutineStarted)
            StartCoroutine(Routine());
    }

    private IEnumerator Routine()
    {
        coroutineStarted = true;
        while (achievementsToShow.Count > 0)
        {
            AchievementsEnum currentAchievement = achievementsToShow.First();
            popupText.text = achievementTexts[currentAchievement];
            yield return popup.transform.DOMoveX(initialPos - ACHIEVEMENT_MENU_SHOWING_OFFSET, ACHIEVEMENT_MENU_ANIMATION_DURATION);
            yield return new WaitForSeconds(SHOW_TIME);
            yield return popup.transform.DOMoveX(initialPos, ACHIEVEMENT_MENU_ANIMATION_DURATION);
            achievementsToShow.Remove(currentAchievement);
        }
        coroutineStarted = false;
    }

    private void OnDestroy()
    {
        Events.AchievementUnlockedEvent -= ShowAchievement;
    }
}
