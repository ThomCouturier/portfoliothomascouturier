using Harmony;
using UnityEngine;

public class PotionManager : MonoBehaviour
{
    public const int INITIAL_NB_POTIONS = 10;
    private const int HEALING = 5;
    private const int MAX_POTIONS_ADDED_ON_DEATH = 3;

    public int PotionCount { get; private set; }
    private Player player;
    private PlayerUI playerUI;
    private PlayerSFX sfx;

    private void Awake()
    {
        sfx = GetComponent<PlayerSFX>();
        Events.PickUpHealEvent += AddPotion;
        Events.PlayerRespawnEvent += OnPlayerRespawn;
    }
    
    private void Start()
    {
        player = Finder.Player;
        playerUI = Finder.PlayerUI;
        OnPlayerRespawn();
    }

    private void Update()
    {
        if(InputManager.Instance.Inputs.Player.Potion.WasPressedThisFrame())
            DrinkPotion();
    }

    private void DrinkPotion()
    {
        if (PotionCount <= 0) return;

        sfx.OnPotionDrank();
        PotionCount--;
        player.Heal(HEALING);
        playerUI.SetPotionCount(PotionCount);
        Finder.SaveManager.UsePotion();
    }

    private void AddPotion()
    {
        PotionCount++;
        playerUI.SetPotionCount(PotionCount);
    }
    
    private void OnPlayerRespawn()
    {
        PotionCount = Finder.SaveManager.GetNumberPotions();
        PotionCount = Mathf.Min(PotionCount += MAX_POTIONS_ADDED_ON_DEATH, INITIAL_NB_POTIONS);
        Finder.SaveManager.UpdatePotionCount(PotionCount);
        playerUI.SetPotionCount(PotionCount);
    }

    private void OnDestroy()
    {
        Events.PickUpHealEvent -= AddPotion;
        Events.PlayerRespawnEvent -= OnPlayerRespawn;
    }
}
