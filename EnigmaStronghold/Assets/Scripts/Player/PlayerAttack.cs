﻿using Harmony;
using JetBrains.Annotations;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    [SerializeField] private float maxCooldown;
    [SerializeField] private int staminaCost = 1;
    [SerializeField] private GameObject swordHandler;

    private StaminaManager staminaManager;
    private Animator hand;
    private Sword sword;
    private SwordSFX swordSFX;
    private float cooldown;
    private int slashState;
    private bool wantAttack;
    public bool Blocking { get; set; }

    private void Start()
    {
        hand = GetComponent<Animator>();
        staminaManager = GameObject.Find(GameObjects.StaminaManager).GetComponent<StaminaManager>();
        sword = transform.GetComponentInChildren<Sword>();
        swordSFX = sword.GetComponent<SwordSFX>();
        cooldown = 0;
        wantAttack = false;
    }

    void Update()
    {
        cooldown = Mathf.Max(0, cooldown - Time.deltaTime);
        if (cooldown == 0)
        {
            if (wantAttack)
            {
                wantAttack = false;
                slashState++;
                if (slashState == 3) slashState = 1;
                swordSFX.OnSwordSwingSFX();
                hand.SetInteger(AnimatorParameters.AttackState, slashState);

                cooldown = maxCooldown;
                staminaManager.LoseStamina(staminaCost);
            }
        }
    }

    public void Attack()
    {
        if (staminaManager.HasStamina())
        {
            wantAttack = true;
        }
    }

    public void Block(bool status)
    {
        Blocking = status && staminaManager.HasStamina();
        hand.SetBool(AnimatorParameters.Block, Blocking);
        staminaManager.SetIsBlocking(Blocking);
        sword.SetBlockStatus(Blocking);
        if (Blocking) sword.SetDangerStatus(false);
    }

    [UsedImplicitly]
    public void ResetSwordAnimation()
    {
        hand.SetInteger(AnimatorParameters.AttackState, 0);
    }

    [UsedImplicitly]
    public void SetSwordDanger(int boolean)
    {
        sword.SetDangerStatus(boolean != 0);
    }
}

