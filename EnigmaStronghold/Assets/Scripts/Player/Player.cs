﻿using Harmony;
using UnityEngine;

[Findable(Tags.Player)]
public class Player : LivingEntity, ISlashable, IUIBar
{
    public const float STAMINA_BLOCK_COST = 2.5f;
    private const float BLOCK_ANGLE = 90;

    [SerializeField] private PlayerMovementController movement;
    private PlayerAttack melee;
    private StaminaManager staminaManager;
    private InputActions inputs;

    private new void Start()
    {
        base.Start();
        melee = transform.GetComponentInChildren<PlayerAttack>();
        staminaManager = transform.GetComponentInChildren<StaminaManager>();
        movement.SetSpeed(GetSpeed());
        inputs = InputManager.Instance.Inputs;
        Health = Finder.SaveManager.GetHealth();
        Events.VictoryEvent += VictoryValidation;
    }

    private void VictoryValidation()
    {
        if (Finder.SaveManager.GetNumberPotionsUsed() != 1) return;
        Finder.SaveManager.NoHealDone();
        if(Health == GetMaxHealth() && Finder.SaveManager.GetNumberOfDeath() == 0)
        {
            Finder.SaveManager.DoomGuyDone();
        }
    }

    private void Update()
    {
        UpdateInvulnerabilityTime();
        if (inputs.Player.Fire.ReadValue<float>() > 0 && inputs.Player.Fire.WasPerformedThisFrame() && !melee.Blocking)
            melee.Attack();
        melee.Block(inputs.Player.Fire2.ReadValue<float>() > 0);
    }

    protected override void Die()
    {
        GlobalSFX.Instance.StopGlobalAudioSource();
        GlobalSFX.Instance.OnPlayerDeath();
        GameManager.Instance.Defeat();
        Finder.SaveManager.Death();
        InputManager.Instance.ActivateUIInputs();
    }

    public void Slash(int damage, GameObject sender)
    {
        if (melee.Blocking)
        {
            Vector3 targetDir = sender.transform.position - transform.position;
            float angle = Vector3.Angle(targetDir, transform.forward);
            if (angle > BLOCK_ANGLE)
            {
                base.Hit(damage);
            }
            else
                staminaManager.LoseStamina(STAMINA_BLOCK_COST);                    
        } else
        {
            base.Hit(damage);
        }
    }

    public float GetMax()
    {
        return GetMaxHealth();
    }

    // Vient de l interface (IBar) pour les barres dans le UI
    public float GetCurrent()
    {
        return Health;
    }

    private void OnDestroy()
    {
        Events.VictoryEvent -= VictoryValidation;
    }
}
