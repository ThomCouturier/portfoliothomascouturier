using System.Linq;
using Game;
using Harmony;
using UnityEngine;

public class ItemPossession : MonoBehaviour
{    
    [SerializeField] private GameObject playerHand;
    private ISensor<DataSkull> skullSensor;
    private ISensor<SkullPlacement> pillarSensor;
    private bool isHoldingItem;
    private GameObject itemPickedUp;

    private void Start()
    {
        isHoldingItem = false;
        Sensor sensor = transform.Find(GameObjects.SkullSensor).GetComponent<Sensor>();
        pillarSensor = sensor.For<SkullPlacement>();
        Events.SkullPickedUpEvent += OnSkullPickup;
    }

    private void Update()
    {
        if (!InputManager.Instance.Inputs.Player.Use.WasPressedThisFrame()) return;

        if (!isHoldingItem) return;
        
        if (pillarSensor.SensedObjects.Count > 0)
        {
            PlaceItem();
        }
        else
        {
            DropItem();
        }
    }

    private void OnSkullPickup(GameObject skull)
    {
        GameObject item = skull;
        if (item == null)
            return;

        itemPickedUp = item;
        itemPickedUp.gameObject.transform.SetParent(playerHand.transform);
        itemPickedUp.transform.position = playerHand.transform.position;
        Rigidbody itemRigidbody = itemPickedUp.gameObject.GetComponent<Rigidbody>();
        itemPickedUp.GetComponent<Collider>().enabled = false;
        itemRigidbody.isKinematic = true;
        isHoldingItem = true;
    }

    private void DropItem()
    {
        Rigidbody itemRigidbody = itemPickedUp.gameObject.GetComponent<Rigidbody>();
        itemPickedUp.GetComponent<Collider>().enabled = true;
        itemRigidbody.isKinematic = false;
        itemPickedUp.transform.parent = null;
        isHoldingItem = false;
    }

    private void PlaceItem()
    {
        pillarSensor.SensedObjects.First().PlaceSkull(itemPickedUp);
    }

    public bool IsHoldingItem()
    {
        return isHoldingItem;
    }

    private void OnDestroy()
    {
        Events.SkullPickedUpEvent -= OnSkullPickup;
    }
}
