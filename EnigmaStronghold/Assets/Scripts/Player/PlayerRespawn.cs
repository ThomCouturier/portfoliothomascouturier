using Harmony;
using UnityEngine;

public class PlayerRespawn : MonoBehaviour
{
    private CameraMovement cameraMovement;

    private void Awake()
    {
        cameraMovement = GetComponentInChildren<CameraMovement>();
    }
    
    public void Respawn(Vector3 position, float xCameraPosition)
    {
        Finder.Events.PlayerRespawn();
        transform.position = position;
        cameraMovement.SetCameraPosition(xCameraPosition);
    }
}
