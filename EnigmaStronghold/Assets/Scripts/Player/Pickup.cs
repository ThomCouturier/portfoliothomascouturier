using Harmony;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    private const float PICKUP_DISTANCE = 3.5f;

    private void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(gameObject.transform.position, gameObject.transform.forward, out hit,
                PICKUP_DISTANCE))
        {
            if (hit.collider.gameObject.CompareTag(Tags.Pillar))
            {
                var skullPlacement = hit.collider.gameObject.GetComponent<SkullPlacement>();
                if(skullPlacement.CanInteract())
                    Finder.PlayerUI.DisplayPickupCrosshair();
            }
            else if (hit.collider.gameObject.CompareTag(Tags.Pickable) ||
                     hit.collider.gameObject.CompareTag(Tags.Skull))
            {
                Finder.PlayerUI.DisplayPickupCrosshair();
                if (InputManager.Instance.Inputs.Player.Use.WasReleasedThisFrame())
                {
                    hit.collider.gameObject.GetComponent<IPickable>().PickUp();
                }
            }
            else
            {
                Finder.PlayerUI.DisplayCrosshair();
            }
        }
        else
        {
            Finder.PlayerUI.DisplayCrosshair();
        }
    }
}
