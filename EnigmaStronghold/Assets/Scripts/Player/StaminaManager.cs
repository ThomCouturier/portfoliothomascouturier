using UnityEngine;

public class StaminaManager : MonoBehaviour, IUIBar
{
    private const int MAX_STAMINA = 10;
    private const float STAMINA_RESTORE_RATE = 2;
    private const float FAST_STAMINA_RESTORE_RATE = 4;
    private const int MAX_OUT_OF_BREATH_COOLDOWN = 2;
    private const int MAX_COOLDOWN_BEFORE_STAMINA_REGENERATION = 2;

    private bool restoreStamina;
    private bool isOutOfBreath;
    public float currentStamina;
    private float restoreStaminaCooldown;
    private float cooldownBeforeStaminaRegeneration;
    private float staminaRestoreRate;
    
    private void Start()
    {
        staminaRestoreRate = STAMINA_RESTORE_RATE;
        isOutOfBreath = false;
        restoreStamina = true;
        currentStamina = MAX_STAMINA;
    }

    private void Update()
    {
        if (isOutOfBreath)
        {
            HandleOutOfBreath();
        }
        
        else if(restoreStamina && Mathf.Max(cooldownBeforeStaminaRegeneration -= Time.deltaTime, 0) == 0)
        {
            currentStamina = Mathf.Min(currentStamina + Time.deltaTime * staminaRestoreRate, MAX_STAMINA);
        }
    }

    private void HandleOutOfBreath()
    {
        if (!((restoreStaminaCooldown += Time.deltaTime) > MAX_OUT_OF_BREATH_COOLDOWN)) return;
        
        restoreStaminaCooldown = 0;
        cooldownBeforeStaminaRegeneration = 0;
        isOutOfBreath = false;
    }

    public void LoseStamina(float staminaLost)
    {
        currentStamina = Mathf.Max(currentStamina - staminaLost, 0);
        cooldownBeforeStaminaRegeneration = MAX_COOLDOWN_BEFORE_STAMINA_REGENERATION;
        if (currentStamina == 0)
            isOutOfBreath = true;
    }

    public bool HasStamina()
    {
        return currentStamina > 0;
    }

    public void FastRestoreSpeed()
    {
        staminaRestoreRate = FAST_STAMINA_RESTORE_RATE;
    }

    public void SlowRestoreSpeed()
    {
        staminaRestoreRate = STAMINA_RESTORE_RATE;
    }

    public float GetCurrentStamina()
    {
        return currentStamina;
    }
    
    public float GetMax()
    {
        return MAX_STAMINA;
    }
        
    public float GetCurrent()
    {
        return currentStamina;
    }

    public void Reset()
    {
        currentStamina = MAX_STAMINA;
    }

    public void SetIsBlocking(bool isBlocking)
    {
        restoreStamina = !isBlocking;
    }
}
