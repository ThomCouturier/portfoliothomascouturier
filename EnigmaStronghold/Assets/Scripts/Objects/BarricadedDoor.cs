using System.Collections;
using UnityEngine;

public class BarricadedDoor : MonoBehaviour, RoomCompleted
{
    private const float DELAY = 10f;
    private WoodSFX sfx;

    private void Awake()
    {
        sfx = GetComponent<WoodSFX>();
        Events.RoomCompletedEvent += OnRoomCompleted;
    }

    public void OnRoomCompleted(RoomType roomType)
    {
        if (roomType.Equals(RoomType.Ghost))
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                var childRigidBody = transform.GetChild(i).GetComponent<Rigidbody>();
                childRigidBody.useGravity = true;
                childRigidBody.isKinematic = false;
                StartCoroutine(UnloadDelay());
            }
        }
        sfx.PlayWoodBreakSFX();
        Events.RoomCompletedEvent -= OnRoomCompleted;
    }

    protected void OnDestroy()
    {
        Events.RoomCompletedEvent -= OnRoomCompleted;
    }

    IEnumerator UnloadDelay()
    {
        yield return new WaitForSeconds(DELAY);
        gameObject.SetActive(false);
    }
}
