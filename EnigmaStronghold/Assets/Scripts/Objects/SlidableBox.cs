using DG.Tweening;
using Game;
using Harmony;
using UnityEngine;

public class SlidableBox : MonoBehaviour, ISlashable, RoomCompleted
{
    [SerializeField] private int moveForce = 50;

    private const int STOP_MOVEMENT_MODIFICATION = 4;
    private const float STOP_MOVEMENT_TIME = 0.5f;
    private const int MOVE_TIME = 1;

    private RigidbodyConstraints originalConstraints;
    private Rigidbody rb;
    private SlidingBoxSFX sfx;
    private ISensor<PressurePlate> pressurePlateSensor;

    private Vector3 lastMovement;
    private bool slashable;
    private bool sliding;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        sfx = GetComponent<SlidingBoxSFX>();

        sliding = false;
    }

    private void Start()
    {
        Sensor sensor = transform.Find(GameObjects.PressurePlateSensor).GetComponent<Sensor>();
        slashable = true;
        rb.isKinematic = true;
        originalConstraints = rb.constraints;

        pressurePlateSensor = sensor.For<PressurePlate>();
        pressurePlateSensor.OnSensedObject += OnPressurePlateContact;
        Events.PressurePlateRoomDoneEvent += OnRoomCompleted;
    }

    public void OnPressurePlateContact(PressurePlate pressurePlate)
    {
        if (pressurePlate.CompareTag(gameObject.tag))
        {
            sfx.StopSlidingSound();
            slashable = false;
            sliding = false;
            rb.velocity = Vector3.zero;
            rb.constraints = RigidbodyConstraints.FreezeAll;
            rb.isKinematic = true;
            Vector3 platePosition = pressurePlate.transform.position;
            transform.DOMove(new Vector3(platePosition.x, transform.position.y, platePosition.z), MOVE_TIME);
            pressurePlate.Activate();
            sfx.OnBoxInPlaceSFX();
        }
    }

    public void Slash(int damage, GameObject sender)
    {
        if (slashable)
        {
            rb.isKinematic = false;
            slashable = false;
            sliding = true;
            Vector3 playerPosition = Finder.Player.transform.position;
            Vector3 targetDir = playerPosition - transform.position;
            float angle = Vector3.Angle(targetDir, transform.forward);

            if (playerPosition.x < transform.position.x)
            {
                angle = -angle;
            }

            if (angle >= -45 && angle <= 45)
            {
                lastMovement = Vector3.forward;
                rb.velocity = lastMovement * moveForce;
                FreezeXPosition();
            }
            else if (angle > 45 && angle < 135)
            {
                lastMovement = Vector3.left;
                rb.velocity = lastMovement * moveForce;
                FreezeZPosition();
            }
            else if (angle < -45 && angle > -135)
            {
                lastMovement = Vector3.right;
                rb.velocity = lastMovement * moveForce;
                FreezeZPosition();
            }
            else
            {
                lastMovement = Vector3.back;
                rb.velocity = lastMovement * moveForce;
                FreezeXPosition();
            }
            sfx.PlaySlidingSound();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (NeedsToStop(collision.gameObject))
        {
            rb.velocity = Vector3.zero;
            rb.isKinematic = true;
            slashable = true;
            sliding = false;
            rb.constraints = originalConstraints;
            rb.transform.DOMove(rb.position - (lastMovement / STOP_MOVEMENT_MODIFICATION), STOP_MOVEMENT_TIME);
            sfx.StopSlidingSound();
        }
    }

    private bool NeedsToStop(GameObject collision)
    {
        if (collision.CompareTag(Tags.Ground) || collision.CompareTag(Tags.Player)) return false;
        //If the collision is a pressure plate, the box does not stop when it is the corresponding plate
        if (TagIsBox(collision))
        {
            if (collision.CompareTag(gameObject.tag)) return false;
        }
        return sliding;
    }

    private bool TagIsBox(GameObject collision)
    {
        return collision.CompareTag(Tags.BlueBox) || collision.CompareTag(Tags.GreenBox) || collision.CompareTag(Tags.RedBox);
    }

    private void FreezeXPosition()
    {
        rb.constraints = originalConstraints | RigidbodyConstraints.FreezePositionX;
    }

    private void FreezeZPosition()
    {
        rb.constraints = originalConstraints | RigidbodyConstraints.FreezePositionZ;
    }

    public void SetSlashable(bool state)
    {
        slashable = state;
    }

    public void OnRoomCompleted(RoomType roomType)
    {
        if (roomType.Equals(RoomType.PressureFinal))
            gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        Events.PressurePlateRoomDoneEvent -= OnRoomCompleted;
    }
}
