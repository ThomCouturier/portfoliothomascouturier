public class PressurePlateRoomDoor : Door
{
    private new void Start()
    {
        base.Start();
        Events.PressurePlateRoomDoneEvent += PressurePlateRoomCompleted;
    }
    
    public void PressurePlateRoomCompleted(RoomType roomType)
    {
        if(roomType.Equals(RoomType.PressureFinal))
        {
            OpenDoor();
        }
    }

    private new void OnDestroy()
    {
        base.OnDestroy();
        Events.PressurePlateRoomDoneEvent -= PressurePlateRoomCompleted;
    }
}
