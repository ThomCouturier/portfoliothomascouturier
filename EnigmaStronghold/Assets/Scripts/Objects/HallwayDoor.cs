using DG.Tweening;
using UnityEngine;

public class HallwayDoor : Door
{
    private const float RATIO_DESCENDING_SCALE = 1.75f;
    
    [SerializeField] private ElementType affectedDragonElement;
    private bool isOpen;

    private void Start()
    {
        isOpen = true;
        Events.KeyCollectedVerificationEvent += CloseDoor;
        distanceToAscend = transform.lossyScale.z * RATIO_DESCENDING_SCALE;
    }

    private void CloseDoor(ElementType elementType)
    {
        if(!isOpen) return;
        if (!elementType.Equals(affectedDragonElement)) return;
        
        transform.DOMoveY(transform.position.y - distanceToAscend, transform.lossyScale.z);
        doorSFX.OnMetalDoorCloseSFX();
        isOpen = false;
    }

    protected new void OnDestroy()
    {
        Events.KeyCollectedVerificationEvent -= CloseDoor;
    }
}
