using Harmony;
using UnityEngine;

public class Potion : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag(Tags.Player) || other.gameObject.CompareTag(Tags.Ghost))
        {
            GlobalSFX.Instance.OnItemPickedUpSFX();
            Finder.Events.PickUpHeal();
            gameObject.SetActive(false);
        }
    }
}
