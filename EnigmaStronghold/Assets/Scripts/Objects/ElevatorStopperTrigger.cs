using Harmony;
using UnityEngine;

public class ElevatorStopperTrigger : MonoBehaviour
{
    [SerializeField] private GameObject stopper;
    private void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.CompareTag(Tags.GreenBox)) return;
        stopper.SetActive(true);
        gameObject.SetActive(false);
    }
}
