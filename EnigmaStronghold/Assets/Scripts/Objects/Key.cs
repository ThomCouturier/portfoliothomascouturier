using DG.Tweening;
using Harmony;
using UnityEngine;

public class Key : MonoBehaviour, IPickable
{
    private const int TRANSITION_TIME = 3;
    
    [SerializeField] private ElementType type;
    private GameObject keyFinalPosition;

    private void OnEnable()
    {
        keyFinalPosition = GameObject.Find(GameObjects.KeyFinalPosition);
    }

    public void SpawnKey(Vector3 position)
    {
        transform.position = position;
        gameObject.SetActive(true);
        transform.DOMove(keyFinalPosition.transform.position, TRANSITION_TIME);
    }

    public void PickUp()
    {
        GlobalSFX.Instance.OnKeyObtainedSFX();
        GlobalSFX.Instance.PlayGlobalAudioSource();
        Finder.Events.KeyPickedUp(type);
        gameObject.SetActive(false);
    }
}
