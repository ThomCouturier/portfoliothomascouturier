using DG.Tweening;
using UnityEngine;

public class Door : MonoBehaviour, RoomCompleted
{
    private const float RATIO_ASCEND_SCALE = 1.75f;

    [SerializeField] public RoomType roomType;
    [SerializeField] private float ascendingSpeedBuffer = 2.0f;
    protected float distanceToAscend;
    protected MetalDoorSFX doorSFX;

    private void Awake()
    {
        doorSFX = GetComponent<MetalDoorSFX>();
    }

    protected void Start()
    {
        Events.RoomCompletedEvent += OnRoomCompleted;
        distanceToAscend = transform.lossyScale.z * RATIO_ASCEND_SCALE;
    }

    protected void OpenDoor()
    {
        transform.DOMoveY(distanceToAscend + transform.position.y, transform.lossyScale.z * ascendingSpeedBuffer);
        doorSFX.OnMetalDoorOpenSFX();
    }

    public void CloseDoor()
    {
        transform.DOMoveY(-distanceToAscend + transform.position.y, transform.lossyScale.z * ascendingSpeedBuffer);
        doorSFX.OnMetalDoorOpenSFX();
    }

    public void OnRoomCompleted(RoomType roomType)
    {
        if (roomType.Equals(this.roomType))
            OpenDoor();
    }

    protected void OnDestroy()
    {
        Events.RoomCompletedEvent -= OnRoomCompleted;
    }
}
