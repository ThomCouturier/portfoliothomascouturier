using DG.Tweening;
using System.Collections;
using UnityEngine;

public class PressurePlate : MonoBehaviour, RoomCompleted
{
    private const int MOVE_TIME = 1;
    private const float Y_MOVEMENT = 0.5f;

    private void Start()
    {
        Events.PressurePlateRoomDoneEvent += OnRoomCompleted;
    }

    public void Activate()
    {
        IEnumerator Routine()
        {
            yield return transform.DOMove(transform.position - new Vector3(0, Y_MOVEMENT, 0), MOVE_TIME);
            PressurePlateRoomManager.instance.OnPressurePlatePressed();
            gameObject.SetActive(false);
        }
        StartCoroutine(Routine());
    }

    public void OnRoomCompleted(RoomType roomType)
    {
        if(roomType.Equals(RoomType.PressureFinal) && gameObject.activeSelf)
            gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        Events.PressurePlateRoomDoneEvent -= OnRoomCompleted;
    }
}
