public enum ElementType
{
    Fire,
    Ice,
    Poison,
    Air,
    Obsidian
}