using UnityEngine;
using Harmony;

public class Lever : MonoBehaviour, RoomCompleted
{
    private const float LEVER_OFF_ANGLE = -65;
    private const float LEVER_ON_ANGLE = -115;
    private bool leverOn = false;

    [SerializeField] private RoomType roomAffected;
    private GameObject lever;

    private void Awake()
    {
        Events.RoomCompletedEvent += OnRoomCompleted;
    }

    private void Start()
    {
        lever = transform.Find(GameObjects.LeverHandle).gameObject;
    }
    
    
    protected void MoveLever()
    {
        leverOn = !leverOn;
        if (leverOn)
        {
            lever.transform.eulerAngles = new Vector3(LEVER_ON_ANGLE, 0, 0);
        }
        else
        {
            lever.transform.eulerAngles = new Vector3(LEVER_OFF_ANGLE, 0, 0);
        }
    }

    public void OnRoomCompleted(RoomType roomType)
    {
        if (roomType.Equals(roomAffected))
        {
            GameObject.Find(GameObjects.LeverStimuli).SetActive(false);
        }
    }

    private void OnDestroy()
    {
        Events.RoomCompletedEvent -= OnRoomCompleted;
    }
}
