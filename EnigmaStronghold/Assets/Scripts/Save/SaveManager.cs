using System;
using System.Collections;
using Harmony;
using UnityEngine;

[Findable(Tags.SaveManager)]
public class SaveManager : MonoBehaviour
{
    public const string FILE_EXTENSION = ".bin";
    private const float ROOM_COMPLETION_VALIDATION_TIME = 0.1f;
    private const int MAX_KEY = 4;
    private static string FileName;
    public static string FileNameComplete
    {
        get => FileName + FILE_EXTENSION;
        set => FileName = value;
    }

    private SaveObject CurrentSave { get; set; }
    
    private void Awake()
    {
        gameObject.DontDestroyOnLoad();
        if (FileSaver.FileExist(FileNameComplete))
            ReadFile();
        else
            CreateNewSave();
    }

    private void Start()
    {
        Events.KeyPickedUpEvent += OnBossCompleted;
        Events.RoomCompletedEvent += OnRoomCompleted;
        Events.CompletedTutorialEvent += OnTutorialCompleted;
        Events.VictoryEvent += CompleteFinalBoss;
    }
    
    private void ReadFile()
    {
        try
        {
            CurrentSave = FileSaver.ReadFile<SaveObject>(FileNameComplete);
            CurrentSave.VerifyNeedToStartNewGame();
        }
        catch (Exception e)
        {
            Debug.LogError("file corrupted: " + e.Message);
            CreateNewSave();
        }
    }
    
    private void Save()
    {
        CurrentSave.Save();
        FileSaver.SaveFile(FileNameComplete, CurrentSave);
    }

    private void CreateNewSave()
    {
        CurrentSave = new SaveObject();
        CurrentSave.SetDefaultValues();
        FileSaver.SaveFile(FileNameComplete, CurrentSave);
    }
    
    public void WentToBossRoomTrigger()
    {
        CurrentSave.ToBossScene = true;
        Save();
    }

    public void CheckRoomCompletionState(RoomType roomType)
    {
        IEnumerator Routine()
        {
            //Attendre que tous les start soient complétés avant de lancer l'event
            yield return new WaitForSeconds(ROOM_COMPLETION_VALIDATION_TIME);
            switch (roomType)
            {
                case RoomType.Chess:
                    if (CurrentSave.ChessRoomDone)
                        Finder.Events.RoomCompleted(roomType);
                    break;
                case RoomType.Ghost:
                    if (CurrentSave.GhostRoomDone)
                        Finder.Events.RoomCompleted(roomType);
                    break;
                case RoomType.PressureFinal:
                    if (CurrentSave.PressureRoomDone)
                        Finder.Events.PressurePlateRoomDone(roomType);
                    break;
                case RoomType.Skull:
                    if (CurrentSave.SkullRoomDone)
                        Finder.Events.RoomCompleted(roomType);
                    break;
            }
        }
        StartCoroutine(Routine());
    }

    public void CheckBossCompleted(ElementType elementType)
    {
        switch (elementType)
        {
            case ElementType.Air:
                if (CurrentSave.AirBossDone)
                {
                    Finder.Events.KeyCollectedVerification(elementType);
                }
                break;
            case ElementType.Fire:
                if (CurrentSave.FireBossDone)
                {
                    Finder.Events.KeyCollectedVerification(elementType);
                }
                break;
            case ElementType.Ice:
                if (CurrentSave.IceBossDone)
                {
                    Finder.Events.KeyCollectedVerification(elementType);
                }
                break;
            case ElementType.Poison:
                if (CurrentSave.PoisonBossDone)
                {
                    Finder.Events.KeyCollectedVerification(elementType);
                }
                break;
        }
    }
    
    private void OnRoomCompleted(RoomType roomType)
    {
        switch (roomType)
        {
            case RoomType.Chess:
                CurrentSave.ChessRoomDone = true;
                if (!CurrentSave.MagnusCarlsenDone)
                {
                    CurrentSave.MagnusCarlsenDone = true;
                    Finder.Events.AchievementUnlocked(AchievementsEnum.MagnusCarlsen);
                }
                break;
            case RoomType.Ghost:
                CurrentSave.GhostRoomDone = true;
                if (!CurrentSave.InvisibleFriendDone)
                {
                    CurrentSave.InvisibleFriendDone = true;
                    Finder.Events.AchievementUnlocked(AchievementsEnum.InvisibleFriend);
                }
                break;
            case RoomType.PressureFinal:
                CurrentSave.PressureRoomDone = true;
                if (!CurrentSave.NoPressureDone)
                {
                    CurrentSave.NoPressureDone = true;
                    Finder.Events.AchievementUnlocked(AchievementsEnum.NoPressure);
                }
                break;
            case RoomType.Skull:
                CurrentSave.SkullRoomDone = true;
                if (!CurrentSave.DiagnosticNotColorBlindDone)
                {
                    CurrentSave.DiagnosticNotColorBlindDone = true;
                    Finder.Events.AchievementUnlocked(AchievementsEnum.DiagnosticNotColorBlind);
                }
                break;
        }

        Save();
    }
    
    private void OnBossCompleted(ElementType elementType)
    {
        switch (elementType)
        {
            case ElementType.Air:
                CurrentSave.AirBossDone = true;
                if (!CurrentSave.WindysDone)
                {
                    CurrentSave.WindysDone = true;
                    Finder.Events.AchievementUnlocked(AchievementsEnum.Windys);
                    ValidationAllKeysCollected();
                }
                break;
            case ElementType.Fire:
                CurrentSave.FireBossDone = true;
                if (!CurrentSave.BlastFlameOusDone)
                {
                    CurrentSave.BlastFlameOusDone = true;
                    Finder.Events.AchievementUnlocked(AchievementsEnum.BlastFlameOus);
                    ValidationAllKeysCollected();
                }
                break;
            case ElementType.Ice:
                CurrentSave.IceBossDone = true;
                if (!CurrentSave.IceToMeetYouDone)
                {
                    CurrentSave.IceToMeetYouDone = true;
                    Finder.Events.AchievementUnlocked(AchievementsEnum.IceToMeetYou);
                    ValidationAllKeysCollected();
                }
                break;
            case ElementType.Poison:
                CurrentSave.PoisonBossDone = true;
                if (!CurrentSave.PeptoBismolDone)
                {
                    CurrentSave.PeptoBismolDone = true;
                    Finder.Events.AchievementUnlocked(AchievementsEnum.PeptoBismol);
                    ValidationAllKeysCollected();
                }
                break;
        }

        Save();
    }

    private void ValidationAllKeysCollected()
    {
        if (GetNumberCollectedKeys() == MAX_KEY)
        {
            if(!CurrentSave.JanitorDone)
            {
                CurrentSave.JanitorDone = true;
                Finder.Events.AchievementUnlocked(AchievementsEnum.Janitor);
            }
        }
    }
    
    public int GetHealth()
    {
        return CurrentSave.PlayerHealth;
    }
    
    private void CompleteFinalBoss()
    {
        CurrentSave.FinalBossDone = true;
        if (!CurrentSave.YouSlayinDone)
        {
            CurrentSave.YouSlayinDone = true;
            Finder.Events.AchievementUnlocked(AchievementsEnum.YouSlayin);
        }
        Save();
    }

    private void OnTutorialCompleted()
    {
        CurrentSave.TutorialDone = true;
        Save();
    }

    public bool HasReachedBoss()
    {
        return CurrentSave.ToBossScene;
    }

    public bool IsTutorialDone()
    {
        return CurrentSave.TutorialDone;
    }

    public int GetNumberPotions()
    {
        return CurrentSave.Potions;
    }

    public void NoHealDone()
    {
        if(!CurrentSave.NoHealDone)
        {
            CurrentSave.NoHealDone = true;
            Finder.Events.AchievementUnlocked(AchievementsEnum.NoHeal);
            Save();
        }
    }
    
    public void DoomGuyDone()
    {
        if(!CurrentSave.DoomGuyDone)
        {
            CurrentSave.DoomGuyDone = true;
            Finder.Events.AchievementUnlocked(AchievementsEnum.DoomGuy);
            Save();
        }
    }

    public int GetNumberOfDeath()
    {
        return CurrentSave.DeathCount;
    }

    public void Death()
    {
        CurrentSave.DeathCount++;
    }

    public void UsePotion()
    {
        CurrentSave.PotionsUsed++;
    }

    public int GetNumberCollectedKeys()
    {
        int collectedKeys = 0;
        if (CurrentSave.AirBossDone)
            collectedKeys++;
        if (CurrentSave.FireBossDone)
            collectedKeys++;
        if (CurrentSave.IceBossDone)
            collectedKeys++;
        if (CurrentSave.PoisonBossDone)
            collectedKeys++;
        return collectedKeys;
    }

    public int GetNumberPotionsUsed()
    {
        return CurrentSave.PotionsUsed;
    }

    public void UpdatePotionCount(int potionCount)
    {
        CurrentSave.Potions = potionCount;
    }

    public bool BossIsCompleted(ElementType elementType)
    {
        switch (elementType)
        {
            case ElementType.Air:
                if (CurrentSave.AirBossDone)
                    return true;
                break;
            case ElementType.Fire:
                if (CurrentSave.FireBossDone)
                    return true;
                break;
            case ElementType.Ice:
                if (CurrentSave.IceBossDone)
                    return true;
                break;
            case ElementType.Poison:
                if (CurrentSave.PoisonBossDone)
                    return true;
                break;
        }

        return false;
    }
}
