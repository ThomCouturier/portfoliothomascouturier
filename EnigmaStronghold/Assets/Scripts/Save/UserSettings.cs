using System;

[Serializable]
public class UserSettings
{
    public bool PostProcessing { get; set; }
    public float Volume { get; set; }
}
