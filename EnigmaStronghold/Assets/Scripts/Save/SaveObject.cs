using System;
using Harmony;

[Serializable]
public class SaveObject
{
    #region Save

    public bool SkullRoomDone { get; set; }
    public bool ChessRoomDone { get; set; }
    public bool PressureRoomDone { get; set; }
    public bool GhostRoomDone { get; set; }
    public bool AirBossDone { get; set; }
    public bool FireBossDone { get; set; }
    public bool IceBossDone { get; set; }
    public bool PoisonBossDone { get; set; }
    public bool FinalBossDone { get; set; }
    public bool TutorialDone { get; set; }
    public bool ToBossScene { get; set; }
    public int PlayerHealth { get; set; }
    public int Potions { get; set; }
    public int DeathCount { get; set; }
    public int PotionsUsed { get; set; }

    #endregion
    
    #region Achivements

    public bool NoPressureDone { get; set; } = false;
    public bool DiagnosticNotColorBlindDone { get; set; } = false;
    public bool MagnusCarlsenDone { get; set; } = false;
    public bool InvisibleFriendDone { get; set; } = false;
    public bool JanitorDone { get; set; } = false;
    public bool IceToMeetYouDone { get; set; } = false;
    public bool PeptoBismolDone { get; set; } = false;
    public bool WindysDone { get; set; } = false;
    public bool BlastFlameOusDone { get; set; } = false;
    public bool YouSlayinDone { get; set; } = false;
    public bool NoHealDone { get; set; } = false;
    public bool DoomGuyDone { get; set; } = false;

    #endregion

    public void Save()
    {
        Player player = Finder.Player;
        PlayerHealth = player.Health;
        Potions = player.gameObject.GetComponent<PotionManager>().PotionCount;
    }
    public void SetDefaultValues()
    {
        ChessRoomDone = false;
        GhostRoomDone = false;
        PressureRoomDone = false;
        SkullRoomDone = false;
        AirBossDone = false;
        FireBossDone = false;
        IceBossDone = false;
        PoisonBossDone = false;
        FinalBossDone = false;
        TutorialDone = false;
        ToBossScene = false;
        
        PlayerHealth = Finder.Player.GetMaxHealth();
        Potions = PotionManager.INITIAL_NB_POTIONS;
    }

    public void VerifyNeedToStartNewGame()
    {
        if(FinalBossDone)
            StartNewGame();
    }

    private void StartNewGame()
    {
        ChessRoomDone = false;
        GhostRoomDone = false;
        PressureRoomDone = false;
        SkullRoomDone = false;
        AirBossDone = false;
        FireBossDone = false;
        IceBossDone = false;
        PoisonBossDone = false;
        FinalBossDone = false;
        TutorialDone = true;
        ToBossScene = false;
        PlayerHealth = Finder.Player.GetMaxHealth();
        Potions = PotionManager.INITIAL_NB_POTIONS;
    }
}