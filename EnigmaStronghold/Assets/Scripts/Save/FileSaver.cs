using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

public static class FileSaver
{
    public static void SaveFile<T>(string fileName, T save)
    {
        IFormatter formatter = new BinaryFormatter();
        Stream stream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.None);
        formatter.Serialize(stream, save);
        stream.Close();
    }

    public static T ReadFile<T>(string fileName)
    {
        IFormatter formatter = new BinaryFormatter();
        Stream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
        T save = (T)formatter.Deserialize(stream);
        stream.Close();
        return save;
    }

    public static bool FileExist(string fileName)
    {
        return File.Exists(fileName);
    }

    public static void DeleteFile(string fileName)
    {
        File.Delete(fileName);
    }
}
