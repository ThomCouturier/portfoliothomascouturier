using JetBrains.Annotations;
using UnityEngine;

public class UserSettingsManager : MonoBehaviour
{
    private const string FILE_NAME = "userSettings.bin";
    private UserSettings UserSettings { get; set; }
    private void Awake()
    {
        if (FileSaver.FileExist(FILE_NAME))
            UserSettings = FileSaver.ReadFile<UserSettings>(FILE_NAME);
        else
        {
            UserSettings = new UserSettings
            {
                Volume = 1,
                PostProcessing = true
            };
            Save();
        }
    }

    public float GetVolume()
    {
        return UserSettings.Volume;
    }

    public bool IsPostProcessing()
    {
        return UserSettings.PostProcessing;
    }

    [UsedImplicitly]
    public void ChangeVolume(float volume)
    {
        UserSettings.Volume = volume;
        Save();
    }
    
    [UsedImplicitly]
    public void ChangePostProcessing(bool postProcessing)
    {
        UserSettings.PostProcessing = postProcessing;
        Save();
    }

    private void Save()
    {
        FileSaver.SaveFile(FILE_NAME, UserSettings);
    }
}
