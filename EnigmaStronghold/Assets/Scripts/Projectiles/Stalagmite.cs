using DG.Tweening;
using Harmony;
using System.Collections;
using UnityEngine;

public class Stalagmite : MonoBehaviour
{
    private const float GROUND_OFFSET_Y = 2.9f;
    private const float IMPALE_DISTANCE = 2.1f;
    private const float SUMMON_TIME = 1f;
    private const float IMPALE_TIME = 0.5f;
    private const float ALIVE_TIME = 3f;

    [SerializeField] private int STALAGMITE_DAMAGE = 1;

    private IceSpecialAttackSFX sfx;

    private GameObject alert;

    private void Awake()
    {
        alert = transform.Find(GameObjects.Alert).gameObject;
        sfx = GetComponent<IceSpecialAttackSFX>();
    }

    public void Summon(Vector3 position)
    {
        alert.SetActive(true);
        sfx.PlaySpecialSpawnSFX();
        transform.position = position - (Vector3.up * GROUND_OFFSET_Y * transform.localScale.y);
        alert.transform.localScale = Vector3.zero;

        StartCoroutine(Impale());
    }

    private IEnumerator Impale()
    {
        alert.transform.DOScale(Vector3.one, SUMMON_TIME);
        yield return new WaitForSeconds(SUMMON_TIME);
        alert.SetActive(false);
        transform.DOMoveY(transform.position.y + (IMPALE_DISTANCE * transform.localScale.y), IMPALE_TIME);
        sfx.PlaySpecialSFX();
        yield return new WaitForSeconds(ALIVE_TIME);
        transform.DOMoveY(transform.position.y - (IMPALE_DISTANCE * transform.localScale.y), IMPALE_TIME);
        yield return new WaitForSeconds(IMPALE_TIME);
        gameObject.SetActive(false);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag(Tags.Player))
        {
            Player player = Finder.Player;
            player.Hit(STALAGMITE_DAMAGE);
            gameObject.SetActive(false);
        }
        else if (collision.gameObject.CompareTag(Tags.Box))
        {
            collision.gameObject.GetComponent<WoodBox>().Slash(STALAGMITE_DAMAGE, gameObject);
        }
    }
}
