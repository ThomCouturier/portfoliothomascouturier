using Harmony;
using UnityEngine;

public class AcidPuddle : MonoBehaviour
{
    [SerializeField] private int puddleDamage = 1;
    private PoisonSpecialAttackSFX sfx;

    private void Awake()
    {
        sfx = GetComponent<PoisonSpecialAttackSFX>();
    }
    private void OnEnable()
    {
        sfx.PlaySpecialSFX();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag(Tags.Player))
        {
            Finder.Player.Hit(puddleDamage);
            transform.parent.gameObject.SetActive(false);
        }
    }
}
