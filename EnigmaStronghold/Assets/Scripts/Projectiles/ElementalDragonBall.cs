using System;
using System.Collections;
using UnityEngine;

public class ElementalDragonBall : DragonBall
{
    protected const float GHOST_TIME = 0.8f;
    [SerializeField] protected Light ambiantLight;

    private new void Start()
    {
        base.Start();
    }

    public override void Summon(GameObject sender, GameObject target, float angle, float time)
    {
        wasDeflected = false;
        base.Summon(sender, target, angle, time);
        ambiantLight.intensity = 0;
    }

    protected override IEnumerator Summoning()
    {
        for (int i = 0; i < SUMMON_ITERATION; i++)
        {
            yield return new WaitForSeconds(summonTime / SUMMON_ITERATION);
            meshRenderer.enabled = true;
            Array.ForEach(colliders, collider => collider.enabled = true);
            float percentage = (i / SUMMON_ITERATION);
            transform.localScale = scale * percentage;
            ambiantLight.intensity = LIGHT_INTENSITY * percentage;
        }
        summoning = false;
        Shoot();
    }

    protected override void Hit(bool isSlash)
    {
        if (isSlash && !isGhost && !wasDeflected)
        {
            projectilesSFX.OnProjectileDeflectedSFX();
            GhostPhase(GHOST_TIME);
            splash.Emit(MAX_SPLASH_PARTICLES);
            Reverse(target, sender);
        }
    }

    private void Reverse(GameObject sender, GameObject target)
    {
        GameObject buffer = target;
        this.sender = sender;
        this.target = buffer;
        transform.LookAt(target.transform);
        wasDeflected = true;
        Shoot();
    }

    protected override void Vanish()
    {
        StartCoroutine(ExtinguishLights());
        base.Vanish();
    }

    private IEnumerator ExtinguishLights()
    {
        for (int i = (int)SUMMON_ITERATION; i >= 0; i--)
        {
            yield return new WaitForSeconds(summonTime / SUMMON_ITERATION);
            float percentage = (i / SUMMON_ITERATION);
            ambiantLight.intensity = LIGHT_INTENSITY * percentage;
        }
    }
}
