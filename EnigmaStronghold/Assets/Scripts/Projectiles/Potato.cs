using Harmony;
using UnityEngine;

public class Potato : MonoBehaviour, IBlockable, ISlashable
{
    [SerializeField] protected float speed;
    [SerializeField] protected float rotationSpeed;
    private GameObject target;
    private GameObject sender;
    private ProjectilesSFX sfx;
    private Rigidbody rb;
    private float timer;
    private bool hasBeenInteractedWith;

    private void OnEnable()
    {
        hasBeenInteractedWith = false;
    }

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Awake()
    {
        sfx = GetComponent<ProjectilesSFX>();
        Events.CompletedTutorialEvent += OnTutorialCompletion;
    }

    private void OnTutorialCompletion()
    {
        Destroy(gameObject);
    }

    public void Summon(GameObject target, GameObject sender)
    {
        sfx.PlayTravelSFX();
        this.sender = sender;
        this.target = target;
        transform.eulerAngles = sender.transform.eulerAngles;
    }

    private void FixedUpdate()
    {
        Vector3 difference = target.transform.position - transform.position;
        Quaternion rotation = Quaternion.LookRotation(difference, Vector3.up);
        timer += Time.deltaTime * rotationSpeed;
        transform.rotation = Quaternion.Lerp(transform.rotation, rotation, timer);
        rb.velocity = transform.forward * speed;
    }

    public void Block(GameObject sender)
    {
        if (hasBeenInteractedWith) return;
        gameObject.SetActive(false);
        Finder.Events.PotatoBlocked();
        hasBeenInteractedWith = true;
    }

    public void Slash(int damage, GameObject sender)
    {
        if(hasBeenInteractedWith) return;
        
        target = this.sender;
        sfx.StopTravelSFX();
        sfx.OnProjectileDeflectedSFX();
        Finder.Events.PotatoSliced();
        hasBeenInteractedWith = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag(Tags.Ballista))
        {
            gameObject.SetActive(false);
        }
    }

    private void OnDestroy()
    {
        Events.CompletedTutorialEvent -= OnTutorialCompletion;
    }
}
