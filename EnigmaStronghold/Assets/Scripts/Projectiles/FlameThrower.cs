using Harmony;
using System.Collections;
using UnityEngine;

public class FlameThrower : MonoBehaviour
{
    [SerializeField] private int damage = 1;
    private ParticleSystem particles;
    private FireSpecialAttackSFX sfx;

    private void Awake()
    {
        sfx = GetComponent<FireSpecialAttackSFX>();
        particles = GetComponent<ParticleSystem>();
        particles.Stop();
    }

    public void Shoot(float time)
    {
        sfx.PlaySpecialSFX();
        IEnumerator Shooting()
        {
            yield return new WaitForSeconds(time);
            particles.Stop();
        }
        particles.Play();
        StartCoroutine(Shooting());
    }

    private void OnParticleCollision(GameObject other)
    {
        if (other.CompareTag(Tags.Player))
        {
            Finder.Player.Hit(damage);
        }
        else if (other.CompareTag(Tags.Box))
        {
            other.GetComponent<WoodBox>().Slash(damage, gameObject);
        }
    }
}
