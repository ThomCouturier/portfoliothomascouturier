using Game;
using Harmony;
using System;
using System.Collections;
using UnityEngine;

public class DragonBall : MonoBehaviour, ISlashable, IBlockable
{
    protected const int MAX_SPLASH_PARTICLES = 150;
    protected const int DRAGON_BALL_DAMAGE = 5;
    protected const float DESTROY_TIME = 1.2f;
    protected const float SUMMON_ITERATION = 24;
    protected const float LIGHT_INTENSITY = 25;
    protected const float GHOST_BASE_TIME = 0.65f;

    [SerializeField] protected ParticleSystem splash;
    [SerializeField] protected ParticleSystem trail;
    [SerializeField] protected float speed;
    [SerializeField] protected float rotationSpeed;
    [SerializeField] protected ElementType type;

    protected Vector3 scale;
    protected GameObject target;
    protected GameObject sender;
    protected Renderer meshRenderer;
    protected Collider[] colliders;
    protected ProjectilesSFX projectilesSFX;
    protected ISensor<ISlashable> entitySensor;
    protected Rigidbody rb;

    protected float summonTime;
    protected bool summoning;
    protected bool destroyed = false;
    protected float timer;
    protected bool isGhost;
    public bool wasDeflected;

    private void Awake()
    {
        scale = transform.localScale;
        projectilesSFX = GetComponent<ProjectilesSFX>();
    }

    protected void Start()
    {
        Sensor sensor = transform.Find(GameObjects.EntitySensor).GetComponent<Sensor>();
        entitySensor = sensor.For<ISlashable>();
        entitySensor.OnSensedObject += OnHit;
        meshRenderer = gameObject.GetComponent<Renderer>();
        rb = GetComponent<Rigidbody>();
    }

    public virtual void Summon(GameObject sender, GameObject target, float angle, float summonTime)
    {
        isGhost = true;
        trail.Stop();
        destroyed = false;
        this.sender = sender;
        this.target = target;
        this.summonTime = summonTime;
        summoning = true;
        transform.localScale = new Vector3(0, 0, 0);
        transform.eulerAngles = sender.transform.eulerAngles - new Vector3(0, angle, 0);
        colliders = GetComponentsInChildren<Collider>();
        StartCoroutine(Summoning());
        StartCoroutine(GhostPhase(summonTime));
    }

    protected virtual IEnumerator Summoning()
    {
        for (int i = 0; i < SUMMON_ITERATION; i++)
        {
            yield return new WaitForSeconds(summonTime / SUMMON_ITERATION);
            meshRenderer.enabled = true;
            float percentage = (i / SUMMON_ITERATION);
            transform.localScale = scale * percentage;
        }
        summoning = false;

        Shoot();
    }

    protected void Shoot()
    {
        if (type == ElementType.Obsidian) wasDeflected = true;
        projectilesSFX.PlayTravelSFX();
        trail.Play();
        timer = 0;
    }

    void FixedUpdate()
    {
        if (target == null || destroyed) return;
        if (summoning)
        {
            transform.position = sender.transform.position + sender.transform.forward * (DragonAttack.DRAGON_MOUTH_OFFSET);
        }
        else
        {
            Vector3 difference = target.transform.position - transform.position;
            Quaternion rotation = Quaternion.LookRotation(difference, Vector3.up);
            timer += Time.deltaTime * rotationSpeed;
            transform.rotation = Quaternion.Lerp(transform.rotation, rotation, timer);
            rb.velocity = transform.forward * speed;
        }
    }

    public void OnHit(ISlashable slashable)
    {
        if (isGhost) return;
        MonoBehaviour slashed = (MonoBehaviour)slashable;
        if (slashed != null)
        {
            if (slashed.CompareTag(Tags.DragonBall)) return;
        }
        if (!slashed.CompareTag(Tags.Player))
        {
            if (!wasDeflected) return;
        }
        projectilesSFX.StopTravelSFX();
        GlobalSFX.Instance.OnProjectileImpactSFX(type);
        slashable.Slash(DRAGON_BALL_DAMAGE, gameObject);
        Vanish();
    }

    // When dragon ball is slashed/blocked (by sword)
    public void Slash(int damage, GameObject sender)
    {
        Hit(true);
    }

    public void Block(GameObject sender)
    {
        Hit(false);
    }

    protected virtual void Hit(bool isSlash)
    {
        Vanish();
    }

    protected virtual void Vanish()
    {
        destroyed = true;
        meshRenderer.enabled = false;
        rb.velocity = Vector3.zero;
        Array.ForEach(colliders, collider => collider.enabled = false);
        trail.Stop();
        splash.Emit(MAX_SPLASH_PARTICLES);
        if(gameObject.activeInHierarchy)
            StartCoroutine(Delete());
    }

    private IEnumerator Delete()
    {
        yield return new WaitForSeconds(DESTROY_TIME);
        gameObject.SetActive(false);
    }

    protected IEnumerator GhostPhase(float ghostTime)
    {
        yield return new WaitForSeconds(GHOST_BASE_TIME + ghostTime);
        Array.ForEach(colliders, collider => collider.enabled = true);
        isGhost = false;
    }
}