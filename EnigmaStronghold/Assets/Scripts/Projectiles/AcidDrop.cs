using Harmony;
using System.Collections;
using UnityEngine;

public class AcidDrop : MonoBehaviour
{
    private const float LIFE_TIME = 8;
    private const int DAMAGE = 2;

    private GameObject acidPuddle;
    private MeshRenderer dropMesh;
    private Collider dropCollider;
    private Rigidbody rb;

    private void Awake()
    {
        acidPuddle = transform.GetChild(0).gameObject;
        dropMesh = GetComponent<MeshRenderer>();
        dropCollider = GetComponent<Collider>();
        rb = GetComponent<Rigidbody>();
    }

    private void OnEnable()
    {
        IEnumerator Despawn()
        {
            yield return new WaitForSeconds(LIFE_TIME);
            gameObject.SetActive(false);
        }

        rb.isKinematic = false;
        dropMesh.enabled = true;
        dropCollider.enabled = true;
        acidPuddle.SetActive(false);
        StartCoroutine(Despawn());
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag(Tags.Ground))
        {
            rb.isKinematic = true;
            dropMesh.enabled = false;
            dropCollider.enabled = false;
            acidPuddle.SetActive(true);
        } else if(collision.gameObject.CompareTag(Tags.Player))
        {
            collision.gameObject.GetComponent<Player>().Hit(DAMAGE);
            gameObject.SetActive(false);
        }
    }
}
