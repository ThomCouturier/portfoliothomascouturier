using Harmony;
using UnityEngine;

public class GhostRoomManager : MonoBehaviour
{
    private void Start()
    {
        Finder.SaveManager.CheckRoomCompletionState(RoomType.Ghost);
    }
}
