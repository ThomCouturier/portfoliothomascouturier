using Harmony;
using UnityEngine;

public class PendantInventoryManager : MonoBehaviour
{
    [SerializeField] private GameObject pendant;
    private bool isEquipped;
    private GhostMovement ghostMovement;
    private PlayerSFX sfx;

    private void Awake()
    {
        sfx = GetComponent<PlayerSFX>();
    }

    private void OnEnable()
    {
        sfx.OnPendantInteraction();
        ghostMovement = GameObject.Find(GameObjects.MovingGhost).GetComponent<GhostMovement>();

        Events.RoomCompletedEvent += OnRoomCompleted;
        Events.GhostRespawnEvent += OnGhostRespawn;
        Events.PlayerRespawnEvent += OnPlayerRespawn;
    }

    private void OnPlayerRespawn()
    {
        if (isEquipped)
        {
            Store(); 
            enabled = false;
        }
        else
        {
            enabled = false;
        }
    }

    private void OnGhostRespawn()
    {
        if (!isEquipped) return;
        Store();
    }

    private void OnRoomCompleted(RoomType roomType)
    {
        if (roomType != RoomType.Ghost) return;
        Store();
        enabled = false;
            
        Events.RoomCompletedEvent -= OnRoomCompleted;
    }

    private void Update()
    {
        if (!ghostMovement)
        {
            Store();
            enabled = false;
            return;
        }
        if (!InputManager.Instance.Inputs.Player.InteractPendant.WasPressedThisFrame()) return;
        
        if (!isEquipped)
        {
            PullOut();
        }
        else
        {
            Store();
        }
        sfx.OnPendantInteraction();
    }

    private void Store()
    {
        isEquipped = false;
        ghostMovement.SetCanMove(false);
        pendant.SetActive(false);
    }

    private void PullOut()
    {
        isEquipped = true;
        ghostMovement.SetCanMove(true);
        pendant.SetActive(true);
    }
    
    private void OnDestroy()
    {
        Events.RoomCompletedEvent -= OnRoomCompleted;
        Events.GhostRespawnEvent -= OnGhostRespawn;
        Events.PlayerRespawnEvent -= OnPlayerRespawn;
    }
}
