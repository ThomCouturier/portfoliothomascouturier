using Harmony;
using UnityEngine;

public class GhostMovement : MonoBehaviour
{
    private const float MOVE_STOP_NERF = 0.8f;
    private const float MOMENTUM_TIME = 0.34f;
    private const float WALKING_PACE = 0.45f;
    private const float BLOCK_SPEED_NERF = 0.5f;

    private Rigidbody rigidbody;
    private InputManager inputManager;
    private GameObject player;
    private PlayerAttack melee;

    private float speed;
    private float walkTimer = 0f;
    private float acceleration = 0f;

    private bool canMove;

    void OnEnable()
    {
        rigidbody = GetComponent<Rigidbody>();
        inputManager = InputManager.Instance;
        player = Finder.Player.gameObject;
        melee = player.GetComponentInChildren<PlayerAttack>();
        speed = player.GetComponent<PlayerMovementController>().GetSpeed();
        canMove = false;
    }

    void FixedUpdate()
    {
        Vector2 movement = Vector2.zero;
        if (canMove)
        {
            movement = inputManager.Inputs.Player.Move.ReadValue<Vector2>();
            transform.eulerAngles = player.transform.eulerAngles;
        }

        Vector3 rbVelocity = rigidbody.velocity;

        if (movement == Vector2.zero)
        {
            acceleration = 0;
            walkTimer = 0f;
            rigidbody.velocity =
                new Vector3(rbVelocity.x * MOVE_STOP_NERF, rbVelocity.y, rbVelocity.z * MOVE_STOP_NERF);
        }
        else
        {
            acceleration = Mathf.Min(MOMENTUM_TIME, acceleration + Time.deltaTime);
            MovementPace();

            Vector3 direction = new Vector3(movement.x, 0, movement.y).normalized;
            Vector3 newVelocity = rigidbody.transform.TransformDirection(direction) * (speed * (acceleration / MOMENTUM_TIME));
            if (melee.Blocking) newVelocity *= BLOCK_SPEED_NERF;
            rigidbody.velocity = new Vector3(newVelocity.x, rbVelocity.y, newVelocity.z);
        }
        
    }

    private void MovementPace()
    {
        float rate = Time.deltaTime;
        walkTimer -= rate;
        if (walkTimer < 0)
        {
            walkTimer = WALKING_PACE;
        }
    }

    public void SetCanMove(bool movement)
    {
        canMove = movement;
    }
}