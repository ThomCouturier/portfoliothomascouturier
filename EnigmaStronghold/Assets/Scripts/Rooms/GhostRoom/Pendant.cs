using Harmony;
using UnityEngine;

public class Pendant : MonoBehaviour, IPickable, RoomCompleted
{
    private GhostMovement ghostMovement;
    private PendantInventoryManager inventory;
    private void OnEnable()
    {
        ghostMovement = Finder.Ghost.gameObject.GetComponent<GhostMovement>();
        inventory = Finder.Player.gameObject.GetComponent<PendantInventoryManager>();
        Events.RoomCompletedEvent += OnRoomCompleted;
    }

    public void PickUp()
    {
        ghostMovement.enabled = true;
        inventory.enabled = true;
        Finder.PlayerUI.DisplayGhostRoomHint();
        gameObject.SetActive(false);
    }

    public void OnRoomCompleted(RoomType roomType)
    {
        if(roomType.Equals(RoomType.Ghost))
            gameObject.SetActive(false);
        Events.RoomCompletedEvent -= OnRoomCompleted;
    }

    private void OnDestroy()
    {
        Events.RoomCompletedEvent -= OnRoomCompleted;
    }
}
