using Harmony;
using UnityEngine;

[Findable(Tags.Ghost)]
public class Ghost : MonoBehaviour, RoomCompleted
{
    private const float MAX_DISTANCE = 12f;
    
    private GameObject spawnPoint;
    private GameObject player;
    private GhostSFX sfx;
    bool playerHasEntered = false;

    private void Awake()
    {
        sfx = GetComponent<GhostSFX>();
    }
    void OnEnable()
    {
        Events.RoomCompletedEvent += OnRoomCompleted;
        Events.PlayerPickedUpPendantEvent += OnPlayerEntered;
        spawnPoint = GameObject.Find(GameObjects.SpawnForceField);
        player = GameManager.Instance.GetPlayer();
        Respawn();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag(Tags.GhostForceField))
        {
            Respawn();
            Finder.Events.GhostRespawn();
        }
    }

    private void Respawn()
    {
        sfx.OnGhostRespawn();
        transform.position = spawnPoint.transform.position;
    }

    private void Update()
    {
        if (transform.position.DistanceTo(player.transform.position) >= MAX_DISTANCE && playerHasEntered)
        {
            Finder.Events.PlayerIsTooFarFromGhost();
        }
        else
        {
            Finder.Events.PlayerIsNearGhost();
        }
    }

    public void OnRoomCompleted(RoomType roomType)
    {
        if (roomType == RoomType.Ghost)
            gameObject.SetActive(false);
        Events.RoomCompletedEvent -= OnRoomCompleted;
        Events.PlayerPickedUpPendantEvent -= OnPlayerEntered;
        playerHasEntered = false;
    }

    private void OnDestroy()
    {
        Events.RoomCompletedEvent -= OnRoomCompleted;
        Events.PlayerPickedUpPendantEvent -= OnPlayerEntered;
        playerHasEntered = false;
    }
    
    private void OnPlayerEntered()
    {
        playerHasEntered = true;
        Events.PlayerPickedUpPendantEvent -= OnPlayerEntered;
    }
}