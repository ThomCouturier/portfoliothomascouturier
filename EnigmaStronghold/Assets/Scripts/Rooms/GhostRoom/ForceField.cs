using Harmony;
using UnityEngine;

public class ForceField : MonoBehaviour
{
    private const float MIN_DISTANCE = 0.1f;
    
    [SerializeField] private Vector3 pointToReach;
    [SerializeField] private float speed;

    private Vector3 initialPosition;
    private Vector3 currentDestination;
    private void Start()
    {
        initialPosition = transform.position;
        currentDestination = pointToReach;
    }

    private void FixedUpdate()
    {
        if (transform.position.DistanceTo(currentDestination) < MIN_DISTANCE)
            currentDestination = currentDestination == initialPosition ? pointToReach : initialPosition;

        MoveForFrameTo(currentDestination);
    }

    private void MoveForFrameTo(Vector3 dest)
    {
        var frameMovement = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, dest, frameMovement);
    }
}
