using Harmony;
using UnityEngine;

public class EndRoomTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.CompareTag(Tags.Ghost)) return;
        
        Finder.Events.RoomCompleted(RoomType.Ghost);
    }
}
