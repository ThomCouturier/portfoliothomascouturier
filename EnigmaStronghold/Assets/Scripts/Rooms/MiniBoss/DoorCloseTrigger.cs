using Harmony;
using UnityEngine;

public class DoorCloseTrigger : MonoBehaviour
{
    [SerializeField] private Door door;
    [SerializeField] private ElementType elementType;
    private bool hasTriggered = false;

    void Start()
    {
        Events.KeyPickedUpEvent += OnKeyAcquired;
    }

    private void OnKeyAcquired(ElementType type)
    {
        if (elementType == type)
        {
            door.OnRoomCompleted(door.roomType);
            gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == Tags.Player && !hasTriggered)
        {
            door.CloseDoor();
            hasTriggered = true;
        }
    }
    
    private void OnDestroy()
    {
        Events.KeyPickedUpEvent -= OnKeyAcquired;
    }
}
