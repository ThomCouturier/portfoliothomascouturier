using Harmony;
using UnityEngine;

public class BossDoneVerificationTrigger : MonoBehaviour
{
    [SerializeField] private ElementType dragonElement;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Tags.Player))
            Finder.SaveManager.CheckBossCompleted(dragonElement);
    }
}
