using System.Collections.Generic;
using System.Linq;
using Harmony;
using UnityEngine;

[Findable(Tags.SkullPuzzleManager)]
public class SkullPuzzleManager : MonoBehaviour
{
    [SerializeField] private AudioClip skeletonFightClip;
    [SerializeField] private AudioClip khrenFightClip;

    private const int SKELETON_NB = 4;

    private Dictionary<ElementType, bool> skullsPlaced;
    private SkullRoomSFX skullSFX;
    private int skeletonKills;

    private void Start()
    {
        skullsPlaced = new Dictionary<ElementType, bool>()
        {
            [ElementType.Air] = false,
            [ElementType.Fire] = false,
            [ElementType.Ice] = false,
            [ElementType.Poison] = false
        };
        skullSFX = GetComponent<SkullRoomSFX>();
        Finder.SaveManager.CheckRoomCompletionState(RoomType.Skull);
    }

    public void SkullPlaced(ElementType type)
    {
        if (!skullsPlaced.ContainsKey(type))
            return;
        skullsPlaced[type] = true;
        skullSFX.OnRightSkullPlacementSFX();
        ValidatePuzzle();
    }
    
    private void ValidatePuzzle()
    {
        if(skullsPlaced.Values.All(skullPlaced => skullPlaced))
        {
            Finder.Events.RoomCompleted(RoomType.Skull);
        }
    }

    public void SkeletonKillTracker()
    {
        skeletonKills++;
        VerifySongStop();
    }

    private void VerifySongStop()
    {
        if (skeletonKills >= SKELETON_NB)
        {
            GlobalSFX.Instance.StopGlobalAudioSource();
        }
    }

    public void OnWrongSkullPlacement()
    {
        skullSFX.OnWrongSkullPlacementSFX();
    }

    public void OnWrongSkullRespawn()
    {
        skullSFX.OnSkullRespawnSFX();
    }

    public void OnSkullRespawn()
    {
        skullSFX.OnSkullRespawnSFX();
    }
}
