using Harmony;
using UnityEngine;

public class SkullPlacement : MonoBehaviour
{
    [SerializeField] private GameObject skullPlacement;
    [SerializeField] private ElementType skullType;
    [SerializeField] private int pushForce = 250;
    private SkullPuzzleManager puzzleManager;
    private bool canInteract = true;

    private void Start()
    {
        puzzleManager = Finder.SkullPuzzleManager;
    }

    public void PlaceSkull(GameObject skull)
    {
        if (IsMatchingSkull(skull))
        {
            skull.transform.SetParent(null);
            PlaceSkullAtPosition(skull);
            skull.GetComponent<DataSkull>().Place();
            puzzleManager.SkullPlaced(skullType);
            Finder.PlayerUI.DisplayCrosshair();
            canInteract = false;
        }
        else
        {
            puzzleManager.OnWrongSkullPlacement();
            PlaceSkullAtPosition(skull);
            RejectSkull(skull);
        }
    }

    private void PlaceSkullAtPosition(GameObject skull)
    {
        skull.transform.position = skullPlacement.transform.position;
        skull.transform.rotation = skullPlacement.transform.rotation;
    }

    private void RejectSkull(GameObject skull)
    {
        Rigidbody skullRigidbody = skull.gameObject.GetComponent<Rigidbody>();
        skull.GetComponent<Collider>().enabled = true;
        skullRigidbody.isKinematic = false;
        skull.transform.parent = null;
        skullRigidbody.AddForce(transform.up * pushForce + transform.forward * (pushForce * 0.2f));
    }

    private bool IsMatchingSkull(GameObject skull)
    {
        DataSkull data = skull.GetComponent<DataSkull>();
        if (data != null)
            return data.type == skullType;

        return false;
    }
    
    public bool CanInteract()
    {
        return canInteract;
    }
}
