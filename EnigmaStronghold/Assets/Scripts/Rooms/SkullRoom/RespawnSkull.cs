using Harmony;
using UnityEngine;

public class SkullRespawn : MonoBehaviour
{
    private const int MAX_RESPANW_COOLDOWN = 10;
    private const float MAX_DISTANCE = 1.5f;

    [SerializeField] private DataSkull skullData;

    private GameObject player;
    private float respawnCooldown;
    private Vector3 initialPosition;
    private ItemPossession playerPossession;

    private void Start()
    {
        player = GameObject.Find(GameObjects.Player);
        playerPossession = player.GetComponent<ItemPossession>();
        respawnCooldown = MAX_RESPANW_COOLDOWN;
        initialPosition = transform.position;
    }

    private void Update()
    {
        if (skullData.IsPlaced())
            return;
        
        if (playerPossession.IsHoldingItem() || transform.position.DistanceTo(initialPosition) < MAX_DISTANCE)
        {
            respawnCooldown = MAX_RESPANW_COOLDOWN;
        }
        else 
        {
            if ((respawnCooldown -= Time.deltaTime) <= 0)
                RespawnSkull();
        }
    }

    private void OnEnable()
    {
        transform.parent = null;
        Rigidbody itemRigidbody = gameObject.GetComponent<Rigidbody>();
        GetComponent<Collider>().enabled = true;
        itemRigidbody.isKinematic = false;
    }

    private void RespawnSkull()
    {
        Finder.SkullPuzzleManager.OnSkullRespawn();
        transform.position = initialPosition;
        respawnCooldown = MAX_RESPANW_COOLDOWN;
    }
}
