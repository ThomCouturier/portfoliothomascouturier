using System.Collections.Generic;
using Harmony;
using UnityEngine;

public class SkullSpawner : MonoBehaviour
{
    private const int AIR_SKULL_LIGHT_INTENSITY = 1;
    private const int ICE_SKULL_LIGHT_INTENSITY = 10;
    private const int POISON_SKULL_LIGHT_INTENSITY = 3;
    private const int FIRE_SKULL_LIGHT_INTENSITY = 3;

    [SerializeField] private PoolManager poolManager;
    [SerializeField] private Texture airTexture;
    [SerializeField] private Texture fireTexture;
    [SerializeField] private Texture iceTexture;
    [SerializeField] private Texture poisonTexture;
    private Dictionary<ElementType, Texture> textures;
    
    public static SkullSpawner Instance;

    private void Start()
    {
        textures = new Dictionary<ElementType, Texture>()
        {
            [ElementType.Air] = airTexture,
            [ElementType.Fire] = fireTexture,
            [ElementType.Ice] = iceTexture,
            [ElementType.Poison] = poisonTexture,
        };
    }

    private void Awake()
    {
        if (Instance == null)
            Instance = this;

        else if (Instance != this)
            Destroy(gameObject);

        gameObject.DontDestroyOnLoad();
    }

    public void Spawn(ElementType type, Vector3 position)
    {
        GameObject skull = poolManager.FetchFirstInactiveActor();
        skull.transform.position = position;
        skull.transform.Find(GameObjects.Skull).GetComponent<Renderer>().material.mainTexture = textures[type];
        DataSkull skullData = skull.GetComponent<DataSkull>();
        skullData.type = type;
        SetColor(type, skull);
        skull.SetActive(true);
    }

    private void SetColor(ElementType type, GameObject skull)
    {
        Light light = skull.GetComponent<Light>();
        
        switch (type)
        {
            case ElementType.Air:
                light.intensity = AIR_SKULL_LIGHT_INTENSITY;
                light.color = Color.white;
                break;
            case ElementType.Fire:
                light.intensity = FIRE_SKULL_LIGHT_INTENSITY;
                light.color = Color.red;
                break;
            case ElementType.Ice:
                light.intensity = ICE_SKULL_LIGHT_INTENSITY;
                light.color = Color.blue;
                break;
            case ElementType.Poison:
                light.intensity = POISON_SKULL_LIGHT_INTENSITY;
                light.color = Color.green;
                break;
        }
    }

    public void DeleteAllSkulls()
    {
        poolManager.DestroyAll();
    }

    private void OnDestroy()
    {
        DeleteAllSkulls();
    }
}
