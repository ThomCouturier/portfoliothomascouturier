using UnityEngine;

public class DataSkull : MonoBehaviour
{
    [SerializeField] public ElementType type;
    private bool isPlaced;

    public bool IsPlaced()
    {
        return isPlaced;
    }

    public void Place()
    {
        isPlaced = true;
    }
}
