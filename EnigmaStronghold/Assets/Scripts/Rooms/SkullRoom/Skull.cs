using Harmony;
using UnityEngine;

public class Skull : MonoBehaviour, IPickable
{
    public void PickUp()
    {
        Finder.Events.SkullPickedUp(gameObject);
    }
}
