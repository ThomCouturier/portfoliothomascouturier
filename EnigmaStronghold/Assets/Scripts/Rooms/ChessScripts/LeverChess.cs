using UnityEngine;

public class LeverChess : Lever, ISlashable
{
    public void Slash(int damage, GameObject sender)
    {
        ChessRoomManager.Instance.ActivateLever();
    }
}
