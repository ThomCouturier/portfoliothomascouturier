using UnityEngine;

public class ChessPiece : MonoBehaviour
{
    private ChessPuzzleManager chessPlayerManager;
    private AudioSource audioSource;

    private void Start()
    {
        chessPlayerManager = ChessPuzzleManager.Instance;
        audioSource = GetComponent<AudioSource>();
    }

    private void OnMouseOver()
    {
        if (chessPlayerManager.SelectedPiece == null)
        {
            chessPlayerManager.SetChessHighlight(gameObject);
        }
    }

    private void OnMouseExit()
    {
        if (chessPlayerManager.SelectedPiece == null)
        {
            chessPlayerManager.DisableChessHighlight();
        }
    }

    private void OnMouseUpAsButton()
    {
        GameObject selectedPiece = chessPlayerManager.SelectedPiece;
        if (selectedPiece == gameObject)
        {
            chessPlayerManager.SelectedPiece = null;
            audioSource.Play();
            
        }
        else
        {
            chessPlayerManager.SelectedPiece = gameObject;
            chessPlayerManager.SetChessHighlight(gameObject);
            audioSource.Play();
        }
    }
}
