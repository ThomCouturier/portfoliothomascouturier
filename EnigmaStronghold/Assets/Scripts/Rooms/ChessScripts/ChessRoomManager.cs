using Harmony;
using UnityEngine;

public class ChessRoomManager : MonoBehaviour
{
    [SerializeField] private GameObject door;
    [SerializeField] private AudioClip knightFightClip;
    [SerializeField] private AudioClip khrallidFightClip;
    
    private ChessPuzzleManager chessPlayerManager;
    private GameObject player;
    private GameObject chessCamera;
    private GameObject leverLight;
    private GameObject kingLight;
    private GameObject hintLight;
    private GameObject queenLight;
    private MeshCollider glassFloor;
    private StoneKnight stoneKnight;
    private ChessRoomSFX roomSFX;

    private const string CHESS_HINT =
        "The opponents king is weak. Find the only move that ends the game. (Positions name : Fool's Mate)";
    
    public GameObject torches;
    public static ChessRoomManager Instance;

    private bool puzzleSuccess;
    private bool enemyDefeated;
    private int failCounter;


    private void Awake()
    {
        if (Instance == null)
            Instance = this;

        else if (Instance != this)
            Destroy(gameObject);
    }
    private void Start()
    {
        chessPlayerManager = GameObject.Find(GameObjects.ChessPuzzleManager).GetComponent<ChessPuzzleManager>();
        chessCamera = GameObject.Find(GameObjects.ChessCamera);
        player = GameObject.Find(GameObjects.Player);
        leverLight = GameObject.Find(GameObjects.LeverLight);
        kingLight = GameObject.Find(GameObjects.KingLight);
        hintLight = GameObject.Find(GameObjects.HintLight);
        queenLight = GameObject.Find(GameObjects.QueenLight);
        roomSFX = GetComponent<ChessRoomSFX>();
        glassFloor = GameObject.Find(GameObjects.Glass_Floor).GetComponent<MeshCollider>();
        stoneKnight = GameObject.Find(GameObjects.StoneKnight).GetComponent<StoneKnight>();
        
        puzzleSuccess = false;
        enemyDefeated = false;
        chessPlayerManager.enabled = false;
        chessCamera.SetActive(false);
        leverLight.SetActive(false);
        kingLight.SetActive(false);
        hintLight.SetActive(false);
        queenLight.SetActive(false);
        Finder.SaveManager.CheckRoomCompletionState(RoomType.Chess);

        failCounter = 0;
    }

    public void PuzzleCompleted(bool succeeded)
    {
        chessPlayerManager.enabled = false;
        chessCamera.SetActive(false);
        glassFloor.enabled = true;
        torches.SetActive(true);
        player.SetActive(true);
        kingLight.SetActive(false);
        leverLight.SetActive(false);
        hintLight.SetActive(false);
        queenLight.SetActive(false);

        InputManager.Instance.ActivatePlayerInputs();
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        failCounter++;

        PuzzleAnalysis(succeeded);
    }

    public void ActivateLever()
    {
        if (!puzzleSuccess && enemyDefeated)
        {
            chessPlayerManager.enabled = true;
            player.SetActive(false);
            torches.SetActive(false);
            leverLight.SetActive(false);
            kingLight.SetActive(true);
            glassFloor.enabled = false;
            chessCamera.SetActive(true);
            Finder.PlayerUI.ShowHint(CHESS_HINT);
            InputManager.Instance.ActivatePlayerChessInputs();
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            
            if (failCounter > 0)
            {
                hintLight.SetActive(true);
                queenLight.SetActive(true);
            }
        }
    }

    private void PuzzleAnalysis(bool succeeded)
    {
        if (succeeded)
        {
            Finder.Events.RoomCompleted(RoomType.Chess);
            puzzleSuccess = true;
            roomSFX.OnPuzzleCompletedSFX();
        }
        else
        {
            puzzleSuccess= false;
            enemyDefeated= false;
            stoneKnight.Revive();
            roomSFX.OnPuzzleFailedSFX();
            GlobalSFX.Instance.PlayGlobalAudioSource();
        }
    }

    public void FightOver()
    {
        enemyDefeated = true;
        leverLight.SetActive(true);
        GlobalSFX.Instance.StopGlobalAudioSource();
    }
}
