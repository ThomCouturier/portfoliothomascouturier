using UnityEngine;

public class ChessCoordinate : MonoBehaviour
{
    private ChessPuzzleManager chessPlayerManager;
    private AudioSource audioSource;

    private void Start()
    {
        chessPlayerManager = ChessPuzzleManager.Instance;
        audioSource = GetComponent<AudioSource>();
    }

    private void OnMouseOver()
    {
        if (chessPlayerManager.SelectedPiece != null && chessPlayerManager.SelectedCoordinate == null)
        {
            chessPlayerManager.SetCoordinateHighlight(gameObject);
        }
    }

    private void OnMouseExit()
    {
        if (chessPlayerManager.SelectedPiece != null && chessPlayerManager.SelectedCoordinate == null)
        {
            chessPlayerManager.DisableCoordinateHighlight();
        }
    }

    private void OnMouseUpAsButton()
    {
        GameObject selectedPiece = chessPlayerManager.SelectedPiece;
        if (selectedPiece != null)
        {
            chessPlayerManager.SelectedCoordinate = gameObject;
            audioSource.Play();
        }

    }
}
