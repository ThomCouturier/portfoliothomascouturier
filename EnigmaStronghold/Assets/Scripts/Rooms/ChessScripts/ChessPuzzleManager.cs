using UnityEngine;

public class ChessPuzzleManager : MonoBehaviour
{
    [SerializeField] private GameObject winningPiece;
    [SerializeField] private GameObject winningCoordinate;
    [SerializeField] private Light pieceLight;
    [SerializeField] private Light positionLight;

    public GameObject SelectedPiece{ get; set; }
    public GameObject SelectedCoordinate { get; set; }

    public static ChessPuzzleManager Instance;
    
    private bool success;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;

        else if (Instance != this)
            Destroy(gameObject);
    }

    private void OnEnable()
    {
        SelectedPiece = null;
        SelectedCoordinate = null;

        pieceLight.enabled = false;
        positionLight.enabled = false;
        success= false;
    }

    private void OnDisable()
    {
        pieceLight.enabled = false;
        positionLight.enabled = false;
    }

    void Update()
    {
        if (SelectedPiece != null && SelectedCoordinate != null)
        {
            VerifyMove();
        }
    }

    private void VerifyMove()
    {
        if (SelectedPiece == winningPiece && SelectedCoordinate == winningCoordinate)
        {
            success = true;
        }
        else
        {
            success = false;
        }
        ChessRoomManager.Instance.PuzzleCompleted(success);
    }

    public void SetChessHighlight(GameObject chessPiece)
    {
        pieceLight.transform.position = new Vector3(chessPiece.transform.position.x, pieceLight.transform.position.y, chessPiece.transform.position.z);
        pieceLight.enabled = true;
    }

    public void SetCoordinateHighlight(GameObject movePosition)
    {
        positionLight.transform.position = new Vector3(movePosition.transform.position.x, positionLight.transform.position.y, movePosition.transform.position.z);
        positionLight.enabled = true;
    }

    public void DisableChessHighlight()
    {
        pieceLight.enabled = false;
    }
    public void DisableCoordinateHighlight()
    {
        positionLight.enabled = false;
    }
}
