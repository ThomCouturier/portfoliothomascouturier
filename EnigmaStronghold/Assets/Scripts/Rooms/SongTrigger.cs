using Harmony;
using UnityEngine;

public class SongTrigger : MonoBehaviour
{
    [SerializeField] public AudioClip song;
    private bool hasTriggered = false;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == Tags.Player && !hasTriggered)
        {
            hasTriggered = true;
            GlobalSFX.Instance.StopGlobalAudioSource();
            GlobalSFX.Instance.SetGlobalAudioSource(song);
            GlobalSFX.Instance.PlayGlobalAudioSource();
        }
    }
}
