public enum RoomType
{
    Chess,
    Ghost,
    PressureRoomFirstDoor,
    PressureRoomSecondDoor,
    PressureFinal,
    Skull
}
