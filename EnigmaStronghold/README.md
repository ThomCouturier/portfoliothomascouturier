<div align="center">

![Enigma: Stronghold](.docs/Logo.png)

# Projet Synthèse

</div>

Enigma : Stronghold, est un jeu d’action et d’aventure en 3D à la première personne se déroulant dans le château de Mal’hek, rempli de bestioles maléfiques comme des morts-vivants, des ogres, des mutants et bien d’autres. 
Le personnage principal, Williaume de Brossard, a pour but de purger le mal de ce lieu et d’en recouvrir les richesses pour couvrir son nom de gloire et d’honneur. 

Pour ce faire, il devra vaincre Khan’Ban the Cruel, le dragon légendaire, ainsi que ses 4 enfants, Khralid the Frozen (dragon de glace), Khrom the Ember of Fear (dragon de feu), Khren the Plaguebearer (dragon du poison) et Khalled, the Breath of Death (dragon de l’air), qui gardent avec avarice les richesses de leur temple. 
Pour accéder aux dragons, le joueur devra acquérir 4 clés cachées derrière des vagues d’ennemis et des énigmes conçues par les cultistes du prêtre Jimmy de Gilbertine, dit le sage de Mal’hek. 

L’ambiance sombre et machiavélique du château a dissuadé les plus braves chevaliers du pays pendant près de trois centenaires. 
Cependant, lorsque la bravoure d’un chevalier n’a d’égal que sa soif de gloire et de pièces d’or, même la promesse d’une mort certaine s’est avérée vaine devant le chevalier sot qu’est Williaume de Brossard.   

<div align="center">

![Aperçu du Projet Synthèse](.docs/Preview.png)

</div>

## Démarrage rapide

Ces instructions vous permettront d'obtenir une copie opérationnelle du projet sur votre machine à des fins de 
développement.

### Prérequis

* [Git] - Système de contrôle de version. Utilisez la dernière version.
* [Rider] ou [Visual Studio] - IDE. Vous pouvez utiliser également n'importe quel autre IDE: assurez-vous simplement 
  qu'il supporte les projets Unity.
* [Unity 2022.2.3f1] - Moteur de jeu. Veuillez utiliser **spécifiquement cette version.** Attention à ne pas installer
  Visual Studio une seconde fois si vous avez déjà un IDE.

*Seul le développement sur Windows est complètement supporté.*

### Compiler une version de développement

Clonez le dépôt et ouvrez le projet dans Unity. Ensuite, ouvrez la scène `Main` et appuyez sur le bouton *Play*.

### Compiler une version stable

Ouvrez le projet dans Unity. Ensuite, allez dans `File | Build Settings` et compilez le projet **dans un dossier vide**.

## Développement

Ces instructions visent à documenter comment contribuer au développement de certaines parties du projet. Les décisions
architecturales importantes seront aussi décrites ici.

### Fichiers auto-générés

Ce projet contient une bonne quantité de code généré automatiquement. Par exemple, la classe `Tags` est générée 
automatiquement à partir des *Tags* du projet Unity. Cependant, la génération en elle-même n'est pas automatique et
doit être lancée manuellement. Pour ce faire, accédez au menu `Tools | Harmony | Code Generator`. Une fenêtre devrait
s'ouvrir où vous pourrez générer le code lorsque nécessaire. 

Il est à noter que ce code est envoyé sur le dépôt. Soyez donc prudents lors des fusions entre les différentes branches
et préférez regénéner le code en cas de doutes.

![Générateur code Code](.docs/CodeGeneratorWindow.png)

Le générateur de code en lui-même est un exécutable externe et fait partie du *package* `Harmony`. Pour plus 
d'informations sur ce qu'il fait, consultez le [dépôt officiel][Harmony Code Generator].

### New Input System

Afin de simplifier la gestion des entrées, le [New Input System] est utilisé au lieu de celui intégré au moteur. Le 
fichier de configuration des entrées se trouve dans `Assets/Settings/InputActions`. Pour plus d'informations sur le 
nouveau *Input System*, consultez son [manuel d'utilisation][New Input System].

Il est à noter que ce fichier génère lui aussi du code C# (voir la classe `InputActions`) et que ce dernier est lui 
aussi inclus dans le dépôt. Soyez donc prudent lors des fusions.

![New Input System](.docs/NewInputSystem.png)

## Intégration Continue

Ce projet utilise [GitLab CI] pour effectuer de l'intégration continue avec les images *Docker* fournies par [GameCI].
Pour fonctionner, ces images ont besoin d'une license *Unity* valide. Cette license doit être ajouté dans la
variable d'environnement `UNITY_LICENSE` pour le projet. 

Voici comment récupérer ce fichier de licence. Premièrement, dans une machine avec *Docker*, exécutez la commande 
suivante. Remplacez le contenu des variables `UNITY_USERNAME` et `UNITY_PASSWORD` par les informations de connexion
pour le compte à utiliser.

```shell
docker run -it --rm \
-e "TEST_PLATFORM=linux" \
unityci/editor:2022.2.2f1-windows-mono-1 \
bash -c \
"unity-editor \
-batchmode \
-nographics \
-createManualActivationFile \
-username \"$UNITY_USERNAME\" -password \"$UNITY_PASSWORD\" > /dev/null; \
cat /*.alf"
```

Vous aurez en sortie un fichier XML. Placez le tout dans un fichier nommé `Unity3d.alf`, que vous utiliserez pour 
effectuer l'activation manuelle sur le site de Unity (voir `https://license.unity3d.com/manual`). En retour, vous 
obtiendrez un fichier `Unity_v20XX.x.ulf` dont le contenu doit être mis dans la variable `UNITY_LICENSE` du projet.

## Tester le projet

Vous êtes fortement encouragés à tester [la dernière version][Develop Download] de développement du jeu. Si vous 
rencontrez un bogue, vous êtes priés de le [signaler][Submit Bug] et de fournir une explication détaillée du problème 
avec les étapes pour le reproduire. Les captures d'écran et les vidéos sont les bienvenues.

## Contribuer au projet

Veuillez lire [CONTRIBUTING.md](CONTRIBUTING.md) pour plus de détails sur notre code de conduite.

## Auteurs

* **Félix Bégin** - *Programmeur*
  * Logique du changement du chargement et déchargement des scènes.
  * Persistence des données d'une partie.
  * Succès ("Achievements").
  * Salle des crânes.
* **Thomas Couturier** - *Programmeur*
  * Salle des plaques de pression.
  * Interface utilisateur.
  * Gitlab CI.
* **Oleg Petukhov** - *Programmeur*
  * Combat final contre Khan’Ban.
  * Mouvement du joueur.
  * Modélisation de différents modèles.
  * Animations.
  * Système d'entités.
  * Système de combat.
  * Miniboss.
  * Boites / potions.
* **Philippe Tremblay-Paradis** - *Programmeur*
  * Recherche des *assets*. 
  * Conception de la salle d'echec (chess room) et implementation de ses mecaniques / combat.
  * Conception de la salle principale (hub world) et implementation des ses mecaniques.
  * Conception et decoration du corridor de gauche (leftHallways).
  * Design sonore.
  * Conceptualisation des combats et habileté des ennemis (brainstorming).
  * Implementation de mecaniques globales : Levier, Clé.
  * Design visuel diverse.
* **Zackary G Tremblay** - *Programmeur*
  * Logique du changement du chargement et déchargement des scènes.
  * Menu principal.
  * Tutoriel.
  * Salle du fantôme.
  * Interface du joueur
  * Contournement *don't destory on load* de Unity.
  * Logique de ramassage d'objets.
* **Benjamin Lemelin** - *Professeur en informatique*
  * Générateur de code. Voir [le dépôt][Harmony Code Generator] pour plus d'informations.

## Remerciements

* [GameCI] - Images Docker d'intégration continue pour *Unity*.

[//]: # (Hyperliens)
[Git]: https://git-scm.com/downloads
[Rider]: https://www.jetbrains.com/rider/
[Visual Studio]: https://www.visualstudio.com/fr/
[Unity 2022.2.3f1]: https://unity3d.com/fr/get-unity/download/
[Harmony Code Generator]: https://gitlab.com/harmony-unity/code-generator
[New Input System]: https://docs.unity3d.com/Packages/com.unity.inputsystem@1.0/manual/index.html
[GitLab CI]: https://docs.gitlab.com/ee/ci/
[GameCI]: https://game.ci/

[Submit Bug]: https://gitlab.com/ggg7575004/projet-synthese-post-alpha/issues/new?issuable_template=Bug
[Stable Download]: https://gitlab.com/ggg7575004/projet-synthese-post-alpha/-/jobs/artifacts/main/download?job=build
[Develop Download]: https://gitlab.com/ggg7575004/projet-synthese-post-alpha/-/jobs/artifacts/staging/download?job=build
