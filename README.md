The first project you will find in the portfolio was made as my synthesis project at the end of my DEC in computer science. This work was done in 6 weeks in collaboration with Philippe Tremblay Paradis, Oleg Petukhov, Zackary G Tremblay and Félix Bégin.
In this project, I was responsible for the pressure plate room, the CI/CD, the UI and helping resolving bugs. 

//

Le premier travail que vous pouvez trouver dans ce portfolio a été effectué dans le cadre de mon projet synthèse à la fin de mon DEC en informatique. Ce travail a été effectué en 6 semaines en collaboration avec Philippe Tremblay Paradis, Oleg Petukhov, Zackary G Tremblay et Félix Bégin. 
Dans ce projet, j'ai été responsable de la salle des plaques de pression, du CI/CD, du UI ainsi que d'aider avec la résolution de bugs. 
![Lobby Enigma Stronghold](Images/EnigmaStrongholdLoadingScreen.png)
![Chess puzzle room](Images/EnigmaStrongholdChess.png)
![Box puzzle room](Images/EngimaStrongholdRoom.png)
To download an executable version of Enigma Stronghold and to see a trailer of the game, follow this link: https://yitzboi.itch.io/enigma-stronghold. 

//

Pour télécharger une version exécutable du projet Enigma Stronghold et pour voir une bande annonce du jeu, diriger-vous vers le lien suivant: https://yitzboi.itch.io/enigma-stronghold. 

The next project in one focused on AI, made in collaboration with Rémy Garenc. In this game, the player has no control over the game and the AI, a state machine, dictates the bahavior of both sides until only one of them remains. 

//

Le prochain travail que vous pouvez consulter est un travail centré autour de l'IA, développé en collaboration avec Rémy Garenc. Dans ce jeu, le joueur n'a pas de contrôle et l'IA, qui est en réalité une machine à états, s'occupe de se faire affronter 2 camps jusqu'à ce que seulement l'un d'entre eux soit encore en vie.
![AI day mode](Images/AIDay.png) 
![AI evening mode](Images/AIEvening.png) 
![AI night mode](Images/AINight.png) 

The last project you will find here is World Task Force, made in collaboration with Philippe Tremblay Paradis and Rémy Garenc. In this game inspired by the X-COM franchise, you select characters and make your way through a map filled with ennemies to collect objectives and win the game. 

//

Le dernier travail que vous pouvez consulter se nomme World Task Force et a été conçu en collaboration avec Philippe Tremblay Paradis et Rémy Garenc. Dans ce jeu inspiré de la franchise X-COM, il est possible de choisir parmi de multiples personnages pour ensuite choisir la bonne stratégie afin de mener ces personnages à différents objectifs qui peuvent être collectés afin de remporter la partie. 
![WorldTaskForce character selection](Images/WorldTaskForce_Selection.png) 
![WorldTaskForce movement](Images/WorldTaskForce_Moving.png) 
![WorldTaskForce shooting](Images/WorldTaskForce_Shooting.png) 


Philippe Tremblay Paradis: https://www.linkedin.com/in/philippe-tremblay-paradis-98bb03224/

Oleg Petukhov: https://www.linkedin.com/in/oleg-petukhov-019353208/

Zackary G Tremblay: https://www.linkedin.com/in/zackary-g-tremblay-625488200/

Félix Bégin: https://www.linkedin.com/in/f%C3%A9lix-b%C3%A9gin-b53939254/

Rémy Garenc: https://www.linkedin.com/in/r%C3%A9my-garenc-277b29173/

My Linkedin: https://www.linkedin.com/in/thomas-couturier-/
