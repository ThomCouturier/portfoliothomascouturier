using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Tilemaps;

public class NodeSelectionTool : MonoBehaviour
{
    private Tilemap _tilemap;
    private List<NodeController> _nodes;
    private NodeController _nodeSelected;
    private EventSystem _eventSystem;

    public void Init(Tilemap tilemap, List<NodeController> nodes)
    {
        _tilemap = tilemap;
        _nodes = nodes;
        _nodeSelected = null;
        _eventSystem = EventSystem.current;
    }

    public NodeController DetectSelectedNode()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (_eventSystem.IsPointerOverGameObject())
        {
            _nodeSelected = null;
            return _nodeSelected;
        }

        if (Physics.Raycast(ray, out hit))
        {
            Vector3Int tilePosition = _tilemap.WorldToCell(hit.point);
            Vector3 tileWorldPosition = _tilemap.GetCellCenterWorld(tilePosition);
            TileBase tile = _tilemap.GetTile(tilePosition);
            NodeController node = _nodes.Where(x => x.WorldPosition.x == tileWorldPosition.x && x.WorldPosition.z == tileWorldPosition.z).FirstOrDefault();
            if (node != _nodeSelected)
            {
                _nodeSelected = node;
                return node;
            }
        }
        else
        {
            _nodeSelected = null;
            return _nodeSelected;
        }
        return _nodeSelected;
    }

    public NodeController DetectCurrentNode(Vector3 position)
    {
        RaycastHit hit;
        if (Physics.Raycast(position, -Vector3.up, out hit))
        {
            Vector3Int tilePosition = _tilemap.WorldToCell(hit.point);

            Vector3 tileWorldPosition = _tilemap.GetCellCenterWorld(tilePosition);
            TileBase tile = _tilemap.GetTile(tilePosition);
            NodeController nodeFound = _nodes.Where(x => x.WorldPosition.x == tileWorldPosition.x && x.WorldPosition.z == tileWorldPosition.z).FirstOrDefault();
            if (nodeFound != null)
                return nodeFound;
        }
        return null;
    }
}
