using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;

public class NodeGenerationTool : MonoBehaviour
{
    [Header("Initialization Properties")]
    [SerializeField] private NodeController _nodePrefab;
    [SerializeField] private TileBase _groundTile;
    [SerializeField] private TileBase _transitionTile;
    [SerializeField] private TileBase _floorTile;
    [SerializeField] private TileBase _obstacleTile;

    private Transform _nodeParent;
    private Tilemap _tilemap;

    public List<NodeController> Init(Tilemap tilemap)
    {
        _nodeParent = GameObject.Find("NodeParent").transform;
        _tilemap = tilemap;
        return GenerateNodes();
    }

    private List<NodeController> GenerateNodes()
    {
        BoundsInt bounds = _tilemap.cellBounds;
        TileBase[] allTiles = _tilemap.GetTilesBlock(bounds);
        Vector3 offset = new Vector3(bounds.x + 0.5f, 0, bounds.y + 0.5f);
        List<NodeController> nodes = new List<NodeController>();
        for (int x = 0; x < bounds.size.x; x++)
        {
            for (int y = 0; y < bounds.size.y; y++)
            {
                TileBase tile = allTiles[x + y * bounds.size.x];
                Vector3 index = new Vector3(x, 0, y) + offset;
                if (tile != null && tile != _obstacleTile)
                {
                    // By default, ground values
                    NodeTypeEnum nodeType = NodeTypeEnum.GROUND;
                    if (tile == _transitionTile)
                        nodeType = NodeTypeEnum.TRANSITION_GF;
                    else if (tile == _floorTile)
                        nodeType = NodeTypeEnum.FLOOR;
                    index.y = ((float)((int)nodeType) / 2f) + 0.076f;
                    NodeController node = Instantiate(_nodePrefab, index, Quaternion.identity);
                    node.gameObject.name = $"Node_{x}_{y}";
                    node.gameObject.transform.SetParent(_nodeParent);
                    node.Init(nodeType, index, x, y);
                    nodes.Add(node);
                }
            }
        }
        foreach (NodeController node in nodes)
        {
            bool isTransition = (node.NodeType == NodeTypeEnum.TRANSITION_GF);
            List<NodeController> neighbours = new List<NodeController>();
            List<Vector3> positions = new List<Vector3> { new Vector3(0, 0, 1), new Vector3(0, 0, -1), new Vector3(1, 0, 0), new Vector3(-1, 0, 0) };
            foreach (Vector3 position in positions)
            {
                NodeController neighbour = nodes.Where(x =>
                    x.WorldPosition.x == node.WorldPosition.x + position.x &&
                    x.WorldPosition.z == node.WorldPosition.z + position.z).FirstOrDefault();
                if (neighbour != null && (isTransition || (neighbour.NodeType == node.NodeType || neighbour.NodeType == NodeTypeEnum.TRANSITION_GF)))
                    neighbours.Add(neighbour);
            }
            node.SetNeighbours(neighbours);
        }
        return nodes;
    }
}
