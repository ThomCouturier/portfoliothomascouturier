using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class NodePathfindingTool : MonoBehaviour
{
    private List<NodeController> _nodes;

    public void Init(List<NodeController> nodes)
    {
        _nodes = nodes;
    }

    public NodeController GetCurrentNode(Vector3 currentPosition)
    {
        NodeController foundNode = _nodes.Where(x =>
            x.WorldPosition.x == currentPosition.x &&
            x.WorldPosition.z == currentPosition.z).SingleOrDefault();
        if (foundNode != null)
            foundNode.SetOccupied(true);
        return foundNode;
    }

    public List<NodeController> FindPath(NodeController nodeStart, NodeController nodeEnd)
    {
        List<NodeController> openNodes = new List<NodeController> { nodeStart };
        List<NodeController> closedNodes = new List<NodeController>();
        NodeController currentNode = openNodes.First();
        while (openNodes.Count > 0)
        {
            currentNode = openNodes.First();
            for (int i = 1; i < openNodes.Count; i++)
            {
                if (openNodes[i].Path.FCost < currentNode.Path.FCost ||
                    openNodes[i].Path.FCost == currentNode.Path.FCost &&
                    openNodes[i].Path.HCost < currentNode.Path.HCost)
                {
                    currentNode = openNodes[i];
                }
            }
            openNodes.Remove(currentNode);
            closedNodes.Add(currentNode);

            if (currentNode == nodeEnd)
                return RetracePath(nodeStart, nodeEnd);

            foreach (NodeController neighbour in currentNode.Neighbours)
            {
                if (neighbour == null || closedNodes.Contains(neighbour) || (neighbour.IsOccupied && neighbour != nodeEnd)) continue;
                int newMovementCostToNeighbour = currentNode.Path.GCost + GetDistance(currentNode, neighbour);
                if (newMovementCostToNeighbour < neighbour.Path.GCost || !openNodes.Contains(neighbour))
                {
                    neighbour.Path.GCost = newMovementCostToNeighbour;
                    neighbour.Path.HCost = GetDistance(neighbour, nodeEnd);
                    neighbour.Path.Parent = currentNode;
                    if (!openNodes.Contains(neighbour))
                        openNodes.Add(neighbour);
                }
            }
        }
        return null;
    }

    public List<NodeController> GetNodesInGridRange(NodeController nodeStart, float range)
    {
        int stepCount = 0;

        List<NodeController> inRangeNodes = new List<NodeController>();
        inRangeNodes.Add(nodeStart);
        List<NodeController> previousStepNodes = new List<NodeController>();
        previousStepNodes.Add(nodeStart);

        while (stepCount < range)
        {
            List<NodeController> surroundingNodes = new List<NodeController>();
            foreach (NodeController node in previousStepNodes)
            {
                foreach (NodeController neighbour in node.Neighbours)
                {
                    if (neighbour.IsOccupied) continue;
                    surroundingNodes.Add(neighbour);
                }
            }
            inRangeNodes.AddRange(surroundingNodes);
            previousStepNodes = surroundingNodes.Distinct().ToList();

            stepCount++;
        }
        return inRangeNodes.Distinct().ToList();
    }
    public List<NodeController> GetNodesInWorldRange(NodeController nodeStart, float range)
    {
        List<NodeController> checkingNodes = new List<NodeController>{nodeStart};
        List<NodeController> foundNodes = new List<NodeController>{nodeStart};

        while(checkingNodes.Count > 0){
            List<NodeController> nodeToAdd = new List<NodeController>();
            checkingNodes.ForEach(x =>{
                x.Neighbours.ForEach(n => {
                    if(Vector3.Distance(nodeStart.NodeViewPoint, n.NodeViewPoint) <= range && !foundNodes.Contains(n) && !nodeToAdd.Contains(n)){
                        nodeToAdd.Add(n);
                    }
                });
            }); 
            foundNodes.AddRange(nodeToAdd);
            checkingNodes = nodeToAdd;
        }

        // List<NodeController> foundNodes = _nodes.Where(x => Vector3.Distance(nodeStart.NodeViewPoint, x.NodeViewPoint) <= range).ToList();
        return foundNodes;
    }

    private List<NodeController> RetracePath(NodeController nodeStart, NodeController nodeEnd)
    {
        List<NodeController> path = new List<NodeController>();
        NodeController currentNode = nodeEnd;
        while (currentNode != nodeStart)
        {
            path.Add(currentNode);
            NodeController childNode = currentNode;
            currentNode = currentNode.Path.Parent;
            currentNode.Path.Child = childNode;
        }

        path.Reverse();
        return path;
    }

    private int GetDistance(NodeController nodeA, NodeController nodeB)
    {
        int dstX = Mathf.Abs(nodeA.X - nodeB.X);
        int dstY = Mathf.Abs(nodeA.Y - nodeB.Y);

        if (dstX > dstY)
            return 14 * dstY + 10 * dstX - dstY;
        return 14 * dstX + 10 * dstY - dstX;
    }
}
