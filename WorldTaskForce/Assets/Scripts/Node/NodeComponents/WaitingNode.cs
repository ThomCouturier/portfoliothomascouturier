using UnityEngine;

public class WaitingNode : MonoBehaviour
{
    [SerializeField] private Transform _waitingGfx;

    public void Activate()
    {
        _waitingGfx.gameObject.SetActive(true);
    }

    public void Deactivate()
    {
        _waitingGfx.gameObject.SetActive(false);
    }
}
