using UnityEngine;

public class SelectionNode : MonoBehaviour
{
    [SerializeField] private Transform _heroSelectionGfx;
    [SerializeField] private Transform _enemySelectionGfx;

    private SpriteController _spriteController;
    private void OnEnable()
    {
        _spriteController = _heroSelectionGfx.GetChild(0).GetComponent<SpriteController>();
    }

    public void Activate(bool isHero)
    {
        if (_spriteController != null)
            _spriteController.Reset();
        if (isHero)
            _heroSelectionGfx.gameObject.SetActive(true);
        else
            _enemySelectionGfx.gameObject.SetActive(true);
    }

    public void Deactivate()
    {
        _heroSelectionGfx.gameObject.SetActive(false);
        _enemySelectionGfx.gameObject.SetActive(false);
    }
}
