using UnityEngine;

public class TargetNode : MonoBehaviour
{
    [SerializeField] private Transform _availableGfx;
    [SerializeField] private Transform _unavailableGfx;

    public void Activate(bool isAvailable)
    {
        if (isAvailable)
            _availableGfx.gameObject.SetActive(true);
        else
            _unavailableGfx.gameObject.SetActive(true);
    }

    public void Deactivate()
    {
        _availableGfx.gameObject.SetActive(false);
        _unavailableGfx.gameObject.SetActive(false);
    }
}
