using UnityEngine;

public class MovementRangeNode : MonoBehaviour
{
    [SerializeField] private Transform _gfx;

    public void Activate()
    {
        _gfx.gameObject.SetActive(true);
    }

    public void Deactivate()
    {
        _gfx.gameObject.SetActive(false);
    }
}
