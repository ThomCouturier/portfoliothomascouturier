using System.Collections.Generic;
using UnityEngine;

public class PathNode : MonoBehaviour
{
    // Runtime Properties
    public int GCost { get; set; }
    public int HCost { get; set; }
    public int FCost { get { return GCost + HCost; } }
    public NodeController Parent { get; set; }
    public NodeController Child { get; set; }

    [Header("Corner points")]
    [SerializeField] private Transform _cornerParent;
    [SerializeField] private List<Transform> _cornerPoints;
    [Header("InRange")]
    [SerializeField] private Transform _inParent;
    [SerializeField] private Transform _inStraight;
    [SerializeField] private Transform _inCorner;
    [SerializeField] private Transform _inStart;
    [SerializeField] private Transform _inEnd;
    [Header("OutRange")]
    [SerializeField] private Transform _outParent;
    [SerializeField] private Transform _outStraight;
    [SerializeField] private Transform _outCorner;
    [SerializeField] private Transform _outStart;
    [SerializeField] private Transform _outEnd;

    public void Activate(bool isStart, bool isEnd, int countAfterMaxRange, int nodeLeft)
    {
        if (isStart)
            DisplayStart(countAfterMaxRange, nodeLeft);
        if (isEnd)
            DisplayEnd(countAfterMaxRange, nodeLeft);
        if (!isEnd && !isStart)
        {
            if (Parent.X == Child.X || Parent.Y == Child.Y)
                DisplayStraight(countAfterMaxRange, nodeLeft);
            else
                DisplayCorner(countAfterMaxRange, nodeLeft);
        }
    }


    private void DisplayStart(int countAfterMaxRange, int nodeLeft)
    {
        Transform parentToRotate = _inParent;
        if (countAfterMaxRange >= 0 && nodeLeft > 0)
        {
            _outStart.gameObject.SetActive(true);
            parentToRotate = _outParent;
        }
        else if (nodeLeft > 0)
        {
            _inStart.gameObject.SetActive(true);
        }
        if (Child != null)
        {
            parentToRotate.LookAt(Child.transform, Vector3.up);
            parentToRotate.rotation = Quaternion.Euler(0, parentToRotate.eulerAngles.y, 0);
        }
    }

    private void DisplayEnd(int countAfterMaxRange, int nodeLeft)
    {
        Transform parentToRotate = _inParent;
        if (countAfterMaxRange <= 0)
            _inEnd.gameObject.SetActive(true);
        else
        {
            _outEnd.gameObject.SetActive(true);
            parentToRotate = _outParent;
        }
        parentToRotate.LookAt(Parent.transform, Vector3.up);
        parentToRotate.Rotate(0, 180, 0);
        parentToRotate.rotation = Quaternion.Euler(0, parentToRotate.eulerAngles.y, 0);
    }

    private void DisplayStraight(int countAfterMaxRange, int nodeLeft)
    {
        Transform parentToRotate = _inParent;
        if (countAfterMaxRange <= 0)
            _inStraight.gameObject.SetActive(true);
        else
        {
            _outStraight.gameObject.SetActive(true);
            parentToRotate = _outParent;
        }
        //Rotation
        parentToRotate.LookAt(Parent.transform, Vector3.up);
        parentToRotate.Rotate(0, 180, 0);
        parentToRotate.rotation = Quaternion.Euler(0, parentToRotate.eulerAngles.y, 0);
    }

    private void DisplayCorner(int countAfterMaxRange, int nodeLeft)
    {
        Transform parentToRotate = _inParent;
        if (countAfterMaxRange <= 0)
            _inCorner.gameObject.SetActive(true);
        else
        {
            _outCorner.gameObject.SetActive(true);
            parentToRotate = _outParent;
        }
        Vector2 parentPos = new Vector2(Parent.transform.position.x, Parent.transform.position.z);
        Vector2 childPos = new Vector2(Child.transform.position.x, Child.transform.position.z);
        for (int i = 0; i < 4; i++)
        {
            Vector2 pointX = new Vector2(_cornerPoints[0].position.x, _cornerPoints[0].position.z);
            Vector2 pointY = new Vector2(_cornerPoints[1].position.x, _cornerPoints[1].position.z);
            if ((pointX == parentPos || pointX == childPos) && (pointY == parentPos || pointY == childPos))
                break;
            else
            {
                _cornerParent.Rotate(0, 90, 0);
                parentToRotate.Rotate(0, 90, 0);
            }
        }
    }

    public void Deactivate()
    {
        // Reset Corner Points
        _cornerParent.rotation = Quaternion.identity;
        // Reset -> In Range
        _inParent.rotation = Quaternion.identity;
        foreach (Transform child in _inParent)
            child.gameObject.SetActive(false);
        // Reset -> Out Range
        _outParent.rotation = Quaternion.identity;
        foreach (Transform child in _outParent)
            child.gameObject.SetActive(false);
        // Reset -> Path Properties
        Parent = null;
        Child = null;
        GCost = 0;
        HCost = 0;
    }
}
