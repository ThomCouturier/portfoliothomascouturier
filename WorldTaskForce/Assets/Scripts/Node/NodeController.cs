using System.Collections.Generic;
using UnityEngine;

public class NodeController : MonoBehaviour
{
    // Const Properties
    public List<NodeController> Neighbours { get; private set; }
    public NodeTypeEnum NodeType { get; private set; }
    public Vector3 WorldPosition { get; private set; }
    public Vector3 NodeViewPoint { get; private set; }
    public int X { get; private set; }
    public int Y { get; private set; }
    // Properties
    public bool IsOccupied { get; private set; }
    // Node components
    [field:SerializeField] public MovementRangeNode MovementRange { get; private set; }
    [field:SerializeField] public ActionRangeNode ActionRange { get; private set; }
    [field:SerializeField] public TargetNode Target { get; private set; }
    [field:SerializeField] public PathNode Path { get; private set; }
    [field:SerializeField] public SelectionNode Selection { get; private set; }
    [field:SerializeField] public WaitingNode Waiting { get; private set; }

    private void Awake()
    {
        IsOccupied = false;

        ResetNode();
    }

    public void Init(NodeTypeEnum nodeType, Vector3 worldPosition, int x, int y)
    {
        NodeType = nodeType;
        WorldPosition = worldPosition;
        Vector3 currentPosition = transform.position;
        currentPosition.y += 0.9f;
        NodeViewPoint = currentPosition;
        X = x;
        Y = y;
    }

    public void SetNeighbours(List<NodeController> neighbours)
    {
        Neighbours = neighbours;
    }

    public void ResetNode()
    {
        MovementRange.Deactivate();
        ActionRange.Deactivate();
        Target.Deactivate();
        Path.Deactivate();
        Selection.Deactivate();
        Waiting.Deactivate();
    }

    public void SetOccupied(bool isOccupied)
    {
        IsOccupied = isOccupied;
    }
}
