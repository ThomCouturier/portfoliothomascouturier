using UnityEngine;

public class SpriteController : MonoBehaviour
{
    [SerializeField] private bool _faceCamera;
    [Header("Bounce Settings")]
    [SerializeField] private bool _bounce;
    [SerializeField] private Vector2 _bounceRangeY;
    [SerializeField] private float _bounceSpeed;
    private float _direction;
    private Vector3 _initialPosition;

    private void Start()
    {
        _initialPosition = transform.position;
        _direction = _bounceSpeed;
    }

    public void Reset()
    {
        transform.position = new Vector3(transform.position.x, _bounceRangeY.x, transform.position.z);
        _direction = _bounceSpeed;
    }

    private void Update()
    {
        if (_faceCamera)
            transform.LookAt(Camera.main.transform.position, Vector3.up);
        if (_bounce)
        {
            Vector3 currentPos = transform.position;
            if (_direction > 0)
            { // Going up
                if (currentPos.y >= _bounceRangeY.y) _direction = -_bounceSpeed;
                else if (currentPos.y > _initialPosition.y + ((_bounceRangeY.y - _initialPosition.y) / 2f) && _direction == _bounceSpeed) _direction /= 1.5f;
            }
            else if (_direction < 0)
            { // Going down
                if (currentPos.y <= _bounceRangeY.x) _direction = _bounceSpeed;
                else if (currentPos.y < _initialPosition.y + ((_bounceRangeY.x - _initialPosition.y) / 2f) && _direction == -_bounceSpeed) _direction /= 1.5f;
            }
            currentPos.y += _direction * Time.deltaTime;
            transform.position = currentPos;
        }
    }
}
