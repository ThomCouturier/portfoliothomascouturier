﻿public class IdleState : State
{
    public override void ManageStateChange()
    {
        if (_enemyController.AreHeroInRange())
        {
            _stateManager.ChangeState(StateEnum.ATTACK);
        }
        else
        {
            _stateManager.ChangeState(StateEnum.ADVANCE);
        }
    }

    public override void UpdateState()
    {
        base.UpdateState();
    }
}
