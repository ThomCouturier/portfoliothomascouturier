﻿public class AdvanceState : State
{
    public override void ManageStateChange()
    {
        if (_enemyController.AreHeroInRange())
        {
            if (_enemyController.IsMoving())
            {
                _enemyController.SetToCurrentnode();
            }
            else
            {
                _enemyController.StopFootstep();
                _stateManager.ChangeState(StateEnum.ATTACK);
            }
        }
    }

    public override void UpdateState()
    {
        _enemyController.MoveToNearestHero();
        base.UpdateState();
    }
}
