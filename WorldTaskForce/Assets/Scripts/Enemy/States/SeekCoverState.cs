﻿public class SeekCoverState : State
{
    public override void ManageStateChange()
    {
        if (_enemyController.IsInCover)
        {
            if (_enemyController.IsMoving())
            {
                _enemyController.SetToCurrentnode();
            }
            else
            {
                _enemyController.StopFootstep();
                _stateManager.ChangeState(StateEnum.INCOVER);
            }
        }
    }

    public override void UpdateState()
    {
        _enemyController.MoveToNearestHidingSpot();
        base.UpdateState();
    }
}
