using UnityEngine;

public abstract class State : MonoBehaviour
{
    protected StateManager _stateManager;
    protected EnemyController _enemyController;
    protected EnemyData _enemyData;

    private void Awake()
    {
        _stateManager = GetComponent<StateManager>();
        _enemyController = GetComponent<EnemyController>();
        _enemyData = GetComponent<EnemyData>();
    }

    public virtual void UpdateState()
    {
        ManageStateChange();
    }
    public abstract void ManageStateChange();
}
