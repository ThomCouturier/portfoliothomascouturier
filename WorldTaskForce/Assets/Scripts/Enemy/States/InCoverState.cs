﻿public class InCoverState : State
{
    public override void ManageStateChange()
    {
        if (!_enemyController.NeedsToHeal() && !_enemyController.IsOutnumbered())
        {
            _stateManager.ChangeState(StateEnum.ADVANCE);
        }
    }

    public override void UpdateState()
    {
        _enemyController.Heal();
        base.UpdateState();
    }
}
