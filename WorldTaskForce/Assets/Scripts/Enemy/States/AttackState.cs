﻿using System.Linq;
using UnityEngine;

public class AttackState : State
{
    private int _currentProjectile;
    private float _curentDelayBetweenShots;
    private HeroController _target;
    protected float _currentDelay;

    private void Start()
    {
        _currentProjectile = 0;
        _curentDelayBetweenShots = 0;
        _currentDelay = 0;
    }

    public override void ManageStateChange()
    {
        if (!_enemyController.AreHeroInRange() && _currentDelay <= 0)
        {
            _stateManager.ChangeState(StateEnum.ADVANCE);
        }
        else if (_enemyController.IsOutnumbered() && _currentDelay <= 0)
        {
            _stateManager.ChangeState(StateEnum.SEEKCOVER);
        }
    }

    public override void UpdateState()
    {
        base.UpdateState();
        if (!_enemyController.IsOutnumbered())
        {
            ShootHero();
        }
    }

    private void ShootHero()
    {
        if (_target == null)
        {
            _target = _enemyController.GetClosestHeroInRange();
            if (_target != null)
            {
                Vector3 viewTarget = _target.GetCurrentNode().NodeViewPoint;
                viewTarget.y = transform.position.y;
                transform.LookAt(viewTarget);
                _enemyController.OnShootingTarget();
                _enemyData.ResetBullets();
            }
        }
        if (_target != null)
        {
            if (_currentProjectile < _enemyData.Projectiles.Count && _curentDelayBetweenShots <= 0f)
            {
                _enemyData.Projectiles[_currentProjectile].Init(_enemyController.GetProjectileOrigin(), _enemyData.StrayFactor, _enemyData.Force);
                _curentDelayBetweenShots = _enemyData.InitialDelayBetweenShots;
                _curentDelayBetweenShots += Random.Range(-_curentDelayBetweenShots / 3f, _curentDelayBetweenShots / 3f);

                _currentProjectile++;
            }
            _curentDelayBetweenShots -= Time.deltaTime;
            _currentDelay -= Time.deltaTime;
            if ((_currentDelay <= 0 && _currentProjectile >= _enemyData.Projectiles.Count) ||
                (_currentDelay <= 0 && _enemyData.Projectiles.Where(x => x.gameObject.activeSelf).ToList().Count == 0))
            {
                _target.TakeDamage(_enemyData.GetRandomDamage());
                ResetProjectiles();
                _enemyController.HasShotTarget();
            }
        }
    }

    private void ResetProjectiles()
    {
        _target = null;
        _currentProjectile = 0;
        _curentDelayBetweenShots = 0;
        _currentDelay = 0;
        _enemyData.ResetBullets();
    }
}
