using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public bool HasActed { get; private set; }
    public bool IsInCover { get; private set; }
    public bool VisibilityOverride { get; private set; }
    private bool IsVisibleToHeroes;

    public bool hasLaughed = false;

    private HeroSFX _enemySFX;
    private HealthController _healthController;
    private StateManager _stateManager;
    private MovementController _movementManager;
    private EnemyActionManager _enemyActionManager;
    private List<HeroController> _heroesInRange;
    private List<EnemyController> _enemiesInRange;
    private EnemyData _enemyData;
    private Animator _animator;

    private Transform _visualGfx;
    private List<HitMarker> _hitMarkers;
    [SerializeField] private bool _usingRifle;

    public void Init(NodeSelectionTool nodeSelectionTool)
    {
        _enemySFX = GetComponent<EnemySFX>();
        _healthController = GetComponent<HealthController>();
        _movementManager = GetComponent<MovementController>();
        _stateManager = GetComponent<StateManager>();
        _animator = GetComponentInChildren<Animator>();
        _movementManager.Init(nodeSelectionTool, _animator);
        _movementManager.SetToCurrentNode();
        _enemyActionManager = GameObject.Find("GameManager").GetComponent<EnemyActionManager>();
        _enemyData = GetComponent<EnemyData>();
        _enemyData.Init();

        // Get all hit markers
        _hitMarkers = new List<HitMarker>();
        _visualGfx = transform.GetChild(0);
        foreach (Transform child in _visualGfx.GetChild(0))
        {
            _hitMarkers.Add(child.GetComponent<HitMarker>());
            child.gameObject.SetActive(false);
        }
        _stateManager.Init();
        IsVisibleToHeroes = false;
    }

    public void InitTurn(bool isVisible)
    {
        IsVisibleToHeroes = isVisible;
        VisibilityOverride = false;
        if (!hasLaughed && isVisible)
        {
            _enemySFX.PlayKillSFX();
            hasLaughed = true;
        }
    }

    public bool IsVisible()
    {
        return _visualGfx.gameObject.activeSelf;
    }

    public void SetVisibilityOverrides()
    {
        VisibilityOverride = true;
    }

    public bool UpdateState()
    {
        UpdateEntitiesInRange();
        _stateManager.UpdateState();
        if (HasActed)
        {
            HasActed = false;
            _movementManager.ResetMovement();
            hasLaughed = false;
            return true;
        }
        return false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "HidingSpot")
        {
            IsInCover = true;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "HidingSpot" && !IsInCover)
        {
            IsInCover = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "HidingSpot")
        {
            IsInCover = false;
        }
    }

    public void UpdateEntitiesInRange()
    {
        NodeController node = GetCurrentNode();
        _heroesInRange = _enemyActionManager.GetHeroesInRange(node.NodeViewPoint, _enemyData.ViewRange);
        _enemiesInRange = _enemyActionManager.GetEnemiesInRange(node.NodeViewPoint, _enemyData.ViewRange);
    }

    public void ActivateVisuals(bool setActive)
    {
        if (VisibilityOverride)
            _visualGfx.gameObject.SetActive(true);
        else
            _visualGfx.gameObject.SetActive(setActive);
    }

    public void OnShootingTarget()
    {
        _animator.SetBool("IsRifle", _usingRifle);
        _animator.SetTrigger("Attack");
        _enemySFX.PlayGunshotSFX();
    }

    public void HasShotTarget()
    {
        HasActed = true;
    }

    public void Reset()
    {
        _hitMarkers.ForEach(x => x.Deactivate());
    }

    public void DisplayHitMarkers(Vector3 hero, float maxRange, float accuracy)
    {
        string[] layerNames = { "Hero", "Enemy" };
        _hitMarkers.ForEach(x =>
        {
            if (!Physics.Linecast(x.transform.position, hero, ~LayerMask.GetMask(layerNames)))
            {
                float distance = Vector3.Distance(x.transform.position, hero) + 1f;
                float rangePercent = 100 / maxRange * distance;
                float currentPercent = -((0.05f * rangePercent) * (0.05f * rangePercent)) + 100;
                float newAccuracy = accuracy / 100 * currentPercent;
                x.Activate(newAccuracy);
            }
        });
    }

    public void HideHitMakers()
    {
        _hitMarkers.ForEach(x => x.gameObject.SetActive(false));
    }

    public bool TakeDamage(HitMarkerTypes type, float damage)
    {
        HitMarker hitMarker = _hitMarkers.Where(x => x.Type == type).FirstOrDefault();
        float multiplier = 1f;
        if (hitMarker.CurrentValue < Random.Range(0, 100))
        {
            // Critical!!!
            switch (type)
            {
                case HitMarkerTypes.HEAD:
                    multiplier = 3f;
                    break;
                case HitMarkerTypes.CHEST:
                    multiplier = 1.25f;
                    break;
                case HitMarkerTypes.LEG:
                    multiplier = 1.5f;
                    break;
                default:
                    break;
            }
        }
        float newDamage = damage * multiplier;
        return TakeRawDamage(newDamage);
    }

    public bool TakeRawDamage(float damage)
    {
        _healthController.DoDamage(damage);
        if (!_healthController.IsAlive())
        {
            _animator.SetTrigger("Death");
            _animator.SetInteger("DeathType", Random.Range(0, 4));
            _movementManager.CurrentNode.SetOccupied(false);
            return true;
        }
        return false;
    }

    public void MoveToNearestHero()
    {
        if (!_movementManager.Moving)
        {
            if (_movementManager.ArrivedAtDestination())
            {
                StopFootstep();
                HasActed = true;
            }
            else
            {
                _movementManager.SetPath(_enemyActionManager.GetPathToNearestHero(_movementManager.CurrentNode));
                _movementManager.StartMoving();
                if (!IsVisibleToHeroes)
                {
                    _movementManager.TeleportToDestination();
                    HasActed = true;
                }
                else
                {
                    _enemySFX.PlayFootstepsSFX();
                }
            }
        }
        else
        {
            _movementManager.Move();
        }
    }

    public void MoveToNearestHidingSpot()
    {
        if (!_movementManager.Moving)
        {
            if (_movementManager.ArrivedAtDestination())
            {
                StopFootstep();
                HasActed = true;
            }
            else
            {
                List<NodeController> path = _enemyActionManager.GetPathToNearestHidingSpot(GetCurrentNode());
                if (path != null)
                {
                    _movementManager.SetPath(path);
                    _movementManager.StartMoving();
                    _enemySFX.PlayFootstepsSFX();
                }
            }
        }
        else
        {
            _movementManager.Move();
        }
    }

    public void StopFootstep()
    {
        _enemySFX.StopFootstepsSFX();
    }

    public void Heal()
    {
        _healthController.Heal(_enemyData.HealPerTurn);
        HasActed = true;
    }

    public bool NeedsToHeal()
    {
        return !_healthController.IsFullHealth();
    }

    public void SetToCurrentnode()
    {
        _movementManager.SetToCurrentNode();
        NodeController node = _movementManager.CurrentNode;
        if(node == null) return;
        _movementManager.SetToCurrentNode();
    }

    public NodeController GetCurrentNode()
    {
        SetToCurrentnode();
        return _movementManager.CurrentNode;
    }

    public HeroController GetClosestHeroInRange()
    {
        if (_heroesInRange.Count == 0) return null;
        HeroController closestHero = null;
        float shortestDistance = Mathf.Infinity;
        foreach (HeroController hero in _heroesInRange)
        {
            float distanceFound = Vector3.Distance(transform.position, hero.transform.position);
            if (distanceFound < shortestDistance)
            {
                closestHero = hero;
                shortestDistance = distanceFound;
            }
        }
        return closestHero;
    }

    public Transform GetProjectileOrigin()
    {
        return _enemyData.ProjectileOrigin;
    }

    public bool AreHeroInRange()
    {
        return _heroesInRange.Count > 0;
    }

    public bool AreEnemiesInRange()
    {
        return _enemiesInRange.Count > 0;
    }

    public bool IsOutnumbered()
    {
        return (_enemiesInRange.Count) < _heroesInRange.Count;
    }

    public bool ArrivedAtDestination()
    {
        return _movementManager.ArrivedAtDestination();
    }

    public bool IsMoving()
    {
        return _movementManager.Moving;
    }

    public bool IsAlive()
    {
        return _healthController.IsAlive();
    }
}
