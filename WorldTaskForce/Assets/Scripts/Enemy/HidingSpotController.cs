using UnityEngine;

public class HidingSpotController : MonoBehaviour
{
    public NodeController CurrentNode { get; private set; }
    private NodeSelectionTool _nodeSelectionTool;

    public void Init(NodeSelectionTool nodeSelectionTool)
    {
        _nodeSelectionTool = GameObject.Find("GameManager").GetComponent<NodeSelectionTool>();
        CurrentNode = _nodeSelectionTool.DetectCurrentNode(transform.position);
        CurrentNode.SetOccupied(false);
    }

    public bool IsAvailable()
    {
        return !CurrentNode.IsOccupied;
    }
}
