using TMPro;
using UnityEngine;

public class HitMarker : MonoBehaviour
{
    [field: SerializeField] public HitMarkerTypes Type { get; private set; }
    public int CurrentValue { get; private set; }
    [SerializeField] private float _accuracyNerf;
    private TextMeshPro _text;

    private void Awake()
    {
        _text = transform.GetChild(0).GetComponent<TextMeshPro>();
        CurrentValue = 0;
    }

    public void Activate(float value)
    {
        CurrentValue = (int)Mathf.Round(value / _accuracyNerf);
        _text.text = $"{CurrentValue}";
        gameObject.SetActive(true);
    }

    public void Deactivate()
    {
        gameObject.SetActive(false);
    }
}
