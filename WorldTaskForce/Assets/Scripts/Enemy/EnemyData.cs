using System.Collections.Generic;
using UnityEngine;

public class EnemyData : MonoBehaviour
{
    [SerializeField] public float DelayBeforeEnding { get; private set; }
    [SerializeField] public float InitialDelayBetweenShots;
    [SerializeField] public float StrayFactor;
    [SerializeField] public float Force;
    [SerializeField] public float ViewRange;
    [SerializeField] public float DetectionRange;
    [SerializeField] public float HealPerTurn;
    [SerializeField] public Transform ProjectileOrigin;
    [SerializeField] public int MaxProjectiles;
    [SerializeField] public Vector2 DamageMatrix;
    public List<ProjectileController> Projectiles { get; private set; }

    public void Init()
    {
        Projectiles = new List<ProjectileController>();
        foreach (Transform projectileTr in GameObject.Find("Projectiles").transform)
        {
            if (Projectiles.Count < MaxProjectiles)
            {
                ProjectileController newBoomBoom = projectileTr.gameObject.GetComponent<ProjectileController>();
                Projectiles.Add(newBoomBoom);
            }
            else break;
        }
    }

    public void ResetBullets()
    {
        Projectiles.ForEach(x => x.gameObject.SetActive(false));
    }

    public float GetRandomDamage()
    {
        return Random.Range(DamageMatrix.x, DamageMatrix.y);
    }
}
