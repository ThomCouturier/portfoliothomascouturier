using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;

public class GameManager : MonoBehaviour
{
    // References
    private ActionMenuController _actionMenuController;
    private NodeGenerationTool _nodeGenerationTool;
    private NodeSelectionTool _nodeSelectionTool;
    private NodePathfindingTool _nodePathfindingTool;
    private HeroActionManager _heroActionManager;
    private EnemyActionManager _enemyActionManager;
    private Tilemap _tilemap;
    private List<NodeController> _nodes;
    private CameraManager _cameraController;
    private List<ObjectiveController> _objectives;
    public ObjectiveController CurrentObjective { get; set; }

    //Game information
    private bool _isPlayerTurn;
    public int CurrentTurn { get; private set; }
    private bool _pauseActive = false;
    private int _objectiveCounter;

    private void Awake()
    {
        FilterHeroes();
        _tilemap = GameObject.Find("Tilemap").GetComponent<Tilemap>();
        _cameraController = GameObject.Find("CameraController").GetComponent<CameraManager>();
        // Action Menu Controller
        _actionMenuController = GetComponent<ActionMenuController>();
        _actionMenuController.Init();
        // Node Generation Tool
        _nodeGenerationTool = GetComponent<NodeGenerationTool>();
        _nodes = _nodeGenerationTool.Init(_tilemap);
        // Node Selection Tool
        _nodeSelectionTool = GetComponent<NodeSelectionTool>();
        _nodeSelectionTool.Init(_tilemap, _nodes);
        // Node Pathfinding Tool
        _nodePathfindingTool = GetComponent<NodePathfindingTool>();
        _nodePathfindingTool.Init(_nodes);
        // Agents Action Manager
        _enemyActionManager = GetComponent<EnemyActionManager>();
        _heroActionManager = GetComponent<HeroActionManager>();
        // Inits
        _enemyActionManager.Init(_nodeSelectionTool, _nodePathfindingTool, _nodes, _heroActionManager);
        _heroActionManager.Init(_nodeSelectionTool, _nodePathfindingTool, _actionMenuController, _enemyActionManager, _nodes, _cameraController);

        _objectives = new List<ObjectiveController>();
        foreach (GameObject objective in GameObject.FindGameObjectsWithTag("Objective"))
        {
            ObjectiveController objectiveController = objective.GetComponent<ObjectiveController>();
            if (objectiveController == null) continue;
            _objectives.Add(objectiveController);
        }

        CurrentTurn = 1;
        _objectiveCounter = 0;
        _isPlayerTurn = true;
        _actionMenuController.DisplayEnemyTurn(!_isPlayerTurn);
        _actionMenuController.UpdateObjectiveCounter(_objectiveCounter, _objectives.Count);
    }

    private void Update()
    {
        GameStateInputs();
        if (!_pauseActive)
        {
            bool isPlayerTurn = _isPlayerTurn;
            if (_isPlayerTurn)
            {
                _isPlayerTurn = _heroActionManager.UpdateHeroes(CurrentTurn);
            }
            else
            {
                _cameraController.UpdateCameraMovement();
                _isPlayerTurn = _enemyActionManager.UpdateEnnemies();
                if (_isPlayerTurn)
                    CurrentTurn++;
            }
            if (isPlayerTurn != _isPlayerTurn) _actionMenuController.DisplayEnemyTurn(!_isPlayerTurn);
            if (_objectiveCounter == _objectives.Count)
            {
                PreGameManager.Instance.SetEndGameInfo(true, _objectiveCounter, CurrentTurn, _heroActionManager.Heroes[0].GetKillAmount(), _heroActionManager.Heroes[1].GetKillAmount(), _heroActionManager.Heroes[2].GetKillAmount(), _heroActionManager.Heroes[0].IsAlive(), _heroActionManager.Heroes[1].IsAlive(), _heroActionManager.Heroes[2].IsAlive());
                PreGameManager.Instance.StartNextScene(0.2f);
            }
            else if (_heroActionManager.GetBreathingHeroes().Count <= 0)
            {
                PreGameManager.Instance.SetEndGameInfo(false, 1, CurrentTurn, _heroActionManager.Heroes[0].GetKillAmount(), _heroActionManager.Heroes[1].GetKillAmount(), _heroActionManager.Heroes[2].GetKillAmount(), _heroActionManager.Heroes[0].IsAlive(), _heroActionManager.Heroes[1].IsAlive(), _heroActionManager.Heroes[2].IsAlive());
                PreGameManager.Instance.StartNextScene(0.2f);
            }

        }
    }

    private void FilterHeroes()
    {
        List<Transform> heroSpawns = new List<Transform>();
        List<Transform> selectedHeroes = new List<Transform>();
        // Initialization
        foreach (Transform spawn in GameObject.Find("Spawns").transform)
        {
            heroSpawns.Add(spawn);
        }
        foreach (Transform hero in GameObject.Find("Heroes").transform)
        {
            selectedHeroes.Add(hero);
        }

        // Filter
        List<Transform> toRemove = new List<Transform>();
        for (int i = 0; i < selectedHeroes.Count; i++)
        {
            if (!PreGameManager.Instance.SelectedHeroes.Contains(i))
            {
                Transform heroToDestroy = selectedHeroes[i];
                heroToDestroy.gameObject.SetActive(false);
                Destroy(heroToDestroy.gameObject, 0);
                toRemove.Add(selectedHeroes[i]);
            }
            else
            {
                selectedHeroes[i].position = heroSpawns[PreGameManager.Instance.SelectedHeroes.IndexOf(i)].position;
                selectedHeroes[i].rotation = heroSpawns[PreGameManager.Instance.SelectedHeroes.IndexOf(i)].rotation;
            }
        }
        toRemove.ForEach(x => selectedHeroes.Remove(x));
    }

    public void FinishCurrentObjective()
    {
        CurrentObjective.OnTriggered();
        // Hero Selection
        HeroController heroOnObjective = _heroActionManager.Heroes.Where(x => x.IsOnObjective).FirstOrDefault();
        heroOnObjective.StartInteraction(false);
        heroOnObjective.StartInteraction(true);
        heroOnObjective.transform.LookAt(CurrentObjective.transform.position);
        heroOnObjective.IsOnObjective = false;

        CurrentObjective = null;
        _actionMenuController.HideObjectiveButton();
        _objectiveCounter++;
        _actionMenuController.UpdateObjectiveCounter(_objectiveCounter, _objectives.Count);
    }

    public void SkipPlayerTurn()
    {
        _isPlayerTurn = false;
    }

    private void GameStateInputs()
    {
        if (Input.GetButtonDown("Pause"))
        {
            if (!_pauseActive)
            {
                _pauseActive = true;
                _actionMenuController.SetButtonsInteractable(false);
                _actionMenuController.DisplayPausePanel();
            }
            else
            {
                _pauseActive = false;
                _actionMenuController.SetButtonsInteractable(true);
                _actionMenuController.HidePausePanel();
            }
        }
        if (Input.GetButtonDown("Quit"))
        {
            Application.Quit();
            UnityEditor.EditorApplication.isPlaying = false;
        }
    }

    public void Return()
    {
        _pauseActive = false;
        _actionMenuController.HidePausePanel();
    }

    public void Quit()
    {
        Application.Quit();
        UnityEditor.EditorApplication.isPlaying = false;
    }
}
