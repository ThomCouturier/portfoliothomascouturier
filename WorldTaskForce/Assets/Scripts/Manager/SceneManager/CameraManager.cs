using UnityEngine;

public class CameraManager : MonoBehaviour
{
    // Runtime
    private Quaternion _spectatorRotation;
    private Vector3 _spectatorPosition;
    private Quaternion _childSpectatorRotation;
    private Vector3 _childSpectatorPosition;
    // Properties
    [SerializeField] private float _rotationSpeed;
    [SerializeField] private float _movementSpeed;
    // References
    private Transform _cameraChild;

    private void Awake()
    {
        _cameraChild = GetComponentsInChildren<Transform>(true)[0];
        // ForRuntime
        SaveTransforms();
    }

    public void UpdateCameraMovement()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        transform.position += transform.right * horizontal * _movementSpeed * Time.deltaTime;
        transform.position += transform.forward * vertical * _movementSpeed * Time.deltaTime;

        if (Input.GetKey(KeyCode.Q))
            transform.Rotate(-Vector3.up * _rotationSpeed * Time.deltaTime);
        if (Input.GetKey(KeyCode.E))
            transform.Rotate(Vector3.up * _rotationSpeed * Time.deltaTime);
        SaveTransforms();
    }

    public void TransferCameraToFpsView(Vector3 newPosition, Vector3 target)
    {
        SaveTransforms();
        _cameraChild.position = newPosition;
        _cameraChild.LookAt(target);
    }

    public void TransferCameraToSpectatorView()
    {
        transform.position = _spectatorPosition;
        transform.rotation = _spectatorRotation;

        _cameraChild.position = _childSpectatorPosition;
        _cameraChild.rotation = _childSpectatorRotation;
        SaveTransforms();
    }

    public void ChangeCameraTarget(Vector3 origin)
    {
        transform.position = origin;
        transform.rotation = _spectatorRotation;
        SaveTransforms();
    }

    private void SaveTransforms()
    {
        if(_cameraChild == null) _cameraChild = GetComponentsInChildren<Transform>(true)[0];
        // Main transform
        _spectatorRotation = transform.rotation;
        _spectatorPosition = transform.position;
        //Rotating transform
        _childSpectatorRotation = _cameraChild.rotation;
        _childSpectatorPosition = _cameraChild.position;
    }
}
