using UnityEngine;

public class IntroManager : MonoBehaviour
{
    void Update()
    {
        if (Input.GetButtonDown("Cancel")) Application.Quit();
    }

    public void Launch()
    {
        PreGameManager.Instance.StartNextScene(0.5f);
    }
}
