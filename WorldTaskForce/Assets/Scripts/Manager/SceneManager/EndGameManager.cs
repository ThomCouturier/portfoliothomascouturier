using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class EndGameManager : MonoBehaviour
{
    private static string GAME_WON = "MISSION SUCCESSFUL";
    private static string GAME_LOSS = "MISSION FAILURE";
    private static string KIA = "Enemy K.I.A. : ";

    private struct HeroInfo
    {
        public string Name;
        public Transform HeroGfx;
        public string NbKill;
        public Light Spotlight;
    }

    private AudioSource _audioSource;

    public int _objectivesCompleted;
    public int _nbTurn;
    public int _hero1Kills;
    public int _hero2Kills;
    public int _hero3Kills;
    public bool _hero1Alive;
    public bool _hero2Alive;
    public bool _hero3Alive;
    public bool _gameWon;

    private List<Transform> _selectedHeroes = new List<Transform>();
    private List<Transform> _spawnPosition = new List<Transform>();
    private List<bool> _aliveStatus = new List<bool>();

    private TextMeshPro endGameStatusText;
    private TextMeshPro nbTurnText;
    private TextMeshPro objectivesCompletedText;
    private TextMeshPro hero1KillsText;
    private TextMeshPro hero2KillsText;
    private TextMeshPro hero3KillsText;
    private TextMeshPro goBackBtn;

    private void Awake()
    {
        endGameStatusText = GameObject.Find("EndGameTitle").GetComponent<TextMeshPro>();
        nbTurnText = GameObject.Find("Turns_TXT").GetComponent<TextMeshPro>();
        objectivesCompletedText = GameObject.Find("Objectives_TXT").GetComponent<TextMeshPro>();
        hero1KillsText = GameObject.Find("KillCount1_TXT").GetComponent<TextMeshPro>();
        hero2KillsText = GameObject.Find("KillCount2_TXT").GetComponent<TextMeshPro>();
        hero3KillsText = GameObject.Find("KillCount3_TXT").GetComponent<TextMeshPro>();
        goBackBtn = GameObject.Find("Go_Back_Btn").GetComponent<TextMeshPro>();
    }
    void Start()
    {
        _audioSource = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<AudioSource>();
        _gameWon = PreGameManager.Instance.GameWon;
        _objectivesCompleted = PreGameManager.Instance.ObjectivesCompleted;
        _nbTurn = PreGameManager.Instance.NbTurn;
        _hero1Kills = PreGameManager.Instance.Hero1Kills;
        _hero2Kills = PreGameManager.Instance.Hero2Kills;
        _hero3Kills = PreGameManager.Instance.Hero3Kills;
        _aliveStatus.Add(PreGameManager.Instance.Hero1Alive);
        _aliveStatus.Add(PreGameManager.Instance.Hero2Alive);
        _aliveStatus.Add(PreGameManager.Instance.Hero3Alive);
        FilterHeroes();
        SetEndGameVisual();
    }

    void Update()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        {
            if (hit.collider.gameObject == goBackBtn.gameObject)
            {
                goBackBtn.color = Color.cyan;
                if (Input.GetButtonDown("Fire1"))
                {
                    PreGameManager.Instance.StartNextScene(0.2f);
                }
            }
            else
            {
                goBackBtn.color = Color.white;
            }
        }

        if (Input.GetButtonDown("Quit"))
        {
            Application.Quit();
            UnityEditor.EditorApplication.isPlaying = false;
        }
    }

    private void SetEndGameVisual()
    {
        if (_gameWon)
        {
            _audioSource.clip = SoundManager.SoundInstance.WinMusic;
            _audioSource.Play();
            endGameStatusText.text = GAME_WON;
            endGameStatusText.color = Color.green;
        }
        else
        {
            _audioSource.clip = SoundManager.SoundInstance.LossMusic;
            _audioSource.Play();
            endGameStatusText.text = GAME_LOSS;
            endGameStatusText.color = Color.red;
        }

        _selectedHeroes.ForEach(x =>
        {
            if (_aliveStatus[_selectedHeroes.IndexOf(x)])
            {
                x.GetComponentInChildren<Light>().color = Color.green;
            }
            else
            {
                x.GetComponentInChildren<Light>().color = Color.red;
            }

        });

        nbTurnText.text = _nbTurn.ToString();
        objectivesCompletedText.text = _objectivesCompleted.ToString();
        hero1KillsText.text = KIA + _hero1Kills.ToString();
        hero2KillsText.text = KIA + _hero2Kills.ToString();
        hero3KillsText.text = KIA + _hero3Kills.ToString();
    }

    private void FilterHeroes()
    {
        foreach (Transform spawn in GameObject.Find("Spawns").transform)
        {
            _spawnPosition.Add(spawn);
        }

        foreach (Transform hero in GameObject.Find("Heroes").transform)
        {
            _selectedHeroes.Add(hero);
        }
        List<Transform> toRemove = new List<Transform>();
        for (int i = 0; i < _selectedHeroes.Count; i++)
        {
            if (!PreGameManager.Instance.SelectedHeroes.Contains(i))
            {
                Transform heroToDestroy = _selectedHeroes[i];
                heroToDestroy.gameObject.SetActive(false);
                Destroy(heroToDestroy.gameObject, 0);
                toRemove.Add(_selectedHeroes[i]);
            }
            else
            {
                _selectedHeroes[i].position = _spawnPosition[PreGameManager.Instance.SelectedHeroes.IndexOf(i)].position;
                _selectedHeroes[i].rotation = _spawnPosition[PreGameManager.Instance.SelectedHeroes.IndexOf(i)].rotation;
            }
        }
        toRemove.ForEach(x => _selectedHeroes.Remove(x));
    }
}
