using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PreGameManager : MonoBehaviour
{
    public static PreGameManager Instance;

    private int actualScene = 0;
    private bool scenesAreInTransition = false;
    private const int firstGamingScene = 0;
    private const int lastGamingScene = 3;

    public List<int> SelectedHeroes { get; private set; }
    public int ObjectivesCompleted { get; private set; }
    public int NbTurn { get; private set; }
    public int Hero1Kills { get; private set; }
    public int Hero2Kills { get; private set; }
    public int Hero3Kills { get; private set; }
    public bool GameWon { get; private set; }
    public bool Hero1Alive { get; private set; }
    public bool Hero2Alive { get; private set; }
    public bool Hero3Alive { get; private set; }

    private void Awake()
    {
        Instance = null;
        if (Instance == null)
            Instance = this;

        else if (Instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }
    public void SetSelectedHeroes(List<int> selectedHeroes)
    {
        SelectedHeroes = selectedHeroes;
    }

    private int GetNextScene()
    {
        if (++actualScene == lastGamingScene + 1)
            actualScene = firstGamingScene;

        return actualScene;
    }

    public void StartNextScene(float delay)
    {
        if (scenesAreInTransition) return;

        scenesAreInTransition = true;

        StartCoroutine(RestartLevelDelay(delay, GetNextScene()));
    }

    private IEnumerator RestartLevelDelay(float delay, int scene)
    {
        yield return new WaitForSeconds(delay);
        //TODO: renommer dans unity la scene
        if (scene == 1)
            SceneManager.LoadScene("HeroSelection3D");
        else if (scene == 2)
            SceneManager.LoadScene("Level");
        else if (scene == 3)
            SceneManager.LoadScene("EndGameScene");
        else
            SceneManager.LoadScene("Intro");

        scenesAreInTransition = false;
    }

    public void SetEndGameInfo(bool gameWon, int nbObjectives, int nbTurn, int nbKills1, int nbKills2, int nbKills3, bool hero1Alive, bool hero2Alive, bool hero3Alive)
    {
        GameWon = gameWon;
        ObjectivesCompleted = nbObjectives;
        NbTurn = nbTurn;
        Hero1Kills = nbKills1;
        Hero2Kills = nbKills2;
        Hero3Kills = nbKills3;
        Hero1Alive = hero1Alive;
        Hero2Alive = hero2Alive;
        Hero3Alive = hero3Alive;
    }

    private void Update()
    {
        if (Input.GetButtonDown("Quit"))
        {
            Application.Quit();
            UnityEditor.EditorApplication.isPlaying = false;
        }
    }
}
