using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemyActionManager : MonoBehaviour
{
    // Runtime Data
    public List<EnemyController> Enemies { get; private set; }
    public List<HidingSpotController> HidingSpots { get; private set; }
    private int EnemiesLeftToAct;
    LayerMask _layerMaskObstacle;

    // References
    private NodeSelectionTool _nodeSelectionTool;
    public NodePathfindingTool NodePathfindingTool { get; private set; }
    private List<NodeController> _nodes;
    private HeroActionManager _heroActionManager;

    public void Init(NodeSelectionTool nodeSelectionTool, NodePathfindingTool nodePathfindingTool, List<NodeController> nodes, HeroActionManager heroActionManager)
    {
        _nodeSelectionTool = nodeSelectionTool;
        NodePathfindingTool = nodePathfindingTool;
        _nodes = nodes;
        _heroActionManager = heroActionManager;

        _layerMaskObstacle = LayerMask.GetMask("Ignore Raycast");
        // Enemies
        Enemies = new List<EnemyController>();
        // Find all heroes
        foreach (Transform enemy in GameObject.Find("Enemies").transform)
        {
            EnemyController enemyController = enemy.gameObject.GetComponent<EnemyController>();
            if (enemyController == null) continue;
            enemyController.Init(_nodeSelectionTool);
            Enemies.Add(enemyController);
        }
        HidingSpots = new List<HidingSpotController>();
        foreach (Transform hidingSpot in GameObject.Find("HidingSpots").transform)
        {
            HidingSpotController hidingSpotController = hidingSpot.GetComponent<HidingSpotController>();
            if (hidingSpotController == null) continue;
            hidingSpotController.Init(_nodeSelectionTool);
            HidingSpots.Add(hidingSpotController);
        }
        EnemiesLeftToAct = 0;
    }

    public bool UpdateEnnemies()
    {
        if (EnemiesLeftToAct == Enemies.Count)
        {
            Enemies[EnemiesLeftToAct - 1].InitTurn(_heroActionManager.VisibleEnemies.Contains(Enemies[EnemiesLeftToAct - 1]));
        }
        if (EnemiesLeftToAct == 0)
        {
            EnemiesLeftToAct = Enemies.Count;
        }
        if (!Enemies[EnemiesLeftToAct - 1].gameObject.activeSelf || !Enemies[EnemiesLeftToAct - 1].IsAlive() || Enemies[EnemiesLeftToAct - 1].UpdateState())
        {
            EnemiesLeftToAct -= 1;
            if (EnemiesLeftToAct - 1 >= 0)
                Enemies[EnemiesLeftToAct - 1].InitTurn(_heroActionManager.VisibleEnemies.Contains(Enemies[EnemiesLeftToAct - 1]));
        }
        return EnemiesLeftToAct == 0;
    }

    public List<HeroController> GetHeroesInRange(Vector3 origin, float range)
    {
        List<HeroController> heroesInRange = new List<HeroController>();
        HeroController heroInPriority = _heroActionManager.Heroes.Where(x => x.IsPriority && x.CanBeSeen).FirstOrDefault();
        if (heroInPriority != null)
        {
            if (Vector3.Distance(origin, heroInPriority.GetCurrentNode().NodeViewPoint) <= range)
                heroesInRange.Add(heroInPriority);
        }
        else
        {
            string[] layerNames = { "Hero", "Enemy" };
            _heroActionManager.Heroes.Where(hero => Vector3.Distance(origin, hero.GetCurrentNode().NodeViewPoint) <= range).ToList()
            .ForEach(hero =>
            {
                if (Physics.Linecast(origin, hero.GetCurrentNode().NodeViewPoint, ~LayerMask.GetMask(layerNames)))
                {
                }
                else if (hero.CanBeSeen && hero.IsAlive())
                {
                    heroesInRange.Add(hero);
                }
            });
        }
        return heroesInRange;
    }

    public List<EnemyController> GetEnemiesInRange(Vector3 position, float range)
    {
        List<EnemyController> enemiesInRange = new List<EnemyController>();
        string[] layerNames = { "Hero", "Enemy" };
        Enemies.ForEach(enemy =>
        {
            
            if (Vector3.Distance(position, enemy.GetCurrentNode().NodeViewPoint) <= range)
            {
                if (!Physics.Linecast(position, enemy.GetCurrentNode().NodeViewPoint, ~LayerMask.GetMask(layerNames)))
                {
                    enemiesInRange.Add(enemy);
                }
            }
        });
        return enemiesInRange;
    }

    public List<NodeController> GetPathToNearestHero(NodeController currentPosition)
    {
        HeroController heroInPriority = _heroActionManager.Heroes.Where(x => x.IsPriority && x.CanBeSeen && x.IsAlive()).FirstOrDefault();
        if (heroInPriority != null)
        {
            List<NodeController> path = NodePathfindingTool.FindPath(currentPosition, GetClosesetNeighbourNode(currentPosition.transform.position, heroInPriority.GetCurrentNode()));
            path.RemoveAt(path.Count - 1);
            return path;
        }
        else
        {
            List<NodeController> nearestHero = null;
            List<NodeController> nextHeroPath = new List<NodeController>();
            foreach (HeroController hero in _heroActionManager.Heroes.Where(x => x.CanBeSeen && x.IsAlive()).ToList())
            {
                nextHeroPath = NodePathfindingTool.FindPath(currentPosition, GetClosesetNeighbourNode(currentPosition.transform.position, hero.GetCurrentNode()));
                if (nearestHero == null || (nextHeroPath != null && nextHeroPath.Count < nearestHero.Count))
                {
                    nearestHero = nextHeroPath;
                }
            }
            nextHeroPath.RemoveAt(nextHeroPath.Count - 1);
            return nearestHero;
        }
    }

    private NodeController GetClosesetNeighbourNode(Vector3 origin, NodeController node){
        NodeController foundNode = null;
        float oldDistance = Mathf.Infinity;
        node.Neighbours.ForEach(x => { 
            float dist = Vector3.Distance(origin, x.transform.position);
            if(dist < oldDistance){
                foundNode = x;
                oldDistance = dist;
            }
        });
        return foundNode;
    }

    public List<NodeController> GetPathToNearestHidingSpot(NodeController currentPosition)
    {
        List<NodeController> shortestPath = null;
        List<NodeController> nextPath = new List<NodeController>();
        foreach (HidingSpotController hidingSpot in HidingSpots.Where(x => x.IsAvailable()).ToList())
        {
            nextPath = NodePathfindingTool.FindPath(currentPosition, hidingSpot.CurrentNode);
            if (shortestPath == null || (nextPath != null && nextPath.Count < shortestPath.Count))
            {
                shortestPath = nextPath;
            }
        }
        return shortestPath;
    }
}
