using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HeroActionManager : MonoBehaviour
{
    // Runtime Properties
    [SerializeField] private HeroController _currentHero;
    [SerializeField] private ActionTypes _currentActionType;
    [SerializeField] private NodeController _selectedNode;
    //Runtime Properties
    private List<NodeController> _movementRangeNodes;

    // References
    public List<HeroController> Heroes;
    private NodeSelectionTool _nodeSelectionTool;
    private NodePathfindingTool _nodePathfindingTool;
    private ActionMenuController _actionMenuController;
    private List<NodeController> _nodes;
    private EnemyActionManager _enemyActionManager;
    private int _cameraHeroTargetIndex;
    private CameraManager _cameraManager;
    public List<EnemyController> VisibleEnemies { get; private set; }
    private List<EnemyController> _enemies;
    //Game properties
    [SerializeField] private int MaxActionPerTurn;
    private int _actionsLeft;
    private int _currentTurn;

    public void Init(NodeSelectionTool nodeSelectionTool, NodePathfindingTool nodePathfindingTool, ActionMenuController actionMenuController, EnemyActionManager enemyActionManager, List<NodeController> nodes, CameraManager cameraManager)
    {
        _nodeSelectionTool = nodeSelectionTool;
        _nodePathfindingTool = nodePathfindingTool;
        _actionMenuController = actionMenuController;
        _enemyActionManager = enemyActionManager;
        _nodes = nodes;
        _cameraManager = cameraManager;
        // Selected node
        _selectedNode = null;
        // Heroes
        Heroes = new List<HeroController>();
        // Find all heroes
        foreach (Transform hero in GameObject.Find("Heroes").transform)
        {
            if (!hero.gameObject.activeSelf) continue;
            HeroController heroController = hero.gameObject.GetComponent<HeroController>();
            if (heroController == null) continue;
            heroController.Init(_nodeSelectionTool, _nodePathfindingTool, _cameraManager, _actionMenuController);
            Heroes.Add(heroController);
        }

        // Enemies
        VisibleEnemies = new List<EnemyController>();
        _enemies = _enemyActionManager.Enemies;
        // Setup
        _currentHero = Heroes[0];
        Heroes.ForEach(x => x.GetCurrentNode());
        _currentHero.ActivateCurrentNode();

        _cameraHeroTargetIndex = Heroes.FindIndex(x => x == _currentHero);
        _cameraManager.ChangeCameraTarget(_currentHero.transform.position);

        _actionMenuController.ResetActionType();
        _actionMenuController.SetNewHero(_currentHero);
        _actionMenuController.UpdateActionInformations(_currentHero.Data, "Waiting", "Select an action", 0, 0, 0, 0);

        _actionsLeft = 0;
    }

    public bool UpdateHeroes(int currentTurn)
    {
        if (currentTurn != _currentTurn)
        {
            _currentTurn = currentTurn;
            Heroes.ForEach(x => x.ResetTurn());
            _actionsLeft = MaxActionPerTurn;
            _actionMenuController.UpdateTurnCounter(currentTurn);
            if (!_currentHero.IsAlive())
            {
                _actionMenuController.ModifyActionType(4);
            }
        }
        CheckIfResetWanted();
        UpdateEnemyVisibility();
        UpdateActionType(currentTurn);
        UpdateAction(currentTurn);
        return _actionsLeft > 0;
    }

    private void CheckIfResetWanted()
    {
        if ((Input.GetKeyDown(KeyCode.Escape) || Input.GetButtonDown("Fire2")))
        {
            if (_currentActionType == ActionTypes.PRIMARY_ACTION || _currentActionType == ActionTypes.SPECIAL_ABILITY)
                _cameraManager.TransferCameraToSpectatorView();
            _cameraManager.ChangeCameraTarget(_currentHero.transform.position);
            if (_currentActionType != ActionTypes.MOVE || !_currentHero.IsMoving()) ResetType();
        }
    }

    private void UpdateEnemyVisibility()
    {
        List<EnemyController> newlyInRangeEnemies = new List<EnemyController>();
        List<EnemyController> newlyOutRangeEnemies = new List<EnemyController>();
        _enemies.ForEach(enemy =>
        {
            Vector3 origin = enemy.transform.position;
            bool oneIsInRange = false;
            Heroes.ForEach(hero =>
            {
                if (enemy.VisibilityOverride || Vector3.Distance(origin, hero.transform.position) <= hero.Data.DetectionRange)
                {
                    oneIsInRange = true;
                }
            });
            if (oneIsInRange)
            {
                if (!VisibleEnemies.Contains(enemy))
                {
                    newlyInRangeEnemies.Add(enemy);
                    VisibleEnemies.Add(enemy);
                }
            }
            else if (VisibleEnemies.Contains(enemy) || !VisibleEnemies.Contains(enemy))
            {
                newlyOutRangeEnemies.Add(enemy);
                VisibleEnemies.Remove(enemy);
            }
        });
        newlyInRangeEnemies.ForEach(x => x.ActivateVisuals(true));
        newlyOutRangeEnemies.ForEach(x => x.ActivateVisuals(false));
    }

    private void UpdateActionType(int currentTurn)
    {
        ActionTypes foundActionType = _actionMenuController.ActionType;
        if (_currentActionType != foundActionType)
        {
            ActionTypes lastActionType = _currentActionType;
            _currentActionType = foundActionType;
            // Check UNDEFINED
            if (lastActionType == ActionTypes.UNDEFINED)
            {
                _currentHero.DeactivateCurrentNode();
            }
            else if (_currentActionType == ActionTypes.UNDEFINED)
            {
                _currentHero.ActivateCurrentNode();
                _actionMenuController.UpdateActionInformations(_currentHero.Data, "Waiting", "Select an action", 0, 0, 0, 0);
            }
            // Check MOVE
            if (lastActionType == ActionTypes.MOVE)
            {
                _currentHero.ResetMovement();
                _movementRangeNodes.ForEach(x => x.MovementRange.Deactivate());
            }
            else if (_currentActionType == ActionTypes.MOVE)
            {
                GenerateMovementRange();
                _actionMenuController.UpdateActionInformations(_currentHero.Data, "Move", "Select a destination", 0, 1, 1, 1);
            }
            // Check SIMPLE_ATTACK
            if (lastActionType == ActionTypes.PRIMARY_ACTION)
            {
                _currentHero.PrimaryActionComponent.EndAction();
                ManageButtonInteractibility(false, false);
            }
            else if (_currentActionType == ActionTypes.PRIMARY_ACTION)
            {
                _currentHero.PrimaryActionComponent.BeginAction(Heroes, _enemyActionManager.Enemies);
                _actionMenuController.UpdateActionInformations(
                    _currentHero.Data,
                    _currentHero.Data.PaName,
                    _currentHero.Data.PaDescription,
                    _currentHero.Data.PaUseAmount,
                    _currentHero.Data.PaMaxUse,
                    _currentHero.Data.PaCost,
                    _currentHero.Data.PaCooldown
                );
            }
            // Check SPECIAL_ABILITY
            if (lastActionType == ActionTypes.SPECIAL_ABILITY)
            {
                _currentHero.SpecialAbilityComponent.EndAbility();
                ManageButtonInteractibility(false, false);
            }
            else if (_currentActionType == ActionTypes.SPECIAL_ABILITY)
            {
                _currentHero.SpecialAbilityComponent.BeginAbility(Heroes, _enemyActionManager.Enemies);
                _actionMenuController.UpdateActionInformations(
                    _currentHero.Data,
                    _currentHero.Data.SaName,
                    _currentHero.Data.SaDescription,
                    _currentHero.Data.SaUseAmount,
                    _currentHero.Data.SaMaxUse,
                    _currentHero.Data.SaCost,
                    _currentHero.Data.SaCooldown
                );
            }

            // Check CHANGE_HERO
            if (lastActionType == ActionTypes.CHANGE_HERO)
            {
                Heroes.ForEach(x => x.GetCurrentNode().Selection.Deactivate());
            }
            else if (_currentActionType == ActionTypes.CHANGE_HERO)
            {
                _cameraHeroTargetIndex = Heroes.FindIndex(x => x == _currentHero);
                Heroes.Where(x => x.IsAlive()).ToList().ForEach(x => x.GetCurrentNode().Selection.Activate(true));
                _actionMenuController.UpdateActionInformations(_currentHero.Data, "Change hero", "Select another hero to be played!", 0, 0, 0, 0);
            }
        }
    }

    private void UpdateAction(int currentTurn)
    {
        bool isNewSelection = DetectNewSelectedNode();
        switch (_currentActionType)
        {
            case ActionTypes.UNDEFINED:
                _cameraManager.UpdateCameraMovement();
                // Nothing to do in UNDEFINED
                ManageButtonInteractibility(false, false);
                break;
            case ActionTypes.MOVE:
                _cameraManager.UpdateCameraMovement();
                if (!_currentHero.IsMoving())
                {
                    if (_currentHero.ArrivedAtDestination())
                    {
                        ResetType();
                        _currentHero.ResetMovement();
                        _actionsLeft -= 1;
                    }
                    else if (_selectedNode == null)
                    {
                        _currentHero.ResetPath();
                    }
                    GeneratePath(isNewSelection);
                    if (Input.GetButtonDown("Fire1") && _selectedNode != null && _currentHero.GetPathCount() > 0)
                    {
                        _currentHero.StartMoving();
                        _movementRangeNodes.ForEach(x => x.MovementRange.Deactivate());
                    }
                }
                else
                {
                    _currentHero.Move();
                    ManageButtonInteractibility(true, false);
                }
                break;
            case ActionTypes.PRIMARY_ACTION:
                bool isActionCompleted = _currentHero.PrimaryActionComponent.UpdateAction(_selectedNode);
                if (isActionCompleted)
                {
                    _actionsLeft -= _currentHero.Data.PaCost;
                    _currentHero.Data.PaTurnLastUsed = currentTurn;
                    ResetType();
                }
                break;
            case ActionTypes.SPECIAL_ABILITY:
                bool isAbilityCompleted = _currentHero.SpecialAbilityComponent.UpdateAbility(_selectedNode);
                if (isAbilityCompleted)
                {
                    _actionsLeft -= _currentHero.Data.SaCost;
                    _currentHero.Data.SaTurnLastUsed = currentTurn;
                    ResetType();
                }
                break;
            case ActionTypes.CHANGE_HERO:
                _cameraManager.UpdateCameraMovement();
                SelectNewHero();
                if (Input.GetKeyDown(KeyCode.Z))
                {
                    _cameraHeroTargetIndex = (_cameraHeroTargetIndex == Heroes.Count - 1) ? 0 : _cameraHeroTargetIndex + 1;
                    _cameraManager.ChangeCameraTarget(Heroes[_cameraHeroTargetIndex].transform.position);
                }
                if (Input.GetKeyDown(KeyCode.X))
                {
                    _cameraHeroTargetIndex = (_cameraHeroTargetIndex == 0) ? Heroes.Count - 1 : _cameraHeroTargetIndex - 1;
                    _cameraManager.ChangeCameraTarget(Heroes[_cameraHeroTargetIndex].transform.position);
                }
                ManageButtonInteractibility(true, false);
                break;
        }
        _actionMenuController.UpdateActionPoint(_actionsLeft, MaxActionPerTurn);
    }

    private bool DetectNewSelectedNode()
    {
        if (_nodeSelectionTool != null)
        {
            NodeController newNode = _nodeSelectionTool.DetectSelectedNode();
            if (newNode != _selectedNode)
            {
                if (_selectedNode != null)
                    _selectedNode.Target.Deactivate();
                _selectedNode = newNode;
                if (_selectedNode != null && _currentActionType != ActionTypes.PRIMARY_ACTION && _currentActionType != ActionTypes.SPECIAL_ABILITY)
                {
                    if (_currentActionType == ActionTypes.MOVE)
                    {
                        EnemyController enemy = VisibleEnemies.Where(x => x.GetCurrentNode() == _selectedNode).FirstOrDefault();
                        _selectedNode.Target.Activate(!_selectedNode.IsOccupied || (_selectedNode.IsOccupied && enemy == null));
                    }
                }
                return true;
            }
        }
        return false;
    }

    private void GenerateMovementRange()
    {
        _movementRangeNodes = _nodePathfindingTool.GetNodesInGridRange(_currentHero.GetCurrentNode(), _currentHero.GetMovementRange());
        _movementRangeNodes.ForEach(x => x.MovementRange.Activate());
    }

    private void GeneratePath(bool isNewSelection)
    {
        if (_selectedNode == null || !isNewSelection || _currentHero == null) return;
        _currentHero.ResetPath();
        if (_selectedNode.IsOccupied) return;
        // Assign path
        List<NodeController> pathFound = _nodePathfindingTool.FindPath(_currentHero.GetCurrentNode(), _selectedNode);
        _currentHero.SetPath(pathFound);
        // Path properties
        int pathCount = pathFound.Count;
        int movementRange = _currentHero.GetMovementRange();
        // Activate the path nodes
        _currentHero.ActivateCurrentNodePath(true, false, -1, pathCount);
        for (int i = 0; i < pathCount; i++)
        {
            pathFound[i].Path.Activate(
                i == movementRange - 1,
                i == movementRange - 1 || i == pathCount - 1,
                i - movementRange + 1,
                pathCount - 1 - i
            );
        }
    }

    private void ManageButtonInteractibility(bool hasOverride, bool overrideValue)
    {
        _actionMenuController.ManageButtonInteractibility(_currentTurn, _actionsLeft, hasOverride, overrideValue, _currentHero);
    }

    private void ResetType()
    {
        _actionMenuController.ResetActionType();
    }

    private void SelectNewHero()
    {
        HeroController hero = null;
        if (Input.GetButtonDown("Fire1"))
            hero = Heroes.Where(x => x.GetCurrentNode() == _selectedNode && x.IsAlive()).SingleOrDefault();
        else if (Input.GetKeyDown(KeyCode.Return) && Heroes[_cameraHeroTargetIndex].IsAlive())
            hero = Heroes[_cameraHeroTargetIndex];
        if (hero != null) SetNewHero(hero);
    }

    private void SetNewHero(HeroController hero)
    {
        _currentHero = hero;
        _actionMenuController.SetNewHero(_currentHero);
        _cameraHeroTargetIndex = Heroes.FindIndex(x => x == _currentHero);
    }

    public List<HeroController> GetBreathingHeroes()
    {
        return Heroes.Where(x => x.IsAlive()).ToList();
    }
}
