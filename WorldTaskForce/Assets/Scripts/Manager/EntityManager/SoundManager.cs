using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager SoundInstance;

    /*********************************************************************************************************************
    **********************************************************************************************************************
    *********************************************************  CLIPS  ****************************************************
    **********************************************************************************************************************
    *********************************************************************************************************************/

    //---------------------------------------------------------------
    //----------------------BACKGROUND MUSIC-------------------------
    //---------------------------------------------------------------
    [SerializeField] private AudioClip introMusic;
    [SerializeField] private AudioClip heroSelectionMusic;
    [SerializeField] private AudioClip gameplayMusic;
    [SerializeField] private AudioClip winMusic;
    [SerializeField] private AudioClip lossMusic;

    //---------------------------------------------------------------
    //-----------------------HERO SELECTION--------------------------
    //---------------------------------------------------------------
    [SerializeField] private AudioClip tankSelection;
    [SerializeField] private AudioClip scoutSelection;
    [SerializeField] private AudioClip supportSelection;
    [SerializeField] private AudioClip sniperSelection;
    [SerializeField] private AudioClip healerSelection;
    [SerializeField] private AudioClip riflemanSelection;

    //---------------------------------------------------------------
    //-------------------------HERO KILL-----------------------------
    //---------------------------------------------------------------
    [SerializeField] private AudioClip tankKill;
    [SerializeField] private AudioClip scoutKill;
    [SerializeField] private AudioClip supportKill;
    [SerializeField] private AudioClip sniperKill;
    [SerializeField] private AudioClip healerKill;
    [SerializeField] private AudioClip riflemanKill;

    //---------------------------------------------------------------
    //-------------------------HERO ULT------------------------------
    //---------------------------------------------------------------
    [SerializeField] private AudioClip tankUlt;
    [SerializeField] private AudioClip scoutUlt;
    [SerializeField] private AudioClip supportUlt;
    [SerializeField] private AudioClip sniperUlt;
    [SerializeField] private AudioClip healerUlt;
    [SerializeField] private AudioClip riflemanUlt;

    //---------------------------------------------------------------
    //------------------------HERO DEATH-----------------------------
    //---------------------------------------------------------------
    [SerializeField] private AudioClip tankDeath;
    [SerializeField] private AudioClip scoutDeath;
    [SerializeField] private AudioClip supportDeath;
    [SerializeField] private AudioClip sniperDeath;
    [SerializeField] private AudioClip healerDeath;
    [SerializeField] private AudioClip riflemanDeath;

    //---------------------------------------------------------------
    //-------------------------GUN SHOT------------------------------
    //---------------------------------------------------------------
    [SerializeField] private AudioClip shotgunSFX;
    [SerializeField] private AudioClip uspSocomSFX;
    [SerializeField] private AudioClip m4SFX;
    [SerializeField] private AudioClip awpSFX;
    [SerializeField] private AudioClip p90SFX;
    [SerializeField] private AudioClip mp7SFX;

    //---------------------------------------------------------------
    //---------------------------SFX---------------------------------
    //---------------------------------------------------------------
    [SerializeField] private AudioClip footsteps;
    [SerializeField] private AudioClip droneSFX;
    [SerializeField] private AudioClip bombSFX;
    [SerializeField] private AudioClip objectiveSFX;

    //---------------------------------------------------------------
    //------------------------ENEMY SFX------------------------------
    //---------------------------------------------------------------
    [SerializeField] private AudioClip enemyLaugh1;
    [SerializeField] private AudioClip enemyLaugh2;
    [SerializeField] private AudioClip enemyLaugh3;
    [SerializeField] private AudioClip enemyLaugh4;


    /********************************************************************************************************************
    *********************************************************************************************************************
    ********************************************************  GETTERS  **************************************************
    *********************************************************************************************************************
    ********************************************************************************************************************/

    //---------------------------------------------------------------
    //------------------BACKGROUND MUSIC GETTERS---------------------
    //---------------------------------------------------------------
    public AudioClip IntroMusic { get { return introMusic; } }
    public AudioClip HeroSelectionMusic { get { return heroSelectionMusic; } }
    public AudioClip GameplayMusic { get { return gameplayMusic; } }
    public AudioClip WinMusic { get { return winMusic; } }
    public AudioClip LossMusic { get { return lossMusic; } }

    //---------------------------------------------------------------
    //-------------------HERO SELECTION GETTERS----------------------
    //---------------------------------------------------------------
    public AudioClip TankSelection { get { return tankSelection; } }
    public AudioClip ScoutSelection { get { return scoutSelection; } }
    public AudioClip SupportSelection { get { return supportSelection; } }
    public AudioClip SniperSelection { get { return sniperSelection; } }
    public AudioClip HealerSelection { get { return healerSelection; } }
    public AudioClip RiflemanSelection { get { return riflemanSelection; } }

    //---------------------------------------------------------------
    //----------------------HERO KILL GETTERS------------------------
    //---------------------------------------------------------------
    public AudioClip TankKill { get { return tankKill; } }
    public AudioClip ScoutKill { get { return scoutKill; } }
    public AudioClip SupportKill { get { return supportKill; } }
    public AudioClip SniperKill { get { return sniperKill; } }
    public AudioClip HealerKill { get { return healerKill; } }
    public AudioClip RiflemanKill { get { return riflemanKill; } }

    //---------------------------------------------------------------
    //----------------------HERO ULT GETTERS-------------------------
    //---------------------------------------------------------------
    public AudioClip TankUlt { get { return tankUlt; } }
    public AudioClip ScoutUlt { get { return scoutUlt; } }
    public AudioClip SupportUlt { get { return supportUlt; } }
    public AudioClip SniperUlt { get { return sniperUlt; } }
    public AudioClip HealerUlt { get { return healerUlt; } }
    public AudioClip RiflemanUlt { get { return riflemanUlt; } }

    //---------------------------------------------------------------
    //---------------------HERO DEATH GETTERS------------------------
    //---------------------------------------------------------------
    public AudioClip TankDeath { get { return tankDeath; } }
    public AudioClip ScoutDeath { get { return scoutDeath; } }
    public AudioClip SupportDeath { get { return supportDeath; } }
    public AudioClip SniperDeath { get { return sniperDeath; } }
    public AudioClip HealerDeath { get { return healerDeath; } }
    public AudioClip RiflemanDeath { get { return riflemanDeath; } }

    //---------------------------------------------------------------
    //----------------------GUN SHOT GETTERS-------------------------
    //---------------------------------------------------------------
    public AudioClip ShotgunSFX { get { return shotgunSFX; } }
    public AudioClip UspSocomSFX { get { return uspSocomSFX; } }
    public AudioClip M4SFX { get { return m4SFX; } }
    public AudioClip AwpSFX { get { return awpSFX; } }
    public AudioClip P90SFX { get { return p90SFX; } }
    public AudioClip Mp7SFX { get { return mp7SFX; } }

    //---------------------------------------------------------------
    //------------------------SFX GETTERS----------------------------
    //---------------------------------------------------------------
    public AudioClip Footsteps { get { return footsteps; } }
    public AudioClip DroneSFX { get { return droneSFX; } }
    public AudioClip BombSFX { get { return bombSFX; } }
    public AudioClip ObjectiveSFX { get { return objectiveSFX; } }

    //---------------------------------------------------------------
    //---------------------ENEMY SFX GETTERS-------------------------
    //---------------------------------------------------------------
    public AudioClip EnemyLaugh1 { get { return enemyLaugh1; } }
    public AudioClip EnemyLaugh2 { get { return enemyLaugh2; } }
    public AudioClip EnemyLaugh3 { get { return enemyLaugh3; } }
    public AudioClip EnemyLaugh4 { get { return enemyLaugh4; } }


    void Start()
    {
        if (SoundInstance == null)
        {
            SoundInstance = this;
        }
        else if (SoundInstance != this)
            Destroy(gameObject);
    }
}
