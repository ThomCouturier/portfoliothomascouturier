using UnityEngine;

[CreateAssetMenu(fileName = "HeroData", menuName = "ScriptableObjects/HeroData", order = 1)]
public class HeroData : ScriptableObject
{
    [field: SerializeField]
    public string Name { get; set; }
    [field: SerializeField]
    public string Role { get; set; }
    [field: SerializeField]
    public float DetectionRange { get; private set; }

    [field: SerializeField]
    [field: Range(0, 100)]
    public float Accuracy { get; private set; }
    [field: Header("Primary Action datas")]
    [field: SerializeField]
    public string PaName { get; private set; }
    [field: SerializeField]
    public string PaDescription { get; private set; }
    [field: SerializeField]
    public Sprite PaIcon { get; private set; }
    [field: SerializeField]
    public Vector2 PaRangeMetrix { get; private set; }
    [field: SerializeField]
    public float PaValue { get; private set; }
    [field: SerializeField]
    public int PaCost { get; private set; }
    [field: SerializeField]
    public int PaCooldown { get; private set; }
    [field: SerializeField]
    public int PaMaxUse { get; private set; }


    [field: Header("Special Ability datas")]
    [field: SerializeField]
    public string SaName { get; private set; }
    [field: SerializeField]
    public string SaDescription { get; private set; }
    [field: SerializeField]
    public Sprite SaIcon { get; private set; }
    [field: SerializeField]
    public Vector2 SaRangeMetrix { get; private set; }
    [field: SerializeField]
    public float SaValue { get; private set; }
    [field: SerializeField]
    public int SaCost { get; private set; }
    [field: SerializeField]
    public int SaCooldown { get; private set; }
    [field: SerializeField]
    public int SaMaxUse { get; private set; }


    [field: Header("Runtime Data -> Reset Often")]
    [field: SerializeField]
    public int PaUseAmount { get; set; }
    [field: SerializeField]
    public int PaTurnLastUsed { get; set; }
    [field: SerializeField]
    public int SaUseAmount { get; set; }
    [field: SerializeField]
    public int SaTurnLastUsed { get; set; }
    [field: SerializeField]
    public int KillAmount { get; set; }
    [field: SerializeField]
    public bool HasMoved { get; set; }

    public void Init()
    {
        KillAmount = 0;
        PaUseAmount = 0;
        SaUseAmount = 0;
        PaTurnLastUsed = 0;
        SaTurnLastUsed = 0;
        HasMoved = false;
    }

    public bool CanUsePrimaryAction(int currentTurn)
    {
        return currentTurn >= PaTurnLastUsed + PaCooldown || PaTurnLastUsed == 0;
    }

    public bool CanUseSpecialAbility(int currentTurn)
    {
        return currentTurn >= SaTurnLastUsed + SaCooldown || SaTurnLastUsed == 0;
    }
}
