using System.Collections.Generic;
using UnityEngine;

public class HeroController : MonoBehaviour
{
    private MovementController _movementManager;
    private CameraManager _cameraController;
    private Animator _animator;
    private ActionMenuController _actionMenuController;
    private GameManager _gameManager;

    [field: SerializeField] public HeroData Data { get; private set; }
    // Detection
    public bool IsPriority { get; set; }
    public bool CanBeSeen { get; set; }
    public bool IsOnObjective { get; set; }

    public PrimaryAction PrimaryActionComponent { get; private set; }
    public SpecialAbility SpecialAbilityComponent { get; private set; }
    private HealthController _healthController;
    private ObjectiveController objectiveController;

    private HeroSFX heroSFX;

    public void Init(NodeSelectionTool nodeSelectionTool, NodePathfindingTool nodePathfindingTool, CameraManager cameraController, ActionMenuController actionMenuController)
    {
        heroSFX = GetComponent<HeroSFX>();
        PrimaryActionComponent = GetComponent<PrimaryAction>();
        SpecialAbilityComponent = GetComponent<SpecialAbility>();
        _healthController = GetComponent<HealthController>();
        _movementManager = GetComponent<MovementController>();
        _animator = GetComponent<Animator>();
        _actionMenuController = actionMenuController;
        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        PrimaryActionComponent.Init(nodePathfindingTool, cameraController, this, actionMenuController, _animator);
        if (SpecialAbilityComponent != null)
            SpecialAbilityComponent.Init(nodePathfindingTool, cameraController, this, actionMenuController);
        _movementManager.Init(nodeSelectionTool, _animator);
        Data.Init();
        IsPriority = false;
        CanBeSeen = true;
        IsOnObjective = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Objective")
        {
            objectiveController = other.gameObject.GetComponent<ObjectiveController>();
            _actionMenuController.DisplayObjectiveButton();
            _gameManager.CurrentObjective = other.gameObject.GetComponent<ObjectiveController>();
            IsOnObjective = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Objective")
        {
            _actionMenuController.HideObjectiveButton();
            _gameManager.CurrentObjective = null;
        }
    }

    public void Heal(float amount)
    {
        _healthController.Heal(amount);
    }

    public void SetResistance(int percent)
    {
        _healthController.SetResistance(percent);
    }

    public void ResetResistance()
    {
        _healthController.ResetResistance();
    }

    public int GetPathCount()
    {
        return _movementManager.Path.Count;
    }

    public int GetMovementRange()
    {
        return _movementManager.MovementRange;
    }

    public NodeController GetCurrentNode()
    {
        _movementManager.SetToCurrentNode();
        return _movementManager.CurrentNode;
    }

    public void ActivateCurrentNodePath(bool isStart, bool isEnd, int countAfterMaxRange, int nodeLeft)
    {
        _movementManager.CurrentNode.Path.Activate(isStart, isEnd, countAfterMaxRange, nodeLeft);
    }

    public void ActivateCurrentNode()
    {
        _movementManager.SetToCurrentNode();
        NodeController node = _movementManager.CurrentNode;
        if(node == null) return;
        node.Waiting.Activate();
    }

    public void DeactivateCurrentNode()
    {
        _movementManager.CurrentNode.Waiting.Deactivate();
    }

    public void Move()
    {
        _movementManager.Move();
    }

    public void StartMoving()
    {
        _movementManager.StartMoving();
        heroSFX.PlayFootstepsSFX();
    }

    public void TakeDamage(float damage)
    {
        _healthController.DoDamage(damage);
        if (!_healthController.IsAlive())
        {
            heroSFX.PlayDeathSFX();
            _animator.SetTrigger("Death");
            _animator.SetInteger("DeathType", Random.Range(0, 4));
        }
    }

    public void SetPath(List<NodeController> pathNodes)
    {
        _movementManager.SetPath(pathNodes);
    }

    public void ResetMovement()
    {
        _movementManager.ResetMovement();
    }

    public void ResetPath()
    {
        _movementManager.ResetPath();
    }

    public void ResetTurn()
    {
        Data.HasMoved = false;
    }

    public void TriggerUltSfx()
    {
        heroSFX.PlayUltSFX();
    }

    public bool ArrivedAtDestination()
    {
        bool hasArrived = _movementManager.ArrivedAtDestination();
        Data.HasMoved = hasArrived;
        if (hasArrived)
            heroSFX.StopFootstepsSFX();
        return hasArrived;
    }

    public bool IsMoving()
    {
        return _movementManager.Moving;
    }

    public bool IsAlive()
    {
        return _healthController.IsAlive();
    }

    public int GetKillAmount()
    {
        return Data.KillAmount;
    }

    public string GetCurrentHealth()
    {
        return ((int)_healthController.CurrentHealth).ToString();
    }

    public string GetMaxHealth()
    {
        return ((int)_healthController.GetMaxHealth()).ToString();
    }

    public void StartInteraction(bool activateTrigger)
    {
        if (activateTrigger) _animator.SetTrigger("Interact");
    }
}
