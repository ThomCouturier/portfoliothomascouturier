using UnityEngine;

public class ProjectileController : MonoBehaviour
{
    private Rigidbody _rigidbody;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    public void Init(Transform origin, float stray, float force)
    {
        transform.position = origin.position;
        transform.rotation = origin.rotation;
        transform.Rotate(90, 90, 0);
        var randX = Random.Range(-stray, stray);
        var ranY = Random.Range(-stray, stray);
        var ranZ = Random.Range(-stray, stray);
        transform.Rotate(randX, ranY, ranZ);
        gameObject.SetActive(true);
        _rigidbody.velocity = Vector3.zero;
        _rigidbody.AddForce(-transform.up * force);
    }

    private void OnCollisionEnter(Collision other)
    {
        gameObject.SetActive(false);
    }
}
