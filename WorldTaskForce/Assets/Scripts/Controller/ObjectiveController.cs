using UnityEngine;

public class ObjectiveController : MonoBehaviour
{
    private AudioSource _audioSource;
    private Rigidbody _rigidbody;
    private Transform _incompleteNode;
    private Transform _completedNode;
    public bool Triggered { get; private set; }

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _completedNode = transform.Find("Completed");
        _incompleteNode = transform.Find("Incomplete");
        _audioSource = GetComponent<AudioSource>();

        _completedNode.gameObject.SetActive(false);

    }

    private void Start()
    {
        Triggered = false;
    }

    public void OnTriggered()
    {
        Triggered = true;
        _rigidbody.detectCollisions = false;
        _incompleteNode.gameObject.SetActive(false);
        _completedNode.gameObject.SetActive(true);
        _audioSource.Play();
    }
}
