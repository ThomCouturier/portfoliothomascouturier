using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class HeroSelectionController : MonoBehaviour
{
    [System.Serializable]
    private struct HeroInfo
    {
        public string Name;
        public Transform HeroGfx;
        public string Role;
        public string Nationality;
        public string Abiltiy;
        public string Damage;
        public string Weapon;
        public Transform WeaponGfx;
        public Light Spotlight;
    }

    private const float TP_VOLUME = 0.2f;
    private const float TACO_VOLUME = 1f;

    private AudioSource _audioSource;
    private AudioSource _voicelineAudioSource;

    // Runtime
    private List<HeroInfo> _selectedHeroes;
    private HeroInfo _currentHeroSelected;
    // References
    [SerializeField] private List<HeroInfo> _heroInfos;
    [SerializeField] private Color _hoverColor;
    [SerializeField] private Color _selectedColor;
    private Transform _deployButton;
    private Light _weaponSpotlight;
    private TextMeshPro selectedHeroNameText;
    private TextMeshPro selectedHeroRoleText;
    private TextMeshPro selectedHeroNationalityText;
    private TextMeshPro selectedHeroAbilityText;
    private TextMeshPro selectedHeroDamageText;
    private TextMeshPro selectedHeroWeaponText;
    private TextMeshPro deployBtn;

    private void Awake()
    {
        _voicelineAudioSource = GetComponent<AudioSource>();
        _heroInfos.ForEach(x => x.WeaponGfx.gameObject.SetActive(false));
        _heroInfos.ForEach(x =>
        {
            x.Spotlight = x.HeroGfx.GetChild(0).GetComponent<Light>(); x.Spotlight.gameObject.SetActive(false);
        });
        _selectedHeroes = new List<HeroInfo>();
        _currentHeroSelected = new HeroInfo();

        _deployButton = GameObject.Find("Deploy_Btn").transform;
        _weaponSpotlight = GameObject.Find("Weapon_Spotlight").GetComponent<Light>();
        _weaponSpotlight.gameObject.SetActive(false);

        // Assigns Infos
        selectedHeroNameText = GameObject.FindGameObjectWithTag("TextName").GetComponent<TextMeshPro>();
        selectedHeroNameText = GameObject.FindGameObjectWithTag("TextName").GetComponent<TextMeshPro>();
        selectedHeroRoleText = GameObject.FindGameObjectWithTag("TextRole").GetComponent<TextMeshPro>();
        selectedHeroNationalityText = GameObject.FindGameObjectWithTag("TextNationality").GetComponent<TextMeshPro>();
        selectedHeroAbilityText = GameObject.FindGameObjectWithTag("TextAbility").GetComponent<TextMeshPro>();
        selectedHeroDamageText = GameObject.FindGameObjectWithTag("TextDamage").GetComponent<TextMeshPro>();
        selectedHeroWeaponText = GameObject.FindGameObjectWithTag("TextWeapon").GetComponent<TextMeshPro>();
        deployBtn = GameObject.FindGameObjectWithTag("DeployBtn").GetComponent<TextMeshPro>();
    }

    private void Start()
    {
        _audioSource = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<AudioSource>();
    }

    private void Update()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        {
            HeroInfo heroSelected = _heroInfos.Where(x => x.HeroGfx.gameObject == hit.collider.gameObject).FirstOrDefault();
            if (heroSelected.HeroGfx != null)
            {
                if (Input.GetButtonDown("Fire1"))
                {
                    _currentHeroSelected = heroSelected;
                    if (_selectedHeroes.Contains(heroSelected))
                        _selectedHeroes.Remove(heroSelected);
                    else if (_selectedHeroes.Count < 3)
                    {
                        _selectedHeroes.Add(heroSelected);
                        PlaySelectionSound(heroSelected.Role);
                    }

                }
                UpdateHoveredHeroDetails(heroSelected);
            }
            else if (hit.collider.gameObject == _deployButton.gameObject)
            {
                if (Input.GetButtonDown("Fire1"))
                {
                    if (_selectedHeroes.Count == 3)
                    {
                        SetSelectedHeroes();
                        PreGameManager.Instance.StartNextScene(0f);
                    }
                }
            }
        }
        ChangeDeployColor();
    }

    private void UpdateHoveredHeroDetails(HeroInfo heroInfo)
    {
        if (!_weaponSpotlight.gameObject.activeSelf) _weaponSpotlight.gameObject.SetActive(true);
        _heroInfos.ForEach(x =>
        {
            if (!_selectedHeroes.Contains(x))
                x.Spotlight.gameObject.SetActive(false);
            x.WeaponGfx.gameObject.SetActive(false);
        });

        selectedHeroNameText.text = heroInfo.Name;
        selectedHeroRoleText.text = heroInfo.Role;
        selectedHeroNationalityText.text = heroInfo.Nationality;
        selectedHeroAbilityText.text = heroInfo.Abiltiy;
        selectedHeroDamageText.text = heroInfo.Damage;
        selectedHeroWeaponText.text = heroInfo.Weapon;

        heroInfo.WeaponGfx.gameObject.SetActive(true);

        if (_selectedHeroes.Contains(heroInfo))
            heroInfo.Spotlight.color = _selectedColor;
        else
            heroInfo.Spotlight.color = _hoverColor;
        heroInfo.Spotlight.gameObject.SetActive(true);
    }

    private void ChangeDeployColor()
    {
        if (_selectedHeroes.Count < 3)
        {
            deployBtn.color = Color.red;
        }
        else
        {
            deployBtn.color = Color.cyan;
        }
    }

    private void SetSelectedHeroes()
    {
        List<int> heroesIndex = new List<int>();
        _selectedHeroes.ForEach(x =>
        {
            heroesIndex.Add(_heroInfos.IndexOf(x));
        });
        PreGameManager.Instance.SetSelectedHeroes(heroesIndex);
    }

    private void PlaySelectionSound(string role)
    {
        if (role == "Tank")
        {
            _voicelineAudioSource.Stop();
            _voicelineAudioSource.clip = SoundManager.SoundInstance.TankSelection;
            _voicelineAudioSource.volume = TP_VOLUME;
            _voicelineAudioSource.Play();
        }

        if (role == "Scout")
        {
            _voicelineAudioSource.Stop();
            _voicelineAudioSource.clip = SoundManager.SoundInstance.ScoutSelection;
            _voicelineAudioSource.volume = TP_VOLUME;
            _voicelineAudioSource.Play();
        }
        if (role == "Support")
        {
            _voicelineAudioSource.Stop();
            _voicelineAudioSource.clip = SoundManager.SoundInstance.SupportSelection;
            _voicelineAudioSource.volume = TACO_VOLUME;
            _voicelineAudioSource.Play();
        }
        if (role == "Sniper")
        {
            _voicelineAudioSource.Stop();
            _voicelineAudioSource.clip = SoundManager.SoundInstance.SniperSelection;
            _voicelineAudioSource.volume = TP_VOLUME;
            _voicelineAudioSource.Play();
        }
        if (role == "Healer")
        {
            _voicelineAudioSource.Stop();
            _voicelineAudioSource.clip = SoundManager.SoundInstance.HealerSelection;
            _voicelineAudioSource.volume = TP_VOLUME;
            _voicelineAudioSource.Play();
        }
        if (role == "Rifleman")
        {
            _voicelineAudioSource.Stop();
            _voicelineAudioSource.clip = SoundManager.SoundInstance.RiflemanSelection;
            _voicelineAudioSource.volume = TACO_VOLUME;
            _voicelineAudioSource.Play();
        }
    }
}
