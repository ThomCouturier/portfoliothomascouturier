using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class MovementController : MonoBehaviour
{
    private NavMeshAgent _navMeshAgent;
    private NodeSelectionTool _nodeSelectionTool;
    private Animator _animator;

    [field: SerializeField] public int MovementRange { get; private set; }
    public NodeController CurrentNode { get; private set; }
    public NodeController TargetNode { get; private set; }

    public bool Moving { get; private set; }
    public List<NodeController> Path { get; private set; }

    public void Init(NodeSelectionTool nodeSelectionTool, Animator animator)
    {
        _nodeSelectionTool = nodeSelectionTool;
        _animator = animator;

        Path = new List<NodeController>();
        Moving = false;
        _navMeshAgent = GetComponent<NavMeshAgent>();
        SetToCurrentNode();
        // CheckIfOnNewNode();
    }

    public void SetPath(List<NodeController> pathNodes)
    {
        Path = pathNodes;
    }

    public void ResetPath()
    {
        CurrentNode.Path.Deactivate();
        foreach (NodeController node in Path)
        {
            node.Path.Deactivate();
        }
        Path = new List<NodeController>();
    }

    public void TeleportToDestination()
    {
        ResetPath();
        gameObject.transform.position = TargetNode.NodeViewPoint;
        CurrentNode = _nodeSelectionTool.DetectCurrentNode(transform.position);
        CurrentNode.SetOccupied(true);
        _navMeshAgent.SetDestination(CurrentNode.NodeViewPoint);
        _animator.SetFloat("Speed", 0f);
        Moving = false;
    }

    public void Move()
    {
        if (Moving)
        {
            // Animator
            _animator.SetFloat("Speed", 0.6f);

            bool isOnNewNode = CheckIfOnNewNode();
            if ((isOnNewNode || CurrentNode == Path[0]) && Path.Count > 0)
            {
                Path[0].Path.Deactivate();
                Path[0].SetOccupied(false);
                Path.Remove(Path[0]);
                if (Path.Count > 0 && CurrentNode != TargetNode)
                    _navMeshAgent.SetDestination(Path[0].NodeViewPoint);
                else
                {

                    Moving = false;
                    _animator.SetFloat("Speed", 0);
                    CurrentNode.SetOccupied(true);
                }
            }
        }
    }

    public void StartMoving()
    {
        if (Path.Count == 0) return;
        _navMeshAgent.enabled = true;
        _navMeshAgent.SetDestination(Path[0].transform.position);
        CurrentNode.SetOccupied(false);
        CurrentNode.Path.Deactivate();
        Moving = true;
        TargetNode = (Path.Count > MovementRange - 1) ? Path[MovementRange - 1] : Path.Last();
        transform.LookAt(Path[0].NodeViewPoint);
    }

    private bool CheckIfOnNewNode()
    {
        NodeController foundNode = _nodeSelectionTool.DetectCurrentNode(transform.position);
        if (foundNode == CurrentNode) return false;
        if (CurrentNode != null)
            CurrentNode.SetOccupied(false);
        CurrentNode = foundNode;
        return true;
    }

    public void SetToCurrentNode()
    {
        CurrentNode = _nodeSelectionTool.DetectCurrentNode(transform.position);
    }

    public void ResetMovement()
    {
        TargetNode = null;
        Moving = false;
        ResetPath();
    }

    public bool ArrivedAtDestination()
    {
        SetToCurrentNode();
        bool result = (TargetNode != null && CurrentNode == TargetNode);
        if (result)
        {
            CurrentNode.SetOccupied(true);
        }
        return result;
    }
}