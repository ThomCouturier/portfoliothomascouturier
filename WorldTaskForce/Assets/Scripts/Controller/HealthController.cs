using UnityEngine;

public class HealthController : MonoBehaviour
{
    [SerializeField] private float _maxHealth;
    [Range(0, 100)]
    [SerializeField] private int _initialResistancePercent;
    private int _resistancePercent;

    public float CurrentHealth { get; private set; }

    private void Awake()
    {
        CurrentHealth = _maxHealth;
        ResetResistance();
    }

    public void DoDamage(float damage)
    {
        CurrentHealth -= damage - (damage * ((float)_resistancePercent / 100f));
        if (CurrentHealth < 0f)
            CurrentHealth = 0f;
    }

    public void Heal(float healing)
    {
        CurrentHealth += healing;
        if (CurrentHealth > _maxHealth)
            CurrentHealth = _maxHealth;
    }

    public void SetResistance(int newPercent)
    {
        _resistancePercent = newPercent;
    }
    public void ResetResistance()
    {
        _resistancePercent = _initialResistancePercent;
    }


    public bool IsHealthBelowThreshold(float threshold)
    {
        return CurrentHealth < threshold;
    }

    public bool IsFullHealth()
    {
        return CurrentHealth >= _maxHealth;
    }

    public float GetMaxHealth()
    {
        return _maxHealth;
    }

    public bool IsAlive()
    {
        return CurrentHealth > 0;
    }
}
