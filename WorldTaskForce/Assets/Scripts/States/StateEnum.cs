public enum StateEnum
{
    UNKNOWN,
    IDLE,
    ADVANCE,
    ATTACK,
    SEEKCOVER,
    INCOVER
}
