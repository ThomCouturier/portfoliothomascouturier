using UnityEngine;

public class StateManager : MonoBehaviour
{
    private StateEnum _currentStateType;
    private State _currentState;

    public void Init()
    {
        ChangeState(StateEnum.IDLE);
    }

    public void UpdateState()
    {
        _currentState.UpdateState();
    }

    public void ChangeState(StateEnum nextStateType)
    {
        // Check next state type, if it's the same, do nothing
        if (nextStateType == _currentStateType) return;

        _currentStateType = nextStateType;

        // Destroy current state script
        if (_currentState != null)
            Destroy(_currentState);

        // Parse through the different values of the enum to create the right state
        switch (_currentStateType)
        {
            case StateEnum.IDLE:
                _currentState = gameObject.AddComponent<IdleState>();
                break;
            case StateEnum.ADVANCE:
                _currentState = gameObject.AddComponent<AdvanceState>();
                break;
            case StateEnum.ATTACK:
                _currentState = gameObject.AddComponent<AttackState>();
                break;
            case StateEnum.SEEKCOVER:
                _currentState = gameObject.AddComponent<SeekCoverState>();
                break;
            case StateEnum.INCOVER:
                _currentState = gameObject.AddComponent<InCoverState>();
                break;
        }
    }
}
