using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ActionMenuController : MonoBehaviour
{
    public ActionTypes ActionType { get; private set; }
    // Panels
    private Transform _pausePanel;
    private Transform _objectivesCounterPanel;
    private Transform _announcementPanel;
    private Transform _heroInfoPanel;
    private Transform _actionInfoPanel;
    private Transform _actionPanel;
    private Transform _skipTurnPanel;
    private Transform _finishObjectivePanel;

    // ObjectivePanel childs
    private TextMeshProUGUI _objectiveCounter;

    // AnnnouncementPanel childs
    private TextMeshProUGUI _title;
    private TextMeshProUGUI _details;

    // HeroInfosPanel childs
    private TextMeshProUGUI _heroName;
    private TextMeshProUGUI _heroRole;
    private TextMeshProUGUI _heroLife;

    // ActionInfoPanel childs
    private TextMeshProUGUI _actionName;
    private TextMeshProUGUI _actionUse;
    private TextMeshProUGUI _actionCost;
    private TextMeshProUGUI _actionCooldown;
    private TextMeshProUGUI _actionDescription;

    // ActionPanel childs
    private Button _actionMoveBtn;
    private Button _actionPrimaryActionBtn;
    private Image _actionPrimaryActionImg;
    private Button _actionSpecialAbilityBtn;
    private Image _actionSpecialAbilityImg;
    private Button _actionChangeHeroBtn;

    // SkipTurnPanel childs
    private Button _skipTurnBtn;

    // Others
    private TextMeshProUGUI _actionPointText;
    private TextMeshProUGUI _currentTurnCounter;

    private List<string> _winningLines = new List<string>{
        "<color=#8AD0FF>You followed the task list!\nGG</color>"
    };
    private List<string> _losingLines = new List<string>{
        "<color=red>Unfortunately, you're bad</color>"
    };

    private const string _startHealthText = "Life: ";
    private const string _middleHealthText = "/";

    public void Init()
    {
        // Panels
        _pausePanel = GameObject.Find("PausePanel").transform;
        _objectivesCounterPanel = GameObject.Find("ObjectivesCounterPanel").transform;
        _announcementPanel = GameObject.Find("AnnouncementPanel").transform;
        _heroInfoPanel = GameObject.Find("HeroInfoPanel").transform;
        _actionInfoPanel = GameObject.Find("ActionInfoPanel").transform;
        _actionPanel = GameObject.Find("ActionPanel").transform;
        _skipTurnPanel = GameObject.Find("SkipTurnPanel").transform;
        _finishObjectivePanel = GameObject.Find("FinishObjectivePanel").transform;

        // AnnnouncementPanel childs
        _title = _announcementPanel.Find("Title").GetComponent<TextMeshProUGUI>();
        _details = _announcementPanel.Find("Details").GetComponent<TextMeshProUGUI>();

        // HeroInfosPanel childs
        _heroName = _heroInfoPanel.Find("HeroName").GetComponent<TextMeshProUGUI>();
        _heroRole = _heroInfoPanel.Find("HeroRole").GetComponent<TextMeshProUGUI>();
        _heroLife = _heroInfoPanel.Find("HeroLife").GetComponent<TextMeshProUGUI>();
        // ActionInfoPanel childs
        _actionName = _actionInfoPanel.Find("ActionName").GetComponent<TextMeshProUGUI>();
        _actionUse = _actionInfoPanel.Find("ActionUse").GetComponent<TextMeshProUGUI>();
        _actionCost = _actionInfoPanel.Find("ActionCost").GetComponent<TextMeshProUGUI>();
        _actionCooldown = _actionInfoPanel.Find("ActionCooldown").GetComponent<TextMeshProUGUI>();
        _actionDescription = _actionInfoPanel.Find("ActionDescription").GetComponent<TextMeshProUGUI>();
        // ActionPanel childs
        _actionMoveBtn = _actionPanel.Find("Move").GetComponent<Button>();
        _actionPrimaryActionBtn = _actionPanel.Find("PrimaryAction").GetComponent<Button>();
        _actionPrimaryActionImg = _actionPrimaryActionBtn.gameObject.GetComponent<Image>();
        _actionSpecialAbilityBtn = _actionPanel.Find("SpecialAbility").GetComponent<Button>();
        _actionSpecialAbilityImg = _actionSpecialAbilityBtn.gameObject.GetComponent<Image>();
        _actionChangeHeroBtn = _actionPanel.Find("ChangeHero").GetComponent<Button>();

        // SkipTurnPanel childs
        _skipTurnBtn = _skipTurnPanel.Find("SkipTurn").GetComponent<Button>();

        // Others
        _actionPointText = GameObject.Find("ActionPoints").GetComponent<TextMeshProUGUI>();
        _currentTurnCounter = GameObject.Find("CurrentTurn").GetComponent<TextMeshProUGUI>();
        _objectiveCounter = GameObject.Find("ObjectiveCounter").GetComponent<TextMeshProUGUI>();

        _pausePanel.gameObject.SetActive(false);
        _finishObjectivePanel.gameObject.SetActive(false);
        SetButtonsInteractable(true);
        ResetActionType();
    }

    public void SetNewHero(HeroController heroController)
    {
        SetButtonsInteractable(true);
        _heroName.text = heroController.Data.Name;
        _heroRole.text = heroController.Data.Role;
        _heroLife.text = _startHealthText + heroController.GetCurrentHealth() + _middleHealthText + heroController.GetMaxHealth();
        _actionPrimaryActionImg.sprite = heroController.Data.PaIcon;
        _actionSpecialAbilityImg.sprite = heroController.Data.SaIcon;
        ResetActionType();
    }

    private void Update()
    {
        if (Input.GetButtonDown("Move") && _actionMoveBtn.interactable) ModifyActionType((int)ActionTypes.MOVE);
        else if (Input.GetButtonDown("PrimaryAction") && _actionPrimaryActionBtn.interactable) ModifyActionType((int)ActionTypes.PRIMARY_ACTION);
        else if (Input.GetButtonDown("SpecialAbility") && _actionSpecialAbilityBtn.interactable) ModifyActionType((int)ActionTypes.SPECIAL_ABILITY);
        else if (Input.GetButtonDown("ChangeHero") && _actionChangeHeroBtn.interactable) ModifyActionType((int)ActionTypes.CHANGE_HERO);
    }

    public void ModifyActionType(int actionTypeIndex)
    {
        ActionType = (ActionTypes)actionTypeIndex;
    }

    public void ResetActionType()
    {
        ModifyActionType((int)ActionTypes.UNDEFINED);
    }

    public void UpdateActionPoint(int currentPoints, int maxPoints)
    {
        _actionPointText.text = $"{currentPoints}/{maxPoints}";
    }

    public void UpdateTurnCounter(int newTurn)
    {
        _currentTurnCounter.text = newTurn.ToString();
    }

    public void UpdateObjectiveCounter(int completedObjective, int maxObjectives)
    {
        _objectiveCounter.text = completedObjective.ToString() + _middleHealthText + maxObjectives.ToString();
    }

    public void UpdateActionInformations(HeroData heroData, string aName, string aDesc, int aUsedNbr, int aUseMaxNbr, int aCost, int aCooldown)
    {
        // ActionInfoPanel
        _actionName.text = aName;
        _actionUse.text = (aUseMaxNbr == 0) ? "∞" : $"{aUseMaxNbr - aUsedNbr}/{aUseMaxNbr}";
        _actionCost.text = $"PA Cost : <color=#8AD0FF>{aCost}</color>";
        _actionCooldown.text = $"Cooldown : <color=#8AD0FF>{aCooldown}</color>";
        _actionDescription.text = aDesc;
    }

    public void DisplayEnemyTurn(bool isEnemyTurn)
    {
        SetButtonsInteractable(!isEnemyTurn);
        _announcementPanel.gameObject.SetActive(isEnemyTurn);
        if (isEnemyTurn)
        {
            _title.text = "<color=red>WARNING</color>";
            _details.text = "<color=red>Enemy Turn</color>";
        }
    }

    public void DisplayPausePanel()
    {
        SetButtonsInteractable(false);
        _pausePanel.gameObject.SetActive(true);
    }

    public void HidePausePanel()
    {
        SetButtonsInteractable(true);
        _pausePanel.gameObject.SetActive(false);
    }

    public void DisplayObjectiveButton()
    {
        _finishObjectivePanel.gameObject.SetActive(true);
    }

    public void HideObjectiveButton()
    {
        _finishObjectivePanel.gameObject.SetActive(false);
    }

    public void DisplayEndGame(bool isGameOver)
    {

        SetButtonsInteractable(false);
        _announcementPanel.gameObject.SetActive(true);
        if (isGameOver)
        {
            _title.text = "<color=red>GAME OVER</color>";
            _details.text = _losingLines[Random.Range(0, _losingLines.Count - 1)];
        }
        else
        {
            _title.text = "<color=#8AD0FF>YOU WON!</color>";
            _details.text = _winningLines[Random.Range(0, _winningLines.Count - 1)];
        }
    }

    public void SetButtonsInteractable(bool interactable)
    {
        _skipTurnBtn.interactable = interactable;
        SetMoveButtonInteractable(interactable);
        SetPrimaryActionButtonInteractable(interactable);
        SetSpecialAbilityButtonInteractable(interactable);
        SetChangeHeroButtonInteractable(interactable);
    }

    public void SetMoveButtonInteractable(bool interactable)
    {
        _actionMoveBtn.interactable = interactable;
    }
    public void SetPrimaryActionButtonInteractable(bool interactable)
    {
        _actionPrimaryActionBtn.interactable = interactable;
    }
    public void SetSpecialAbilityButtonInteractable(bool interactable)
    {
        _actionSpecialAbilityBtn.interactable = interactable;
    }
    public void SetChangeHeroButtonInteractable(bool interactable)
    {
        _actionChangeHeroBtn.interactable = interactable;
    }

    public void ManageButtonInteractibility(int currentTurn, int actionsLeft, bool hasOverride, bool overrideValue, HeroController currentHero)
    {
        // Moving
        bool moveButton = (hasOverride) ? overrideValue : !currentHero.Data.HasMoved && !currentHero.IsMoving() && actionsLeft >= 1;
        SetMoveButtonInteractable(moveButton);
        // PrimaryAction
        bool primaryActionButton = (hasOverride) ? overrideValue : currentHero.Data.CanUsePrimaryAction(currentTurn) && actionsLeft >= currentHero.Data.PaCost;
        SetPrimaryActionButtonInteractable(primaryActionButton);
        // SpecialAbility
        bool specialAbilityButton = (hasOverride) ? overrideValue : currentHero.Data.CanUseSpecialAbility(currentTurn) && actionsLeft >= currentHero.Data.SaCost;
        SetSpecialAbilityButtonInteractable(specialAbilityButton);
        // ChangeHero
        bool changeHeroButton = (hasOverride) ? overrideValue : ActionType != ActionTypes.CHANGE_HERO;
        SetChangeHeroButtonInteractable(changeHeroButton);
    }


}
