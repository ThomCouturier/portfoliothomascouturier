public enum ActionTypes
{
    UNDEFINED,
    MOVE,
    PRIMARY_ACTION,
    SPECIAL_ABILITY,
    CHANGE_HERO
}
