using System.Collections.Generic;
using UnityEngine;

public class DroneController : MonoBehaviour
{
    // References
    private NodeSelectionTool _nodeSelectionTool;
    private GameManager _gameManager;
    private AudioSource _audioSource;

    private HeroController _hero;
    private Transform _offsetOrigin;
    private Transform _home;
    // Runtime
    private EnemyController _target;
    private List<EnemyController> _enemies;
    private int _activationTurn;
    private int _activationDuration;
    private float _detectionRange;
    private bool _returningHome;

    private void Awake()
    {
        if (_offsetOrigin == null)
            _offsetOrigin = transform.Find("Offset").transform;
        if (_gameManager == null)
            _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        if (_nodeSelectionTool == null)
            _nodeSelectionTool = _gameManager.gameObject.GetComponent<NodeSelectionTool>();
        if (_audioSource == null)
            _audioSource = GetComponent<AudioSource>();
        gameObject.SetActive(false);
    }

    public void Init(EnemyController target, List<EnemyController> enemies, HeroController hero, Transform home)
    {
        _target = target;
        _enemies = enemies;
        _hero = hero;
        _home = home;

        _returningHome = false;

        _activationDuration = (int)_hero.Data.SaValue;
        _activationTurn = _gameManager.CurrentTurn;
        _detectionRange = _hero.Data.SaRangeMetrix.y;
        _offsetOrigin.LookAt(_target.transform.position);
        _audioSource.Play();
    }

    private void Update()
    {
        if (_target != null && !_returningHome)
        {
            if (_gameManager.CurrentTurn >= _activationTurn + _activationDuration)
            {
                _returningHome = true;
                _target = null;
                return;
            }
            if (Vector3.Distance(transform.position, _target.transform.position) < 0.001f)
            {
                _offsetOrigin.Rotate(0, 90 * Time.deltaTime, 0);
            }
            else
            {
                // Rotation
                Vector3 direction = _target.transform.position - transform.position;
                Vector3 newDirection = Vector3.RotateTowards(_offsetOrigin.forward, direction, 5 * Time.deltaTime, 0.0f);
                _offsetOrigin.rotation = Quaternion.LookRotation(newDirection);
                // Movement
                transform.position = Vector3.MoveTowards(transform.position, _target.transform.position, 5 * Time.deltaTime);
            }
            _enemies.ForEach(x =>
            {
                if (Vector3.Distance(x.transform.position, transform.position) < _detectionRange)
                {
                    x.SetVisibilityOverrides();
                }
            });
        }
        else if (_returningHome)
        {
            if (Vector3.Distance(transform.position, _home.position) < 0.001f)
            {
                gameObject.SetActive(false);
                _audioSource.Stop();
            }
            Vector3 direction = _home.position - transform.position;
            Vector3 newDirection = Vector3.RotateTowards(_offsetOrigin.forward, direction, 5 * Time.deltaTime, 0.0f);
            _offsetOrigin.rotation = Quaternion.LookRotation(newDirection);
            // Movement
            transform.position = Vector3.MoveTowards(transform.position, _home.transform.position, 5 * Time.deltaTime);
        }
    }
}
