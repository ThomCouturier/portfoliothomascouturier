using UnityEngine;

public class BerserkController : MonoBehaviour
{
    private ParticleSystem _tankVFX;
    private GameManager _gameManager;

    private HeroController _hero;
    private bool _trigger;

    private int _activationTurn;
    private int _activationDuration;

    private void Awake()
    {
        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        _tankVFX = GetComponent<ParticleSystem>();
        _trigger = false;
    }

    public void Init(HeroController hero)
    {
        _hero = hero;
        _trigger = true;

        _hero.IsPriority = true;
        _hero.SetResistance((int)_hero.Data.SaValue);

        _activationDuration = (int)_hero.Data.SaRangeMetrix.x;
        if (_gameManager == null) _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        _activationTurn = _gameManager.CurrentTurn;

        _tankVFX.Play();
    }

    private void Update()
    {
        if (_trigger && _gameManager.CurrentTurn >= _activationTurn + _activationDuration)
        {
            _trigger = false;
            _hero.IsPriority = false;
            _tankVFX.Stop();
            gameObject.SetActive(false);
            return;
        }
    }
}
