using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ArtilleryController : MonoBehaviour
{
    // Can't get them with code, idk why
    [SerializeField] private ParticleSystem _explosionVfx;
    [SerializeField] private ParticleSystem _flashVfx;

    private GameManager _gameManager;
    private AudioSource _audioSource;

    private List<EnemyController> _enemies;
    private HeroController _hero;

    private int _activationTurn;
    private int _activationDuration;
    private float _explosionRadius;
    private float _damages;
    private bool _trigger;

    private void Awake()
    {
        if (_gameManager == null)
            _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        if (_audioSource == null)
            _audioSource = GetComponent<AudioSource>();
        string[] layerNames = { "Hero", "Enemy" };

        gameObject.SetActive(false);
    }

    public void Init(NodeController target, List<EnemyController> enemies, HeroController hero)
    {
        transform.position = target.transform.position;
        _enemies = enemies;
        _hero = hero;

        _activationDuration = (int)_hero.Data.SaRangeMetrix.x;
        _activationTurn = _gameManager.CurrentTurn;
        _explosionRadius = _hero.Data.SaRangeMetrix.y;
        _damages = _hero.Data.SaValue;
        _trigger = true;
    }

    private void Update()
    {
        if (_trigger && _gameManager.CurrentTurn >= _activationTurn + _activationDuration)
        {
            _explosionVfx.Play();
            _explosionVfx.Play();
            _audioSource.Play();
            _enemies.Where(x => Vector3.Distance(x.transform.position, transform.position) <= _explosionRadius).ToList()
                    .ForEach(x =>
                    {
                        if (!Physics.Linecast(x.transform.position, transform.position, ~LayerMask.GetMask("Enemy")))
                        {
                            x.TakeRawDamage(_damages);
                        }
                    });
            _trigger = false;
            return;
        }
    }
}
