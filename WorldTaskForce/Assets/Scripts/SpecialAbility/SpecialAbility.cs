using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class SpecialAbility : MonoBehaviour
{
    protected NodePathfindingTool _nodePathfindingTool;
    protected CameraManager _cameraController;
    protected HeroController _hero;
    protected ActionMenuController _actionMenuController;

    protected HeroData _heroData;

    protected bool _triggered;
    [SerializeField] protected float _delayBeforeEnding;
    protected float _currentDelay;
    protected Vector2 _range;
    protected NodeController _currentNode;

    protected List<NodeController> _actionRangeNodes;
    protected List<HeroController> _heroes;
    protected List<EnemyController> _enemies;

    public virtual void Init(NodePathfindingTool nodePathfindingTool, CameraManager cameraController, HeroController hero, ActionMenuController actionMenuController)
    {
        _nodePathfindingTool = nodePathfindingTool;
        _cameraController = cameraController;
        _hero = hero;
        _actionMenuController = actionMenuController;
        _currentDelay = _delayBeforeEnding;
        _heroData = _hero.Data;
        _range = _heroData.SaRangeMetrix;
        _actionRangeNodes = new List<NodeController>();
    }

    // Runtime Methods
    public virtual void BeginAbility(List<HeroController> heroes, List<EnemyController> enemies)
    {
        _heroes = heroes;
        _enemies = enemies;
        _currentDelay = _delayBeforeEnding;
        // Range creation
        List<NodeController> nodes = _nodePathfindingTool.GetNodesInWorldRange(_hero.GetCurrentNode(), _range.y);
        _actionRangeNodes = nodes.Where(x => Vector3.Distance(x.NodeViewPoint, transform.position) >= _range.x && Vector3.Distance(x.NodeViewPoint, transform.position) <= _range.y).ToList();
    }

    public virtual bool UpdateAbility(NodeController selectedNode)
    {
        return true;
    }

    public virtual void EndAbility()
    {
        _triggered = false;
        ClearActionRangeNodes();
    }

    protected void ClearActionRangeNodes()
    {
        _actionRangeNodes.ForEach(x => x.ActionRange.Deactivate());
        _actionRangeNodes.Clear();
    }
}
