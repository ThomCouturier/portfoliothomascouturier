using UnityEngine;

public class InvisibilityController : MonoBehaviour
{
    private ParticleSystem _invisibilityVFX;
    private GameManager _gameManager;

    private HeroController _hero;
    private bool _trigger;

    private int _activationTurn;
    private int _activationDuration;

    private void Awake()
    {
        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        _invisibilityVFX = GetComponent<ParticleSystem>();
        _trigger = false;
    }

    public void Init(HeroController hero)
    {
        _hero = hero;
        _trigger = true;

        _hero.CanBeSeen = false;

        _activationDuration = (int)_hero.Data.SaValue;
        if (_gameManager == null) _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        _activationTurn = _gameManager.CurrentTurn;

        _invisibilityVFX.Play();
    }

    private void Update()
    {
        if (_trigger && _gameManager.CurrentTurn >= _activationTurn + _activationDuration)
        {
            _trigger = false;
            _hero.CanBeSeen = true;
            _invisibilityVFX.Stop();
            gameObject.SetActive(false);
            return;
        }
    }
}
