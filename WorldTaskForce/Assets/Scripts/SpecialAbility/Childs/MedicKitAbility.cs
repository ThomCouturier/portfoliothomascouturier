using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MedicKitAbility : SpecialAbility
{
    protected ParticleSystem _healingPS;
    protected LayerMask _layerMaskObstacle;
    private HeroController _target;

    public override void Init(NodePathfindingTool nodePathfindingTool, CameraManager cameraController, HeroController hero, ActionMenuController actionMenuController)
    {
        base.Init(nodePathfindingTool, cameraController, hero, actionMenuController);
        _layerMaskObstacle = LayerMask.GetMask("Ignore Raycast");
        _healingPS = GameObject.FindObjectsOfType<ParticleSystem>(true).Where(x => x.gameObject.name == "HealingParticles").First();
        _healingPS.gameObject.SetActive(false);
    }

    public override void BeginAbility(List<HeroController> heroes, List<EnemyController> enemies)
    {
        base.BeginAbility(heroes, enemies);
        FilterActionRange();
    }

    public override bool UpdateAbility(NodeController selectedNode)
    {
        _cameraController.UpdateCameraMovement();
        if (Input.GetButtonDown("Fire1") && !_triggered)
        {
            HeroController heroSelected = _heroes.Where(x => x.GetCurrentNode() == selectedNode).FirstOrDefault();
            if (heroSelected != null)
            {
                _actionMenuController.SetButtonsInteractable(false);
                _triggered = true;
                // Particle System
                _healingPS.transform.position = heroSelected.transform.position;
                _healingPS.gameObject.SetActive(true);
                _healingPS.Play();
                // Ability
                heroSelected.Heal(_hero.Data.SaValue);
                // Increment
                _hero.Data.SaUseAmount++;
                _hero.TriggerUltSfx();
            }
        }
        else
        {
            UpdateNodeAndTarget(selectedNode);
        }
        if (_triggered)
        {
            _currentDelay -= Time.deltaTime;
            if (_currentDelay <= 0)
            {
                _actionMenuController.SetButtonsInteractable(true);
                _healingPS.Stop();
                _healingPS.gameObject.SetActive(false);
                return true;
            }
        }
        return false;
    }


    private void FilterActionRange()
    {
        List<NodeController> nodeToRemove = new List<NodeController>();
        nodeToRemove.Add(_hero.GetCurrentNode());
        nodeToRemove[0].ActionRange.Deactivate();
        _actionRangeNodes.Remove(nodeToRemove[0]);

        _actionRangeNodes.ForEach(x =>
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, x.NodeViewPoint - transform.position, out hit, Vector3.Distance(x.NodeViewPoint, transform.position), _layerMaskObstacle))
            {
                x.ActionRange.Deactivate();
                nodeToRemove.Add(x);
            }
            else
            {
                _heroes.ForEach(hero =>
                {
                    if (hero.GetCurrentNode() == x)
                        x.ActionRange.Activate(true);
                });
            }
        });
        nodeToRemove.ForEach(x => _actionRangeNodes.Remove(x));
    }

    private void UpdateNodeAndTarget(NodeController selectedNode)
    {
        if (selectedNode != null && selectedNode != _currentNode)
        {
            if (_currentNode != null)
                _currentNode.Target.Deactivate();
            _currentNode = selectedNode;
            _target = _heroes.Where(x => x.GetCurrentNode() == selectedNode).FirstOrDefault();
            if (selectedNode.IsOccupied && _actionRangeNodes.Contains(selectedNode) && _target != null)
            {
                selectedNode.Target.Activate(true);
            }
            else
            {
                selectedNode.Target.Activate(false);
            }
        }
    }
}
