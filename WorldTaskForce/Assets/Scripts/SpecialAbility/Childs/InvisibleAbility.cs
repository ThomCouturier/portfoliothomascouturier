using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class InvisibleAbility : SpecialAbility
{
    InvisibilityController _invisibilityController;

    public override void Init(NodePathfindingTool nodePathfindingTool, CameraManager cameraController, HeroController hero, ActionMenuController actionMenuController)
    {
        base.Init(nodePathfindingTool, cameraController, hero, actionMenuController);
        _invisibilityController = (InvisibilityController)GameObject.FindObjectsOfType<InvisibilityController>(true).First();
    }

    public override void BeginAbility(List<HeroController> heroes, List<EnemyController> enemies)
    {
        base.BeginAbility(heroes, enemies);
        FilterActionRange();
    }

    public override bool UpdateAbility(NodeController selectedNode)
    {
        _cameraController.UpdateCameraMovement();
        if (Input.GetButtonDown("Fire1") && !_triggered)
        {
            if (selectedNode == _hero.GetCurrentNode())
            {
                _actionMenuController.SetButtonsInteractable(false);
                _triggered = true;
                // Ability
                _invisibilityController.gameObject.SetActive(true);

                _invisibilityController.Init(_hero);
                // Increment
                _hero.Data.SaUseAmount++;
                _hero.TriggerUltSfx();
            }
        }
        else
        {
            UpdateNodeAndTarget(selectedNode);
        }
        if (_triggered)
        {
            _currentDelay -= Time.deltaTime;
            if (_currentDelay <= 0)
            {
                _actionMenuController.SetButtonsInteractable(true);
                return true;
            }
        }
        return false;
    }

    private void FilterActionRange()
    {
        List<NodeController> nodeToRemove = new List<NodeController>();
        _actionRangeNodes.ForEach(x => x.ActionRange.Deactivate());
        _actionRangeNodes.Add(_hero.GetCurrentNode());
        _hero.GetCurrentNode().ActionRange.Activate(true);
    }

    private void UpdateNodeAndTarget(NodeController selectedNode)
    {
        if (selectedNode != null && selectedNode != _currentNode)
        {
            if (_currentNode != null)
                _currentNode.Target.Deactivate();
            _currentNode = selectedNode;
            if (_currentNode == _hero.GetCurrentNode() && _actionRangeNodes.Contains(_currentNode))
            {
                selectedNode.Target.Activate(true);
            }
            else
            {
                selectedNode.Target.Activate(false);
            }
        }
    }
}
