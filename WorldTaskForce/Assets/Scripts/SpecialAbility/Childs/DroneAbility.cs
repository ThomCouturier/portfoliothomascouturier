using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DroneAbility : SpecialAbility
{
    private DroneController _droneController;
    private Transform _droneOrigin;
    private EnemyController _target;

    public override void Init(NodePathfindingTool nodePathfindingTool, CameraManager cameraController, HeroController hero, ActionMenuController actionMenuController)
    {
        base.Init(nodePathfindingTool, cameraController, hero, actionMenuController);
        _droneController = GameObject.FindObjectsOfType<DroneController>(true).First();
        _droneOrigin = transform.Find("DroneOrigin").transform;
        _droneController.gameObject.SetActive(false);
    }

    public override void BeginAbility(List<HeroController> heroes, List<EnemyController> enemies)
    {
        _heroes = heroes;
        _enemies = enemies;
        _currentDelay = _delayBeforeEnding;
        FilterActionRange();
    }

    public override bool UpdateAbility(NodeController selectedNode)
    {
        _cameraController.UpdateCameraMovement();
        UpdateNodeAndTarget(selectedNode);
        if (Input.GetButtonDown("Fire1") && !_triggered)
        {
            EnemyController enemy = _enemies.Where(x => x.GetCurrentNode() == selectedNode).FirstOrDefault();
            if (enemy != null)
            {
                _triggered = true;
                _actionMenuController.SetButtonsInteractable(false);
                // Ability
                _droneController.transform.position = transform.position;
                _droneController.gameObject.SetActive(true);
                _droneController.Init(enemy, _enemies, _hero, _droneOrigin);
                // Increment
                _hero.Data.SaUseAmount++;
                _hero.TriggerUltSfx();
            }
        }
        if (_triggered)
        {
            _currentDelay -= Time.deltaTime;
            if (_currentDelay <= 0)
            {
                _actionMenuController.SetButtonsInteractable(true);
                return true;
            }
        }
        return false;
    }

    private void FilterActionRange()
    {
        List<NodeController> nodes = new List<NodeController>();
        _enemies.Where(x => x.IsVisible()).ToList().ForEach(x =>
        {
            NodeController node = x.GetCurrentNode();
            nodes.Add(node);
            node.ActionRange.Activate(true);
        });
        _actionRangeNodes = nodes;
    }

    private void UpdateNodeAndTarget(NodeController selectedNode)
    {
        if (selectedNode != null && selectedNode != _currentNode)
        {
            if (_currentNode != null)
                _currentNode.Target.Deactivate();
            _currentNode = selectedNode;
            _target = _enemies.Where(x => x.GetCurrentNode() == selectedNode).FirstOrDefault();
            if (selectedNode.IsOccupied && _actionRangeNodes.Contains(selectedNode) && _target != null)
            {
                selectedNode.Target.Activate(true);
            }
            else
            {
                selectedNode.Target.Activate(false);
            }
        }
    }
}
