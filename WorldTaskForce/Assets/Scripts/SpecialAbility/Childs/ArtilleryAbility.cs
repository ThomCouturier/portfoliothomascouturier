using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ArtilleryAbility : SpecialAbility
{
    private ArtilleryController _artilleryController;

    public override void Init(NodePathfindingTool nodePathfindingTool, CameraManager cameraController, HeroController hero, ActionMenuController actionMenuController)
    {
        base.Init(nodePathfindingTool, cameraController, hero, actionMenuController);
        _artilleryController = GameObject.FindObjectsOfType<ArtilleryController>(true).First();
        _artilleryController.gameObject.SetActive(false);
    }

    public override void BeginAbility(List<HeroController> heroes, List<EnemyController> enemies)
    {
        base.BeginAbility(heroes, enemies);
        FilterActionRange();
    }

    public override bool UpdateAbility(NodeController selectedNode)
    {
        _cameraController.UpdateCameraMovement();
        UpdateNodeAndTarget(selectedNode);
        if (Input.GetButtonDown("Fire1") && !_triggered && _actionRangeNodes.Contains(_currentNode))
        {
            _triggered = true;
            _actionMenuController.SetButtonsInteractable(false);
            // Ability
            _artilleryController.gameObject.SetActive(true);
            _artilleryController.Init(_currentNode, _enemies, _hero);
            // Increment
            _hero.Data.SaUseAmount++;
            _hero.TriggerUltSfx();
            ClearActionRangeNodes();
        }
        if (_triggered)
        {
            _currentDelay -= Time.deltaTime;
            if (_currentDelay <= 0)
            {
                _actionMenuController.SetButtonsInteractable(true);
                return true;
            }
        }
        return false;
    }


    private void FilterActionRange()
    {
        _actionRangeNodes.ForEach(x => x.ActionRange.Activate(true));
    }

    private void UpdateNodeAndTarget(NodeController selectedNode)
    {
        if (selectedNode != null && selectedNode != _currentNode)
        {
            if (_currentNode != null)
                _currentNode.Target.Deactivate();
            _currentNode = selectedNode;
            if (_actionRangeNodes.Contains(selectedNode))
            {
                selectedNode.Target.Activate(true);
            }
            else
            {
                selectedNode.Target.Activate(false);
            }
        }
    }
}