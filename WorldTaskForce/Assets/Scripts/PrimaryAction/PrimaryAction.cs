using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class PrimaryAction : MonoBehaviour
{
    [SerializeField] protected float _delayBeforeEnding;
    protected NodePathfindingTool _nodePathfindingTool;
    protected CameraManager _cameraController;
    protected HeroController _hero;
    protected ActionMenuController _actionMenuController;
    protected Animator _animator;

    protected float _currentDelay;
    protected NodeController _currentNode;
    protected List<NodeController> _actionRangeNodes;
    protected LayerMask _layerMaskObstacle;
    protected LayerMask _hitMarkerLayerMask;
    protected bool _isAiming;
    protected Vector2 _range;
    protected HeroData _data;
    protected List<HeroController> _heroes;
    protected List<EnemyController> _enemies;
    protected int _useAmount;

    protected HeroSFX heroSFX;

    public virtual void Init(NodePathfindingTool nodePathfindingTool, CameraManager cameraController, HeroController hero, ActionMenuController actionMenuController, Animator animator)
    {
        heroSFX = GetComponent<HeroSFX>();
        _nodePathfindingTool = nodePathfindingTool;
        _cameraController = cameraController;
        _hero = hero;
        _actionMenuController = actionMenuController;
        _animator = animator;
        _data = _hero.Data;
        _range = _data.PaRangeMetrix;
        _actionRangeNodes = new List<NodeController>();
        _layerMaskObstacle = LayerMask.GetMask("Ignore Raycast");
        _hitMarkerLayerMask = LayerMask.GetMask("HitMarker");
        _currentNode = null;
        _isAiming = false;
        _useAmount = 0;
    }

    // Runtime Methods
    public virtual void BeginAction(List<HeroController> heroes, List<EnemyController> enemies)
    {
        _heroes = heroes;
        _enemies = enemies;
        _currentDelay = _delayBeforeEnding;
        // Range creation
        List<NodeController> nodes = _nodePathfindingTool.GetNodesInWorldRange(_hero.GetCurrentNode(), _range.y);
        _actionRangeNodes = nodes.Where(x => Vector3.Distance(x.NodeViewPoint, transform.position) >= _range.x && Vector3.Distance(x.NodeViewPoint, transform.position) <= _range.y).ToList();
    }

    public virtual bool UpdateAction(NodeController selectedNode)
    {
        return true;
    }

    public virtual void EndAction()
    {
        ClearActionRangeNodes();
    }

    protected void ClearActionRangeNodes()
    {
        _actionRangeNodes.ForEach(x => x.ActionRange.Deactivate());
        _actionRangeNodes.Clear();
    }
}
