using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GunPrimaryAction : PrimaryAction
{
    [SerializeField] private Transform _projectileOrigin;
    [SerializeField] private int _maxProjectiles;
    private List<ProjectileController> _projectiles;
    [SerializeField] private float _initialDelayBetweenShots;
    [SerializeField] private float _strayFactor;
    [SerializeField] private float _force;

    private EnemyController _target;
    private HitMarkerTypes hitMarkerType;
    private bool _triggered;
    private bool _activated;
    private int _currentProjectile;
    private float _currentDelayShot;

    public override void Init(NodePathfindingTool nodePathfindingTool, CameraManager cameraController, HeroController hero, ActionMenuController actionMenuController, Animator animator)
    {
        base.Init(nodePathfindingTool, cameraController, hero, actionMenuController, animator);
        _target = null;
        _triggered = false;
        _activated = false;
        _currentProjectile = 0;
        _currentDelayShot = _initialDelayBetweenShots;
        _animator.SetBool("IsRifle", _initialDelayBetweenShots > 0);

        _projectiles = new List<ProjectileController>();
        foreach (Transform projectileTr in GameObject.Find("Projectiles").transform)
        {
            if (_projectiles.Count < _maxProjectiles)
            {
                ProjectileController newBoomBoom = projectileTr.gameObject.GetComponent<ProjectileController>();
                _projectiles.Add(newBoomBoom);
            }
            else break;
        }
    }

    public override void BeginAction(List<HeroController> heroes, List<EnemyController> enemies)
    {
        base.BeginAction(heroes, enemies);
        FilterActionRange();
        _currentDelayShot = _initialDelayBetweenShots;
        _currentProjectile = 0;
    }

    public override bool UpdateAction(NodeController selectedNode)
    {
        if (!_triggered && !_activated)
        {
            _cameraController.UpdateCameraMovement();
            UpdateNodeAndTarget(selectedNode);
        }
        else if (_triggered && _activated)
        {
            if (_currentProjectile <= _projectiles.Count - 1 && _currentDelayShot <= 0f)
            {
                _projectiles[_currentProjectile].Init(_projectileOrigin, _strayFactor, _force);
                _currentDelayShot = _initialDelayBetweenShots;
                _currentDelayShot += Random.Range(-_currentDelayShot / 3f, _currentDelayShot / 3f);
                heroSFX.PlayGunshotSFX();
                _currentProjectile++;
                _animator.SetTrigger("Attack");
            }
            _currentDelayShot -= Time.deltaTime;
            _currentDelay -= Time.deltaTime;
            if ((_currentDelay <= 0 && _currentProjectile >= _projectiles.Count - 1) ||
                (_currentDelay <= 0 && _projectiles.Where(x => x.gameObject.activeSelf).ToList().Count == 0))
            {
                _data.PaUseAmount++;
                _target.TakeDamage(hitMarkerType, _hero.Data.PaValue);
                _actionMenuController.SetButtonsInteractable(true);
                _cameraController.TransferCameraToSpectatorView();
                return true;
            }
        }
        if (Input.GetButtonDown("Fire1") && _currentNode != null && _target != null)
        {
            if (_triggered && !_activated)
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit, Mathf.Infinity, _hitMarkerLayerMask))
                {
                    _activated = true;
                    _actionMenuController.ManageButtonInteractibility(0, 0, true, true, _hero);
                    hitMarkerType = new HitMarkerTypes();

                    GameObject markerSelected = hit.collider.gameObject;
                    if (markerSelected.CompareTag("HitMark_Head")) hitMarkerType = HitMarkerTypes.HEAD;
                    else if (markerSelected.CompareTag("HitMark_Leg")) hitMarkerType = HitMarkerTypes.LEG;
                    else hitMarkerType = HitMarkerTypes.CHEST;

                    _target.HideHitMakers();
                    _actionMenuController.SetButtonsInteractable(false);
                }
            }
            else if (_actionRangeNodes.Contains(_currentNode))
            {
                ClearActionRangeNodes();
                _triggered = true;
                _target.DisplayHitMarkers(_hero.GetCurrentNode().NodeViewPoint, _hero.Data.PaRangeMetrix.y, _hero.Data.Accuracy);
                // Position and rotation
                transform.LookAt(_target.transform.position);
                // Camera data
                Vector3 position = transform.position + (transform.forward * -1.5f) + (transform.up * 1.5f) + (transform.right * 1.5f);
                _cameraController.TransferCameraToFpsView(position, _target.GetCurrentNode().NodeViewPoint);
            }
        }
        return false;
    }

    public override void EndAction()
    {
        base.EndAction();
        if (_target != null)
            _target.Reset();
        _target = null;
        _triggered = false;
        _activated = false;
        _projectiles.ForEach(x => x.gameObject.SetActive(false));
        _cameraController.TransferCameraToSpectatorView();
    }

    private void FilterActionRange()
    {
        List<NodeController> nodeToRemove = new List<NodeController>();
        _actionRangeNodes.ForEach(x =>
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, x.NodeViewPoint - transform.position, out hit, Vector3.Distance(x.NodeViewPoint, transform.position), _layerMaskObstacle))
            {
                x.ActionRange.Deactivate();
                nodeToRemove.Add(x);
            }
            else
            {
                _enemies.Where(enemy => enemy.gameObject.activeSelf && enemy.IsAlive()).ToList().ForEach(enemy =>
                {
                    if (enemy.GetCurrentNode() == x && enemy.IsVisible())
                        x.ActionRange.Activate(true);
                    else
                        x.ActionRange.Activate(false);
                });
            }
        });
        nodeToRemove.ForEach(x => _actionRangeNodes.Remove(x));
    }

    private void UpdateNodeAndTarget(NodeController selectedNode)
    {
        if (selectedNode != null && selectedNode != _currentNode)
        {
            if (_currentNode != null)
                _currentNode.Target.Deactivate();
            _currentNode = selectedNode;
            _target = _enemies.Where(x => x.GetCurrentNode() == selectedNode).FirstOrDefault();
            if (selectedNode.IsOccupied && _actionRangeNodes.Contains(selectedNode) && _target != null)
            {
                selectedNode.Target.Activate(true);
            }
            else
            {
                selectedNode.Target.Activate(false);
            }
        }
    }
}
