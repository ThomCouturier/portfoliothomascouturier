using UnityEngine;

public class HealerSFX : MonoBehaviour, HeroSFX
{
    private AudioSource _audioSource;

    void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    public void PlayDeathSFX()
    {
        _audioSource.PlayOneShot(SoundManager.SoundInstance.HealerDeath, 0.3f);
    }

    public void PlayGunshotSFX()
    {
        _audioSource.PlayOneShot(SoundManager.SoundInstance.UspSocomSFX, 0.5f);
    }

    public void PlayKillSFX()
    {
        _audioSource.PlayOneShot(SoundManager.SoundInstance.HealerKill, 0.3f);
    }

    public void PlayUltSFX()
    {
        _audioSource.PlayOneShot(SoundManager.SoundInstance.HealerUlt, 0.3f);
    }

    public void PlayFootstepsSFX()
    {
        _audioSource.Play();
    }

    public void StopFootstepsSFX()
    {
        _audioSource.Stop();
    }
}
