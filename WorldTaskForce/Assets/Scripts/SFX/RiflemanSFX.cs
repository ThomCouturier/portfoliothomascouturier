using UnityEngine;

public class RiflemanSFX : MonoBehaviour, HeroSFX
{
    private AudioSource _audioSource;

    void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    public void PlayDeathSFX()
    {
        _audioSource.PlayOneShot(SoundManager.SoundInstance.RiflemanDeath, 1f);
    }

    public void PlayGunshotSFX()
    {
        _audioSource.PlayOneShot(SoundManager.SoundInstance.M4SFX, 0.5f);
    }

    public void PlayKillSFX()
    {
        _audioSource.PlayOneShot(SoundManager.SoundInstance.RiflemanKill, 1f);
    }

    public void PlayUltSFX()
    {
        _audioSource.PlayOneShot(SoundManager.SoundInstance.RiflemanUlt, 1f);
    }

    public void PlayFootstepsSFX()
    {
        _audioSource.Play();
    }

    public void StopFootstepsSFX()
    {
        _audioSource.Stop();
    }
}
