using UnityEngine;

public class ScoutSFX : MonoBehaviour, HeroSFX
{
    private AudioSource _audioSource;

    void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    public void PlayDeathSFX()
    {
        _audioSource.PlayOneShot(SoundManager.SoundInstance.ScoutDeath, 0.3f);
    }

    public void PlayGunshotSFX()
    {
        _audioSource.PlayOneShot(SoundManager.SoundInstance.P90SFX, 0.5f);
    }

    public void PlayKillSFX()
    {
        _audioSource.PlayOneShot(SoundManager.SoundInstance.ScoutKill, 0.3f);
    }

    public void PlayUltSFX()
    {
        _audioSource.PlayOneShot(SoundManager.SoundInstance.ScoutUlt, 0.3f);
    }

    public void PlayFootstepsSFX()
    {
        _audioSource.Play();
    }

    public void StopFootstepsSFX()
    {
        _audioSource.Stop();
    }
}
