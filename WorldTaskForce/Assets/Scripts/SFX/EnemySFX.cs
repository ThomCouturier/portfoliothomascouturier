using System.Collections.Generic;
using UnityEngine;

public class EnemySFX : MonoBehaviour, HeroSFX
{
    private AudioSource _audioSource;
    private List<AudioClip> _killAudioClips = new List<AudioClip>();
    private List<AudioClip> _gunshotAudioClips = new List<AudioClip>();

    void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    private void Start()
    {
        _killAudioClips.Clear();
        _gunshotAudioClips.Clear();
        _killAudioClips.Add(SoundManager.SoundInstance.EnemyLaugh1);
        _killAudioClips.Add(SoundManager.SoundInstance.EnemyLaugh2);
        _killAudioClips.Add(SoundManager.SoundInstance.EnemyLaugh3);
        _killAudioClips.Add(SoundManager.SoundInstance.EnemyLaugh4);
        _gunshotAudioClips.Add(SoundManager.SoundInstance.Mp7SFX);
        _gunshotAudioClips.Add(SoundManager.SoundInstance.P90SFX);
        _gunshotAudioClips.Add(SoundManager.SoundInstance.UspSocomSFX);
    }

    public void PlayGunshotSFX()
    {
        int rand = Random.Range(0, 3);
        _audioSource.PlayOneShot(_gunshotAudioClips[rand], 1f);
    }

    public void PlayKillSFX()
    {
        int playing = Random.Range(0, 100);
        if (playing <= 10)
        {
            int rand = Random.Range(0, 2);
            _audioSource.PlayOneShot(_killAudioClips[rand], 1f);
        }
    }

    public void PlayFootstepsSFX()
    {
        _audioSource.Play();
    }

    public void StopFootstepsSFX()
    {
        _audioSource.Stop();
    }

    public void PlayDeathSFX()
    {
        throw new System.NotImplementedException();
    }

    public void PlayUltSFX()
    {
        throw new System.NotImplementedException();
    }
}
