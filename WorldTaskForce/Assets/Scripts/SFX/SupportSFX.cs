using UnityEngine;

public class SupportSFX : MonoBehaviour, HeroSFX
{
    private AudioSource _audioSource;

    void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    public void PlayDeathSFX()
    {
        _audioSource.PlayOneShot(SoundManager.SoundInstance.SupportDeath, 1f);
    }

    public void PlayGunshotSFX()
    {
        _audioSource.PlayOneShot(SoundManager.SoundInstance.Mp7SFX, 0.5f);
    }

    public void PlayKillSFX()
    {
        _audioSource.PlayOneShot(SoundManager.SoundInstance.SupportKill, 1f);
    }

    public void PlayUltSFX()
    {
        _audioSource.PlayOneShot(SoundManager.SoundInstance.SupportUlt, 1f);
    }

    public void PlayFootstepsSFX()
    {
        _audioSource.Play();
    }

    public void StopFootstepsSFX()
    {
        _audioSource.Stop();
    }
}
