using UnityEngine;

public class SniperSFX : MonoBehaviour, HeroSFX
{
    private AudioSource _audioSource;

    void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    public void PlayDeathSFX()
    {
        _audioSource.PlayOneShot(SoundManager.SoundInstance.SniperDeath, 0.3f);
    }

    public void PlayGunshotSFX()
    {
        _audioSource.PlayOneShot(SoundManager.SoundInstance.AwpSFX, 0.5f);
    }

    public void PlayKillSFX()
    {
        _audioSource.PlayOneShot(SoundManager.SoundInstance.SniperKill, 0.3f);
    }

    public void PlayUltSFX()
    {
        _audioSource.PlayOneShot(SoundManager.SoundInstance.SniperUlt, 0.3f);
    }

    public void PlayFootstepsSFX()
    {
        _audioSource.Play();
    }

    public void StopFootstepsSFX()
    {
        _audioSource.Stop();
    }
}
