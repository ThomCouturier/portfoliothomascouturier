public interface HeroSFX
{
    void PlayGunshotSFX();
    void PlayDeathSFX();
    void PlayKillSFX();
    void PlayUltSFX();
    void PlayFootstepsSFX();
    void StopFootstepsSFX();
}
