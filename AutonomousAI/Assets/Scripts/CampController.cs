using UnityEngine;

public class CampController : MonoBehaviour
{
    [Header("Scene References")]
    [SerializeField] private Transform _gfxAlive;
    [SerializeField] private Transform _gfxDestroyed;
    [SerializeField] private ProjectilePool _opponentProjectilePool;
    [SerializeField] private ProjectilePool _alliedProjectilePool;
    [SerializeField] private GameManager _gameManager;

    [Header("Scripts")]
    [SerializeField] private CampHealthController _campHealthController;

    [field: Header("Runtime")]
    [field: SerializeField] public bool IsDestroyed { get; private set; }


    private void Start()
    {
        // Health Controller
        if (_campHealthController == null)
            _campHealthController = GetComponent<CampHealthController>();
    }

    void Update()
    {
        if (_campHealthController.IsHealthDepleted() && !IsDestroyed)
        {
            _gfxAlive.gameObject.SetActive(false);
            _gfxDestroyed.gameObject.SetActive(true);
            IsDestroyed = true;
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Projectile")
        {
            Projectile projectile = _opponentProjectilePool.FindProjectileInPool(other.transform);
            if (projectile == null) projectile = _alliedProjectilePool.FindProjectileInPool(other.transform);
            _campHealthController.TakeDamage(projectile.Damage, 1);
            if (_campHealthController.IsHealthDepleted())
                _gameManager.RemoveCamp(this);
        }
    }
}
