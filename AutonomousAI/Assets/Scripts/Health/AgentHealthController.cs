using UnityEngine;

public class AgentHealthController : HealthController
{
    [Header("Agent Specific")]
    [SerializeField] private AgentConfig _agentConfig;

    public bool IsFleePercentReached()
    {
        return (base.GetHealthPercent() <= _agentConfig.FleeConditionBelowHealthPercent);
    }

    public void Heal(float amount)
    {
        if (amount < 0) return;
        _currentHealth += amount;
        if (_currentHealth > _initialHealth) _currentHealth = _initialHealth;
    }

    public bool IsFullHealth()
    {
        return _currentHealth == _initialHealth;
    }

    protected override void Die()
    {
        gameObject.SetActive(false);
    }
}
