using UnityEngine;

public abstract class HealthController : MonoBehaviour
{
    [Header("Properties")]
    [SerializeField] protected float _initialHealth;

    [Header("Runtime")]
    [SerializeField] protected float _currentHealth;

    private void OnEnable()
    {
        _currentHealth = _initialHealth;
    }

    public virtual int GetHealthPercent()
    {
        return (int)Mathf.Round((100f / _initialHealth) * _currentHealth);
    }

    public virtual bool IsHealthDepleted()
    {
        return _currentHealth == 0f;
    }

    public virtual void TakeDamage(int amount, float multiplier)
    {
        if (amount < 0) return;
        _currentHealth -= amount * multiplier;
        if (_currentHealth <= 0)
        {
            _currentHealth = 0;
            Die();
        }
    }

    protected abstract void Die();
}
