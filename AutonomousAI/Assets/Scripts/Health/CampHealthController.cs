using UnityEngine;

public class CampHealthController : HealthController
{
    [Header("Camp Specific")]
    [SerializeField] private AudioSource _audioSource;

    protected override void Die()
    {
        _audioSource.Play();
    }
}
