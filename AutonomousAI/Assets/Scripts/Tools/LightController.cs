using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LightController : MonoBehaviour
{
    [System.Serializable]
    private struct LightEntity
    {
        public Transform Transform;
        public LightEnum Type;
    }

    [Header("Scene References")]
    [SerializeField] private TMP_Dropdown _dropdown;
    [SerializeField] private List<LightEntity> _lightEntities;

    [Header("Runtime")]
    [SerializeField] private int _currentDropdownValue;

    private void Start()
    {
        _currentDropdownValue = _dropdown.value;
        SetNewLight((LightEnum)_currentDropdownValue);
    }

    private void Update()
    {
        if (_currentDropdownValue != _dropdown.value)
        {
            _currentDropdownValue = _dropdown.value;
            SetNewLight((LightEnum)_currentDropdownValue);
        }

    }

    public void SetNewLight(LightEnum lightType)
    {
        foreach (LightEntity light in _lightEntities)
        {
            if (light.Type == lightType)
                light.Transform.gameObject.SetActive(true);
            else
                light.Transform.gameObject.SetActive(false);
        }
    }
}
