using System.Collections.Generic;
using UnityEngine;

public static class Tools
{
    private static readonly System.Random _rng = new System.Random();

    //Fisher - Yates shuffle
    public static List<T> Shuffle<T>(this List<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = _rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
        return list;
    }

    public static Transform ClosestTransform(this List<Transform> list, Vector3 target)
    {
        Transform bestTarget = null;
        float closestDist = Mathf.Infinity;
        foreach (Transform entity in list)
        {
            float dist = Vector3.Distance(entity.position, target);
            if (dist < closestDist)
            {
                closestDist = dist;
                bestTarget = entity;
            }
        }
        return bestTarget;
    }

    public static CampController ClosestCamp(List<CampController> camps, Vector3 target)
    {
        CampController closestCamp = null;
        float closestDist = Mathf.Infinity;
        foreach (CampController camp in camps)
        {
            float dist = Vector3.Distance(camp.transform.position, target);
            if (dist < closestDist && !camp.IsDestroyed)
            {
                closestDist = dist;
                closestCamp = camp;
            }
        }
        return closestCamp;
    }


    public static List<Transform> CleanListForInactive(List<Transform> list, Vector3 origin)
    {
        List<Transform> newList = new List<Transform>();
        foreach (Transform entity in list)
        {
            if (entity.gameObject.activeSelf && Vector3.Distance(entity.position, origin) <= 6f)
                newList.Add(entity);
        }
        return newList;
    }
}