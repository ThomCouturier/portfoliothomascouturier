using UnityEngine;

public class CameraController : MonoBehaviour
{
    [Header("Scene Reference")]
    [SerializeField] private GameObject _cinemachine;


    [Header("Runtime")]
    [SerializeField] private bool _cursorLock;

    private void Start()
    {
        _cursorLock = false;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            _cursorLock = !_cursorLock;
            Cursor.lockState = _cursorLock ? CursorLockMode.Locked : CursorLockMode.None;
            Cursor.visible = !_cursorLock;
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            _cinemachine.SetActive(!_cinemachine.activeSelf);
        }
    }
}
