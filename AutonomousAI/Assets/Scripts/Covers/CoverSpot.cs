using UnityEngine;

public class CoverSpot : MonoBehaviour
{
    [Header("Runtime")]
    [SerializeField] private Transform _nextOccupant;
    [SerializeField] private bool _isAvailable;

    private void OnEnable()
    {
        _nextOccupant = null;
        _isAvailable = true;
    }

    private void Update()
    {
        if (_nextOccupant != null && !_nextOccupant.gameObject.activeSelf)
        {
            _isAvailable = true;
            _nextOccupant = null;
        }
    }

    public bool CheckIfAvailable()
    {
        return (_isAvailable && _nextOccupant == null);
    }

    public void SetNextOccupant(Transform occupant)
    {
        _nextOccupant = occupant;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (_nextOccupant != null)
        {
            FleeState fleeState = _nextOccupant.GetComponent<FleeState>();
            if (other.gameObject == _nextOccupant.gameObject && fleeState != null)
            {
                _isAvailable = false;
                fleeState.ArrivedAtCoverSpot();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (_nextOccupant != null && other.gameObject == _nextOccupant.gameObject)
        {
            _isAvailable = true;
            _nextOccupant = null;
        }
    }
}
