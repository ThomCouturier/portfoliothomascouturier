using System.Collections.Generic;
using UnityEngine;

public class Cover : MonoBehaviour
{
    [SerializeField] private List<CoverSpot> _coverSpots;

    public CoverSpot GetClosestAvailableCoverSpot(Transform target)
    {
        CoverSpot closest = null;
        float distance = Mathf.Infinity;
        foreach (CoverSpot spot in _coverSpots)
        {
            CoverSpot newClosest = spot.CheckIfAvailable() ? spot : null;
            if (newClosest != null)
            {
                float newDist = Vector3.Distance(newClosest.transform.position, target.position);
                if (newDist < distance)
                {
                    distance = newDist;
                    closest = newClosest;
                }
            }
        }
        return closest;
    }
}

