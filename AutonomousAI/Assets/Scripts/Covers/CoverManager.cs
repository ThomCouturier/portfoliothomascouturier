using System.Collections.Generic;
using UnityEngine;

public class CoverManager : MonoBehaviour
{

    [SerializeField] private List<Cover> _covers;

    public CoverSpot FindAvailableSpot(Transform target)
    {
        CoverSpot closest = null;
        float distance = Mathf.Infinity;
        foreach (Cover cover in _covers)
        {
            CoverSpot availableSpot = cover.GetClosestAvailableCoverSpot(target);
            if (availableSpot != null)
            {
                float newDist = Vector3.Distance(availableSpot.transform.position, target.position);
                if (newDist < distance)
                {
                    distance = newDist;
                    closest = availableSpot;
                }
            }
        }

        if (closest != null)
        {
            closest.SetNextOccupant(target);
            return closest;
        }
        return null;
    }
}
