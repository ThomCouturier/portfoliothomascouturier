using System.Collections.Generic;
using UnityEngine;

public class AgentData : MonoBehaviour
{
    [field: Header("Scripts")]
    [field: SerializeField] public AgentMovementController AgentMovementController { get; private set; }
    [field: SerializeField] public AgentHealthController AgentHealthController { get; private set; }
    [field: SerializeField] public Weapon Weapon { get; private set; }
    [field: SerializeField] public AgentAnimator AgentAnimator { get; private set; }
    [field: SerializeField] public CoverManager CoverManager { get; private set; }
    [field: SerializeField] public AgentConfig AgentConfig { get; private set; }
    [SerializeField] private SphereCollider _attackRangeCollider;

    [field: Header("Scene References")]
    [field: SerializeField] public ProjectilePool ProjectilePool { get; private set; }
    [field: SerializeField] public ProjectilePool OpponentProjectilePool { get; private set; }
    [SerializeField] private List<Transform> _allies;
    [SerializeField] private List<Transform> _opponents;
    [SerializeField] private List<CampController> _opponentCamps;

    [Header("Runtime")]
    [SerializeField] private int _numberOfKill;
    [field: SerializeField] public CoverSpot CoverSpot { get; set; }
    [SerializeField] private float _defaultRange;

    public void Init(List<CampController> opponentCamps, ProjectilePool projectilePool, ProjectilePool opponentProjectilePool, CoverManager _coverManager)
    {
        _opponentCamps = opponentCamps;
        this.ProjectilePool = projectilePool;
        this.OpponentProjectilePool = opponentProjectilePool;
        this.CoverManager = _coverManager;
    }

    private void Awake()
    {
        // Assign script variables
        if (this.AgentMovementController == null)
            this.AgentMovementController = GetComponent<AgentMovementController>();
        if (this.AgentHealthController == null)
            this.AgentHealthController = GetComponent<AgentHealthController>();
        if (this.Weapon == null)
            this.Weapon = GetComponent<Weapon>();
        if (this.AgentAnimator == null)
            this.AgentAnimator = GetComponent<AgentAnimator>();
        if (this.AgentConfig == null)
            this.AgentConfig = GetComponent<AgentConfig>();
        if (_attackRangeCollider == null)
            _attackRangeCollider = gameObject.transform.Find("Detection").GetComponent<SphereCollider>();
        _defaultRange = _attackRangeCollider.radius;
    }

    private void OnEnable()
    {
        Reset();
    }

    private void Update()
    {
        _opponents = Tools.CleanListForInactive(_opponents, transform.position);
        _allies = Tools.CleanListForInactive(_allies, transform.position);
        for (int i = 0; i < _opponentCamps.Count; i++)
        {
            if (_opponentCamps[i].IsDestroyed) _opponentCamps.Remove(_opponentCamps[i]);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == AgentConfig.OpponentTag && !_opponents.Contains(other.transform))
            _opponents.Add(other.transform);
        else if (other.gameObject.tag == gameObject.tag)
            _allies.Add(other.transform);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == AgentConfig.OpponentTag && _opponents.Contains(other.transform))
            _opponents.Remove(other.transform);
        else if (other.gameObject.tag == gameObject.tag)
            _allies.Remove(other.transform);
    }

    public void Reset()
    {
        _opponents = new List<Transform>();
        _numberOfKill = 0;
    }

    public void ResetRange()
    {
        _attackRangeCollider.radius = _defaultRange;
    }

    public void EnhanceAttackRange()
    {
        _attackRangeCollider.radius *= (1 + AgentConfig.HidingRangeModifier);
    }

    public Transform ClosestOpponent() => Tools.ClosestTransform(_opponents, transform.position);
    public Transform ClosestCamp()
    {
        CampController campController = Tools.ClosestCamp(_opponentCamps, transform.position);
        if (campController == null) return null;
        return campController.gameObject.transform;
    }

    public int GetOpponentCount() => _opponents.Count;

    public bool IsOverwhelmed() => (GetOpponentCount() - _allies.Count >= this.AgentConfig.FleeConditionOpponentSuperiority);
    public void HasKilledEnnemy() => _numberOfKill++;
    public bool IsGoingBerserk() => (_numberOfKill >= this.AgentConfig.BerserkKillCondition);
}
