using UnityEngine;

public class AgentSpawn : MonoBehaviour
{
    [field: Header("Scene References")]
    [field: SerializeField] public Transform SpawnPosition { get; private set; }
    [field: SerializeField] public CampController CampController { get; private set; }

    private void OnEnable()
    {
        if (this.CampController == null)
            this.CampController = GetComponent<CampController>();
    }
}
