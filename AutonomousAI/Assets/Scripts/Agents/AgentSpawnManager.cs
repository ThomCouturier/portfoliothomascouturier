using System.Collections.Generic;
using UnityEngine;

public class AgentSpawnManager : MonoBehaviour
{
    [Header("Scene Reference")]
    [SerializeField] private GameManager _gameManager;

    [Header("Scripts")]
    [SerializeField] private List<AgentSpawn> _agentSpawns;
    [SerializeField] private AgentPool _agentPool;

    [Header("Properties")]
    [SerializeField] private float _delayBetweenSpawn;

    [Header("Runtime data")]
    [SerializeField] private float _currentDelay;

    private void Start()
    {
        // Initialize variables
        if (_agentPool == null)
            _agentPool = GetComponent<AgentPool>();
        _agentPool.InitPool();
        _currentDelay = 0f;
    }

    private void Update()
    {
        _currentDelay += Time.deltaTime;
        if (_currentDelay >= _delayBetweenSpawn && !_gameManager.GameOver)
        {
            Transform agent = _agentPool.GetInactiveEntity();
            // Agent is null, then continue looking for an active one
            if (agent == null) return;
            // Reset delay
            _currentDelay = 0f;
            // Set agent active 
            _agentSpawns = Tools.Shuffle<AgentSpawn>(_agentSpawns);
            for (int i = 0; i < _agentSpawns.Count; i++)
            {
                if (!_agentSpawns[i].CampController.IsDestroyed)
                {
                    agent.SetPositionAndRotation(_agentSpawns[i].SpawnPosition.position, _agentSpawns[i].transform.rotation);
                    agent.gameObject.SetActive(true);
                    break;
                }
            }
        }
    }
}
