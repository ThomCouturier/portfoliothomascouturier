using UnityEngine;

public class AgentConfig : MonoBehaviour
{
    [field: Header("Properties")]
    [field: SerializeField] public float FleeDamageInputModifier { get; private set; }
    [field: SerializeField] public float HidingDamageInputModifier { get; private set; }
    [field: SerializeField] public float HealingDamageInputModifier { get; private set; }
    [field: SerializeField] public float BerserkDamageInputModifier { get; private set; }
    [field: SerializeField] public int HealPerSecond { get; private set; }
    [field: SerializeField] public int FleeConditionOpponentSuperiority { get; private set; }
    [field: Range(1, 100)]
    [field: SerializeField] public int FleeConditionBelowHealthPercent { get; private set; }
    [field: Range(0f, 1f)]
    [field: SerializeField] public float HidingRangeModifier { get; private set; }
    [field: SerializeField] public int BerserkKillCondition { get; private set; }
    [field: SerializeField] public string OpponentTag { get; private set; }
}
