using UnityEngine;
using UnityEngine.AI;

public class AgentMovementController : MonoBehaviour
{
    [Header("Scripts")]
    [SerializeField] private NavMeshAgent _navMeshAgent;

    private void Awake()
    {
        if (_navMeshAgent == null)
            _navMeshAgent = GetComponent<NavMeshAgent>();
    }

    public void SetDestination(Vector3 destination)
    {
        _navMeshAgent.SetDestination(destination);
    }

    public void ResetDestination()
    {
        _navMeshAgent.isStopped = true;
        _navMeshAgent.ResetPath();
    }

    public float GetCurrentVelocity()
    {
        float velocity = _navMeshAgent.velocity.magnitude / _navMeshAgent.speed;
        return velocity;
    }
}
