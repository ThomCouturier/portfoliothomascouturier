using UnityEngine;

public class AgentAnimator : MonoBehaviour
{
    [Header("Scripts")]
    [SerializeField] private AgentMovementController _agentMovementController;

    [Header("Animations")]
    [SerializeField] private Animator _animator;
    [SerializeField] private bool _moving = true;


    private void Update()
    {
        float speed = (_moving) ? _agentMovementController.GetCurrentVelocity() : 0f;
        _animator.SetFloat("Speed", speed);
    }

    public void Moving()
    {
        _animator.speed = 1f;
        _moving = true;
    }

    public void Die()
    {
        SetSpeed(1f);
        _animator.Play("Dead");
    }

    public void Shoot()
    {
        _animator.Play("Shoot");
        _moving = false;
    }

    public void SetSpeed(float newSpeed)
    {
        _animator.speed = newSpeed;
    }
}
