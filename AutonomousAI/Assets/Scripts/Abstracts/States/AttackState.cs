using UnityEngine;

public class AttackState : State
{
    protected Weapon _weapon;
    protected Transform _projectileSpawn;

    [Header("Runtime")]
    [SerializeField] protected Transform _foundTarget;
    [SerializeField] protected Transform _target;

    [SerializeField] protected Transform _initialCamp;
    [SerializeField] protected Transform _initialOpponent;


    public override void InitState(AgentData agentData)
    {
        base.InitState(agentData);
        _weapon = _agentData.Weapon;
        _projectileSpawn = _weapon.ProjectileSpawn;

        _initialCamp = _agentData.ClosestCamp();
        _initialOpponent = _agentData.ClosestOpponent();

        if (_initialCamp != null && Vector3.Distance(_initialCamp.position, transform.position) <= 5f) _target = _initialCamp;
        else if (_initialOpponent != null) _target = _initialOpponent;
    }

    public override void UpdateState()
    {
        Transform closestCamp = _agentData.ClosestCamp();
        Transform closestOpponent = _agentData.ClosestOpponent();
        if (_initialCamp != closestCamp || _initialOpponent != closestOpponent)
            _target = null;
        if (_target == null) return;

        LookTowardTarget();
        _foundTarget = CheckIfFacingTarget();
        if (_foundTarget != null)
            _weapon.Shoot();
    }

    public override StateEnum CheckConditions()
    {
        if (CheckFleeConditions()) return StateEnum.FLEE;
        if (_agentData.IsGoingBerserk()) return StateEnum.BERSERK;
        if (_target == null) return StateEnum.SIEGE;
        // Return current state type
        return StateEnum.ATTACK;
    }

    private void LookTowardTarget()
    {
        // Doc : https://docs.unity3d.com/ScriptReference/Vector3.RotateTowards.html
        Vector3 targetDirection = _target.position - _projectileSpawn.position;
        float singleStep = 3f * Time.deltaTime;
        Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, singleStep, 0.0f);
        Debug.DrawRay(transform.position, newDirection, Color.green);
        transform.rotation = Quaternion.LookRotation(newDirection);
    }

    private Transform CheckIfFacingTarget()
    {
        // Doc : https://docs.unity3d.com/ScriptReference/Physics.Raycast.html
        RaycastHit hit;
        Vector3 rayOrigin = _projectileSpawn.position;
        if (Physics.Raycast(rayOrigin, transform.forward, out hit))
        {
            if (hit.collider.gameObject == _target.gameObject)
            {
                Debug.DrawRay(rayOrigin, transform.forward, Color.red);
                return _target;
            }
        }
        return null;
    }
}
