﻿using UnityEngine;

public class BerserkState : State
{
    protected Weapon _weapon;
    protected Transform _projectileSpawn;
    protected AgentAnimator _agentAnimator;

    [Header("Runtime")]
    [SerializeField] protected Transform _target;
    [SerializeField] protected Transform _foundTarget;

    public override void InitState(AgentData agentData)
    {
        base.InitState(agentData);
        _weapon = _agentData.Weapon;
        _projectileSpawn = _weapon.ProjectileSpawn;
        _agentAnimator = _agentData.AgentAnimator;
    }

    public override StateEnum CheckConditions()
    {
        // The only way to leave this state it to die
        // Return current state type
        return StateEnum.BERSERK;
    }

    public override void UpdateState()
    {
        Transform newTarget = _agentData.ClosestCamp();
        if (newTarget != _target)
        {
            _target = newTarget;
            if (_target == null) return;
            _agentMovementController.SetDestination(_target.position);
        }

        float dist = Vector3.Distance(_target.position, transform.position);
        if (dist <= 5f) _agentMovementController.SetDestination(transform.position);
        else _agentAnimator.Moving();

        LookTowardTarget();
        _foundTarget = CheckIfFacingTarget();
        if (_foundTarget != null && dist <= 5f)
            _weapon.Shoot();
    }

    private void LookTowardTarget()
    {
        // Doc : https://docs.unity3d.com/ScriptReference/Vector3.RotateTowards.html
        Vector3 targetDirection = _target.position - _projectileSpawn.position;
        float singleStep = 2f * Time.deltaTime;
        Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, singleStep, 0.0f);
        Debug.DrawRay(transform.position, newDirection, Color.green);
        transform.rotation = Quaternion.LookRotation(newDirection);
    }

    private Transform CheckIfFacingTarget()
    {
        // Doc : https://docs.unity3d.com/ScriptReference/Physics.Raycast.html
        RaycastHit hit;
        Vector3 rayOrigin = _projectileSpawn.position;
        if (Physics.Raycast(rayOrigin, transform.forward, out hit))
        {
            if (hit.collider.gameObject == _target.gameObject)
            {
                Debug.DrawRay(rayOrigin, transform.forward, Color.red);
                return _target;
            }
        }
        return null;
    }
}
