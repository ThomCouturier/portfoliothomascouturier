using UnityEngine;

public class SearchState : State
{
    [Header("Properties")]
    [SerializeField] protected Transform _projectileSpawn;

    [Header("Runtime")]
    [SerializeField] protected Transform _foundTarget;
    [SerializeField] protected Transform _target;

    [SerializeField] protected Transform _currentCamp;
    [SerializeField] protected Transform _currentOpponent;

    public override void InitState(AgentData agentData)
    {
        base.InitState(agentData);
        _projectileSpawn = _agentData.Weapon.ProjectileSpawn;
        _agentMovementController.ResetDestination();
    }

    public override StateEnum CheckConditions()
    {
        if (CheckFleeConditions()) return StateEnum.FLEE;
        if (_target == null) return StateEnum.SIEGE;
        if (_target != null && _foundTarget != null) return StateEnum.ATTACK;
        // Return current state type
        return StateEnum.SEARCH;
    }

    public override void UpdateState()
    {
        Transform camp = _agentData.ClosestCamp();
        Transform opponent = _agentData.ClosestOpponent();
        if (camp != _currentCamp && camp != null && Vector3.Distance(camp.position, transform.position) <= 5f)
        {
            _currentCamp = camp;
            _target = _currentCamp;
        }
        else if (opponent != _currentOpponent)
        {
            _currentOpponent = opponent;
            _target = _currentOpponent;
        }
        if (_target == null) return;

        LookTowardTarget();
        _foundTarget = CheckIfFacingTarget();
    }

    private void LookTowardTarget()
    {
        // Doc : https://docs.unity3d.com/ScriptReference/Vector3.RotateTowards.html
        Vector3 targetDirection = _target.position - _projectileSpawn.position;
        float singleStep = 3f * Time.deltaTime;
        Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, singleStep, 0.0f);
        Debug.DrawRay(transform.position, newDirection, Color.green);
        transform.rotation = Quaternion.LookRotation(newDirection);
    }

    private Transform CheckIfFacingTarget()
    {
        // Doc : https://docs.unity3d.com/ScriptReference/Physics.Raycast.html
        RaycastHit hit;
        Vector3 rayOrigin = _projectileSpawn.position;
        Debug.DrawRay(rayOrigin, transform.forward, Color.red);
        if (Physics.Raycast(rayOrigin, transform.forward, out hit))
        {
            if (hit.collider.gameObject == _target.gameObject)
                return _target;
        }
        return null;
    }
}
