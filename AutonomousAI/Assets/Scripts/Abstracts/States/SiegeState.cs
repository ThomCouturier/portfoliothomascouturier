using UnityEngine;

public class SiegeState : State
{
    [Header("Runtime")]
    [SerializeField] protected Transform _destination;
    [SerializeField] protected Transform _closestOpponent;

    public override StateEnum CheckConditions()
    {
        if (CheckFleeConditions()) return StateEnum.FLEE;
        if (_closestOpponent != null) return StateEnum.SEARCH;
        if (_destination == null) return StateEnum.VEGETABLE;
        if (Vector3.Distance(_destination.position, transform.position) <= 5f) return StateEnum.SEARCH;
        // Return current state type
        return StateEnum.SIEGE;
    }

    public override void UpdateState()
    {
        Transform newDest = _agentData.ClosestCamp();
        _closestOpponent = _agentData.ClosestOpponent();

        if (_destination != newDest)
        {
            _destination = newDest;
            if (_destination != null)
                _agentMovementController.SetDestination(_destination.position);
        }
    }
}
