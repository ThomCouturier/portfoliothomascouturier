﻿using UnityEngine;

public class HealState : State
{
    // Consts
    private const int TIME_BETWEEN_HEAL = 1;

    [Header("Runtime")]
    [SerializeField] private float _currentDelay;

    public override void InitState(AgentData agentData)
    {
        base.InitState(agentData);
    }

    public override StateEnum CheckConditions()
    {
        if (_agentHealthController.IsFullHealth())
        {
            if (_agentData.GetOpponentCount() >= 1) return StateEnum.HIDE;
            return StateEnum.SIEGE;
        }

        // Return current state type
        return StateEnum.HEAL;
    }

    public override void UpdateState()
    {
        _currentDelay += Time.deltaTime;
        if (_currentDelay >= TIME_BETWEEN_HEAL)
        {
            _agentHealthController.Heal(_agentConfig.HealPerSecond);
            _currentDelay = 0f;
        }
    }
}
