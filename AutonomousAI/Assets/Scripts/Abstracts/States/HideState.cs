﻿using UnityEngine;

public class HideState : State
{
    protected Weapon _weapon;
    protected Transform _projectileSpawn;

    [Header("Runtime")]
    [SerializeField] protected Transform _target;
    [SerializeField] protected Transform _foundTarget;

    public override void InitState(AgentData agentData)
    {
        base.InitState(agentData);
        _weapon = _agentData.Weapon;
        _projectileSpawn = _weapon.ProjectileSpawn;
        _agentData.EnhanceAttackRange();
    }

    public override StateEnum CheckConditions()
    {
        if (_agentData.IsGoingBerserk()) return StateEnum.BERSERK;
        if (CheckFleeConditions()) return StateEnum.HEAL;
        if (_agentData.GetOpponentCount() == 0) return StateEnum.SIEGE;
        // Return current state type
        return StateEnum.HIDE;
    }

    public override void UpdateState()
    {
        Transform opponent = _agentData.ClosestOpponent();

        if (opponent != _target && opponent != null)
            _target = opponent;

        if (_foundTarget != _target)
        {
            LookTowardTarget();
            _foundTarget = CheckIfFacingTarget();
        }
        else
            _weapon.Shoot();
    }

    private void LookTowardTarget()
    {
        // Doc : https://docs.unity3d.com/ScriptReference/Vector3.RotateTowards.html
        Vector3 targetDirection = _target.position - _projectileSpawn.position;
        float singleStep = 2f * Time.deltaTime;
        Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, singleStep, 0.0f);
        Debug.DrawRay(transform.position, newDirection, Color.green);
        transform.rotation = Quaternion.LookRotation(newDirection);
    }

    private Transform CheckIfFacingTarget()
    {
        // Doc : https://docs.unity3d.com/ScriptReference/Physics.Raycast.html
        RaycastHit hit;
        Vector3 rayOrigin = _projectileSpawn.position;
        if (Physics.Raycast(rayOrigin, transform.forward, out hit))
        {
            if (hit.collider.gameObject == _target.gameObject)
            {
                Debug.DrawRay(rayOrigin, transform.forward, Color.red);
                return _target;
            }
        }
        return null;
    }
}
