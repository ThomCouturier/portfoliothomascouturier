public class VegetableState : State
{
    public override void InitState(AgentData agentData)
    {
        base.InitState(agentData);
        _agentMovementController.SetDestination(transform.position);
        _agentData.AgentAnimator.Moving();
    }

    public override void UpdateState()
    {
        // Nothing to do
    }

    public override StateEnum CheckConditions()
    {
        // Return current state type
        return StateEnum.VEGETABLE;
    }
}
