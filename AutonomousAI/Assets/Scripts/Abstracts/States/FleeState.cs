using UnityEngine;

public class FleeState : State
{
    [Header("Runtime")]
    [SerializeField] protected bool _arrived;
    [SerializeField] protected CoverSpot _coverSpot;

    public override void InitState(AgentData agentData)
    {
        base.InitState(agentData);
        _arrived = false;
        _coverSpot = agentData.CoverSpot;

        if (_coverSpot != null)
        {
            _agentMovementController.SetDestination(_coverSpot.transform.position);
            _coverSpot.SetNextOccupant(transform);
        }
    }

    public override StateEnum CheckConditions()
    {
        if (_arrived)
        {
            if (!_agentHealthController.IsFullHealth()) return StateEnum.HEAL;
            return StateEnum.HIDE;
        }
        // Return current state type
        return StateEnum.FLEE;
    }

    public override void UpdateState()
    {
        // Nothing to do
    }

    public void ArrivedAtCoverSpot()
    {
        _arrived = true;
    }
}

