using System.Collections.Generic;
using UnityEngine;

public abstract class EntityPool : MonoBehaviour
{
    [Header("Properties")]
    [SerializeField] protected GameObject _prefab;
    [SerializeField] protected Transform _parent;
    [SerializeField] protected int _initialCount;
    [SerializeField] protected string _name;

    [Header("Runtime")]
    [SerializeField] protected List<Transform> entities;

    public virtual void InitPool()
    {
        for (int i = 0; i < _initialCount; i++)
        {
            GameObject entity = Instantiate(_prefab, Vector3.zero, Quaternion.identity);
            entity.name = _name.Replace("{0}", i.ToString());
            entity.SetActive(false);
            entity.transform.SetParent(_parent);
            // Add new entity to the pool list
            entities.Add(entity.transform);
        }
    }

    public virtual Transform GetInactiveEntity()
    {
        for (int i = 0; i < entities.Count; i++)
        {
            if (!entities[i].gameObject.activeSelf)
                return entities[i];
        }
        // No inactive entity found -> return null
        return null;
    }
}
