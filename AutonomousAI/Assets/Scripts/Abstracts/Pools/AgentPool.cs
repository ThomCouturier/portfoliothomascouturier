using System.Collections.Generic;
using UnityEngine;

public class AgentPool : EntityPool
{
    [Header("Agent Specific Properties")]
    [SerializeField] private List<CampController> _opponentCamps;
    [SerializeField] private List<StateManager> _stateManagers;
    [SerializeField] private ProjectilePool _projectilePool;
    [SerializeField] private ProjectilePool _opponentProjectilePool;
    [SerializeField] private CoverManager _coverManager;

    public override void InitPool()
    {
        for (int i = 0; i < _initialCount; i++)
        {
            GameObject entity = Instantiate(_prefab, Vector3.zero, Quaternion.identity);
            entity.name = _name.Replace("(Clone)", "").Replace("{0}", i.ToString());
            entity.SetActive(false);
            entity.transform.SetParent(_parent);
            // AgentPool specific's
            AgentData agentData = entity.gameObject.GetComponent<AgentData>();
            if (agentData != null) agentData.Init(_opponentCamps, _projectilePool, _opponentProjectilePool, _coverManager);
            _stateManagers.Add(entity.GetComponent<StateManager>());
            // Add new entity to the pool list
            entities.Add(entity.transform);
        }
    }

    public void DispatchVegetableState()
    {
        foreach (StateManager stateManager in _stateManagers)
        {
            if (stateManager.gameObject.activeSelf)
                stateManager.ChangeState(StateEnum.VEGETABLE);
        }
    }

    public int GetActiveCount()
    {
        int count = 0;
        foreach (Transform entity in entities)
        {
            if (entity.gameObject.activeSelf)
                count++;
        }
        return count;
    }
}
