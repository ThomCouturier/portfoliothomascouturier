using System.Collections.Generic;
using UnityEngine;

public class ProjectilePool : EntityPool
{
    [Header("Projectile Specific Properties")]
    [SerializeField] protected List<Projectile> entityProjectileComponents;

    private void Start()
    {
        base.InitPool();
        foreach (Transform entity in entities)
        {
            entityProjectileComponents.Add(entity.gameObject.GetComponent<Projectile>());
        }
    }

    public Projectile FindProjectileInPool(Transform projectile)
    {
        for (int i = 0; i < entities.Count; i++)
        {
            if (entities[i] == projectile)
            {
                return entityProjectileComponents[i];
            }
        }
        return null;
    }
}
