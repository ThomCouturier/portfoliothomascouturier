using UnityEngine;

public abstract class State : MonoBehaviour
{
    protected AgentData _agentData;
    protected AgentConfig _agentConfig;
    protected CoverManager _coverManager;
    protected AgentMovementController _agentMovementController;
    protected AgentHealthController _agentHealthController;

    public virtual void InitState(AgentData agentData)
    {
        _agentData = agentData;
        _agentConfig = _agentData.AgentConfig;
        _coverManager = _agentData.CoverManager;
        _agentMovementController = _agentData.AgentMovementController;
        _agentHealthController = _agentData.AgentHealthController;
    }

    public abstract StateEnum CheckConditions();
    public abstract void UpdateState();

    protected bool CheckFleeConditions()
    {
        if (_coverManager != null && (_agentHealthController.IsFleePercentReached() || _agentData.IsOverwhelmed()))
        {
            CoverSpot spot = _coverManager.FindAvailableSpot(transform);
            if (spot != null)
            {
                _agentData.CoverSpot = spot;
                return true;
            }
        }
        return false;
    }
}
