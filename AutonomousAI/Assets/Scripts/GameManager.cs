using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [Header("Scene References")]
    [SerializeField] private List<CampController> _campControllersA;
    [SerializeField] private List<CampController> _campControllersB;
    [SerializeField] private AgentPool _agentPoolA;
    [SerializeField] private AgentPool _agentPoolB;
    [SerializeField] private TMP_Text _agentCountA;
    [SerializeField] private TMP_Text _agentCountB;

    [Header("Properties")]
    [SerializeField] private string _agentCountTextA;
    [SerializeField] private string _agentCountTextB;

    [field: Header("Runtime")]
    [SerializeField] public bool GameOver { get; private set; }

    private void Start()
    {
        GameOver = false;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
        if (Input.GetKeyDown(KeyCode.Return))
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);

        _agentCountA.text = _agentCountTextA.Replace("{0}", _agentPoolA.GetActiveCount().ToString());
        _agentCountB.text = _agentCountTextB.Replace("{0}", _agentPoolB.GetActiveCount().ToString());
    }

    public void RemoveCamp(CampController campController)
    {
        if (_campControllersA.Contains(campController))
            _campControllersA.Remove(campController);
        if (_campControllersB.Contains(campController))
            _campControllersB.Remove(campController);

        if (_campControllersA.Count == 0 || _campControllersB.Count == 0)
            DispatchVegetableState();
    }

    private void DispatchVegetableState()
    {
        _agentPoolA.DispatchVegetableState();
        _agentPoolB.DispatchVegetableState();
        GameOver = true;
    }
}
