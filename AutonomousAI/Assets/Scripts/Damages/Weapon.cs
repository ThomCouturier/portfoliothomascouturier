using UnityEngine;

public class Weapon : MonoBehaviour
{
    [Header("Scripts")]
    [SerializeField] private AgentData _agentData;
    [SerializeField] private AgentAnimator _agentAnimator;
    [SerializeField] private AudioSource _audioSource;

    [Header("Scene References")]
    [SerializeField] private ProjectilePool _projectilePool;
    [SerializeField] private GameObject _flash;
    [field: SerializeField] public Transform ProjectileSpawn { get; private set; }

    [Header("Properties")]
    [SerializeField] private float _delayBetweenShots;
    [SerializeField] private float _flashDelay;

    [Header("Runtime")]
    [SerializeField] private float _currentDelay;
    [SerializeField] private float _currentFlashDelay;

    private void Start()
    {
        if (_agentData == null)
            _agentData = GetComponent<AgentData>();
        _agentAnimator = _agentData.AgentAnimator;
        _projectilePool = _agentData.ProjectilePool;
    }

    private void OnEnable()
    {
        _currentDelay = 0f;
        _flash.SetActive(false);
    }

    private void Update()
    {
        if (_currentDelay < _delayBetweenShots)
            _currentDelay += Time.deltaTime;
        _currentFlashDelay += Time.deltaTime;
        if (_currentFlashDelay >= _flashDelay)
            _flash.SetActive(false);
    }

    public void Shoot()
    {
        if (_currentDelay >= _delayBetweenShots)
        {
            Transform projectileObj = _agentData.ProjectilePool.GetInactiveEntity();
            if (projectileObj == null) return;
            Projectile projectile = _projectilePool.FindProjectileInPool(projectileObj);
            // Animation
            _agentAnimator.SetSpeed(1f / _delayBetweenShots);
            _agentAnimator.Shoot();
            // Audio
            if (_audioSource.enabled)
                _audioSource.Play();
            // Object
            projectileObj.SetPositionAndRotation(ProjectileSpawn.position, ProjectileSpawn.rotation);
            projectileObj.gameObject.SetActive(true);
            _flash.SetActive(true);

            // Initialization
            projectile.Init(_agentData);
            _currentFlashDelay = 0f;
            _currentDelay = 0f;
        }
    }

}
