using UnityEngine;

public class Projectile : MonoBehaviour
{
    [Header("Script")]
    [SerializeField] private Rigidbody _rBody;
    [SerializeField] private TrailRenderer _tRenderer;
    [SerializeField] private AgentData _agentData;

    [Header("Properties")]
    [SerializeField] private float _delayBeforeDest;
    [SerializeField] private float _speed;
    [field: SerializeField] public int Damage { get; private set; }

    [Header("Runtime")]
    [SerializeField] private float _currentDelay;

    private void Start()
    {
        _rBody = GetComponent<Rigidbody>();
        _tRenderer = GetComponent<TrailRenderer>();
    }

    private void Update()
    {
        _rBody.velocity = transform.forward * _speed;
        _currentDelay += Time.deltaTime;
        if (_currentDelay >= _delayBeforeDest)
            Deactivate();
    }

    private void Deactivate()
    {
        gameObject.SetActive(false);
        _tRenderer.Clear();
        _rBody.velocity = Vector3.zero;
    }

    public void Init(AgentData agentData)
    {
        _currentDelay = 0f;
        transform.rotation = transform.rotation;
        _agentData = agentData;
    }

    private void OnCollisionEnter(Collision other)
    {
        Deactivate();
    }

    public void OnKill() => _agentData.HasKilledEnnemy();
}
