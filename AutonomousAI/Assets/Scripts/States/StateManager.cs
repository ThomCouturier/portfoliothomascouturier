using UnityEngine;

public class StateManager : MonoBehaviour
{
    [Header("Scripts")]
    [SerializeField] private AgentData _agentData;
    [SerializeField] private AgentConfig _agentConfig;
    [SerializeField] private AgentHealthController _agentHealthController;

    [Header("State Data")]
    [SerializeField] private StateEnum _currentStateType;
    [SerializeField] private State _currentState;

    [Header("Properties")]
    [SerializeField] private bool _debug;

    private void Awake()
    {
        // Scene scripts
        if (_agentData == null)
            _agentData = GetComponent<AgentData>();
        // State data
        if (_currentState == null)
            _currentState = GetComponent<State>();
        _agentConfig = _agentData.AgentConfig;
        _agentHealthController = _agentData.AgentHealthController;
    }

    private void OnEnable()
    {
        _agentData.Reset();
        ChangeState(StateEnum.SIEGE);
    }

    private void Update()
    {
        _currentState.UpdateState();
        StateEnum nextStateType = _currentState.CheckConditions();
        if (nextStateType != _currentStateType) ChangeState(nextStateType);
    }

    public void ChangeState(StateEnum nextStateType)
    {
        // Check next state type, if it's the same, do nothing
        if (nextStateType == _currentStateType) return;

        _currentStateType = nextStateType;
        _agentData.ResetRange();

        // Destroy current state script
        if (_currentState != null)
            Destroy(_currentState);

        _agentData.AgentAnimator.Moving();
        // Parse through the different values of the enum to create the right state
        switch (_currentStateType)
        {
            case StateEnum.VEGETABLE:
                _currentState = gameObject.AddComponent<VegetableState>();
                break;
            case StateEnum.SIEGE:
                _currentState = gameObject.AddComponent<SiegeState>();
                break;
            case StateEnum.SEARCH:
                _currentState = gameObject.AddComponent<SearchState>();
                break;
            case StateEnum.ATTACK:
                _currentState = gameObject.AddComponent<AttackState>();
                break;
            case StateEnum.FLEE:
                _currentState = gameObject.AddComponent<FleeState>();
                break;
            case StateEnum.HEAL:
                _currentState = gameObject.AddComponent<HealState>();
                break;
            case StateEnum.HIDE:
                _currentState = gameObject.AddComponent<HideState>();
                break;
            case StateEnum.BERSERK:
                _currentState = gameObject.AddComponent<BerserkState>();
                break;
        }
        _currentState.InitState(_agentData);
        if (_debug)
            Debug.Log($"{gameObject.name}::Changing state to {_currentStateType.ToString()}");
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Projectile")
        {
            // Fetch projectile
            Projectile projectile = _agentData.OpponentProjectilePool.FindProjectileInPool(other.transform);
            if (projectile == null) projectile = _agentData.ProjectilePool.FindProjectileInPool(other.transform);
            // Multiplier verification
            float multiplier = 1f;
            if (_currentStateType == StateEnum.FLEE) multiplier = _agentConfig.FleeDamageInputModifier;
            if (_currentStateType == StateEnum.HEAL) multiplier = _agentConfig.HealingDamageInputModifier;
            if (_currentStateType == StateEnum.HIDE) multiplier = _agentConfig.HidingDamageInputModifier;
            if (_currentStateType == StateEnum.BERSERK) multiplier = _agentConfig.BerserkDamageInputModifier;
            // Damage intake
            _agentHealthController.TakeDamage(projectile.Damage, multiplier);
            if (_agentHealthController.IsHealthDepleted())
                projectile.OnKill();
        }
    }
}
